"@fileOverview\nIn-memory implementation class for generating phrases.";
var Generator, MemoryGenerator, util, _;

util = require('util');

_ = require('underscore');

Generator = require('../generator');

"@class\nClass for generating deterministic unique phrases from a dictionary, with\nin-memory storage.\n\nThis is suitable for testing and experimentation.\n\n@param  {object} options\n        Configuration for this instance.";

MemoryGenerator = function(options) {
  MemoryGenerator.super_.call(this, options);
  this.dictionary = [];
  return this.count = 0;
};

util.inherits(MemoryGenerator, Generator);

"@see Generator.appendWordsToDictionary";

MemoryGenerator.prototype.appendWordsToDictionary = function(words, callback) {
  if (!_.isArray(words)) {
    words = [words];
  }
  this.dictionary = _.union(this.dictionary, words);
  return callback();
};

"* @see Generator#getWord";

MemoryGenerator.prototype.getWord = function(wordIndex, callback) {
  return callback(null, this.dictionary[wordIndex]);
};

"@see Generator.getDictionaryLength";

MemoryGenerator.prototype.getDictionaryLength = function(callback) {
  return callback(null, this.dictionary.length);
};

"@see Generator.getCount";

MemoryGenerator.prototype.getCount = function(callback) {
  return callback(null, this.count);
};

"@see Generator.setCount";

MemoryGenerator.prototype.setCount = function(count, callback) {
  this.count = count;
  return callback();
};

"@see Generator.incrementCount";

MemoryGenerator.prototype.incrementCount = function(callback) {
  this.count = this.count + 1;
  return callback(null, this.count);
};

module.exports = MemoryGenerator;

//# sourceMappingURL=memoryGenerator.js.map
