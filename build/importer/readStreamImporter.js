var DictionaryTransformStream, Importer, ReadStreamImporter, util, _;

util = require('util');

_ = require('underscore');

DictionaryTransformStream = require('../util/dictionaryTransformStream');

Importer = require('../importer');

"@class\nA dictionary importer class working from streamed text.\n\n@see Importer";

ReadStreamImporter = function(generator) {
  return ReadStreamImporter.super_.call(this, generator);
};

util.inherits(ReadStreamImporter, Importer);

"Import a dictionary from a read stream.\n\nThe options have the form:\n\n{\n  readStream: readStream,\n              For tokenizing the stream of words.\n  wordDelimiter: /[\s!\?]+/,\n              All words failing this regular expression are rejected. Use it to\n              control acceptable length and characters. The word is lowercased\n              before matching.\n  acceptanceRegExp: /^[a-z\-]{6,14}$/,\n              Limit duplicate cache size if memory is a concern. This will let\n              duplicates through.\n  duplicateCacheSize: Infinity\n}\n\n@see Importer#import";

ReadStreamImporter.prototype["import"] = function(options, callback) {
  var dts, dtsOptions, finish, newWord, self, words;
  self = this;
  options = options || {};
  words = {};
  dtsOptions = _.pick({
    options: options,
    'wordDelimiter': 'wordDelimiter',
    'acceptanceRegExp': 'acceptanceRegExp',
    'duplicateCacheSize': 'duplicateCacheSize'
  });
  dts = new DictionaryTransformStream(dtsOptions);
  if (!options.readStream) {
    return callback('options.readStream is required.');
  }
  newWord = function(word) {
    return words[word] = true;
  };
  finish = function(error) {
    if (error) {
      options.readStream.unpipe(dts);
      callback(error);
    }
    return self.generator.appendWordsToDictionary(_.keys(words), callback);
  };
  dts.on('data', newWord);
  dts.on('error', finish);
  dts.on('finish', finish);
  return options.readStream.pipe(dts);
};

module.exports = ReadStreamImporter;

//# sourceMappingURL=readStreamImporter.js.map
