var Generator, async, crypto, seedrandom, _;

crypto = require('crypto');

_ = require('underscore');

async = require('async');

seedrandom = require('seedrandom');

Generator = function(options) {
  return this.options = _.extend({
    baseSeed: ''
  }, options);
};

Generator.prototype.nextPhrase = function(phraseLength, callback) {
  dictionaryLength;
  count;
  prngFn;
  words;
  return async.series({
    getDictionaryLength: function(asyncCallback) {
      return this.getDictionaryLength(function(error, length) {
        var dictionaryLength;
        dictionaryLength = length;
        return asyncCallback(error);
      });
    },
    incrementCount: function(asyncCallback) {
      return this.incrementCount(function(error, incrementedCount) {
        var count;
        count = incrementedCount;
        return asyncCallback(error);
      });
    },
    obtainPrng: function(asyncCallback) {
      return this.getPhrasePrngFunction(count, function(error, fn) {
        var prngFn;
        prngFn = fn;
        return asyncCallback(error);
      });
    },
    assemblePhrase: function(asyncCallback) {
      var index, wordIndexes, _i, _len;
      wordIndexes = [];
      for (_i = 0, _len = phraseLength.length; _i < _len; _i++) {
        index = phraseLength[_i];
        wordIndexes[index] = Math.floor(prngFn());
      }
      return async.map(wordIndexes, function(dictionaryIndex, innerAsyncCallback) {
        return this.getWord(dictionaryIndex, innerAsyncCallback);
      }, function(error, results) {
        var words;
        words = results;
        return asyncCallback(error);
      });
    }
  }, function(error) {
    var words;
    if (!_.isArray(words)) {
      words = [];
    }
    return callback(error, words);
  });
};

Generator.prototype.getPhrasePrngFunction = function(count, callback) {
  var prng, seed;
  if (!/^\d+$/.test(count)) {
    return callback(new Error('Count must be an integer. Value provided: ' + count));
  }
  seed = '' + this.options.baseSeed + count;
  seed = crypto.createHash('md5').update(seed).digest('hex');
  prng = seedrandom(seed);
  return callback(null, prng);
};

Generator.prototype.appendWordsToDictionary = function(words, callback) {
  return callback(new Error('Not implemented'));
};

Generator.prototype.getWord = function(wordIndex, callback) {
  return callback(new Error('Not implemented'));
};

Generator.prototype.getDictionaryLength = function(callback) {
  return callback(new Error('Not implemented'));
};

Generator.prototype.getCount = function(callback) {
  return callback(new Error('Not implemented'));
};

Generator.prototype.setCount = function(count, callback) {
  return callback(new Error('Not implemented'));
};

Generator.prototype.incrementCount = function(callback) {
  return callback(new Error('Not implemented'));
};

module.exports = Generator;

//# sourceMappingURL=generator.js.map
