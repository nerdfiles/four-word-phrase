We asked some of Nation readers’ favorite writers what they’d ask Hillary Clinton and Donald Trump if they were moderating the first 2016 presidential debate on Monday night—just in case NBC’s Lester Holt needs some inspiration from his friends in independent media. Read on for questions from Katha Pollitt, Joan Walsh, David Cole, Laila Lalami, Mark Hertsgaard, and more!

On growing the economy and curbing inequality

Joan Walsh asks Hillary Clinton: You have an expansive, progressive economic agenda. But you’ve repeatedly said it must be deficit neutral. A lot of progressive economists say your plans can’t be enacted without at least a short-term increase in the deficit. The economist Paul Krugman, who supports you, says while interest rates are low, this is actually an opportune time to borrow money, especially for our infrastructure needs—which can both employ people and repair roads, bridges, and mass transit in ways that help businesses and workers. Why do you insist your programs remain deficit neutral?

On climate change

Mark Hertsgaard asks Clinton: The Democratic Party platform includes a pledge to mount a “World War II–style mobilization” against climate change, which scientists say is urgently needed to avoid leaving our kids a ruined planet. Would this mobilization copy what the United States actually did during World War II, such as retooling automobile factories to produce electric vehicles and introducing price controls to discourage burning coal and oil?

On race and desegregation

Eric Foner asks both candidates: Half a century after the civil-rights revolution, racial segregation in public schools and housing is as entrenched as ever. Should reducing such segregation be an objective of the federal government and, if so, what specific policies should be adopted?

On abortion

Katha Pollitt asks Donald Trump: You’ve called for criminalizing abortion and even for “punishing” women who seek them. You’ve surrounded yourself with people who want to ban abortion entirely: Your vice-presidential pick, Mike Pence, is one of the most aggressively anti-abortion governors; Marjorie Dannenfelser, the nation’s most powerful anti-choice lobbyist, has joined your team; and you’ve been endorsed by the extremist leader Troy Newman, who has long called for violence against abortion providers. You’ve promised to nominate Supreme Court justices who will overturn Roe v. Wade. You want contraception to be available over the counter, but without insurance coverage, which means many women won’t be able to afford it. Could you explain how your position on reproductive rights benefits women’s equality, freedom, and health?

On foreign policy

William Greider asks Hillary Clinton: What’s wrong with Trump’s idea of America First? Isn’t that what Americans want when they say the country is on the wrong track? Isn’t it true that competing nations—Germany, Japan, and China, for instance—pursue the same goal?

Laila Lalami asks Clinton: The Iraq War, for which you voted, has been an unmitigated disaster for the region. How do you propose handling the war in Syria without creating more of the chaos we have seen for the last decade?

David Cole asks both candidates: President Obama has admitted that the targeted-killing program has killed hundreds of innocent civilians, but he has specifically acknowledged, publicly apologized  and paid reparations to only Western victims. Would you support public apologies and reparations for all innocent victims of drone strikes?

Greg Grandin asks Clinton: You have said that ex-Colombian president Álvaro Uribe is a “friend” and have praised him as a “hard-nosed, hands-on leader.” Uribe, however, has been implicated in paramilitary repression responsible for the deaths of tens of thousands of Colombians. Next week, Colombians will go to the polls to vote on whether to approve a peace settlement that might end their country’s many-decades-long civil war, but Uribe is campaigning hard against it, dangerously pandering to a hard-line “law and order” constituency and accusing the current president, who negotiated the settlement, of being a traitor who is “delivering the country to terrorism.” At least 13 activists promoting peace have been murdered, most likely at the hands of Uribe’s paramilitary allies. Assuming that, as polls indicate, Colombians vote in favor of peace, and considering your longstanding working relationship with Uribe, as president what steps would you take to strengthen Colombia’s peace process?

Michael Klare asks both candidates: China has converted disputed reefs and atolls in the South China Sea into military bases, threatening the security of American allies and the regular transit by American warships. How would you counter these Chinese moves without provoking a military clash with grave risks of escalation?

Klare asks both candidates: Would you advocate including Ukraine in NATO, knowing this would provoke a harsh Russian reaction but also provide reassurance to those in Ukraine who fear further Russian incursions?

Katha Pollitt asks Hillary Clinton: Could you please say something critical about Henry Kissinger so Nation readers will vote for you?

On immigration

Laila Lalami asks Donald Trump: Refugees entering the United States already go through extensive vetting process by the FBI and other agencies. What specific questions do you think these agencies are not asking?

Another question for Trump from Lalami: You’ve proposed building a wall along the southern border and making Mexico pay for it. Would you propose building a wall along the northern border and making Canada pay for it?

On crime and policing

David Cole asks Donald Trump: How do you justify your call to resume “stop and frisk” policing, given the findings in New York City that it disproportionately targeted young people of color who were entirely innocent, almost never led to the discovery of contraband or guns, and its termination led to no rise in crime?

Cole asks both candidates: What would you do as president to address the growing distrust between law enforcement and the communities they serve?

On nuclear weapons

Mark Hertsgaard asks Trump: You have said, “I love nuclear weapons.” Meanwhile, you take offense easily and enjoy feuding with critics. Why should Americans allow someone with your volatile temperament to have his finger on the nuclear button?

Michael Klare asks both candidates: China says it deplores the recent nuclear tests by North Korea and will enforce UN sanctions against that country, yet it continues to keep North Korea afloat by serving as its major trading partner. What will you do to get China to really apply pressure on North Korea to halt its nuclear-weapons program?

On the candidates’ campaigns and the big picture

Katha Pollitt asks Clinton: People are fed up with the political class. They want to feel some hope and change, some morning in America. You have many great policy proposals up on your Web site, but they aren’t getting through to enough people emotionally. Could you paint a picture that will inspire people to go to the polls for you?

William Greider asks Donald Trump: Why did you poison your political message with personal smears and cheap-shot bigotry? Do you now regret your nasty slurs toward women, Muslims, and others? Haven’t you made it easier for critics to dismiss you as an ugly crank?	
