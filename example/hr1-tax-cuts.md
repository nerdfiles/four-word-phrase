[Congressional Bills 115th Congress]
[From the U.S. Government Publishing Office]
[H.R. 1 Placed on Calendar Senate (PCS)]

<DOC>





                                                       Calendar No. 266
115th CONGRESS
  1st Session
                                 H. R. 1


_______________________________________________________________________


                   IN THE SENATE OF THE UNITED STATES

                           November 27, 2017

                     Received; read the first time

                           November 28, 2017

            Read the second time and placed on the calendar

_______________________________________________________________________

                                 AN ACT



   To provide for reconciliation pursuant to titles II and V of the
       concurrent resolution on the budget for fiscal year 2018.

    Be it enacted by the Senate and House of Representatives of the
United States of America in Congress assembled,

SECTION 1. SHORT TITLE; ETC.

    (a) Short Title.--This Act may be cited as the ``Tax Cuts and Jobs
Act''.
    (b) Amendment of 1986 Code.--Except as otherwise expressly
provided, whenever in this Act an amendment or repeal is expressed in
terms of an amendment to, or repeal of, a section or other provision,
the reference shall be considered to be made to a section or other
provision of the Internal Revenue Code of 1986.
    (c) Table of Contents.--The table of contents for this Act is as
follows:

Sec. 1. Short title; etc.
                  TITLE I--TAX REFORM FOR INDIVIDUALS

Subtitle A--Simplification and Reform of Rates, Standard Deduction, and
                               Exemptions

Sec. 1001. Reduction and simplification of individual income tax rates.
Sec. 1002. Enhancement of standard deduction.
Sec. 1003. Repeal of deduction for personal exemptions.
Sec. 1004. Maximum rate on business income of individuals.
Sec. 1005. Conforming amendments related to simplification of
                            individual income tax rates.
  Subtitle B--Simplification and Reform of Family and Individual Tax
                                Credits

Sec. 1101. Enhancement of child tax credit and new family tax credit.
Sec. 1102. Repeal of nonrefundable credits.
Sec. 1103. Refundable credit program integrity.
Sec. 1104. Procedures to reduce improper claims of earned income
                            credit.
Sec. 1105. Certain income disallowed for purposes of the earned income
                            tax credit.
     Subtitle C--Simplification and Reform of Education Incentives

Sec. 1201. American opportunity tax credit.
Sec. 1202. Consolidation of education savings rules.
Sec. 1203. Reforms to discharge of certain student loan indebtedness.
Sec. 1204. Repeal of other provisions relating to education.
Sec. 1205. Rollovers between qualified tuition programs and qualified
                            ABLE programs.
          Subtitle D--Simplification and Reform of Deductions

Sec. 1301. Repeal of overall limitation on itemized deductions.
Sec. 1302. Mortgage interest.
Sec. 1303. Repeal of deduction for certain taxes not paid or accrued in
                            a trade or business.
Sec. 1304. Repeal of deduction for personal casualty losses.
Sec. 1305. Limitation on wagering losses.
Sec. 1306. Charitable contributions.
Sec. 1307. Repeal of deduction for tax preparation expenses.
Sec. 1308. Repeal of medical expense deduction.
Sec. 1309. Repeal of deduction for alimony payments.
Sec. 1310. Repeal of deduction for moving expenses.
Sec. 1311. Termination of deduction and exclusions for contributions to
                            medical savings accounts.
Sec. 1312. Denial of deduction for expenses attributable to the trade
                            or business of being an employee.
    Subtitle E--Simplification and Reform of Exclusions and Taxable
                              Compensation

Sec. 1401. Limitation on exclusion for employer-provided housing.
Sec. 1402. Exclusion of gain from sale of a principal residence.
Sec. 1403. Repeal of exclusion, etc., for employee achievement awards.
Sec. 1404. Sunset of exclusion for dependent care assistance programs.
Sec. 1405. Repeal of exclusion for qualified moving expense
                            reimbursement.
Sec. 1406. Repeal of exclusion for adoption assistance programs.
 Subtitle F--Simplification and Reform of Savings, Pensions, Retirement

Sec. 1501. Repeal of special rule permitting recharacterization of Roth
                            IRA contributions as traditional IRA
                            contributions.
Sec. 1502. Reduction in minimum age for allowable in-service
                            distributions.
Sec. 1503. Modification of rules governing hardship distributions.
Sec. 1504. Modification of rules relating to hardship withdrawals from
                            cash or deferred arrangements.
Sec. 1505. Extended rollover period for the rollover of plan loan
                            offset amounts in certain cases.
Sec. 1506. Modification of nondiscrimination rules to protect older,
                            longer service participants.
    Subtitle G--Estate, Gift, and Generation-skipping Transfer Taxes

Sec. 1601. Increase in credit against estate, gift, and generation-
                            skipping transfer tax.
Sec. 1602. Repeal of estate and generation-skipping transfer taxes.
                TITLE II--ALTERNATIVE MINIMUM TAX REPEAL

Sec. 2001. Repeal of alternative minimum tax.
                     TITLE III--BUSINESS TAX REFORM

                         Subtitle A--Tax Rates

Sec. 3001. Reduction in corporate tax rate.
                       Subtitle B--Cost Recovery

Sec. 3101. Increased expensing.
                   Subtitle C--Small Business Reforms

Sec. 3201. Expansion of section 179 expensing.
Sec. 3202. Small business accounting method reform and simplification.
Sec. 3203. Small business exception from limitation on deduction of
                            business interest.
Sec. 3204. Modification of treatment of S corporation conversions to C
                            corporations.
  Subtitle D--Reform of Business-related Exclusions, Deductions, etc.

Sec. 3301. Interest.
Sec. 3302. Modification of net operating loss deduction.
Sec. 3303. Like-kind exchanges of real property.
Sec. 3304. Revision of treatment of contributions to capital.
Sec. 3305. Repeal of deduction for local lobbying expenses.
Sec. 3306. Repeal of deduction for income attributable to domestic
                            production activities.
Sec. 3307. Entertainment, etc. expenses.
Sec. 3308. Unrelated business taxable income increased by amount of
                            certain fringe benefit expenses for which
                            deduction is disallowed.
Sec. 3309. Limitation on deduction for FDIC premiums.
Sec. 3310. Repeal of rollover of publicly traded securities gain into
                            specialized small business investment
                            companies.
Sec. 3311. Certain self-created property not treated as a capital
                            asset.
Sec. 3312. Repeal of special rule for sale or exchange of patents.
Sec. 3313. Repeal of technical termination of partnerships.
Sec. 3314. Recharacterization of certain gains in the case of
                            partnership profits interests held in
                            connection with performance of investment
                            services.
Sec. 3315. Amortization of research and experimental expenditures.
Sec. 3316. Uniform treatment of expenses in contingency fee cases.
                 Subtitle E--Reform of Business Credits

Sec. 3401. Repeal of credit for clinical testing expenses for certain
                            drugs for rare diseases or conditions.
Sec. 3402. Repeal of employer-provided child care credit.
Sec. 3403. Repeal of rehabilitation credit.
Sec. 3404. Repeal of work opportunity tax credit.
Sec. 3405. Repeal of deduction for certain unused business credits.
Sec. 3406. Termination of new markets tax credit.
Sec. 3407. Repeal of credit for expenditures to provide access to
                            disabled individuals.
Sec. 3408. Modification of credit for portion of employer social
                            security taxes paid with respect to
                            employee tips.
                       Subtitle F--Energy Credits

Sec. 3501. Modifications to credit for electricity produced from
                            certain renewable resources.
Sec. 3502. Modification of the energy investment tax credit.
Sec. 3503. Extension and phaseout of residential energy efficient
                            property.
Sec. 3504. Repeal of enhanced oil recovery credit.
Sec. 3505. Repeal of credit for producing oil and gas from marginal
                            wells.
Sec. 3506. Modifications of credit for production from advanced nuclear
                            power facilities.
                        Subtitle G--Bond Reforms

Sec. 3601. Termination of private activity bonds.
Sec. 3602. Repeal of advance refunding bonds.
Sec. 3603. Repeal of tax credit bonds.
Sec. 3604. No tax exempt bonds for professional stadiums.
                         Subtitle H--Insurance

Sec. 3701. Net operating losses of life insurance companies.
Sec. 3702. Repeal of small life insurance company deduction.
Sec. 3703. Surtax on life insurance company taxable income.
Sec. 3704. Adjustment for change in computing reserves.
Sec. 3705. Repeal of special rule for distributions to shareholders
                            from pre-1984 policyholders surplus
                            account.
Sec. 3706. Modification of proration rules for property and casualty
                            insurance companies.
Sec. 3707. Modification of discounting rules for property and casualty
                            insurance companies.
Sec. 3708. Repeal of special estimated tax payments.
                        Subtitle I--Compensation

Sec. 3801. Modification of limitation on excessive employee
                            remuneration.
Sec. 3802. Excise tax on excess tax-exempt organization executive
                            compensation.
Sec. 3803. Treatment of qualified equity grants.
        TITLE IV--TAXATION OF FOREIGN INCOME AND FOREIGN PERSONS

    Subtitle A--Establishment of Participation Exemption System for
                       Taxation of Foreign Income

Sec. 4001. Deduction for foreign-source portion of dividends received
                            by domestic corporations from specified 10-
                            percent owned foreign corporations.
Sec. 4002. Application of participation exemption to investments in
                            United States property.
Sec. 4003. Limitation on losses with respect to specified 10-percent
                            owned foreign corporations.
Sec. 4004. Treatment of deferred foreign income upon transition to
                            participation exemption system of taxation.
     Subtitle B--Modifications Related to Foreign Tax Credit System

Sec. 4101. Repeal of section 902 indirect foreign tax credits;
                            determination of section 960 credit on
                            current year basis.
Sec. 4102. Source of income from sales of inventory determined solely
                            on basis of production activities.
            Subtitle C--Modification of Subpart F Provisions

Sec. 4201. Repeal of inclusion based on withdrawal of previously
                            excluded subpart F income from qualified
                            investment.
Sec. 4202. Repeal of treatment of foreign base company oil related
                            income as subpart F income.
Sec. 4203. Inflation adjustment of de minimis exception for foreign
                            base company income.
Sec. 4204. Look-thru rule for related controlled foreign corporations
                            made permanent.
Sec. 4205. Modification of stock attribution rules for determining
                            status as a controlled foreign corporation.
Sec. 4206. Elimination of requirement that corporation must be
                            controlled for 30 days before subpart F
                            inclusions apply.
                 Subtitle D--Prevention of Base Erosion

Sec. 4301. Current year inclusion by United States shareholders with
                            foreign high returns.
Sec. 4302. Limitation on deduction of interest by domestic corporations
                            which are members of an international
                            financial reporting group.
Sec. 4303. Excise tax on certain payments from domestic corporations to
                            related foreign corporations; election to
                            treat such payments as effectively
                            connected income.
   Subtitle E--Provisions Related to Possessions of the United States

Sec. 4401. Extension of deduction allowable with respect to income
                            attributable to domestic production
                            activities in Puerto Rico.
Sec. 4402. Extension of temporary increase in limit on cover over of
                            rum excise taxes to Puerto Rico and the
                            Virgin Islands.
Sec. 4403. Extension of American Samoa economic development credit.
                Subtitle F--Other International Reforms

Sec. 4501. Restriction on insurance business exception to passive
                            foreign investment company rules.
                     TITLE V--EXEMPT ORGANIZATIONS

               Subtitle A--Unrelated Business Income Tax

Sec. 5001. Clarification of unrelated business income tax treatment of
                            entities treated as exempt from taxation
                            under section 501(a).
Sec. 5002. Exclusion of research income limited to publicly available
                            research.
                        Subtitle B--Excise Taxes

Sec. 5101. Simplification of excise tax on private foundation
                            investment income.
Sec. 5102. Private operating foundation requirements relating to
                            operation of art museum.
Sec. 5103. Excise tax based on investment income of private colleges
                            and universities.
Sec. 5104. Exception from private foundation excess business holding
                            tax for independently-operated
                            philanthropic business holdings.
       Subtitle C--Requirements for Organizations Exempt From Tax

Sec. 5201. 501(c)(3) organizations permitted to make statements
                            relating to political campaign in ordinary
                            course of activities.
Sec. 5202. Additional reporting requirements for donor advised fund
                            sponsoring organizations.

                  TITLE I--TAX REFORM FOR INDIVIDUALS

Subtitle A--Simplification and Reform of Rates, Standard Deduction, and
                               Exemptions

SEC. 1001. REDUCTION AND SIMPLIFICATION OF INDIVIDUAL INCOME TAX RATES.

    (a) In General.--Section 1 is amended by striking subsection (i)
and by striking all that precedes subsection (h) and inserting the
following:

``SEC. 1. TAX IMPOSED.

    ``(a) In General.--There is hereby imposed on the income of every
individual a tax equal to the sum of--
            ``(1) 12 percent bracket.--12 percent of so much of the
        taxable income as does not exceed the 25-percent bracket
        threshold amount,
            ``(2) 25 percent bracket.--25 percent of so much of the
        taxable income as exceeds the 25-percent bracket threshold
        amount but does not exceed the 35-percent bracket threshold
        amount, plus
            ``(3) 35 percent bracket.--35 percent of so much of taxable
        income as exceeds the 35-percent bracket threshold amount but
        does not exceed the 39.6 percent bracket threshold amount.
            ``(4) 39.6 percent bracket.--39.6 percent of so much of
        taxable income as exceeds the 39.6-percent bracket threshold
        amount.
    ``(b) Bracket Threshold Amounts.--For purposes of this section--
            ``(1) 25-percent bracket threshold amount.--The term `25-
        percent bracket threshold amount' means--
                    ``(A) in the case of a joint return or surviving
                spouse, $90,000,
                    ``(B) in the case of an individual who is the head
                of a household (as defined in section 2(b)), $67,500,
                    ``(C) in the case of any other individual (other
                than an estate or trust), an amount equal to \1/2\ of
                the amount in effect for the taxable year under
                subparagraph (A), and
                    ``(D) in the case of an estate or trust, $2,550.
            ``(2) 35-percent bracket threshold amount.--The term `35-
        percent bracket threshold amount' means--
                    ``(A) in the case of a joint return or surviving
                spouse, $260,000,
                    ``(B) in the case of a married individual filing a
                separate return, an amount equal to \1/2\ of the amount
                in effect for the taxable year under subparagraph (A),
                and
                    ``(C) in the case of any other individual (other
                than an estate or trust), $200,000, and
                    ``(D) in the case of an estate or trust, $9,150.
            ``(3) 39.6-percent bracket threshold amount.--The term
        `39.6-percent bracket threshold amount' means--
                    ``(A) in the case of a joint return or surviving
                spouse, $1,000,000,
                    ``(B) in the case of any other individual (other
                than an estate or trust), an amount equal to \1/2\ of
                the amount in effect for the taxable year under
                subparagraph (A), and
                    ``(C) in the case of an estate or trust, $12,500.
    ``(c) Inflation Adjustment.--
            ``(1) In general.--In the case of any taxable year
        beginning after 2018, each dollar amount in subsections (b) and
        (e)(3) (other than any amount determined by reference to such a
        dollar amount) shall be increased by an amount equal to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment determined
                under this subsection for the calendar year in which
                the taxable year begins by substituting `2017' for
                `2016' in paragraph (2)(A)(ii).
        If any increase determined under the preceding sentence is not
        a multiple of $100, such increase shall be rounded to the next
        lowest multiple of $100.
            ``(2) Cost-of-living adjustment.--For purposes of this
        subsection--
                    ``(A) In general.--The cost-of-living adjustment
                for any calendar year is the percentage (if any) by
                which--
                            ``(i) the C-CPI-U for the preceding
                        calendar year, exceeds
                            ``(ii) the normalized CPI for calendar year
                        2016.
                    ``(B) Special rule for adjustments with a base year
                after 2016.--For purposes of any provision which
                provides for the substitution of a year after 2016 for
                `2016' in subparagraph (A)(ii), subparagraph (A) shall
                be applied by substituting `C-CPI-U' for `normalized
                CPI' in clause (ii).
            ``(3) Normalized cpi.--For purposes of this subsection, the
        normalized CPI for any calendar year is the product of--
                    ``(A) the CPI for such calendar year, multiplied by
                    ``(B) the C-CPI-U transition multiple.
            ``(4) C-CPI-U transition multiple.--For purposes of this
        subsection, the term `C-CPI-U transition multiple' means the
        amount obtained by dividing--
                    ``(A) the C-CPI-U for calendar year 2016, by
                    ``(B) the CPI for calendar year 2016.
            ``(5) C-CPI-U.--For purposes of this subsection--
                    ``(A) In general.--The term `C-CPI-U' means the
                Chained Consumer Price Index for All Urban Consumers
                (as published by the Bureau of Labor Statistics of the
                Department of Labor). The values of the Chained
                Consumer Price Index for All Urban Consumers taken into
                account for purposes of determining the cost-of-living
                adjustment for any calendar year under this subsection
                shall be the latest values so published as of the date
                on which such Bureau publishes the initial value of the
                Chained Consumer Price Index for All Urban Consumers
                for the month of August for the preceding calendar
                year.
                    ``(B) Determination for calendar year.--The C-CPI-U
                for any calendar year is the average of the C-CPI-U as
                of the close of the 12-month period ending on August 31
                of such calendar year.
            ``(6) CPI.--For purposes of this subsection--
                    ``(A) In general.--The term `Consumer Price Index'
                means the last Consumer Price Index for All Urban
                Consumers published by the Department of Labor. For
                purposes of the preceding sentence, the revision of the
                Consumer Price Index which is most consistent with the
                Consumer Price Index for calendar year 1986 shall be
                used.
                    ``(B) Determination for calendar year.--The CPI for
                any calendar year is the average of the Consumer Price
                Index as of the close of the 12-month period ending on
                August 31 of such calendar year.
    ``(d) Special Rules for Certain Children With Unearned Income.--
            ``(1) In general.--In the case of any child to whom this
        subsection applies for any taxable year--
                    ``(A) the 25-percent bracket threshold amount shall
                not be more than the taxable income of such child for
                the taxable year reduced by the net unearned income of
                such child, and
                    ``(B) the 35-percent bracket threshold amount shall
                not be more than the sum of--
                            ``(i) the taxable income of such child for
                        the taxable year reduced by the net unearned
                        income of such child, plus
                            ``(ii) the dollar amount in effect under
                        subsection (b)(2)(D) for the taxable year.
                    ``(C) the 39.6-percent bracket threshold amount
                shall not be more than the sum of--
                            ``(i) the taxable income of such child for
                        the taxable year reduced by the net unearned
                        income of such child, plus
                            ``(ii) the dollar amount in effect under
                        subsection (b)(3)(C).
            ``(2) Child to whom subsection applies.--This subsection
        shall apply to any child for any taxable year if--
                    ``(A) such child--
                            ``(i) has not attained age 18 before the
                        close of the taxable year, or
                            ``(ii) has attained age 18 before the close
                        of the taxable year and is described in
                        paragraph (3),
                    ``(B) either parent of such child is alive at the
                close of the taxable year, and
                    ``(C) such child does not file a joint return for
                the taxable year.
            ``(3) Certain children whose earned income does not exceed
        one-half of individual's support.--A child is described in this
        paragraph if--
                    ``(A) such child--
                            ``(i) has not attained age 19 before the
                        close of the taxable year, or
                            ``(ii) is a student (within the meaning of
                        section 7706(f)(2)) who has not attained age 24
                        before the close of the taxable year, and
                    ``(B) such child's earned income (as defined in
                section 911(d)(2)) for such taxable year does not
                exceed one-half of the amount of the individual's
                support (within the meaning of section 7706(c)(1)(D)
                after the application of section 7706(f)(5) (without
                regard to subparagraph (A) thereof)) for such taxable
                year.
            ``(4) Net unearned income.--For purposes of this
        subsection--
                    ``(A) In general.--The term `net unearned income'
                means the excess of--
                            ``(i) the portion of the adjusted gross
                        income for the taxable year which is not
                        attributable to earned income (as defined in
                        section 911(d)(2)), over
                            ``(ii) the sum of--
                                    ``(I) the amount in effect for the
                                taxable year under section 63(c)(2)(A)
                                (relating to limitation on standard
                                deduction in the case of certain
                                dependents), plus
                                    ``(II) The greater of the amount
                                described in subclause (I) or, if the
                                child itemizes his deductions for the
                                taxable year, the amount of the
                                itemized deductions allowed by this
                                chapter for the taxable year which are
                                directly connected with the production
                                of the portion of adjusted gross income
                                referred to in clause (i).
                    ``(B) Limitation based on taxable income.--The
                amount of the net unearned income for any taxable year
                shall not exceed the individual's taxable income for
                such taxable year.
    ``(e) Phaseout of 12-percent Rate.--
            ``(1) In general.--The amount of tax imposed by this
        section (determined without regard to this subsection) shall be
        increased by 6 percent of the excess (if any) of--
                    ``(A) adjusted gross income, over
                    ``(B) the applicable dollar amount.
            ``(2) Limitation.--The increase determined under paragraph
        (1) with respect to any taxpayer for any taxable year shall not
        exceed 27.6 percent of the lesser of--
                    ``(A) the taxpayer's taxable income for such
                taxable year, or
                    ``(B) the 25-percent bracket threshold amount in
                effect with respect to the taxpayer for such taxable
                year.
            ``(3) Applicable dollar amount.--For purposes of this
        subsection, the term `applicable dollar amount' means--
                    ``(A) in the case of a joint return or a surviving
                spouse, $1,200,000,
                    ``(B) in the case of a married individual filing a
                separate return, an amount equal to \1/2\ of the amount
                in effect for the taxable year under subparagraph (A),
                and
                    ``(C) in the case of any other individual,
                $1,000,000.
            ``(4) Estates and trusts.--Paragraph (1) shall not apply in
        the case of an estate or trust.''.
    (b) Application of Current Income Tax Brackets to Capital Gains
Brackets.--
            (1) In general.--
                    (A) 0-percent capital gains bracket.--Section
                1(h)(1) is amended by striking ``which would (without
                regard to this paragraph) be taxed at a rate below 25
                percent'' in subparagraph (B)(i) and inserting ``below
                the 15-percent rate threshold''.
                    (B) 15-percent capital gains bracket.--Section
                1(h)(1)(C)(ii)(I) is amended by striking ``which would
                (without regard to this paragraph) be taxed at a rate
                below 39.6 percent'' and inserting ``below the 20-
                percent rate threshold''.
            (2) Rate thresholds defined.--Section 1(h) is amended by
        adding at the end the following new paragraph:
            ``(12) Rate thresholds defined.--For purposes of this
        subsection--
                    ``(A) 15-percent rate threshold.--The 15-percent
                rate threshold shall be--
                            ``(i) in the case of a joint return or
                        surviving spouse, $77,200 (\1/2\ such amount in
                        the case of a married individual filing a
                        separate return),
                            ``(ii) in the case of an individual who is
                        the head of a household (as defined in section
                        2(b)), $51,700,
                            ``(iii) in the case of any other individual
                        (other than an estate or trust), an amount
                        equal to \1/2\ of the amount in effect for the
                        taxable year under clause (i), and
                            ``(iv) in the case of an estate or trust,
                        $2,600.
                    ``(B) 20-percent rate threshold.--The 20-percent
                rate threshold shall be--
                            ``(i) in the case of a joint return or
                        surviving spouse, $479,000 (\1/2\ such amount
                        in the case of a married individual filing a
                        separate return),
                            ``(ii) in the case of an individual who is
                        the head of a household (as defined in section
                        2(b)), $452,400,
                            ``(iii) in the case of any other individual
                        (other than an estate or trust), $425,800, and
                            ``(iv) in the case of an estate or trust,
                        $12,700.
                    ``(C) Inflation adjustment.--In the case of any
                taxable year beginning after 2018, each of the dollar
                amounts in subparagraphs (A) and (B) shall be increased
                by an amount equal to--
                            ``(i) such dollar amount, multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under subsection (c)(2)(A) for the
                        calendar year in which the taxable year begins,
                        determined by substituting `calendar year 2017'
                        for `calendar year 2016' in clause (ii)
                        thereof.''.
    (c) Application of Section 15.--
            (1) In general.--Subsection (a) of section 15 is amended by
        striking ``by this chapter'' and inserting ``by section 11 (or
        by reference to any such rates)''.
            (2) Conforming amendments.--
                    (A) Section 15 is amended by striking subsections
                (d) and (f) and by redesignating subsection (e) as
                subsection (d).
                    (B) Section 15(d), as redesignated by subparagraph
                (A), is amended by striking ``section 1 or 11(b)'' and
                inserting ``section 11(b)''.
                    (C) Section 6013(c) is amended by striking
                ``sections 15, 443, and 7851(a)(1)(A)'' and inserting
                ``sections 443 and 7851(a)(1)(A)''.
            (3) Application to this act.--Section 15 of the Internal
        Revenue Code of 1986 shall not apply to any change in a rate of
        tax imposed by chapter 1 of such Code which occurs by reason of
        any amendment made by this Act (other than the amendments made
        by section 3001).
    (d) Effective Date.--
            (1) In general.--The amendments made by this section shall
        apply to taxable years beginning after December 31, 2017.
            (2) Subsection (c).--The amendments made by subsection (c)
        shall take effect on the date of the enactment of this Act.

SEC. 1002. ENHANCEMENT OF STANDARD DEDUCTION.

    (a) Increase in Standard Deduction.--Section 63(c) is amended to
read as follows:
    ``(c) Standard Deduction.--For purposes of this subtitle--
            ``(1) In general.--Except as otherwise provided in this
        subsection, the term `standard deduction' means--
                    ``(A) $24,400, in the case of a joint return (or a
                surviving spouse (as defined in section 2(a)),
                    ``(B) three-quarters of the amount in effect under
                subparagraph (A) for the taxable year, in the case of
                the head of a household (as defined in section 2(b)),
                and
                    ``(C) one-half of the amount in effect under
                subparagraph (A) for the taxable year, in any other
                case.
            ``(2) Limitation on standard deduction in the case of
        certain dependents.--In the case of an individual who is a
        dependent of another taxpayer for a taxable year beginning in
        the calendar year in which the individual's taxable year
        begins, the standard deduction applicable to such individual
        for such individual's taxable year shall not exceed the greater
        of--
                    ``(A) $500, or
                    ``(B) the sum of $250 and such individual's earned
                income (within the means of section 32).
            ``(3) Certain individuals, etc., not eligible for standard
        deduction.--In the case of--
                    ``(A) a married individual filing a separate return
                where either spouse itemizes deductions,
                    ``(B) a nonresident alien individual,
                    ``(C) an individual making a return under section
                443(a)(1) for a period of less than 12 months on
                account of a change in his annual accounting period, or
                    ``(D) an estate or trust, common trust fund, or
                partnership,
        the standard deduction shall be zero.
            ``(4) Unmarried individual.--For purposes of this section,
        the term `unmarried individual' means any individual who--
                    ``(A) is not married as of the close of the taxable
                year (as determined by applying section 7703),
                    ``(B) is not a surviving spouse (as defined in
                section 2(a)) for the taxable year, and
                    ``(C) is not a dependent of another taxpayer for a
                taxable year beginning in the calendar year in which
                the individual's taxable year begins.
            ``(5) Inflation adjustments.--
                    ``(A) Standard deduction amount.--In the case of
                any taxable year beginning after 2019, the dollar
                amount in paragraph (1)(A) shall be increased by an
                amount equal to--
                            ``(i) such dollar amount, multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under section 1(c)(2)(A) for the
                        calendar year in which the taxable year begins,
                        determined by substituting `calendar year 2018'
                        for `calendar year 2016' in clause (ii)
                        thereof.
                    ``(B) Limitation amount in case of certain
                dependents.--In the case of any taxable year beginning
                after 2017, each of the dollar amounts in paragraph (2)
                shall be increased by an amount equal to--
                            ``(i) such dollar amount, multiplied by
                            ``(ii)(I) in the case of the dollar amount
                        in paragraph (2)(A), under section 1(c)(2)(A)
                        for the calendar year in which the taxable year
                        begins determined by substituting `calendar
                        year 1987' for `calendar year 2016' in clause
                        (ii) thereof, and
                            ``(II) in the case of the dollar amount in
                        paragraph (2)(B), under section 1(c)(2)(A) for
                        the calendar year in which the taxable year
                        begins determined by substituting `calendar
                        year 1997' for `calendar year 2016' in clause
                        (ii) thereof.
        If any increase determined under this paragraph is not a
        multiple of $100, such increase shall be rounded to the next
        lowest multiple of $100.''.
    (b) Conforming Amendments.--
            (1) Section 63(b) is amended by striking ``, minus--'' and
        all that follows and inserting ``minus the standard
        deduction''.
            (2) Section 63 is amended by striking subsections (f) and
        (g).
            (3) Section 1398(c) is amended--
                    (A) by striking ``Basic'' in the heading thereof,
                    (B) by striking ``Basic standard'' in the heading
                of paragraph (3) and inserting ``Standard'', and
                    (C) by striking ``basic'' in paragraph (3).
            (4) Section 3402(m)(3) is amended by striking ``(including
        the additional standard deduction under section 63(c)(3) for
        the aged and blind)''.
            (5) Section 6014(b)(4) is amended by striking ``section
        63(c)(5)'' and inserting ``section 63(c)(2)''.
    (c) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

SEC. 1003. REPEAL OF DEDUCTION FOR PERSONAL EXEMPTIONS.

    (a) In General.--Part V of subchapter B of chapter 1 is hereby
repealed.
    (b) Definition of Dependent Retained.--Section 152, prior to repeal
by subsection (a), is hereby redesignated as section 7706 and moved to
the end of chapter 79.
    (c) Application to Estates and Trusts.--Subsection (b) of section
642 is amended--
            (1) by striking paragraph (2)(C),
            (2) by striking paragraph (3), and
            (3) by striking ``Deduction for Personal Exemption'' in the
        heading thereof and inserting ``Basic Deduction''.
    (d) Application to Nonresident Aliens.--Section 873(b) is amended
by striking paragraph (3).
    (e) Modification of Wage Withholding Rules.--
            (1) In general.--Section 3402(a) is amended by striking
        paragraph (2).
            (2) Conforming amendment.--Section 3402(a) is amended--
                    (A) by redesignating subparagraphs (A) and (B) of
                paragraph (1) as paragraphs (1) and (2) and moving such
                redesignated paragraphs 2 ems to the left, and
                    (B) by striking all that precedes ``otherwise
                provided in this section'' and inserting the following:
    ``(a) Requirement of Withholding.--Except as''.
            (3) Number of exemptions.--Section 3402(f)(1) is amended--
                    (A) in subparagraph (A), by striking ``an
                individual described in section 151(d)(2)'' and
                inserting ``a dependent of any other taxpayer'', and
                    (B) in subparagraph (C), by striking ``with respect
                to whom, on the basis of facts existing at the
                beginning of such day, there may reasonably be expected
                to be allowable an exemption under section 151(c)'' and
                inserting ``who, on the basis of facts existing at the
                beginning of such day, is reasonably expected to be a
                dependent of the employee''.
    (f) Modification of Return Requirement.--
            (1) In general.--Paragraph (1) of section 6012(a) is
        amended to read as follows:
            ``(1) Every individual who has gross income for the taxable
        year, except that a return shall not be required of--
                    ``(A) an individual who is not married (determined
                by applying section 7703) and who has gross income for
                the taxable year which does not exceed the standard
                deduction applicable to such individual for such
                taxable year under section 63, or
                    ``(B) an individual entitled to make a joint return
                if--
                            ``(i) the gross income of such individual,
                        when combined with the gross income of such
                        individual's spouse, for the taxable year does
                        not exceed the standard deduction which would
                        be applicable to the taxpayer for such taxable
                        year under section 63 if such individual and
                        such individual's spouse made a joint return,
                            ``(ii) such individual and such
                        individual's spouse have the same household as
                        their home at the close of the taxable year,
                            ``(iii) such individual's spouse does not
                        make a separate return, and
                            ``(iv) neither such individual nor such
                        individual's spouse is an individual described
                        in section 63(c)(2) who has income (other than
                        earned income) in excess of the amount in
                        effect under section 63(c)(2)(A).''.
            (2) Bankruptcy estates.--Paragraph (8) of section 6012(a)
        is amended by striking ``the sum of the exemption amount plus
        the basic standard deduction under section 63(c)(2)(D)'' and
        inserting ``the standard deduction in effect under section
        63(c)(1)(B)''.
    (g) Conforming Amendments.--
            (1) Section 2(a)(1)(B) is amended by striking ``a
        dependent'' and all that follows through ``section 151'' and
        inserting ``a dependent who (within the meaning of section
        7706, determined without regard to subsections (b)(1), (b)(2)
        and (d)(1)(B) thereof) is a son, stepson, daughter, or
        stepdaughter of the taxpayer''.
            (2) Section 36B(b)(2)(A) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (3) Section 36B(b)(3)(B) is amended by striking ``unless a
        deduction is allowed under section 151 for the taxable year
        with respect to a dependent'' in the flush matter at the end
        and inserting ``unless the taxpayer has a dependent for the
        taxable year''.
            (4) Section 36B(c)(1)(D) is amended by striking ``with
        respect to whom a deduction under section 151 is allowable to
        another taxpayer'' and inserting ``who is a dependent of
        another taxpayer''.
            (5) Section 36B(d)(1) is amended by striking ``equal to the
        number of individuals for whom the taxpayer is allowed a
        deduction under section 151 (relating to allowance of deduction
        for personal exemptions) for the taxable year'' and inserting
        ``the sum of 1 (2 in the case of a joint return) plus the
        number of the taxpayer's dependents for the taxable year''.
            (6) Section 36B(e)(1) is amended by striking ``1 or more
        individuals for whom a taxpayer is allowed a deduction under
        section 151 (relating to allowance of deduction for personal
        exemptions) for the taxable year (including the taxpayer or his
        spouse)'' and inserting ``1 or more of the taxpayer, the
        taxpayer's spouse, or any dependent of the taxpayer''.
            (7) Section 42(i)(3)(D)(ii)(I) is amended--
                    (A) by striking ``section 152'' and inserting
                ``section 7706'', and
                    (B) by striking the period at the end and inserting
                a comma.
            (8) Section 72(t)(2)(D)(i)(III) is amended by striking
        ``section 152'' and inserting ``section 7706''.
            (9) Section 72(t)(7)(A)(iii) is amended by striking
        ``section 152(f)(1)'' and inserting ``section 7706(f)(1)''.
            (10) Section 105(b) is amended--
                    (A) by striking ``as defined in section 152'' and
                inserting ``as defined in section 7706'',
                    (B) by striking ``section 152(f)(1)'' and inserting
                ``section 7706(f)(1)'' and
                    (C) by striking ``section 152(e)'' and inserting
                ``section 7706(e)''.
            (11) Section 105(c)(1) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (12) Section 125(e)(1)(D) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (13) Section 132(h)(2)(B) is amended--
                    (A) by striking ``section 152(f)(1)'' and inserting
                ``section 7706(f)(1)'', and
                    (B) by striking ``section 152(e)'' and inserting
                ``section 7706(e)''.
            (14) Section 139D(c)(5) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (15) Section 162(l)(1)(D) is amended by striking ``section
        152(f)(1)'' and inserting ``section 7706(f)(1)''.
            (16) Section 170(g)(1) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (17) Section 170(g)(3) is amended by striking ``section
        152(d)(2)'' and inserting ``section 7706(d)(2)''.
            (18) Section 172(d) is amended by striking paragraph (3).
            (19) Section 220(b)(6) is amended by striking ``with
        respect to whom a deduction under section 151 is allowable to''
        and inserting ``who is a dependent of''.
            (20) Section 220(d)(2)(A) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (21) Section 223(b)(6) is amended by striking ``with
        respect to whom a deduction under section 151 is allowable to''
        and inserting ``who is a dependent of''.
            (22) Section 223(d)(2)(A) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (23) Section 401(h) is amended by striking ``section
        152(f)(1)'' in the last sentence and inserting ``section
        7706(f)(1)''.
            (24) Section 402(l)(4)(D) is amended by striking ``section
        152'' and inserting ``section 7706''.
            (25) Section 409A(a)(2)(B)(ii)(I) is amended by striking
        ``section 152(a)'' and inserting ``section 7706(a)''.
            (26) Section 501(c)(9) is amended by striking ``section
        152(f)(1)'' and inserting ``section 7706(f)(1)''.
            (27) Section 529(e)(2)(B) is amended by striking ``section
        152(d)(2)'' and inserting ``section 7706(d)(2)''.
            (28) Section 703(a)(2) is amended by striking subparagraph
        (A) and by redesignating subparagraphs (B) through (F) as
        subparagraphs (A) through (E), respectively.
            (29) Section 874 is amended by striking subsection (b) and
        by redesignating subsection (c) as subsection (b).
            (30) Section 891 is amended by striking ``under section 151
        and''.
            (31) Section 904(b) is amended by striking paragraph (1).
            (32) Section 931(b)(1) is amended by striking ``(other than
        the deduction under section 151, relating to personal
        exemptions)''.
            (33) Section 933 is amended--
                    (A) by striking ``(other than the deduction under
                section 151, relating to personal exemptions)'' in
                paragraph (1), and
                    (B) by striking ``(other than the deduction for
                personal exemptions under section 151)'' in paragraph
                (2).
            (34) Section 1212(b)(2)(B)(ii) is amended to read as
        follows:
                            ``(ii) in the case of an estate or trust,
                        the deduction allowed for such year under
                        section 642(b).''.
            (35) Section 1361(c)(1)(C) is amended by striking ``section
        152(f)(1)(C)'' and inserting ``section 7706(f)(1)(C)''.
            (36) Section 1402(a) is amended by striking paragraph (7).
            (37) Section 2032A(c)(7)(D) is amended by striking
        ``section 152(f)(2)'' and inserting ``section 7706(f)(2)''.
            (38) Section 3402(m)(1) is amended by striking ``other than
        the deductions referred to in section 151 and''.
            (39) Section 3402(r)(2) is amended by striking ``the sum
        of--'' and all that follows and inserting ``the standard
        deduction in effect under section 63(c)(1)(B).''.
            (40) Section 5000A(b)(3)(A) is amended by striking
        ``section 152'' and inserting ``section 7706''.
            (41) Section 5000A(c)(4)(A) is amended by striking ``the
        number of individuals for whom the taxpayer is allowed a
        deduction under section 151 (relating to allowance of deduction
        for personal exemptions) for the taxable year'' and inserting
        ``the sum of 1 (2 in the case of a joint return) plus the
        number of the taxpayer's dependents for the taxable year''.
            (42) Section 6013(b)(3)(A) is amended--
                    (A) by striking ``had less than the exemption
                amount of gross income'' in clause (ii) and inserting
                ``had no gross income'',
                    (B) by striking ``had gross income of the exemption
                amount or more'' in clause (iii) and inserting ``had
                any gross income'', and
                    (C) by striking the flush language following clause
                (iii).
            (43) Section 6103(l)(21)(A)(iii) is amended to read as
        follows:
                            ``(iii) the number of the taxpayer's
                        dependents,''.
            (44) Section 6213(g)(2) is amended by striking subparagraph
        (H).
            (45) Section 6334(d)(2) is amended to read as follows:
            ``(2) Exempt amount.--
                    ``(A) In general.--For purposes of paragraph (1),
                the term `exempt amount' means an amount equal to--
                            ``(i) the standard deduction, divided by
                            ``(ii) 52.
                    ``(B) Verified statement.--Unless the taxpayer
                submits to the Secretary a written and properly
                verified statement specifying the facts necessary to
                determine the proper amount under subparagraph (A),
                subparagraph (A) shall be applied as if the taxpayer
                were a married individual filing a separate return with
                no dependents.''.
            (46) Section 7702B(f)(2)(C)(iii) is amended by striking
        ``section 152(d)(2)'' and inserting ``section 7706(d)(2)''.
            (47) Section 7703(a) is amended by striking ``part V of
        subchapter B of chapter 1 and''.
            (48) Section 7703(b)(1) is amended by striking ``section
        152(f)(1)'' and all that follows and inserting ``section
        7706(f)(1),''.
            (49) Section 7706(a), as redesignated by this section, is
        amended by striking ``this subtitle'' and inserting ``subtitle
        A''.
            (50)(A) Section 7706(d)(1)(B), as redesignated by this
        section, is amended by striking ``the exemption amount (as
        defined in section 151(d))'' and inserting ``$4,150''.
            (B) Section 7706(d), as redesignated by this section, is
        amended by adding at the end the following new paragraph:
            ``(6) Inflation adjustment.--In the case of any calendar
        year beginning after 2018, the $4,150 amount in paragraph
        (1)(B) shall be increased by an amount equal to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment determined
                under section 1(c)(2)(A) for such calendar year,
                determined by substituting `calendar year 2017' for
                `calendar year 2016' in clause (ii) thereof.
        If any increase determined under the preceding sentence is not
        a multiple of $100, such increase shall be rounded to the next
        lowest multiple of $100.''.
            (51) The table of sections for chapter 79 is amended by
        adding at the end the following new item:

``Sec. 7706. Dependent defined.''.
    (h) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1004. MAXIMUM RATE ON BUSINESS INCOME OF INDIVIDUALS.

    (a) In General.--Part I of subchapter A of chapter 1 is amended by
inserting after section 3 the following new section:

``SEC. 4. 25 PERCENT MAXIMUM RATE ON BUSINESS INCOME OF INDIVIDUALS.

    ``(a) Reduction in Tax to Achieve 25 Percent Maximum Rate.--The tax
imposed by section 1 shall be reduced by the sum of--
            ``(1) 10 percent of the lesser of--
                    ``(A) qualified business income, or
                    ``(B) the excess (if any) of--
                            ``(i) taxable income reduced by net capital
                        gain (as defined in section 1(h)(11)(A)), over
                            ``(ii) the maximum dollar amount for the
                        25-percent rate bracket which applies to the
                        taxpayer under section 1 for the taxable year,
                        and
            ``(2) 4.6 percent of the excess (if any) of--
                    ``(A) the lesser of--
                            ``(i) qualified business income, or
                            ``(ii) the excess (if any) determined under
                        paragraph (1)(B), over
                    ``(B) the excess of--
                            ``(i) the maximum dollar amount for the 35-
                        percent rate bracket which applies to the
                        taxpayer under section 1 for the taxable year,
                        over
                            ``(ii) the maximum dollar amount for the
                        25-percent rate bracket which applies to the
                        taxpayer under section 1 for the taxable year.
    ``(b) Qualified Business Income.--For purposes of this section, the
term `qualified business income' means the excess (if any) of--
            ``(1) the sum of--
                    ``(A) 100 percent of any net business income
                derived from any passive business activity, plus
                    ``(B) the capital percentage of any net business
                income derived from any active business activity, over
            ``(2) the sum of--
                    ``(A) 100 percent of any net business loss derived
                from any passive business activity,
                    ``(B) except as provided in subsection (e)(3)(A),
                30 percent of any net business loss derived from any
                active business activity, plus
                    ``(C) any carryover business loss determined for
                the preceding taxable year.
    ``(c) Determination of Net Business Income or Loss.--For purposes
of this section--
            ``(1) In general.--Net business income or loss shall be
        determined with respect to any business activity by
        appropriately netting items of income, gain, deduction, and
        loss with respect to such business activity.
            ``(2) Wages, etc.--Any wages (as defined in section 3401),
        payments described in subsection (a) or (c) of section 707, or
        directors' fees received by the taxpayer which are properly
        attributable to any business activity shall be taken into
        account under paragraph (1) as an item of income with respect
        to such business activity.
            ``(3) Exception for certain investment-related items.--
        There shall not be taken into account under paragraph (1)--
                    ``(A) any item of short-term capital gain, short-
                term capital loss, long-term capital gain, or long-term
                capital loss,
                    ``(B) any dividend, income equivalent to a
                dividend, or payment in lieu of dividends described in
                section 954(c)(1)(G),
                    ``(C) any interest income other than interest
                income which is properly allocable to a trade or
                business,
                    ``(D) any item of gain or loss described in
                subparagraph (C) or (D) of section 954(c)(1) (applied
                by substituting `business activity' for `controlled
                foreign corporation'),
                    ``(E) any item of income, gain, deduction, or loss
                taken into account under section 954(c)(1)(F)
                (determined without regard to clause (ii) thereof and
                other than items attributable to notional principal
                contracts entered into in transactions qualifying under
                section 1221(a)(7)),
                    ``(F) any amount received from an annuity which is
                not received in connection with the trade or business
                of the business activity, and
                    ``(G) any item of deduction or loss properly
                allocable to an amount described in any of the
                preceding subparagraphs.
            ``(4) Application of restrictions applicable to determining
        taxable income.--Net business income or loss shall be
        appropriately adjusted so as only to take into account any
        amount of income, gain, deduction, or loss to the extent such
        amount affects the determination of taxable income for the
        taxable year.
            ``(5) Carryover business loss.--For purposes of subsection
        (b)(2)(C), the carryover business loss determined for any
        taxable year is the excess (if any) of the sum described in
        subsection (b)(2) over the sum described in subsection (b)(1)
        for such taxable year.
    ``(d) Passive and Active Business Activity.--For purposes of this
section--
            ``(1) Passive business activity.--The term `passive
        business activity' means any passive activity as defined in
        section 469(c) determined without regard to paragraphs (3) and
        (6)(B) thereof.
            ``(2) Active business activity.--The term `active business
        activity' means any business activity which is not a passive
        business activity.
            ``(3) Business activity.--The term `business activity'
        means any activity (within the meaning of section 469) which
        involves the conduct of any trade or business.
    ``(e) Capital Percentage.--For purposes of this section--
            ``(1) In general.--Except as otherwise provided in this
        section, the term `capital percentage' means 30 percent.
            ``(2) Increased percentage for capital-intensive business
        activities.--In the case of a taxpayer who elects the
        application of this paragraph with respect to any active
        business activity (other than a specified service activity),
        the capital percentage shall be equal to the applicable
        percentage (as defined in subsection (f)) for each taxable year
        with respect to which such election applies. Any election made
        under this paragraph shall apply to the taxable year for which
        such election is made and each of the 4 subsequent taxable
        years. Such election shall be made not later than the due date
        (including extensions) for the return of tax for the taxable
        year for which such election is made, and, once made, may not
        be revoked.
            ``(3) Treatment of specified service activities.--
                    ``(A) In general.--In the case of any active
                business activity which is a specified service
                activity--
                            ``(i) the capital percentage shall be 0
                        percent, and
                            ``(ii) subsection (b)(2)(B) shall be
                        applied by substituting `0 percent' for `30
                        percent'.
                    ``(B) Exception for capital-intensive specified
                service activities.--If--
                            ``(i) the taxpayer elects the application
                        of this subparagraph with respect to such
                        activity for any taxable year, and
                            ``(ii) the applicable percentage (as
                        defined in subsection (f)) with respect to such
                        activity for such taxable year is at least 10
                        percent,
                then subparagraph (A) shall not apply and the capital
                percentage with respect to such activity shall be equal
                to such applicable percentage.
                    ``(C) Specified service activity.--The term
                `specified service activity' means any activity
                involving the performance of services described in
                section 1202(e)(3)(A), including investing, trading, or
                dealing in securities (as defined in section
                475(c)(2)), partnership interests, or commodities (as
                defined in section 475(e)(2)).
            ``(4) Reduction in capital percentage in certain cases.--
        The capital percentage (determined after the application of
        paragraphs (2) and (3)) with respect to any active business
        activity shall not exceed 1 minus the quotient (not greater
        than 1) of--
                    ``(A) any amounts described in subsection (c)(2)
                which are taken into account in determining the net
                business income derived from such activity, divided by
                    ``(B) such net business income.
    ``(f) Applicable Percentage.--For purposes of this section--
            ``(1) In general.--The term `applicable percentage' means,
        with respect to any active business activity for any taxable
        year, the quotient (not greater than 1) of--
                    ``(A) the specified return on capital with respect
                to such activity for such taxable year, divided by
                    ``(B) the taxpayer's net business income derived
                from such activity for such taxable year.
            ``(2) Specified return on capital.--The term `specified
        return on capital' means, with respect to any active business
        activity referred to in paragraph (1), the excess of--
                    ``(A) the product of--
                            ``(i) the deemed rate of return for the
                        taxable year, multiplied by
                            ``(ii) the asset balance with respect to
                        such activity for such taxable year, over
                    ``(B) an amount equal to the interest which is paid
                or accrued, and for which a deduction is allowed under
                this chapter, with respect to such activity for such
                taxable year.
            ``(3) Deemed rate of return.--The term `deemed rate of
        return' means, with respect to any taxable year, the Federal
        short-term rate (determined under section 1274(d) for the month
        in which or with which such taxable year ends) plus 7
        percentage points.
            ``(4) Asset balance.--
                    ``(A) In general.--The asset balance with respect
                to any active business activity referred to in
                paragraph (1) for any taxable year equals the
                taxpayer's adjusted basis of any property described in
                section 1221(a)(2) which is used in connection with
                such activity as of the end of the taxable year
                (determined without regard to sections 168(k) and 179).
                    ``(B) Application to activities carried on through
                partnerships and s corporations.--In the case of any
                active business activity carried on through a
                partnership or S corporation, the taxpayer shall take
                into account such taxpayer's distributive or pro rata
                share (as the case may be) of the asset balance with
                respect to such activity as determined with respect to
                such partnership or S corporation under subparagraph
                (A) (applied by substituting `the partnership's or S
                corporation's adjusted basis' for `the taxpayer's
                adjusted basis').
    ``(g) Reduced Rate for Small Businesses With Net Active Business
Income.--
            ``(1) In general.--The tax imposed by section 1 shall be
        reduced by 3 percent of the excess (if any) of--
                    ``(A) the least of--
                            ``(i) qualified active business income,
                            ``(ii) taxable income reduced by net
                        capital gain (as defined in section
                        1(h)(11)(A)), or
                            ``(iii) the 9-percent bracket threshold
                        amount, over
                    ``(B) the excess (if any) of taxable income over
                the applicable threshold amount.
            ``(2) Phase-in of rate reduction.--In the case of any
        taxable year beginning before January 1, 2022, paragraph (1)
        shall be applied by substituting for `3 percent'--
                    ``(A) in the case of any taxable year beginning
                after December 31, 2017, and before January 1, 2020, `1
                percent', and
                    ``(B) in the case of any taxable year beginning
                after December 31, 2019, and before January 1, 2022, `2
                percent'.
            ``(3) Qualified active business income.--For purposes of
        this subsection, the term `qualified active business income'
        means the excess (if any) of--
                    ``(A) any net business income derived from any
                active business activity, over
                    ``(B) any net business loss derived from any active
                business activity.
            ``(4) 9-percent bracket threshold amount.--For purposes of
        this subsection, the term `9-percent bracket threshold amount'
        means--
                    ``(A) in the case of a joint return or surviving
                spouse, $75,000,
                    ``(B) in the case of an individual who is the head
                of a household (as defined in section 2(b)), \3/4\ of
                the amount in effect for the taxable year under
                subparagraph (A), and
                    ``(C) in the case of any other individual, \1/2\ of
                the amount in effect for the taxable year under
                subparagraph (A).
            ``(5) Applicable threshold amount.--For purposes of this
        subsection, the term `applicable threshold amount' means--
                    ``(A) in the case of a joint return or surviving
                spouse, $150,000,
                    ``(B) in the case of an individual who is the head
                of a household (as defined in section 2(b)), \3/4\ of
                the amount in effect for the taxable year under
                subparagraph (A), and
                    ``(C) in the case of any other individual, \1/2\ of
                the amount in effect for the taxable year under
                subparagraph (A).
            ``(6) Estates and trusts.--Paragraph (1) shall not apply to
        any estate or trust.
            ``(7) Inflation adjustment.--In the case of any taxable
        year beginning after 2018, the dollar amounts in paragraphs
        (4)(A) and (5)(A) shall each be increased by an amount equal
        to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment determined
                under subsection (c)(2)(A) for the calendar year in
                which the taxable year begins, determined by
                substituting `calendar year 2017' for `calendar year
                2016' in clause (ii) thereof.
        If any increase determined under the preceding sentence is not
        a multiple of $100, such increase shall be rounded to the next
        lowest multiple of $100.
    ``(h) Regulations.--The Secretary may issue such regulations or
other guidance as may be necessary or appropriate to carry out the
purposes of this section, including regulations or other guidance--
            ``(1) which ensures that no amount is taken into account
        under subsection (f)(4) with respect to more than one activity,
        and
            ``(2) which treats all specified service activities of the
        taxpayer as a single business activity for purposes of this
        section to the extent that such activities would be treated as
        a single employer under subsection (a) or (b) of section 52 or
        subsection (m) or (o) of section 414.
    ``(i) References.--Any reference in this title to section 1 shall
be treated as including a reference to this section unless the context
of such reference clearly indicates otherwise.''.
    (b) 25 Percent Rate for Certain Dividends of Real Estate Investment
Trusts and Cooperatives.--Section 1(h), as amended by the preceding
provisions of this Act, is amended by adding at the end the following
new paragraph:
            ``(13) 25 percent rate for certain dividends of real estate
        investment trusts and cooperatives.--
                    ``(A) In general.--For purposes of this subsection,
                net capital gain (as defined in paragraph (11)) and
                unrecaptured section 1250 gain (as defined in paragraph
                (6)) shall each be increased by specified dividend
                income.
                    ``(B) Specified dividend income.--For purposes of
                this paragraph, the term `specified dividend income'
                means--
                            ``(i) in the case of any dividend received
                        from a real estate investment trust, the
                        portion of such dividend which is neither--
                                    ``(I) a capital gain dividend (as
                                defined in section 852(b)(3)), nor
                                    ``(II) taken into account in
                                determining qualified dividend income
                                (as defined in paragraph (11)), and
                            ``(ii) any dividend which is includible in
                        gross income and which is received from an
                        organization or corporation described in
                        section 501(c)(12) or 1381(a).''.
    (c) Clerical Amendment.--The table of sections for part I of
subchapter A of chapter 1 is amended by inserting after the item
relating to section 3 the following new item:

``Sec. 4. 25 percent maximum rate on business income of individuals.''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.
    (e) Transition Rule.--In the case of any taxable year which
includes December 31, 2017, the amendment made by subsection (a) shall
apply with respect to such taxable year adjusted--
            (1) so as to apply with respect to the rates of tax in
        effect under section 1 of the Internal Revenue Code of 1986
        with respect to such taxable year (and so as to achieve a 25
        percent effective rate of tax on the business income
        (determined without regard to paragraph (2)) in the same manner
        as such amendment applies to taxable years beginning after such
        date with respect to the rates of tax in effect for such
        years), and
            (2) by reducing the amount of the reduction in tax (as
        otherwise determined under paragraph (1)) by the amount which
        bears the same proportion to the amount of such reduction as
        the number of days in the taxable year which are before January
        1, 2018, bears to the number of days in the entire taxable
        year.

SEC. 1005. CONFORMING AMENDMENTS RELATED TO SIMPLIFICATION OF
              INDIVIDUAL INCOME TAX RATES.

    (a) Amendments Related to Modification of Inflation Adjustment.--
            (1) Section 32(b)(2)(B)(ii)(II) is amended by striking
        ``section 1(f)(3) for the calendar year in which the taxable
        year begins determined by substituting `calendar year 2008' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) for the calendar year in which
        the taxable year begins determined by substituting `calendar
        year 2008' for `calendar year 2016' in clause (ii) thereof''.
            (2) Section 32(j)(1)(B) is amended--
                    (A) in the matter preceding clause (i), by striking
                ``section 1(f)(3)'' and inserting ``section
                1(c)(2)(A)'',
                    (B) in clause (i), by striking ``for `calendar year
                1992' in subparagraph (B) thereof'' and inserting ``for
                `calendar year 2016' in clause (ii) thereof'', and
                    (C) in clause (ii), by striking ``for `calendar
                year 1992' in subparagraph (B) of such section 1'' and
                inserting ``for `calendar year 2016' in clause (ii)
                thereof''.
            (3) Section 36B(b)(3)(A)(ii)(II) is amended by striking
        ``consumer price index'' and inserting ``C-CPI-U (as defined in
        section 1(c))''.
            (4) Section 41(e)(5)(C) is amended to read as follows:
                    ``(C) Cost-of-living adjustment defined.--
                            ``(i) In general.--The cost-of-living
                        adjustment for any calendar year is the cost-
                        of-living adjustment for such calendar year
                        determined under section 1(c)(2)(A), by
                        substituting `calendar year 1987' for `calendar
                        year 2016' in clause (ii) thereof.
                            ``(ii) Special rule where base period ends
                        in a calendar year other than 1983 or 1984.--If
                        the base period of any taxpayer does not end in
                        1983 or 1984, clause (i) shall be applied by
                        substituting the calendar year in which such
                        base period ends for 1987.''.
            (5) Section 42(e)(3)(D)(ii) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting
        `calendar year 2008' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for such
        calendar year by substituting `calendar year 2008' for
        `calendar year 2016' in clause (ii) thereof''.
            (6) Section 42(h)(3)(H)(i)(II) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting
        `calendar year 2001' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for such
        calendar year by substituting `calendar year 2001' for
        `calendar year 2016' in clause (ii) thereof''.
            (7) Section 45R(d)(3)(B)(ii) is amended by striking
        ``section 1(f)(3) for the calendar year, determined by
        substituting `calendar year 2012' for `calendar year 1992' in
        subparagraph (B) thereof'' and inserting ```section 1(c)(2)(A)
        for such calendar year, determined by substituting ``calendar
        year 2012'' for ``calendar year 2016'' in clause (ii)
        thereof'''.
            (8) Section 125(i)(2) is amended--
                    (A) by striking ``section 1(f)(3) for the calendar
                year in which the taxable year begins by substituting
                `calendar year 2012' for `calendar year 1992' in
                subparagraph (B) thereof'' in subparagraph (B) and
                inserting ``section 1(c)(2)(A) for the calendar year in
                which the taxable year begins'', and
                    (B) by striking ``$50'' both places it appears in
                the last sentence and inserting ``$100''.
            (9) Section 162(o)(3) is amended by inserting ``as in
        effect before enactment of the Tax Cuts and Jobs Act'' after
        ``section 1(f)(5)''.
            (10) Section 220(g)(2) is amended by striking ``section
        1(f)(3) for the calendar year in which the taxable year begins
        by substituting `calendar year 1997' for `calendar year 1992'
        in subparagraph (B) thereof'' and inserting ``section
        1(c)(2)(A) for the calendar year in which the taxable year
        begins, determined by substituting `calendar year 1997' for
        `calendar year 2016' in clause (ii) thereof''.
            (11) Section 223(g)(1) is amended by striking all that
        follows subparagraph (A) and inserting the following:
                    ``(B) the cost-of-living adjustment determined
                under section 1(c)(2)(A) for the calendar year in which
                the taxable year begins, determined--
                            ``(i) by substituting for `calendar year
                        2016' in clause (ii) thereof--
                                    ``(I) except as provided in clause
                                (ii), `calendar year 1997', and
                                    ``(II) in the case of each dollar
                                amount in subsection (c)(2)(A),
                                `calendar year 2003', and
                            ``(ii) by substituting `March 31' for
                        `August 31' in paragraphs (5)(B) and (6)(B) of
                        section 1(c).
                The Secretary shall publish the dollar amounts as
                adjusted under this subsection for taxable years
                beginning in any calendar year no later than June 1 of
                the preceding calendar year.''.
            (12) Section 430(c)(7)(D)(vii)(II) is amended by striking
        ``section 1(f)(3) for the calendar year, determined by
        substituting `calendar year 2009' for `calendar year 1992' in
        subparagraph (B) thereof'' and inserting ``section 1(c)(2)(A)
        for the calendar year, determined by substituting `calendar
        year 2009' for `calendar year 2016' in clause (ii) thereof''.
            (13) Section 512(d)(2)(B) is amended by striking ``section
        1(f)(3) for the calendar year in which the taxable year begins,
        by substituting `calendar year 1994' for `calendar year 1992'
        in subparagraph (B) thereof''and inserting ``section 1(c)(2)(A)
        for the calendar year in which the taxable year begins,
        determined by substituting `calendar year 1994' for `calendar
        year 2016' in clause (ii) thereof''.
            (14) Section 513(h)(2)(C)(ii) is amended by striking
        ``section 1(f)(3) for the calendar year in which the taxable
        year begins by substituting `calendar year 1987' for `calendar
        year 1992' in subparagraph (B) thereof'' and inserting
        ``section 1(c)(2)(A) for the calendar year in which the taxable
        year begins, determined by substituting `calendar year 1987'
        for `calendar year 2016' in clause (ii) thereof''.
            (15) Section 831(b)(2)(D)(ii) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting
        `calendar year 2013' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for such
        calendar year by substituting `calendar year 2013' for
        `calendar year 2016' in clause (ii) thereof''.
            (16) Section 877A(a)(3)(B)(i)(II) is amended by striking
        ``section 1(f)(3) for the calendar year in which the taxable
        year begins, by substituting `calendar year 2007' for `calendar
        year 1992' in subparagraph (B) thereof'' and inserting
        ``section 1(c)(2)(A) for the calendar year in which the taxable
        year begins, determined by substituting `calendar year 2007'
        for `calendar year 2016' in clause (ii) thereof''.
            (17) Section 911(b)(2)(D)(ii)(II) is amended by striking
        ``section 1(f)(3) for the calendar year in which the taxable
        year begins, determined by substituting `2004' for `1992' in
        subparagraph (B) thereof'' and inserting ``section 1(c)(2)(A)
        for the calendar year in which the taxable year begins,
        determined by substituting `calendar year 2004' for `calendar
        year 2016' in clause (ii) thereof''.
            (18) Section 1274A(d)(2) is amended to read as follows:
            ``(2) Inflation adjustment.--
                    ``(A) In general.--In the case of any debt
                instrument arising out of a sale or exchange during any
                calendar year after 2018, each adjusted dollar amount
                shall be increased by an amount equal to--
                            ``(i) such adjusted dollar amount,
                        multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under section 1(c)(2)(A) for such
                        calendar year, determined by substituting
                        `calendar year 2017' for `calendar year 2016'
                        in clause (ii) thereof.
                    ``(B) Adjusted dollar amounts.--For purposes of
                this paragraph, the term `adjusted dollar amount' means
                the dollar amounts in subsections (b) and (c), in each
                case as in effect for calendar year 2018.
                    ``(C) Rounding.--Any increase under subparagraph
                (A) shall be rounded to the nearest multiple of
                $100.''.
            (19) Section 2010(c)(3)(B)(ii) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting
        `calendar year 2010' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for such
        calendar year, determined by substituting `calendar year 2010'
        for `calendar year 2016' in clause (ii) thereof''.
            (20) Section 2032A(a)(3)(B) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting
        `calendar year 1997' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for such
        calendar year, determined by substituting `calendar year 1997'
        for `calendar year 2016' in clause (ii) thereof''.
            (21) Section 2503(b)(2)(B) is amended by striking ``section
        1(f)(3) for such calendar year by substituting `calendar year
        1997' for `calendar year 1992' in subparagraph (B) thereof''
        and inserting ``section 1(c)(2)(A) for the calendar year,
        determined by substituting `calendar year 1997' for `calendar
        year 2016' in clause (ii) thereof''.
            (22) Section 4161(b)(2)(C)(i)(II) is amended by striking
        ``section 1(f)(3) for such calendar year, determined by
        substituting `2004' for `1992' in subparagraph (B) thereof''
        and inserting ``section 1(c)(2)(A) for such calendar year,
        determined by substituting `calendar year 2004' for `calendar
        year 2016' in clause (ii) thereof''.
            (23) Section 4261(e)(4)(A)(ii) is amended by striking
        ``section 1(f)(3) for such calendar year by substituting the
        year before the last nonindexed year for `calendar year 1992'
        in subparagraph (B) thereof'' and inserting ``section
        1(c)(2)(A) for such calendar year, determined by substituting
        the year before the last nonindexed year for `calendar year
        2016' in clause (ii) thereof''.
            (24) Section 4980I(b)(3)(C)(v)(II) is amended--
                    (A) by striking ``section 1(f)(3)'' and inserting
                ``section 1(c)(2)(A)'',
                    (B) by striking ``subparagraph (B)'' and inserting
                ``clause (ii)'', and
                    (C) by striking ``1992'' and inserting ``2016''.
            (25) Section 5000A(c)(3)(D)(ii) is amended--
                    (A) by striking ``section 1(f)(3)'' and inserting
                ``section 1(c)(2)(A)'',
                    (B) by striking ``subparagraph (B)'' and inserting
                ``clause (ii)'', and
                    (C) by striking ``1992'' and inserting ``2016''.
            (26) Section 6039F(d) is amended by striking ``section
        1(f)(3), except that subparagraph (B) thereof'' and inserting
        ``section 1(c)(2)(A), except that clause (ii) thereof''.
            (27) Section 6323(i)(4)(B) is amended by striking ``section
        1(f)(3) for the calendar year, determined by substituting
        `calendar year 1996' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for the
        calendar year, determined by substituting `calendar year 1996'
        for `calendar year 2016' in clause (ii) thereof''.
            (28) Section 6334(g)(1)(B) is amended by striking ``section
        1(f)(3) for such calendar year, by substituting `calendar year
        1998' for `calendar year 1992' in subparagraph (B) thereof''
        and inserting ``section 1(c)(2)(A) for such calendar year,
        determined by substituting `calendar year 1999' for `calendar
        year 2016' in clause (ii) thereof''.
            (29) Section 6601(j)(3)(B) is amended by striking ``section
        1(f)(3) for such calendar year by substituting `calendar year
        1997' for `calendar year 1992' in subparagraph (B) thereof''
        and inserting ``section 1(c)(2)(A) for such calendar year by
        substituting `calendar year 1997' for `calendar year 2016' in
        clause (ii) thereof''.
            (30) Section 6651(i)(1) is amended by striking ``section
        1(f)(3) determined by substituting `calendar year 2013' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) determined by substituting
        `calendar year 2013' for `calendar year 2016' in clause (ii)
        thereof''.
            (31) Section 6721(f)(1) is amended--
                    (A) by striking ``section 1(f)(3)'' and inserting
                ``section 1(c)(2)(A)'',
                    (B) by striking ``subparagraph (B)'' and inserting
                ``clause (ii)'', and
                    (C) by striking ``1992'' and inserting ``2016''.
            (32) Section 6722(f)(1) is amended--
                    (A) by striking ``section 1(f)(3)'' and inserting
                ``section 1(c)(2)(A)'',
                    (B) by striking ``subparagraph (B)'' and inserting
                ``clause (ii)'', and
                    (C) by striking ``1992'' and inserting ``2016''.
            (33) Section 6652(c)(7)(A) is amended by striking ``section
        1(f)(3) determined by substituting `calendar year 2013' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) determined by substituting
        `calendar year 2013' for `calendar year 2016' in clause (ii)
        thereof''.
            (34) Section 6695(h)(1) is amended by striking ``section
        1(f)(3) determined by substituting `calendar year 2013' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) determined by substituting
        `calendar year 2013' for `calendar year 2016' in clause (ii)
        thereof''.
            (35) Section 6698(e)(1) is amended by striking ``section
        1(f)(3) determined by substituting `calendar year 2013' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) determined by substituting
        `calendar year 2013' for `calendar year 2016' in clause (ii)
        thereof''.
            (36) Section 6699(e)(1) is amended by striking ``section
        1(f)(3) determined by substituting `calendar year 2013' for
        `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) determined by substituting
        `calendar year 2013' for `calendar year 2016' in clause (ii)
        thereof''.
            (37) Section 7345(f)(2) is amended by striking ``section
        1(f)(3) for the calendar year, determined by substituting
        `calendar year 2015' for `calendar year 1992' in subparagraph
        (B) thereof'' and inserting ``section 1(c)(2)(A) for the
        calendar year, determined by substituting `calendar year 2015'
        for `calendar year 2016' in clause (ii) thereof''.
            (38) Section 7430(c)(1) is amended by striking ``section
        1(f)(3) for such calendar year, by substituting `calendar year
        1995' for `calendar year 1992' in subparagraph (B) thereof'' in
        the flush text at the end and inserting ``section 1(c)(2)(A)
        for such calendar year, determined by substituting `calendar
        year 1995' for `calendar year 2016' in clause (ii) thereof''.
            (39) Section 7872(g)(5) is amended to read as follows:
            ``(5) Inflation adjustment.--
                    ``(A) In general.--In the case of any loan made
                during any calendar year after 2018 to which paragraph
                (1) applies, the adjusted dollar amount shall be
                increased by an amount equal to--
                            ``(i) such adjusted dollar amount,
                        multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under section 1(c)(2)(A) for such
                        calendar year, determined by substituting
                        `calendar year 2017' for `calendar year 2016'
                        in clause (ii) thereof.
                    ``(B) Adjusted dollar amount.--For purposes of this
                paragraph, the term `adjusted dollar amount' means the
                dollar amount in paragraph (2) as in effect for
                calendar year 2018.
                    ``(C) Rounding.--Any increase under subparagraph
                (A) shall be rounded to the nearest multiple of
                $100.''.
            (40) Section 219(b)(5)(C)(i)(II) is amended by striking
        ``section 1(f)(3) for the calendar year in which the taxable
        year begins, determined by substituting `calendar year 2007'
        for `calendar year 1992' in subparagraph (B) thereof'' and
        inserting ``section 1(c)(2)(A) for the calendar year in which
        the taxable year begins, determined by substituting `calendar
        year 2007' for `calendar year 2016' in clause (ii) thereof''.
            (41) Section 219(g)(8)(B) is amended by striking ``section
        1(f)(3) for the calendar year in which the taxable year begins,
        determined by substituting `calendar year 2005' for `calendar
        year 1992' in subparagraph (B) thereof'' and inserting
        ``section 1(c)(2)(A) for the calendar year in which the taxable
        year begins, determined by substituting `calendar year 2005'
        for `calendar year 2016' in clause (ii) thereof''.
    (b) Other Conforming Amendments.--
            (1) Section 36B(b)(3)(B)(ii)(I)(aa) is amended to read as
        follows:
                                            ``(aa) who is described in
                                        section 1(b)(1)(B) and who does
                                        not have any dependents for the
                                        taxable year,''.
            (2) Section 486B(b)(1) is amended--
                    (A) by striking ``maximum rate in effect'' and
                inserting ``highest rate specified'', and
                    (B) by striking ``section 1(e)'' and inserting
                ``section 1''.
            (3) Section 511(b)(1) is amended by striking ``section
        1(e)'' and inserting ``section 1''.
            (4) Section 641(a) is amended by striking ``section 1(e)
        shall apply to the taxable income'' and inserting ``section 1
        shall apply to the taxable income''.
            (5) Section 641(c)(2)(A) is amended to read as follows:
                    ``(A) Except to the extent provided in section
                1(h), the rate of tax shall be treated as being the
                highest rate of tax set forth in section 1(a).''.
            (6) Section 646(b) is amended to read as follows:
    ``(b) Taxation of Income of Trust.--Except as provided in
subsection (f)(1)(B)(ii), there is hereby imposed on the taxable income
of an electing Settlement Trust a tax at the rate specified in section
1(a)(1). Such tax shall be in lieu of the income tax otherwise imposed
by this chapter on such income.''.
            (7) Section 685(c) is amended by striking ``Section 1(e)''
        and inserting ``Section 1''.
            (8) Section 904(b)(3)(E)(ii)(I) is amended by striking
        ``set forth in subsection (a), (b), (c), (d), or (e) of section
        1 (whichever applies)'' and inserting ``the highest rate of tax
        specified in section 1''.
            (9) Section 1398(c)(2) is amended by striking ``subsection
        (d) of''.
            (10) Section 3402(p)(1)(B) is amended by striking ``any
        percentage applicable to any of the 3 lowest income brackets in
        the table under section 1(c),'' and inserting ``12 percent, 25
        percent,''.
            (11) Section 3402(q)(1) is amended by striking ``the
        product of third lowest rate of tax applicable under section
        1(c) and'' and inserting ``25 percent of''.
            (12) Section 3402(r)(3) is amended by striking ``the amount
        of tax which would be imposed by section 1(c) (determined
        without regard to any rate of tax in excess of the fourth
        lowest rate of tax applicable under section 1(c)) on an amount
        of taxable income equal to'' and inserting ``an amount equal to
        the product of 25 percent multiplied by''.
            (13) Section 3406(a)(1) is amended by striking ``the
        product of the fourth lowest rate of tax applicable under
        section 1(c) and'' and inserting ``25 percent of''.
            (14) Section 6103(e)(1)(A)(iii) is amended by inserting
        ``(as in effect on the day before the date of the enactment of
        the Tax Cuts and Jobs Act)'' after ``section 1(g)''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

  Subtitle B--Simplification and Reform of Family and Individual Tax
                                Credits

SEC. 1101. ENHANCEMENT OF CHILD TAX CREDIT AND NEW FAMILY TAX CREDIT.

    (a) Increase in Credit Amount and Addition of Other Dependents.--
    (1) In General.--Section 24(a) is amended to read as follows:
    ``(a) Allowance of Credit.--There shall be allowed as a credit
against the tax imposed by this chapter for the taxable year an amount
equal to the sum of--
            ``(1) with respect to each qualifying child of the
        taxpayer, $1,600, and
            ``(2) for taxable years beginning before January 1, 2023,
        with respect to the taxpayer (each spouse in the case of a
        joint return) and each dependent of the taxpayer to whom
        paragraph (1) does not apply, $300.''.
    (2) Conforming Amendments.--
            (A) Section 24(c) is amended--
                    (i) by redesignating paragraphs (1) and (2) as
                paragraphs (2) and (3), respectively,
                    (ii) by striking ``152(c)'' in paragraph (2) (as so
                redesignated) and inserting ``7706(c)'',
                    (iii) by inserting before paragraph (2) (as so
                redesignated) the following new paragraph:
            ``(1) Dependent.--
                    ``(A) In general.--The term `dependent' shall have
                the meaning given such term by section 7706.
                    ``(B) Certain individuals not treated as
                dependents.--In the case of an individual with respect
                to whom a credit under this section is allowable to
                another taxpayer for a taxable year beginning in the
                calendar year in which the individual's taxable year
                begins, the amount applicable to such individual under
                subsection (a) for such individual's taxable year shall
                be zero.'',
                    (iv) in paragraph (3) (as so redesignated)--
                            (I) by striking ``term `qualifying child'''
                        and inserting ``terms `qualifying child' and
                        `dependent''', and
                            (II) by striking ``152(b)(3)'' and
                        inserting ``7706(b)(3)'', and
                    (v) in the heading by striking ``Qualifying'' and
                inserting ``Dependent; Qualifying''.
            (B) The heading for section 24 is amended by inserting
        ``and family'' after ``child''.
            (C) The table of sections for subpart A of part IV of
        subchapter A of chapter 1 is amended by striking the item
        relating to section 24 and inserting the following new item:

``Sec. 24. Child and family tax credit.''.
    (b) Elimination of Marriage Penalty.--Section 24(b)(2) is amended--
    (1) by striking ``$110,000'' in subparagraph (A) and inserting
``$230,000'',
    (2) by inserting ``and'' at the end of subparagraph (A),
    (3) by striking ``$75,000 in the case of an individual who is not
married'' and all that follows through the period at the end and
inserting ``one-half of the amount in effect under subparagraph (A) for
the taxable year in the case of any other individual.''.
    (c) Credit Refundable up to $1,000 Per Child.--
    (1) In General.--Section 24(d)(1)(A) is amended by striking all
that follows ``under this section'' and inserting the following:
``determined--
                            ``(i) without regard to this subsection and
                        the limitation under section 26(a),
                            ``(ii) without regard to subsection (a)(2),
                        and
                            ``(iii) by substituting `$1,000' for
                        `$1,600' in subsection (a)(1), or''.
    (2) Inflation Adjustment.--Section 24(d) is amended by inserting
after paragraph (2) the following new paragraph:
            ``(3) Inflation adjustment.--In the case of any taxable
        year beginning in a calendar year after 2017, the $1,000 amount
        in paragraph (1)(A)(iii) shall be increased by an amount equal
        to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment under section
                1(c)(2)(A) for such calendar year.
        Any increase determined under the preceding sentence shall be
        rounded to the next highest multiple of $100 and shall not
        exceed the amount in effect under subsection (a)(2).''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1102. REPEAL OF NONREFUNDABLE CREDITS.

    (a) Repeal of Section 22.--
            (1) In general.--Subpart A of part IV of subchapter A of
        chapter 1 is amended by striking section 22 (and by striking
        the item relating to such section in the table of sections for
        such subpart).
            (2) Conforming amendment.--
                    (A) Section 86(f) is amended by striking paragraph
                (1) and by redesignating paragraphs (2), (3), and (4)
                as paragraphs (1), (2), and (3), respectively.
                    (B)(i) Subsections (c)(3)(B) and (d)(4)(A) of
                section 7706, as redesignated by this Act, are each
                amended by striking ``(as defined in section
                22(e)(3)''.
                    (ii) Section 7706(f), as redesignated by this Act,
                is amended by redesignating paragraph (7) as paragraph
                (8) and by inserting after paragraph (6) the following
                new paragraph:
            ``(7) Permanent and total disability defined.--An
        individual is permanently and totally disabled if he is unable
        to engage in any substantial gainful activity by reason of any
        medically determinable physical or mental impairment which can
        be expected to result in death or which has lasted or can be
        expected to last for a continuous period of not less than 12
        months. An individual shall not be considered to be permanently
        and totally disabled unless he furnishes proof of the existence
        thereof in such form and manner, and at such times, as the
        Secretary may require.''.
                    (iii) Section 415(c)(3)(C)(i) is amended by
                striking ``22(e)(3)'' and inserting ``7706(f)(7)''.
                    (iv) Section 422(c)(6) is amended by striking
                ``22(e)(3)'' and inserting ``7706(f)(7)''.
    (b) Termination of Section 25.--Section 25, as amended by section
3601, is amended by adding at the end the following new subsection:
    ``(k) Termination.--No credit shall be allowed under this section
with respect to any mortgage credit certificate issued after December
31, 2017.''.
    (c) Repeal of Section 30D.--
            (1) In general.--Subpart B of part IV of subchapter A of
        chapter 1 is amended by striking section 30D (and by striking
        the item relating to such section in the table of sections for
        such subpart).
            (2) Conforming amendments.--
                    (A) Section 38(b) is amended by striking paragraph
                (35).
                    (B) Section 1016(a) is amended by striking
                paragraph (37).
                    (C) Section 6501(m) is amended by striking
                ``30D(e)(4),''.
    (d) Effective Date.--
            (1) In general.--Except as provided in paragraphs (2) and
        (3), the amendments made by this section shall apply to taxable
        years beginning after December 31, 2017.
            (2) Subsection (b).--The amendment made by subsection (c)
        shall apply to taxable years ending after December 31, 2017.
            (3) Subsection (c).--The amendments made by subsection (d)
        shall apply to vehicles placed in service in taxable years
        beginning after December 31, 2017.

SEC. 1103. REFUNDABLE CREDIT PROGRAM INTEGRITY.

    (a) Identification Requirements for Child and Family Tax Credit.--
            (1) In general.--Section 24(e) is amended to read as
        follows:
    ``(e) Identification Requirements.--
            ``(1) Requirements for qualifying child.--No credit shall
        be allowed under this section to a taxpayer with respect to any
        qualifying child unless the taxpayer includes the name and
        social security number of such qualifying child on the return
        of tax for the taxable year. The preceding sentence shall not
        prevent a qualifying child from being treated as a dependent
        described in subsection (a)(2).
            ``(2) Other identification requirements.--No credit shall
        be allowed under this section with respect to any individual
        unless the taxpayer identification number of such individual is
        included on the return of tax for the taxable year and such
        identifying number was issued before the due date for filing
        the return for the taxable year.
            ``(3) Social security number.--For purposes of this
        subsection, the term `social security number' means a social
        security number issued by the Social Security Administration
        (but only if the social security number is issued to a citizen
        of the United States or pursuant to subclause (I) (or that
        portion of subclause (III) that relates to subclause (I)) of
        section 205(c)(2)(B)(i) of the Social Security Act)).''.
            (2) Omissions treated as mathematical or clerical error.--
                    (A) In general.--Section 6213(g)(2)(I) is amended
                to read as follows:
                    ``(I) an omission of a correct social security
                number, or a correct TIN, required under section 24(e)
                (relating to child tax credit), to be included on a
                return,''.
    (b) Social Security Number Must Be Provided.--
            (1) In general.--Section 25A(f)(1)(A), as amended by
        section 1201 of this Act, is amended by striking ``taxpayer
        identification number'' each place it appears and inserting
        ``social security number''.
            (2) Omission treated as mathematical or clerical error.--
        Section 6213(g)(2)(J) is amended by striking ``TIN'' and
        inserting ``social security number and employer identification
        number''.
    (c) Individuals Prohibited From Engaging in Employment in United
States Not Eligible for Earned Income Tax Credit.--Section 32(m) is
amended--
            (1) by striking ``(other than:'' and all that follows
        through ``of the Social Security Act)'', and
            (2) by inserting before the period at the end the
        following: ``, but only if, in the case of subsection
        (c)(1)(E), the social security number is issued to a citizen of
        the United States or pursuant to subclause (I) (or that portion
        of subclause (III) that relates to subclause (I)) of section
        205(c)(2)(B)(i) of the Social Security Act''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1104. PROCEDURES TO REDUCE IMPROPER CLAIMS OF EARNED INCOME
              CREDIT.

    (a) Clarification Regarding Determination of Self-employment Income
Which Is Treated as Earned Income.--Section 32(c)(2)(B) is amended by
striking ``and'' at the end of clause (v), by striking the period at
the end of clause (vi) and inserting ``, and'', and by adding at the
end the following new clause:
                            ``(vii) in determining the taxpayer's net
                        earnings from self-employment under
                        subparagraph (A)(ii) there shall not fail to be
                        taken into account any deduction which is
                        allowable to the taxpayer under this
                        subtitle.''.
    (b) Required Quarterly Reporting of Wages of Employees.--Section
6011 is amended by adding at the end the following new subsection:
    ``(i) Employer Reporting of Wages.--Every person required to deduct
and withhold from an employee a tax under section 3101 or 3402 shall
include on each return or statement submitted with respect to such tax,
the name and address of such employee and the amount of wages for such
employee on which such tax was withheld.''.
    (c) Effective Date.--
            (1) In general.--Except as provided in paragraph (2), the
        amendments made by this section shall apply to taxable years
        ending after the date of the enactment of this Act.
            (2) Reporting.--The Secretary of the Treasury, or his
        designee, may delay the application of the amendment made by
        subsection (b) for such period as such Secretary (or designee)
        determines to be reasonable to allow persons adequate time to
        modify electronic (or other) systems to permit such person to
        comply with the requirements of such amendment.

SEC. 1105. CERTAIN INCOME DISALLOWED FOR PURPOSES OF THE EARNED INCOME
              TAX CREDIT.

    (a) Substantiation Requirement.--Section 32 is amended by adding at
the end the following new subsection:
    ``(n) Inconsistent Income Reporting.--If the earned income of a
taxpayer claimed on a return for purposes of this section is not
substantiated by statements or returns under sections 6051, 6052,
6041(a), or 6050W with respect to such taxpayer, the Secretary may
require such taxpayer to provide books and records to substantiate such
income, including for the purpose of preventing fraud.''.
    (b) Exclusion of Unsubstantiated Amount From Earned Income.--
Section 32(c)(2) is amended by adding at the end the following new
subparagraph:
                    ``(C) Exclusion.--In the case of a taxpayer with
                respect to which there is an inconsistency described in
                subsection (n) who fails to substantiate such
                inconsistency to the satisfaction of the Secretary, the
                term `earned income' shall not include amounts to the
                extent of such inconsistency.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years ending after the date of the enactment of this
Act.

     Subtitle C--Simplification and Reform of Education Incentives

SEC. 1201. AMERICAN OPPORTUNITY TAX CREDIT.

    (a) In General.--Section 25A is amended to read as follows:

``SEC. 25A. AMERICAN OPPORTUNITY TAX CREDIT.

    ``(a) In General.--In the case of an individual, there shall be
allowed as a credit against the tax imposed by this chapter for the
taxable year an amount equal to the sum of--
            ``(1) 100 percent of so much of the qualified tuition and
        related expenses paid by the taxpayer during the taxable year
        (for education furnished to any eligible student for whom an
        election is in effect under this section for such taxable year
        during any academic period beginning in such taxable year) as
        does not exceed $2,000, plus
            ``(2) 25 percent of so much of such expenses so paid as
        exceeds the dollar amount in effect under paragraph (1) but
        does not exceed twice such dollar amount.
    ``(b) Portion of Credit Refundable.--40 percent of the credit
allowable under subsection (a)(1) (determined without regard to this
subsection and section 26(a) and after application of all other
provisions of this section) shall be treated as a credit allowable
under subpart C (and not under this part). The preceding sentence shall
not apply to any taxpayer for any taxable year if such taxpayer is a
child to whom section 1(d) applies for such taxable year.
    ``(c) Limitation Based on Modified Adjusted Gross Income.--
            ``(1) In general.--The amount allowable as a credit under
        subsection (a) for any taxable year shall be reduced (but not
        below zero) by an amount which bears the same ratio to the
        amount so allowable (determined without regard to this
        subsection and subsection (b) but after application of all
        other provisions of this section) as--
                    ``(A) the excess of--
                            ``(i) the taxpayer's modified adjusted
                        gross income for such taxable year, over
                            ``(ii) $80,000 (twice such amount in the
                        case of a joint return), bears to
                    ``(B) $10,000 (twice such amount in the case of a
                joint return).
            ``(2) Modified adjusted gross income.--For purposes of this
        subsection, the term `modified adjusted gross income' means the
        adjusted gross income of the taxpayer for the taxable year
        increased by any amount excluded from gross income under
        section 911, 931, or 933.
    ``(d) Other Limitations.--
            ``(1) Credit allowed only for 5 taxable years.--An election
        to have this section apply may not be made for any taxable year
        if such an election (by the taxpayer or any other individual)
        is in effect with respect to such student for any 5 prior
        taxable years.
            ``(2) Credit allowed only for first 5 years of
        postsecondary education.--
                    ``(A) In general.--No credit shall be allowed under
                subsection (a) for a taxable year with respect to the
                qualified tuition and related expenses of an eligible
                student if the student has completed (before the
                beginning of such taxable year) the first 5 years of
                postsecondary education at an eligible educational
                institution.
                    ``(B) Fifth year limitations.--In the case of an
                eligible student with respect to whom an election has
                been in effect for 4 preceding taxable years for
                purposes of the fifth taxable year--
                            ``(i) the amount of the credit allowed
                        under this section for the taxable year shall
                        not exceed an amount equal to 50 percent of the
                        credit otherwise determined with respect to
                        such student under this section (without regard
                        to this subparagraph), and
                            ``(ii) the amount of the credit determined
                        under subsection (b) and allowable under
                        subpart C shall not exceed an amount equal to
                        40 percent of the amount determined with
                        respect to such student under clause (i).
    ``(e) Definitions.--For purposes of this section--
            ``(1) Eligible student.--The term `eligible student' means,
        with respect to any academic period, a student who--
                    ``(A) meets the requirements of section 484(a)(1)
                of the Higher Education Act of 1965 (20 U.S.C.
                1091(a)(1)), as in effect on August 5, 1997, and
                    ``(B) is carrying at least \1/2\ the normal full-
                time work load for the course of study the student is
                pursuing.
            ``(2) Qualified tuition and related expenses.--
                    ``(A) In general.--The term `qualified tuition and
                related expenses' means tuition, fees, and course
                materials, required for enrollment or attendance of--
                            ``(i) the taxpayer,
                            ``(ii) the taxpayer's spouse, or
                            ``(iii) any dependent of the taxpayer,
                at an eligible educational institution for courses of
                instruction of such individual at such institution.
                    ``(B) Exception for education involving sports,
                etc.--Such term does not include expenses with respect
                to any course or other education involving sports,
                games, or hobbies, unless such course or other
                education is part of the individual's degree program.
                    ``(C) Exception for nonacademic fees.--Such term
                does not include student activity fees, athletic fees,
                insurance expenses, or other expenses unrelated to an
                individual's academic course of instruction.
            ``(3) Eligible educational institution.--The term `eligible
        educational institution' means an institution--
                    ``(A) which is described in section 481 of the
                Higher Education Act of 1965 (20 U.S.C. 1088), as in
                effect on August 5, 1997, and
                    ``(B) which is eligible to participate in a program
                under title IV of such Act.
    ``(f) Special Rules.--
            ``(1) Identification requirements.--
                    ``(A) Student.--No credit shall be allowed under
                subsection (a) to a taxpayer with respect to the
                qualified tuition and related expenses of an individual
                unless the taxpayer includes the name and taxpayer
                identification number of such individual on the return
                of tax for the taxable year, and such taxpayer
                identification number was issued on or before the due
                date for filing such return.
                    ``(B) Taxpayer.--No credit shall be allowed under
                this section if the identifying number of the taxpayer
                was issued after the due date for filing the return for
                the taxable year.
                    ``(C) Institution.--No credit shall be allowed
                under this section unless the taxpayer includes the
                employer identification number of any institution to
                which qualified tuition and related expenses were paid
                with respect to the individual.
            ``(2) Adjustment for certain scholarships, etc.--The amount
        of qualified tuition and related expenses otherwise taken into
        account under subsection (a) with respect to an individual for
        an academic period shall be reduced (before the application of
        subsection (c)) by the sum of any amounts paid for the benefit
        of such individual which are allocable to such period as--
                    ``(A) a qualified scholarship which is excludable
                from gross income under section 117,
                    ``(B) an educational assistance allowance under
                chapter 30, 31, 32, 34, or 35 of title 38, United
                States Code, or under chapter 1606 of title 10, United
                States Code, and
                    ``(C) a payment (other than a gift, bequest,
                devise, or inheritance within the meaning of section
                102(a)) for such individual's educational expenses, or
                attributable to such individual's enrollment at an
                eligible educational institution, which is excludable
                from gross income under any law of the United States.
            ``(3) Treatment of expenses paid by dependent.--If an
        individual is a dependent of another taxpayer for a taxable
        year beginning in the calendar year in which such individuals
        taxable year begins--
                    ``(A) no credit shall be allowed under subsection
                (a) to such individual for such individual's taxable
                year, and
                    ``(B) qualified tuition and related expenses paid
                by such individual during such individual's taxable
                year shall be treated for purposes of this section as
                paid by such other taxpayer.
            ``(4) Treatment of certain prepayments.--If qualified
        tuition and related expenses are paid by the taxpayer during a
        taxable year for an academic period which begins during the
        first 3 months following such taxable year, such academic
        period shall be treated for purposes of this section as
        beginning during such taxable year.
            ``(5) Denial of double benefit.--No credit shall be allowed
        under this section for any amount for which a deduction is
        allowed under any other provision of this chapter.
            ``(6) No credit for married individuals filing separate
        returns.--If the taxpayer is a married individual (within the
        meaning of section 7703), this section shall apply only if the
        taxpayer and the taxpayer's spouse file a joint return for the
        taxable year.
            ``(7) Nonresident aliens.--If the taxpayer is a nonresident
        alien individual for any portion of the taxable year, this
        section shall apply only if such individual is treated as a
        resident alien of the United States for purposes of this
        chapter by reason of an election under subsection (g) or (h) of
        section 6013.
            ``(8) Restrictions on taxpayers who improperly claimed
        credit in prior year.--
                    ``(A) Taxpayers making prior fraudulent or reckless
                claims.--
                            ``(i) In general.--No credit shall be
                        allowed under this section for any taxable year
                        in the disallowance period.
                            ``(ii) Disallowance period.--For purposes
                        of clause (i), the disallowance period is--
                                    ``(I) the period of 10 taxable
                                years after the most recent taxable
                                year for which there was a final
                                determination that the taxpayer's claim
                                of credit under this section was due to
                                fraud, and
                                    ``(II) the period of 2 taxable
                                years after the most recent taxable
                                year for which there was a final
                                determination that the taxpayer's claim
                                of credit under this section was due to
                                reckless or intentional disregard of
                                rules and regulations (but not due to
                                fraud).
                    ``(B) Taxpayers making improper prior claims.--In
                the case of a taxpayer who is denied credit under this
                section for any taxable year as a result of the
                deficiency procedures under subchapter B of chapter 63,
                no credit shall be allowed under this section for any
                subsequent taxable year unless the taxpayer provides
                such information as the Secretary may require to
                demonstrate eligibility for such credit.
    ``(g) Inflation Adjustment.--
            ``(1) In general.--In the case of a taxable year beginning
        after 2018, the $80,000 amount in subsection (c)(1)(A)(ii)
        shall each be increased by an amount equal to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment determined
                under section 1(c)(2)(A) for the calendar year in which
                the taxable year begins, determined by substituting
                `calendar year 2017' for `calendar year 2016' in clause
                (ii) thereof.
            ``(2) Rounding.--If any amount as adjusted under paragraph
        (1) is not a multiple of $1,000, such amount shall be rounded
        to the next lowest multiple of $1,000.
    ``(h) Regulations.--The Secretary may prescribe such regulations or
other guidance as may be necessary or appropriate to carry out this
section, including regulations providing for a recapture of the credit
allowed under this section in cases where there is a refund in a
subsequent taxable year of any amount which was taken into account in
determining the amount of such credit.''.
    (b) Conforming Amendments.--
            (1) Section 72(t)(7)(B) is amended by striking ``section
        25A(g)(2)'' and inserting ``section 25A(f)(2)''.
            (2) Section 529(c)(3)(B)(v)(I) is amended by striking
        ``section 25A(g)(2)'' and inserting ``section 25A(f)(2)''.
            (3) Section 529(e)(3)(B)(i) is amended by striking
        ``section 25A(b)(3)'' and inserting ``section 25A(d)''.
            (4) Section 530(d)(2)(C) is amended--
                    (A) by striking ``section 25A(g)(2)'' in clause
                (i)(I) and inserting ``section 25A(f)(2)'', and
                    (B) by striking ``Hope and lifetime learning
                credits'' in the heading and inserting ``American
                opportunity tax credit''.
            (5) Section 530(d)(4)(B)(iii) is amended by striking
        ``section 25A(g)(2)'' and inserting ``section 25A(d)(4)(B)''.
            (6) Section 6050S(e) is amended by striking ``subsection
        (g)(2)'' and inserting ``subsection (f)(2)''.
            (7) Section 6211(b)(4)(A) is amended by striking
        ``subsection (i)(6)'' and inserting ``subsection (b)''.
            (8) Section 6213(g)(2)(J) is amended by striking ``TIN
        required under section 25A(g)(1)'' and inserting ``TIN, and
        employer identification number, required under section
        25A(f)(1)''.
            (9) Section 6213(g)(2)(Q) is amended to read as follows:
                    ``(Q) an omission of information required by
                section 25A(f)(8)(B) or an entry on the return claiming
                the credit determined under section 25A(a) for a
                taxable year for which the credit is disallowed under
                section 25A(f)(8)(A).''.
            (10) Section 1004(c) of division B of the American Recovery
        and Reinvestment Tax Act of 2009 is amended--
                    (A) in paragraph (1)--
                            (i) by striking ``section 25A(i)(6)'' each
                        place it appears and inserting ``section
                        25A(b)'', and
                            (ii) by striking ``with respect to taxable
                        years beginning after 2008 and before 2018''
                        each place it appears and inserting ``with
                        respect to each taxable year'',
                    (B) in paragraph (2), by striking ``Section
                25A(i)(6)'' and inserting ``Section 25A(b)'', and
                    (C) in paragraph (3)(C), by striking ``subsection
                (i)(6)'' and inserting ``subsection (b)''.
            (11) The table of sections for subpart A of part IV of
        subchapter A of chapter 1 is amended by striking the item
        relating to section 25A and inserting the following new item:

``Sec. 25A. American opportunity tax credit.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1202. CONSOLIDATION OF EDUCATION SAVINGS RULES.

    (a) No New Contributions to Coverdell Education Savings Account.--
Section 530(b)(1)(A) is amended to read as follows:
                    ``(A) Except in the case of rollover contributions,
                no contribution will be accepted after December 31,
                2017.''.
    (b) Limited Distribution Allowed for Elementary and Secondary
Tuition.--
            (1) In general.--Section 529(c) is amended by adding at the
        end the following new paragraph:
            ``(7) Treatment of elementary and secondary tuition.--Any
        reference in this subsection to the term `qualified higher
        education expense' shall include a reference to expenses for
        tuition in connection with enrollment at an elementary or
        secondary school.''.
            (2) Limitation.--Section 529(e)(3)(A) is amended by adding
        at the end the following: ``The amount of cash distributions
        from all qualified tuition programs described in subsection
        (b)(1)(A)(ii) with respect to a beneficiary during any taxable
        year, shall, in the aggregate, include not more than $10,000 in
        expenses for tuition incurred during the taxable year in
        connection with the enrollment or attendance of the beneficiary
        as an elementary or secondary school student at a public,
        private, or religious school.''.
    (c) Rollovers to Qualified Tuition Programs Permitted.--Section
530(d)(5) is amended by inserting ``, or into (by purchase or
contribution) a qualified tuition program (as defined in section
529),'' after ``into another Coverdell education savings account''.
    (d) Distributions From Qualified Tuition Programs for Certain
Expenses Associated With Registered Apprenticeship Programs.--Section
529(e)(3) is amended by adding at the end the following new
subparagraph:
                    ``(C) Certain expenses associated with registered
                apprenticeship programs.--The term `qualified higher
                education expenses' shall include books, supplies, and
                equipment required for the enrollment or attendance of
                a designated beneficiary in an apprenticeship program
                registered and certified with the Secretary of Labor
                under section 1 of the National Apprenticeship Act (29
                U.S.C. 50).''.
    (e) Unborn Children Allowed as Account Beneficiaries.--Section
529(e) is amended by adding at the end the following new paragraph:
            ``(6) Treatment of unborn children.--
                    ``(A) In general.--Nothing shall prevent an unborn
                child from being treated as a designated beneficiary or
                an individual under this section.
                    ``(B) Unborn child.--For purposes of this
                paragraph--
                            ``(i) In general.--The term `unborn child'
                        means a child in utero.
                            ``(ii) Child in utero.--The term `child in
                        utero' means a member of the species homo
                        sapiens, at any stage of development, who is
                        carried in the womb.''.
    (f) Effective Dates.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        contributions made after December 31, 2017.
            (2) Rollovers to qualified tuition programs.--The
        amendments made by subsection (b) shall apply to distributions
        after December 31, 2017.

SEC. 1203. REFORMS TO DISCHARGE OF CERTAIN STUDENT LOAN INDEBTEDNESS.

    (a) Treatment of Student Loans Discharged on Account of Death or
Disability.--Section 108(f) is amended by adding at the end the
following new paragraph:
            ``(5) Discharges on account of death or disability.--
                    ``(A) In general.--In the case of an individual,
                gross income does not include any amount which (but for
                this subsection) would be includible in gross income by
                reasons of the discharge (in whole or in part) of any
                loan described in subparagraph (B) if such discharge
                was--
                            ``(i) pursuant to subsection (a) or (d) of
                        section 437 of the Higher Education Act of 1965
                        or the parallel benefit under part D of title
                        IV of such Act (relating to the repayment of
                        loan liability),
                            ``(ii) pursuant to section 464(c)(1)(F) of
                        such Act, or
                            ``(iii) otherwise discharged on account of
                        the death or total and permanent disability of
                        the student.
                    ``(B) Loans described.--A loan is described in this
                subparagraph if such loan is--
                            ``(i) a student loan (as defined in
                        paragraph (2)), or
                            ``(ii) a private education loan (as defined
                        in section 140(7) of the Consumer Credit
                        Protection Act (15 U.S.C. 1650(7))).''.
    (b) Exclusion From Gross Income for Payments Made Under Indian
Health Service Loan Repayment Program.--
            (1) In general.--Section 108(f)(4) is amended by inserting
        ``under section 108 of the Indian Health Care Improvement
        Act,'' after ``338I of such Act,''.
            (2) Clerical amendment.--The heading for section 108(f)(4)
        is amended by striking ``and certain'' and inserting ``, indian
        health service loan repayment program, and certain''.
    (c) Effective Dates.--
            (1) Subsection (a).--The amendment made by subsection
        (a)(1) shall apply to discharges of indebtedness after December
        31, 2017.
            (2) Subsection (b).--The amendments made by subsection (b)
        shall apply to amounts received in taxable years beginning
        after December 31, 2017.

SEC. 1204. REPEAL OF OTHER PROVISIONS RELATING TO EDUCATION.

    (a) In General.--Subchapter B of chapter 1 is amended--
            (1) in part VII by striking sections 221 and 222 (and by
        striking the items relating to such sections in the table of
        sections for such part),
            (2) in part VII by striking sections 135 and 127 (and by
        striking the items relating to such sections in the table of
        sections for such part), and
            (3) by striking subsection (d) of section 117.
    (b) Conforming Amendment Relating to Section 221.--
            (1) Section 62(a) is amended by striking paragraph (17).
            (2) Section 74(d) is amended by striking ``221,''.
            (3) Section 86(b)(2)(A) is amended by striking ``221,''.
            (4) Section 219(g)(3)(A)(ii) is amended by striking
        ``221,''.
            (5) Section 163(h)(2) is amended by striking subparagraph
        (F).
            (6) Section 6050S(a) is amended--
                    (A) by inserting ``or'' at the end of paragraph
                (1),
                    (B) by striking ``or'' at the end of paragraph (2),
                and
                    (C) by striking paragraph (3).
            (7) Section 6050S(e) is amended by striking all that
        follows ``thereof)'' and inserting a period.
    (c) Conforming Amendments Related to Section 222.--
            (1) Section 62(a) is amended by striking paragraph (18).
            (2) Section 74(d)(2)(B) is amended by striking ``222,''.
            (3) Section 86(b)(2)(A) is amended by striking ``222,''.
            (4) Section 219(g)(3)(A)(ii) is amended by striking
        ``222,''.
    (d) Conforming Amendments Relating to Section 127.--
            (1) Section 125(f)(1) is amended by striking ``127,''.
            (2) Section 132(j)(8) is amended by striking ``which are
        not excludable from gross income under section 127''.
            (3) Section 414(n)(3)(C) is amended by striking ``127,''.
            (4) Section 414(t)(2) is amended by striking ``127,''.
            (5) Section 3121(a)(18) is amended by striking ``127,''.
            (6) Section 3231(e) is amended by striking paragraph (6).
            (7) Section 3306(b)(13) is amended by ``127,''.
            (8) Section 3401(a)(18) is amended by striking ``127,''.
            (9) Section 6039D(d)(1) is amended by striking ``, 127''.
    (e) Conforming Amendments Relating to Section 117(d).--
            (1) Section 117(c)(1) is amended--
                    (A) by striking ``subsections (a) and (d)'' and
                inserting ``subsection (a)'', and
                    (B) by striking ``or qualified tuition reduction''.
            (2) Section 414(n)(3)(C) is amended by striking
        ``117(d),''.
            (3) Section 414(t)(2) is amended by striking ``117(d),''.
    (f) Conforming Amendments Related to Section 135.--
            (1) Section 74(d)(2)(B) is amended by striking ``135,''.
            (2) Section 86(b)(2)(A) is amended by striking ``135,''.
            (3) Section 219(g)(3)(A)(ii) is amended by striking
        ``135,''.
    (g) Effective Dates.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        taxable years beginning after December 31, 2017.
            (2) Amendments relating to section 117(d).--The amendments
        made by subsections (a)(3) and (e) shall apply to amounts paid
        or incurred after December 31, 2017.

SEC. 1205. ROLLOVERS BETWEEN QUALIFIED TUITION PROGRAMS AND QUALIFIED
              ABLE PROGRAMS.

    (a) Rollovers From Qualified Tuition Programs to Qualified ABLE
Programs.--Section 529(c)(3)(C)(i) is amended by striking ``or'' at the
end of subclause (I), by striking the period at the end of subclause
(II) and inserting ``, or'', and by adding at the end the following new
subclause:
                                    ``(III) to an ABLE account (as
                                defined in section 529A(e)(6)) of the
                                designated beneficiary or a member of
                                the family of the designated
                                beneficiary.
                        Subclause (III) shall not apply to so much of a
                        distribution which, when added to all other
                        contributions made to the ABLE account for the
                        taxable year, exceeds the limitation under
                        section 529A(b)(2)(B).''.
    (b) Effective Date.--The amendments made by this section shall
apply to distributions after December 31, 2017.

          Subtitle D--Simplification and Reform of Deductions

SEC. 1301. REPEAL OF OVERALL LIMITATION ON ITEMIZED DEDUCTIONS.

    (a) In General.--Part 1 of subchapter B of chapter 1 is amended by
striking section 68 (and the item relating to such section in the table
of sections for such part).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1302. MORTGAGE INTEREST.

    (a) Modification of Limitations.--
            (1) In general.--Section 163(h)(3) is amended to read as
        follows:
            ``(3) Qualified residence interest.--For purposes of this
        subsection--
                    ``(A) In general.--The term `qualified residence
                interest' means any interest which is paid or accrued
                during the taxable year on indebtedness which--
                            ``(i) is incurred in acquiring,
                        constructing, or substantially improving any
                        qualified residence (determined as of the time
                        the interest is accrued) of the taxpayer, and
                            ``(ii) is secured by such residence.
                Such term also includes interest on any indebtedness
                secured by such residence resulting from the
                refinancing of indebtedness meeting the requirements of
                the preceding sentence (or this sentence); but only to
                the extent the amount of the indebtedness resulting
                from such refinancing does not exceed the amount of the
                refinanced indebtedness.
                    ``(B) Limitation.--The aggregate amount of
                indebtedness taken into account under subparagraph (A)
                for any period shall not exceed $500,000 (half of such
                amount in the case of a married individual filing a
                separate return).
                    ``(C) Treatment of indebtedness incurred on or
                before november 2, 2017.--
                            ``(i) In general.--In the case of any pre-
                        November 2, 2017, indebtedness, this paragraph
                        shall apply as in effect immediately before the
                        enactment of the Tax Cuts and Jobs Act.
                            ``(ii) Pre-november 2, 2017,
                        indebtedness.--For purposes of this
                        subparagraph, the term `pre-November 2, 2017,
                        indebtedness' means--
                                    ``(I) any principal residence
                                acquisition indebtedness which was
                                incurred on or before November 2, 2017,
                                or
                                    ``(II) any principal residence
                                acquisition indebtedness which is
                                incurred after November 2, 2017, to
                                refinance indebtedness described in
                                clause (i) (or refinanced indebtedness
                                meeting the requirements of this
                                clause) to the extent (immediately
                                after the refinancing) the principal
                                amount of the indebtedness resulting
                                from the refinancing does not exceed
                                the principal amount of the refinanced
                                indebtedness (immediately before the
                                refinancing).
                            ``(iii) Limitation on period of
                        refinancing.--clause (ii)(II) shall not apply
                        to any indebtedness after--
                                    ``(I) the expiration of the term of
                                the original indebtedness, or
                                    ``(II) if the principal of such
                                original indebtedness is not amortized
                                over its term, the expiration of the
                                term of the 1st refinancing of such
                                indebtedness (or if earlier, the date
                                which is 30 years after the date of
                                such 1st refinancing).
                            ``(iv) Binding contract exception.--In the
                        case of a taxpayer who enters into a written
                        binding contract before November 2, 2017, to
                        close on the purchase of a principal residence
                        before January 1, 2018, and who purchases such
                        residence before April 1, 2018, subparagraphs
                        (A) and (B) shall be applied by substituting
                        `April 1, 2018' for `November 2, 2017'.''.
            (2) Conforming amendments.--
                    (A) Section 108(h)(2) is by striking ``for
                `$1,000,000 ($500,000' in clause (ii) thereof'' and
                inserting ``for `$500,000 ($250,000' in paragraph
                (2)(A), and `$1,000,000' for `$500,000' in paragraph
                (2)(B), thereof''.
                    (B) Section 163(h) is amended by striking
                subparagraphs (E) and (F) in paragraph (4).
    (b) Taxpayers Limited to 1 Qualified Residence.--Section
163(h)(4)(A)(i) is amended to read as follows:
                            ``(i) In general.--The term `qualified
                        residence' means the principal residence
                        (within the meaning of section 121) of the
                        taxpayer.''.
    (c) Effective Dates.--
            (1) In general.--The amendments made by this section shall
        apply to interest paid or accrued in taxable years beginning
        after December 31, 2017, with respect to indebtedness incurred
        before, on, or after such date.
            (2) Treatment of grandfathered indebtedness.--For
        application of the amendments made by this section to
        grandfathered indebtedness, see paragraph (3)(C) of section
        163(h) of the Internal Revenue Code of 1986, as amended by this
        section.

SEC. 1303. REPEAL OF DEDUCTION FOR CERTAIN TAXES NOT PAID OR ACCRUED IN
              A TRADE OR BUSINESS.

    (a) In General.--Section 164(b)(5) is amended to read as follows:
            ``(5) Limitation in case of individuals.--In the case of a
        taxpayer other than a corporation--
                    ``(A) foreign real property taxes (other than taxes
                which are paid or accrued in carrying on a trade or
                business or an activity described in section 212) shall
                not be taken into account under subsection (a)(1),
                    ``(B) the aggregate amount of taxes (other than
                taxes which are paid or accrued in carrying on a trade
                or business or an activity described in section 212)
                taken into account under subsection (a)(1) for any
                taxable year shall not exceed $10,000 ($5,000 in the
                case of a married individual filing a separate return),
                    ``(C) subsection (a)(2) shall only apply to taxes
                which are paid or accrued in carrying on a trade or
                business or an activity described in section 212, and
                    ``(D) subsection (a)(3) shall not apply to State
                and local taxes.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1304. REPEAL OF DEDUCTION FOR PERSONAL CASUALTY LOSSES.

    (a) In General.--Section 165(c) is amended by inserting ``and'' at
the end of paragraph (1), by striking ``; and'' at the end of paragraph
(2) and inserting a period, and by striking paragraph (3).
    (b) Conforming Amendments.--
            (1) Section 165(h) is amended to read as follows:
    ``(h) Special Rule Where Personal Casualty Gains Exceed Personal
Casualty Losses.--
            ``(1) In general.--If the personal casualty gains for any
        taxable year exceed the personal casualty losses for such
        taxable year--
                    ``(A) all such gains shall be treated as gains from
                sales or exchanges of capital assets, and
                    ``(B) all such losses shall be treated as losses
                from sales or exchanges of capital assets.
            ``(2) Definitions of personal casualty gain and personal
        casualty loss.--For purposes of this subsection--
                    ``(A) Personal casualty loss.--The term `personal
                casualty loss' means any loss of property not connected
                with a trade or business or a transaction entered into
                for profit, if such loss arises from fire, storm,
                shipwreck, or other casualty, or from theft.
                    ``(B) Personal casualty gain.--The term `personal
                casualty gain' means the recognized gain from any
                involuntary conversion of property which is described
                in subparagraph (A) arising from fire, storm,
                shipwreck, or other casualty, or from theft.''.
            (2) Section 165 is amended by striking subsection (k).
            (3)(A) Section 165(l)(1) is amended by striking ``a loss
        described in subsection (c)(3)'' and inserting ``an ordinary
        loss described in subsection (c)(2)''.
            (B) Section 165(l) is amended--
                    (i) by striking paragraph (5),
                    (ii) by redesignating paragraphs (2), (3), and (4)
                as paragraphs (3), (4), and (5), respectively, and
                    (iii) by inserting after paragraph (1) the
                following new paragraph:
            ``(2) Limitations.--
                    ``(A) Deposit may not be federally insured.--No
                election may be made under paragraph (1) with respect
                to any loss on a deposit in a qualified financial
                institution if part or all of such deposit is insured
                under Federal law.
                    ``(B) Dollar limitation.--With respect to each
                financial institution, the aggregate amount of losses
                attributable to deposits in such financial institution
                to which an election under paragraph (1) may be made by
                the taxpayer for any taxable year shall not exceed
                $20,000 ($10,000 in the case of a separate return by a
                married individual). The limitation of the preceding
                sentence shall be reduced by the amount of any
                insurance proceeds under any State law which can
                reasonably be expected to be received with respect to
                losses on deposits in such institution.''.
            (4) Section 172(b)(1)(E)(ii), prior to amendment under
        title III, is amended by striking subclause (I) and by
        redesignating subclauses (II) and (III) as subclauses (I) and
        (II), respectively.
            (5) Section 172(d)(4)(C) is amended by striking ``paragraph
        (2) or (3) of section 165(c)'' and inserting ``section
        165(c)(2)''.
            (6) Section 274(f) is amended by striking ``Casualty
        Losses,'' in the heading thereof.
            (7) Section 280A(b) is amended by striking ``Casualty
        Losses,'' in the heading thereof.
            (8) Section 873(b), as amended by the preceding provisions
        of this Act, is amended by striking paragraph (1) and by
        redesignating paragraphs (2) and (3) as paragraphs (1) and (2),
        respectively.
            (9) Section 504(b) of the Disaster Tax Relief and Airport
        and Airway Extension Act of 2017 is amended by adding at the
        end the following new paragraph:
            ``(4) Coordination with tax reform.--This subsection shall
        be applied without regard to the amendments made by section
        1304 of the Tax Cuts and Jobs Act.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1305. LIMITATION ON WAGERING LOSSES.

    (a) In General.--Section 165(d) is amended by adding at the end the
following: ``For purposes of the preceding sentence, the term `losses
from wagering transactions' includes any deduction otherwise allowable
under this chapter incurred in carrying on any wagering transaction.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1306. CHARITABLE CONTRIBUTIONS.

    (a) Increased Limitation for Cash Contributions.--Section 170(b)(1)
is amended by redesignating subparagraph (G) as subparagraph (H) and by
inserting after subparagraph (F) the following new subparagraph:
                    ``(G) Increased limitation for cash
                contributions.--
                            ``(i) In general.--In the case of any
                        contribution of cash to an organization
                        described in subparagraph (A), the total amount
                        of such contributions which may be taken into
                        account under subsection (a) for any taxable
                        year shall not exceed 60 percent of the
                        taxpayer's contribution base for such year.
                            ``(ii) Carryover.--If the aggregate amount
                        of contributions described in clause (i)
                        exceeds the applicable limitation under clause
                        (i), such excess shall be treated (in a manner
                        consistent with the rules of subsection (d)(1))
                        as a charitable contribution to which clause
                        (i) applies in each of the 5 succeeding years
                        in order of time.
                            ``(iii) Coordination with subparagraphs (A)
                        and (B).--
                                    ``(I) In general.--Contributions
                                taken into account under this
                                subparagraph shall not be taken into
                                account under subparagraph (A).
                                    ``(II) Limitation reduction.--
                                Subparagraphs (A) and (B) shall be
                                applied by reducing (but not below
                                zero) the aggregate contribution
                                limitation allowed for the taxable year
                                under each such subparagraph by the
                                aggregate contributions allowed under
                                this subparagraph for such taxable
                                year.''.
    (b) Denial of Deduction for College Athletic Event Seating
Rights.--Section 170(l)(1) is amended to read as follows:
            ``(1) In general.--No deduction shall be allowed under this
        section for any amount described in paragraph (2).''.
    (c) Charitable Mileage Rate Adjusted for Inflation.--Section 170(i)
is amended by striking ``shall be 14 cents per mile'' and inserting
``shall be a rate which takes into account the variable cost of
operating an automobile''.
    (d) Repeal of Substantiation Exception in Case of Contributions
Reported by Donee.--Section 170(f)(8) is amended by striking
subparagraph (D) and by redesignating subparagraph (E) as subparagraph
(D).
    (e) Effective Date.--The amendments made by this section shall
apply to contributions made in taxable years beginning after December
31, 2017.

SEC. 1307. REPEAL OF DEDUCTION FOR TAX PREPARATION EXPENSES.

    (a) In General.--Section 212 is amended by adding ``or'' at the end
of paragraph (1), by striking ``; or'' at the end of paragraph (2) and
inserting a period, and by striking paragraph (3).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1308. REPEAL OF MEDICAL EXPENSE DEDUCTION.

    (a) In General.--Part VII of subchapter B is amended by striking by
striking section 213 (and by striking the item relating to such section
in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1)(A) Section 105(f) is amended to read as follows:
    ``(f) Medical Care.--For purposes of this section--
            ``(1) In general.--The term `medical care' means amounts
        paid--
                    ``(A) for the diagnosis, cure, mitigation,
                treatment, or prevention of disease, or for the purpose
                of affecting any structure or function of the body,
                    ``(B) for transportation primarily for and
                essential to medical care referred to in subparagraph
                (A),
                    ``(C) for qualified long-term care services (as
                defined in section 7702B(c)), or
                    ``(D) for insurance (including amounts paid as
                premiums under part B of title XVIII of the Social
                Security Act, relating to supplementary medical
                insurance for the aged) covering medical care referred
                to in subparagraphs (A) and (B) or for any qualified
                long-term care insurance contract (as defined in
                section 7702B(b)).
        In the case of a qualified long-term care insurance contract
        (as defined in section 7702B(b)), only eligible long-term care
        premiums (as defined in paragraph (7)) shall be taken into
        account under subparagraph (D).
            ``(2) Amounts paid for certain lodging away from home
        treated as paid for medical care.--Amounts paid for lodging
        (not lavish or extravagant under the circumstances) while away
        from home primarily for and essential to medical care referred
        to in paragraph (1)(A) shall be treated as amounts paid for
        medical care if--
                    ``(A) the medical care referred to in paragraph
                (1)(A) is provided by a physician in a licensed
                hospital (or in a medical care facility which is
                related to, or the equivalent of, a licensed hospital),
                and
                    ``(B) there is no significant element of personal
                pleasure, recreation, or vacation in the travel away
                from home.
        The amount taken into account under the preceding sentence
        shall not exceed $50 for each night for each individual.
            ``(3) Physician.--The term `physician' has the meaning
        given to such term by section 1861(r) of the Social Security
        Act (42 U.S.C. 1395x(r)).
            ``(4) Contracts covering other than medical care.--In the
        case of an insurance contract under which amounts are payable
        for other than medical care referred to in subparagraphs (A),
        (B) and (C) of paragraph (1)--
                    ``(A) no amount shall be treated as paid for
                insurance to which paragraph (1)(D) applies unless the
                charge for such insurance is either separately stated
                in the contract, or furnished to the policyholder by
                the insurance company in a separate statement,
                    ``(B) the amount taken into account as the amount
                paid for such insurance shall not exceed such charge,
                and
                    ``(C) no amount shall be treated as paid for such
                insurance if the amount specified in the contract (or
                furnished to the policyholder by the insurance company
                in a separate statement) as the charge for such
                insurance is unreasonably large in relation to the
                total charges under the contract.
            ``(5) Certain pre-paid contracts.--Subject to the
        limitations of paragraph (4), premiums paid during the taxable
        year by a taxpayer before he attains the age of 65 for
        insurance covering medical care (within the meaning of
        subparagraphs (A), (B), and (C) of paragraph (1)) for the
        taxpayer, his spouse, or a dependent after the taxpayer attains
        the age of 65 shall be treated as expenses paid during the
        taxable year for insurance which constitutes medical care if
        premiums for such insurance are payable (on a level payment
        basis) under the contract for a period of 10 years or more or
        until the year in which the taxpayer attains the age of 65 (but
        in no case for a period of less than 5 years).
            ``(6) Cosmetic surgery.--
                    ``(A) In general.--The term `medical care' does not
                include cosmetic surgery or other similar procedures,
                unless the surgery or procedure is necessary to
                ameliorate a deformity arising from, or directly
                related to, a congenital abnormality, a personal injury
                resulting from an accident or trauma, or disfiguring
                disease.
                    ``(B) Cosmetic surgery defined .--For purposes of
                this paragraph, the term `cosmetic surgery' means any
                procedure which is directed at improving the patient's
                appearance and does not meaningfully promote the proper
                function of the body or prevent or treat illness or
                disease.
            ``(7) Eligible long-term care premiums.--
                    ``(A) In general.--For purposes of this section,
                the term `eligible long-term care premiums' means the
                amount paid during a taxable year for any qualified
                long-term care insurance contract (as defined in
                section 7702B(b)) covering an individual, to the extent
                such amount does not exceed the limitation determined
                under the following table:

------------------------------------------------------------------------
``In the case of an individual with
an attained age before the close of           The limitation is:
        the taxable year of:
------------------------------------------------------------------------
40 or less                           $200
More than 40 but not more than 50    $375
More than 50 but not more than 60    $750
More than 60 but not more than 70    $2,000
More than 70                         $2,500
------------------------------------------------------------------------

                    ``(B) Indexing.--
                            ``(i) In general.--In the case of any
                        taxable year beginning after 1997, each dollar
                        amount in subparagraph (A) shall be increased
                        by the medical care cost adjustment of such
                        amount for such calendar year. Any increase
                        determined under the preceding sentence shall
                        be rounded to the nearest multiple of $10.
                            ``(ii) Medical care cost adjustment.--For
                        purposes of clause (i), the medical care cost
                        adjustment for any calendar year is the
                        adjustment prescribed by the Secretary, in
                        consultation with the Secretary of Health and
                        Human Services, for purposes of such clause. To
                        the extent that CPI (as defined section 1(c)),
                        or any component thereof, is taken into account
                        in determining such adjustment, such adjustment
                        shall be determined by taking into account C-
                        CPI-U (as so defined), or the corresponding
                        component thereof, in lieu of such CPI (or
                        component thereof), but only with respect to
                        the portion of such adjustment which relates to
                        periods after December 31, 2017.
            ``(8) Certain payments to relatives treated as not paid for
        medical care.--An amount paid for a qualified long-term care
        service (as defined in section 7702B(c)) provided to an
        individual shall be treated as not paid for medical care if
        such service is provided--
                    ``(A) by the spouse of the individual or by a
                relative (directly or through a partnership,
                corporation, or other entity) unless the service is
                provided by a licensed professional with respect to
                such service, or
                    ``(B) by a corporation or partnership which is
                related (within the meaning of section 267(b) or
                707(b)) to the individual.
        For purposes of this paragraph, the term `relative' means an
        individual bearing a relationship to the individual which is
        described in any of subparagraphs (A) through (G) of section
        7706(d)(2). This paragraph shall not apply for purposes of
        subsection (b) with respect to reimbursements through
        insurance.''.
            (B) Section 72(t)(2)(D)(i)(III) is amended by striking
        ``section 213(d)(1)(D)'' and inserting ``section
        105(f)(1)(D)''.
            (C) Section 104(a) is amended by striking ``section
        213(d)(1)'' in the last sentence and inserting ``section
        105(f)(1)''.
            (D) Section 105(b) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (E) Section 139D is amended by striking ``section 213'' and
        inserting ``section 223''.
            (F) Section 162(l)(2) is amended by striking ``section
        213(d)(10)'' and inserting ``section 105(f)(7)''.
            (G) Section 220(d)(2)(A) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (H) Section 223(d)(2)(A) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (I) Section 419A(f)(2) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (J) Section 501(c)(26)(A) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (K) Section 2503(e) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (L) Section 4980B(c)(4)(B)(i)(I) is amended by striking
        ``section 213(d)'' and inserting ``section 105(f)''.
            (M) Section 6041(f) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (N) Section 7702B(a)(2) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (O) Section 7702B(a)(4) is amended by striking ``section
        213(d)(1)(D)'' and inserting ``section 105(f)(1)(D)''.
            (P) Section 7702B(d)(5) is amended by striking ``section
        213(d)(10)'' and inserting ``section 105(f)(7)''.
            (Q) Section 9832(d)(3) is amended by striking ``section
        213(d)'' and inserting ``section 105(f)''.
            (2) Section 72(t)(2)(B) is amended to read as follows:
                    ``(B) Medical expenses.--Distributions made to an
                individual (other than distributions described in
                subparagraph (A), (C), or (D) to the extent such
                distributions do not exceed the excess of--
                            ``(i) the expenses paid by the taxpayer
                        during the taxable year, not compensated for by
                        insurance or otherwise, for medical care (as
                        defined in 105(f)) of the taxpayer, his spouse,
                        or a dependent (as defined in section 7706,
                        determined without regard to subsections
                        (b)(1), (b)(2), and (d)(1)(B) thereof), over
                            ``(ii) 10 percent of the taxpayer's
                        adjusted gross income.''.
            (3) Section 162(l) is amended by striking paragraph (3).
            (4) Section 402(l) is amended by striking paragraph (7) and
        redesignating paragraph (8) as paragraph (7).
            (5) Section 220(f) is amended by striking paragraph (6).
            (6) Section 223(f) is amended by striking paragraph (6).
            (7) Section 7702B(e) is amended by striking paragraph (2).
            (8) Section 7706(f)(7), as redesignated by this Act, is
        amended by striking ``sections 105(b), 132(h)(2)(B), and
        213(d)(5)'' and inserting ``sections 105(b) and 132(h)(2)(B)''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1309. REPEAL OF DEDUCTION FOR ALIMONY PAYMENTS.

    (a) In General.--Part VII of subchapter B is amended by striking by
striking section 215 (and by striking the item relating to such section
in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1) Corresponding repeal of provisions providing for
        inclusion of alimony in gross income.--
                    (A) Subsection (a) of section 61 is amended by
                striking paragraph (8) and by redesignating paragraphs
                (9) through (15) as paragraphs (8) through (14),
                respectively.
                    (B) Part II of subchapter B of chapter 1 is amended
                by striking section 71 (and by striking the item
                relating to such section in the table of sections for
                such part).
                    (C) Subpart F of part I of subchapter J of chapter
                1 is amended by striking section 682 (and by striking
                the item relating to such section in the table of
                sections for such subpart).
            (2) Related to repeal of section 215.--
                    (A) Section 62(a) is amended by striking paragraph
                (10).
                    (B) Section 3402(m)(1) is amended by striking
                ``(other than paragraph (10) thereof)''.
            (3) Related to repeal of section 71.--
                    (A) Section 121(d)(3) is amended--
                            (i) by striking ``(as defined in section
                        71(b)(2))'' in subparagraph (B), and
                            (ii) by adding at the end the following new
                        subparagraph:
                    ``(C) Divorce or separation instrument.--For
                purposes of this paragraph, the term `divorce or
                separation instrument' means--
                            ``(i) a decree of divorce or separate
                        maintenance or a written instrument incident to
                        such a decree,
                            ``(ii) a written separation agreement, or
                            ``(iii) a decree (not described in clause
                        (i)) requiring a spouse to make payments for
                        the support or maintenance of the other
                        spouse.''.
                    (B) Section 220(f)(7) is amended by striking
                ``subparagraph (A) of section 71(b)(2)'' and inserting
                ``clause (i) of section 121(d)(3)(C)''.
                    (C) Section 223(f)(7) is amended by striking
                ``subparagraph (A) of section 71(b)(2)'' and inserting
                ``clause (i) of section 121(d)(3)(C)''.
                    (D) Section 382(l)(3)(B)(iii) is amended by
                striking ``section 71(b)(2)'' and inserting ``section
                121(d)(3)(C)''.
                    (E) Section 408(d)(6) is amended by striking
                ``subparagraph (A) of section 71(b)(2)'' and inserting
                ``clause (i) of section 121(d)(3)(C)''.
    (c) Effective Date.--The amendments made by this section shall
apply to--
            (1) any divorce or separation instrument (as defined in
        section 71(b)(2) of the Internal Revenue Code of 1986 as in
        effect before the date of the enactment of this Act) executed
        after December 31, 2017, and
            (2) any divorce or separation instrument (as so defined)
        executed on or before such date and modified after such date if
        the modification expressly provides that the amendments made by
        this section apply to such modification.

SEC. 1310. REPEAL OF DEDUCTION FOR MOVING EXPENSES.

    (a) In General.--Part VII of subchapter B is amended by striking by
striking section 217 (and by striking the item relating to such section
in the table of sections for such subpart).
    (b) Retention of Moving Expenses for Members of Armed Forces.--
Section 134(b) is amended by adding at the end the following new
paragraph:
            ``(7) Moving expenses.--The term `qualified military
        benefit' includes any benefit described in section 217(g) (as
        in effect before the enactment of the Tax Cuts And Jobs
        Act).''.
    (c) Conforming Amendments.--
            (1) Section 62(a) is amended by striking paragraph (15).
            (2) Section 274(m)(3) is amended by striking ``(other than
        section 217)''.
            (3) Section 3121(a) is amended by striking paragraph (11).
            (4) Section 3306(b) is amended by striking paragraph (9).
            (5) Section 3401(a) is amended by striking paragraph (15).
            (6) Section 7872(f) is amended by striking paragraph (11).
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1311. TERMINATION OF DEDUCTION AND EXCLUSIONS FOR CONTRIBUTIONS TO
              MEDICAL SAVINGS ACCOUNTS.

    (a) Termination of Income Tax Deduction.--Section 220 is amended by
adding at the end the following new subsection:
    ``(k) Termination.--No deduction shall be allowed under subsection
(a) with respect to any taxable year beginning after December 31,
2017.''.
    (b) Termination of Exclusion for Employer-Provided Contributions.--
Section 106 is amended by striking subsection (b).
    (c) Conforming Amendments.--
            (1) Section 62(a) is amended by striking paragraph (16).
            (2) Section 106(d) is amended by striking paragraph (2), by
        redesignating paragraph (3) as paragraph (6), and by inserting
        after paragraph (1) the following new paragraphs:
            ``(2) No constructive receipt.--No amount shall be included
        in the gross income of any employee solely because the employee
        may choose between the contributions referred to in paragraph
        (1) and employer contributions to another health plan of the
        employer.
            ``(3) Special rule for deduction of employer
        contributions.--Any employer contribution to a health savings
        account (as so defined), if otherwise allowable as a deduction
        under this chapter, shall be allowed only for the taxable year
        in which paid.
            ``(4) Employer health savings account contribution required
        to be shown on return.--Every individual required to file a
        return under section 6012 for the taxable year shall include on
        such return the aggregate amount contributed by employers to
        the health savings accounts (as so defined) of such individual
        or such individual's spouse for such taxable year.
            ``(5) Health savings account contributions not part of
        cobra coverage.--Paragraph (1) shall not apply for purposes of
        section 4980B.''.
            (3) Section 223(b)(4) is amended by striking subparagraph
        (A), by redesignating subparagraphs (B) and (C) as
        subparagraphs (A) and (B), respectively, and by striking the
        second sentence thereof.
            (4) Section 223(b)(5) is amended by striking ``under
        paragraph (3))'' and all that follows through ``shall be
        divided equally between them'' and inserting the following:
        ``under paragraph (3)) shall be divided equally between the
        spouses''.
            (5) Section 223(c) is amended by striking paragraph (5).
            (6) Section 3231(e) is amended by striking paragraph (10).
            (7) Section 3306(b) is amended by striking paragraph (17).
            (8) Section 3401(a) is amended by striking paragraph (21).
            (9) Chapter 43 is amended by striking section 4980E (and by
        striking the item relating to such section in the table of
        sections for such chapter).
            (10) Section 4980G is amended to read as follows:

``SEC. 4980G. FAILURE OF EMPLOYER TO MAKE COMPARABLE HEALTH SAVINGS
              ACCOUNT CONTRIBUTIONS.

    ``(a) In General.--In the case of an employer who makes a
contribution to the health savings account of any employee during a
calendar year, there is hereby imposed a tax on the failure of such
employer to meet the requirements of subsection (d) for such calendar
year.
    ``(b) Amount of Tax.--The amount of the tax imposed by subsection
(a) on any failure for any calendar year is the amount equal to 35
percent of the aggregate amount contributed by the employer to health
savings accounts of employees for taxable years of such employees
ending with or within such calendar year.
    ``(c) Waiver by Secretary.--In the case of a failure which is due
to reasonable cause and not to willful neglect, the Secretary may waive
part or all of the tax imposed by subsection (a) to the extent that the
payment of such tax would be excessive relative to the failure
involved.
    ``(d) Employer Required To Make Comparable Health Savings Account
Contributions for All Participating Employees.--
            ``(1) In general.--An employer meets the requirements of
        this subsection for any calendar year if the employer makes
        available comparable contributions to the health savings
        accounts of all comparable participating employees for each
        coverage period during such calendar year.
            ``(2) Comparable contributions.--
                    ``(A) In general.--For purposes of paragraph (1),
                the term `comparable contributions' means
                contributions--
                            ``(i) which are the same amount, or
                            ``(ii) which are the same percentage of the
                        annual deductible limit under the high
                        deductible health plan covering the employees.
                    ``(B) Part-year employees.--In the case of an
                employee who is employed by the employer for only a
                portion of the calendar year, a contribution to the
                health savings account of such employee shall be
                treated as comparable if it is an amount which bears
                the same ratio to the comparable amount (determined
                without regard to this subparagraph) as such portion
                bears to the entire calendar year.
            ``(3) Comparable participating employees.--
                    ``(A) In general.--For purposes of paragraph (1),
                the term `comparable participating employees' means all
                employees--
                            ``(i) who are eligible individuals covered
                        under any high deductible health plan of the
                        employer, and
                            ``(ii) who have the same category of
                        coverage.
                    ``(B) Categories of coverage.--For purposes of
                subparagraph (B), the categories of coverage are self-
                only and family coverage.
            ``(4) Part-time employees.--
                    ``(A) In general .--Paragraph (3) shall be applied
                separately with respect to part-time employees and
                other employees.
                    ``(B) Part-time employee.--For purposes of
                subparagraph (A), the term `part-time employee' means
                any employee who is customarily employed for fewer than
                30 hours per week.
            ``(5) Special rule for non-highly compensated employees.--
        For purposes of applying this section to a contribution to a
        health savings account of an employee who is not a highly
        compensated employee (as defined in section 414(q)), highly
        compensated employees shall not be treated as comparable
        participating employees.
    ``(e) Controlled Groups.--For purposes of this section, all persons
treated as a single employer under subsection (b), (c), (m), or (o) of
section 414 shall be treated as 1 employer.
    ``(f) Definitions.--Terms used in this section which are also used
in section 223 have the respective meanings given such terms in section
223.
    ``(g) Regulations.--The Secretary shall issue regulations to carry
out the purposes of this section.''.
            (11) Section 6051(a) is amended by striking paragraph (11).
            (12) Section 6051(a)(14)(A) is amended by striking
        ``paragraphs (11) and (12)'' and inserting ``paragraph (12)''.
    (d) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

SEC. 1312. DENIAL OF DEDUCTION FOR EXPENSES ATTRIBUTABLE TO THE TRADE
              OR BUSINESS OF BEING AN EMPLOYEE.

    (a) In General.--Part IX of subchapter B of chapter 1 is amended by
inserting after the item relating to section 262 the following new
item:

``SEC. 262A. EXPENSES ATTRIBUTABLE TO BEING AN EMPLOYEE.

    ``(a) In General.--Except as otherwise provided in this section, no
deduction shall be allowed with respect to any trade or business of the
taxpayer which consists of the performance of services by the taxpayer
as an employee.
    ``(b) Exception for Above-the-line Deductions.--Subsection (a)
shall not apply to any deduction allowable (determined without regard
to subsection (a)) in determining adjusted gross income.''.
    (b) Repeal of Certain Above-the-line Trade and Business Deductions
of Employees.--
            (1) In general.--Section 62(a)(2) is amended--
                    (A) by striking subparagraphs (B), (C), and (D),
                and
                    (B) by redesignating subparagraph (E) as
                subparagraph (B).
            (2) Conforming amendments.--
                    (A) Section 62 is amended by striking subsections
                (b) and (d) and by redesignating subsections (c) and
                (e) as subsections (b) and (c), respectively.
                    (B) Section 62(a)(20) is amended by striking
                ``subsection (e)'' and inserting ``subsection (c)''.
    (c) Continued Exclusion of Working Condition Fringe Benefits.--
Section 132(d) is amended by inserting ``(determined without regard to
section 262A)'' after ``section 162''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

    Subtitle E--Simplification and Reform of Exclusions and Taxable
                              Compensation

SEC. 1401. LIMITATION ON EXCLUSION FOR EMPLOYER-PROVIDED HOUSING.

    (a) In General.--Section 119 is amended by adding at the end the
following new subsection:
    ``(e) Limitation on Exclusion of Lodging.--
            ``(1) In general.--The aggregate amount excluded from gross
        income of the taxpayer under subsections (a) and (d) with
        respect to lodging for any taxable year shall not exceed
        $50,000 (half such amount in the case of a married individual
        filing a separate return).
            ``(2) Limitation to 1 home.--Subsections (a) and (d)
        (separately and in combination) shall not apply with respect to
        more than 1 residence of the taxpayer at any given time. In the
        case of a joint return, the preceding sentence shall apply
        separately to each spouse for any period during which each
        spouse resides separate from the other spouse in a residence
        which is provided in connection with the employment of each
        spouse, respectively.
            ``(3) Limitation for highly compensated employees.--
                    ``(A) Reduced for excess compensation.--In the case
                of an individual whose compensation for the taxable
                year exceeds the amount in effect under section
                414(q)(1)(B)(i) for the calendar in which such taxable
                year begins, the $50,000 amount under paragraph (1)
                shall be reduced (but not below zero) by an amount
                equal to 50 percent of such excess. For purposes of the
                preceding sentence, the term `compensation' means wages
                (as defined in section 3121(a) (without regard to the
                contribution and benefit base limitation in section
                3121(a)(1)).
                    ``(B) Exclusion denied for 5-percent owners.--In
                the case of an individual who is a 5-percent owner (as
                defined in section 416(i)(1)(B)(i)) of the employer at
                any time during the taxable year, the amount under
                paragraph (1) shall be zero.''.
    (b) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

SEC. 1402. EXCLUSION OF GAIN FROM SALE OF A PRINCIPAL RESIDENCE.

    (a) Requirement That Residence Be Principal Residence for 5 Years
During 8-year Period.--Subsection (a) of section 121 is amended--
            (1) by striking ``5-year period'' and inserting ``8-year
        period'', and
            (2) by striking ``2 years'' and inserting ``5 years''.
    (b) Application to Only 1 Sale or Exchange Every 5 Years.--
Paragraph (3) of section 121(b) is amended to read as follows:
            ``(3) Application to only 1 sale or exchange every 5
        years.--Subsection (a) shall not apply to any sale or exchange
        by the taxpayer if, during the 5-year period ending on the date
        of such sale or exchange, there was any other sale or exchange
        by the taxpayer to which subsection (a) applied.''.
    (c) Phaseout Based on Modified Adjusted Gross Income.--Section 121
is amended by adding at the end the following new subsection:
    ``(h) Phaseout Based on Modified Adjusted Gross Income.--
            ``(1) In general.--If the average modified adjusted gross
        income of the taxpayer for the taxable year and the 2 preceding
        taxable years exceeds $250,000 (twice such amount in the case
        of a joint return), the amount which would (but for this
        subsection) be excluded from gross income under subsection (a)
        for such taxable year shall be reduced (but not below zero) by
        the amount of such excess.
            ``(2) Modified adjusted gross income.--For purposes of this
        subsection, the term `modified adjusted gross income' means,
        with respect to any taxable year, adjusted gross income
        determined after application of this section (but without
        regard to subsection (b)(1) and this subsection).
            ``(3) Special rule for joint returns.--In the case of a
        joint return, the average modified adjusted gross income of the
        taxpayer shall be determined without regard to any taxable year
        with respect to which the taxpayer did not file a joint
        return.''.
    (d) Conforming Amendments.--
            (1) The following provisions of section 121 are each
        amended by striking ``5-year period'' each place it appears
        therein and inserting ``8-year period'':
                    (A) Subsection (b)(5)(C)(ii)(I).
                    (B) Subsection (c)(1)(B)(i)(I).
                    (C) Subsection (d)(7)(B).
                    (D) Subparagraphs (A) and (B) of subsection (d)(9).
                    (E) Subsection (d)(10).
                    (F) Subsection (d)(12)(A).
            (2) Section 121(c)(1)(B)(ii) is amended by striking ``2
        years'' and inserting ``5 years'':
    (e) Effective Date.--The amendments made by this section shall
apply to sales and exchanges after December 31, 2017.

SEC. 1403. REPEAL OF EXCLUSION, ETC., FOR EMPLOYEE ACHIEVEMENT AWARDS.

    (a) In General.--Section 74 is amended by striking subsection (c).
    (b) Repeal of Limitation on Deduction.--Section 274 is amended by
striking subsection (j).
    (c) Conforming Amendments.--
            (1) Section 102(c)(2) is amended by striking the first
        sentence.
            (2) Section 414(n)(3)(C) is amended by striking
        ``274(j),''.
            (3) Section 414(t)(2) is amended by striking ``274(j),''.
            (4) Section 3121(a)(20) is amended by striking ``74(c)''.
            (5) Section 3231(e)(5) is amended by striking ``74(c),''.
            (6) Section 3306(b)(16) is amended by striking ``74(c),''.
            (7) Section 3401(a)(19) is amended by striking ``74(c),''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1404. SUNSET OF EXCLUSION FOR DEPENDENT CARE ASSISTANCE PROGRAMS.

    (a) In General.--Section 129 is amended by adding at the end the
following new subsection:
    ``(f) Termination.--Subsection (a) shall not apply to taxable years
beginning after December 31, 2022.''.
    (b) Effective Date.--The amendment made by this section shall take
effect on the date of the enactment of this Act.

SEC. 1405. REPEAL OF EXCLUSION FOR QUALIFIED MOVING EXPENSE
              REIMBURSEMENT.

    (a) In General.--Section 132(a) is amended by striking paragraph
(6).
    (b) Conforming Amendments.--
            (1) Section 82 is amended by striking ``Except as provided
        in section 132(a)(6), there'' and inserting ``There''.
            (2) Section 132 is amended by striking subsection (g).
            (3) Section 132(l) is amended by striking by striking
        ``subsections (e) and (g)'' and inserting ``subsection (e)''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1406. REPEAL OF EXCLUSION FOR ADOPTION ASSISTANCE PROGRAMS.

    (a) In General.--Part III of subchapter B of chapter 1 is amended
by striking section 137 (and by striking the item relating to such
section in the table of sections for such part).
    (b) Conforming Amendments.--
            (1) Sections 414(n)(3)(C), 414(t)(2), 74(d)(2)(B),
        86(b)(2)(A), 219(g)(3)(A)(ii) are each amended by striking ``,
        137''.
            (2) Section 1016(a), as amended by the preceding provision
        of this Act, is amended by striking paragraph (26).
            (3) Section 6039D(d)(1), as amended by the preceding
        provisions of this Act, is amended--
                    (A) by striking ``, or 137'', and
                    (B) by inserting ``or'' before ``125''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

 Subtitle F--Simplification and Reform of Savings, Pensions, Retirement

SEC. 1501. REPEAL OF SPECIAL RULE PERMITTING RECHARACTERIZATION OF ROTH
              IRA CONTRIBUTIONS AS TRADITIONAL IRA CONTRIBUTIONS.

    (a) In General.--Section 408A(d) is amended by striking paragraph
(6) and by redesignating paragraph (7) as paragraph (6).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1502. REDUCTION IN MINIMUM AGE FOR ALLOWABLE IN-SERVICE
              DISTRIBUTIONS.

    (a) In General.--Section 401(a)(36) is amended by striking ``age
62'' and inserting ``age 59 \1/2\''.
    (b) Application to Governmental Section 457(b) Plans.--Clause (i)
of section 457(d)(1)(A) is amended by inserting ``(in the case of a
plan maintained by an employer described in subsection (e)(1)(A), age
59 \1/2\)'' before the comma at the end.
    (c) Effective Date.--The amendments made by this section shall
apply to plan years beginning after December 31, 2017.

SEC. 1503. MODIFICATION OF RULES GOVERNING HARDSHIP DISTRIBUTIONS.

    (a) In General.--Not later than 1 year after the date of the
enactment of this Act, the Secretary of the Treasury shall modify
Treasury Regulation section 1.401(k)-1(d)(3)(iv)(E) to--
            (1) delete the 6-month prohibition on contributions imposed
        by paragraph (2) thereof, and
            (2) make any other modifications necessary to carry out the
        purposes of section 401(k)(2)(B)(i)(IV) of the Internal Revenue
        Code of 1986.
    (b) Effective Date.--The revised regulations under this section
shall apply to plan years beginning after December 31, 2017.

SEC. 1504. MODIFICATION OF RULES RELATING TO HARDSHIP WITHDRAWALS FROM
              CASH OR DEFERRED ARRANGEMENTS.

    (a) In General.--Section 401(k) is amended by adding at the end the
following:
            ``(14) Special rules relating to hardship withdrawals.--For
        purposes of paragraph (2)(B)(i)(IV)--
                    ``(A) Amounts which may be withdrawn.--The
                following amounts may be distributed upon hardship of
                the employee:
                            ``(i) Contributions to a profit-sharing or
                        stock bonus plan to which section 402(e)(3)
                        applies.
                            ``(ii) Qualified nonelective contributions
                        (as defined in subsection (m)(4)(C)).
                            ``(iii) Qualified matching contributions
                        described in paragraph (3)(D)(ii)(I).
                            ``(iv) Earnings on any contributions
                        described in clause (i), (ii), or (iii).
                    ``(B) No requirement to take available loan.--A
                distribution shall not be treated as failing to be made
                upon the hardship of an employee solely because the
                employee does not take any available loan under the
                plan.".''.
    (b) Conforming Amendment.--Section 401(k)(2)(B)(i)(IV) is amended
to read as follows:
                                    ``(IV) subject to the provisions of
                                paragraph (14), upon hardship of the
                                employee, or".''.
    (c) Effective Date.--The amendments made by this section shall
apply to plan years beginning after December 31, 2017.

SEC. 1505. EXTENDED ROLLOVER PERIOD FOR THE ROLLOVER OF PLAN LOAN
              OFFSET AMOUNTS IN CERTAIN CASES.

    (a) In General.--Paragraph (3) of section 402(c) is amended by
adding at the end the following new subparagraph:
                    ``(C) Rollover of certain plan loan offset
                amounts.--
                            ``(i) In general.--In the case of a
                        qualified plan loan offset amount, paragraph
                        (1) shall not apply to any transfer of such
                        amount made after the due date (including
                        extensions) for filing the return of tax for
                        the taxable year in which such amount is
                        treated as distributed from a qualified
                        employer plan.
                            ``(ii) Qualified plan loan offset amount.--
                        For purposes of this subparagraph, the term
                        `qualified plan loan offset amount' means a
                        plan loan offset amount which is treated as
                        distributed from a qualified employer plan to a
                        participant or beneficiary solely by reason
                        of--
                                    ``(I) the termination of the
                                qualified employer plan, or
                                    ``(II) the failure to meet the
                                repayment terms of the loan from such
                                plan because of the separation from
                                service of the participant (whether due
                                to layoff, cessation of business,
                                termination of employment, or
                                otherwise).
                            ``(iii) Plan loan offset amount.--For
                        purposes of clause (ii), the term `plan loan
                        offset amount' means the amount by which the
                        participant's accrued benefit under the plan is
                        reduced in order to repay a loan from the plan.
                            ``(iv) Limitation.--This subparagraph shall
                        not apply to any plan loan offset amount unless
                        such plan loan offset amount relates to a loan
                        to which section 72(p)(1) does not apply by
                        reason of section 72(p)(2).
                            ``(v) Qualified employer plan.--For
                        purposes of this subsection, the term
                        `qualified employer plan' has the meaning given
                        such term by section 72(p)(4).''.
    (b) Conforming Amendment.--Subparagraph (A) of section 402(c)(3) is
amended by striking ``subparagraph (B)'' and inserting ``subparagraphs
(B) and (C)''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 1506. MODIFICATION OF NONDISCRIMINATION RULES TO PROTECT OLDER,
              LONGER SERVICE PARTICIPANTS.

    (a) In General.--Section 401 is amended--
            (1) by redesignating subsection (o) as subsection (p), and
            (2) by inserting after subsection (n) the following new
        subsection:
    ``(o) Special Rules for Applying Nondiscrimination Rules to Protect
Older, Longer Service and Grandfathered Participants.--
            ``(1) Testing of defined benefit plans with closed classes
        of participants.--
                    ``(A) Benefits, rights, or features provided to
                closed classes.--A defined benefit plan which provides
                benefits, rights, or features to a closed class of
                participants shall not fail to satisfy the requirements
                of subsection (a)(4) by reason of the composition of
                such closed class or the benefits, rights, or features
                provided to such closed class, if--
                            ``(i) for the plan year as of which the
                        class closes and the 2 succeeding plan years,
                        such benefits, rights, and features satisfy the
                        requirements of subsection (a)(4) (without
                        regard to this subparagraph but taking into
                        account the rules of subparagraph (I)),
                            ``(ii) after the date as of which the class
                        was closed, any plan amendment which modifies
                        the closed class or the benefits, rights, and
                        features provided to such closed class does not
                        discriminate significantly in favor of highly
                        compensated employees, and
                            ``(iii) the class was closed before April
                        5, 2017, or the plan is described in
                        subparagraph (C).
                    ``(B) Aggregate testing with defined contribution
                plans permitted on a benefits basis.--
                            ``(i) In general.--For purposes of
                        determining compliance with subsection (a)(4)
                        and section 410(b), a defined benefit plan
                        described in clause (iii) may be aggregated and
                        tested on a benefits basis with 1 or more
                        defined contribution plans, including with the
                        portion of 1 or more defined contribution plans
                        which--
                                    ``(I) provides matching
                                contributions (as defined in subsection
                                (m)(4)(A)),
                                    ``(II) provides annuity contracts
                                described in section 403(b) which are
                                purchased with matching contributions
                                or nonelective contributions, or
                                    ``(III) consists of an employee
                                stock ownership plan (within the
                                meaning of section 4975(e)(7)) or a tax
                                credit employee stock ownership plan
                                (within the meaning of section 409(a)).
                            ``(ii) Special rules for matching
                        contributions.--For purposes of clause (i), if
                        a defined benefit plan is aggregated with a
                        portion of a defined contribution plan
                        providing matching contributions--
                                    ``(I) such defined benefit plan
                                must also be aggregated with any
                                portion of such defined contribution
                                plan which provides elective deferrals
                                described in subparagraph (A) or (C) of
                                section 402(g)(3), and
                                    ``(II) such matching contributions
                                shall be treated in the same manner as
                                nonelective contributions, including
                                for purposes of applying the rules of
                                subsection (l).
                            ``(iii) Plans described.--A defined benefit
                        plan is described in this clause if--
                                    ``(I) the plan provides benefits to
                                a closed class of participants,
                                    ``(II) for the plan year as of
                                which the class closes and the 2
                                succeeding plan years, the plan
                                satisfies the requirements of section
                                410(b) and subsection (a)(4) (without
                                regard to this subparagraph but taking
                                into account the rules of subparagraph
                                (I)),
                                    ``(III) after the date as of which
                                the class was closed, any plan
                                amendment which modifies the closed
                                class or the benefits provided to such
                                closed class does not discriminate
                                significantly in favor of highly
                                compensated employees, and
                                    ``(IV) the class was closed before
                                April 5, 2017, or the plan is described
                                in subparagraph (C).
                    ``(C) Plans described.--A plan is described in this
                subparagraph if, taking into account any predecessor
                plan--
                            ``(i) such plan has been in effect for at
                        least 5 years as of the date the class is
                        closed, and
                            ``(ii) during the 5-year period preceding
                        the date the class is closed, there has not
                        been a substantial increase in the coverage or
                        value of the benefits, rights, or features
                        described in subparagraph (A) or in the
                        coverage or benefits under the plan described
                        in subparagraph (B)(iii) (whichever is
                        applicable).
                    ``(D) Determination of substantial increase for
                benefits, rights, and features.--In applying
                subparagraph (C)(ii) for purposes of subparagraph
                (A)(iii), a plan shall be treated as having had a
                substantial increase in coverage or value of the
                benefits, rights, or features described in subparagraph
                (A) during the applicable 5-year period only if, during
                such period--
                            ``(i) the number of participants covered by
                        such benefits, rights, or features on the date
                        such period ends is more than 50 percent
                        greater than the number of such participants on
                        the first day of the plan year in which such
                        period began, or
                            ``(ii) such benefits, rights, and features
                        have been modified by 1 or more plan amendments
                        in such a way that, as of the date the class is
                        closed, the value of such benefits, rights, and
                        features to the closed class as a whole is
                        substantially greater than the value as of the
                        first day of such 5-year period, solely as a
                        result of such amendments.
                    ``(E) Determination of substantial increase for
                aggregate testing on benefits basis.--In applying
                subparagraph (C)(ii) for purposes of subparagraph
                (B)(iii)(IV), a plan shall be treated as having had a
                substantial increase in coverage or benefits during the
                applicable 5-year period only if, during such period--
                            ``(i) the number of participants
                        benefitting under the plan on the date such
                        period ends is more than 50 percent greater
                        than the number of such participants on the
                        first day of the plan year in which such period
                        began, or
                            ``(ii) the average benefit provided to such
                        participants on the date such period ends is
                        more than 50 percent greater than the average
                        benefit provided on the first day of the plan
                        year in which such period began.
                    ``(F) Certain employees disregarded.--For purposes
                of subparagraphs (D) and (E), any increase in coverage
                or value or in coverage or benefits, whichever is
                applicable, which is attributable to such coverage and
                value or coverage and benefits provided to employees--
                            ``(i) who became participants as a result
                        of a merger, acquisition, or similar event
                        which occurred during the 7-year period
                        preceding the date the class is closed, or
                            ``(ii) who became participants by reason of
                        a merger of the plan with another plan which
                        had been in effect for at least 5 years as of
                        the date of the merger,
                shall be disregarded, except that clause (ii) shall
                apply for purposes of subparagraph (D) only if, under
                the merger, the benefits, rights, or features under 1
                plan are conformed to the benefits, rights, or features
                of the other plan prospectively.
                    ``(G) Rules relating to average benefit.--For
                purposes of subparagraph (E)--
                            ``(i) the average benefit provided to
                        participants under the plan will be treated as
                        having remained the same between the 2 dates
                        described in subparagraph (E)(ii) if the
                        benefit formula applicable to such participants
                        has not changed between such dates, and
                            ``(ii) if the benefit formula applicable to
                        1 or more participants under the plan has
                        changed between such 2 dates, then the average
                        benefit under the plan shall be considered to
                        have increased by more than 50 percent only
                        if--
                                    ``(I) the total amount determined
                                under section 430(b)(1)(A)(i) for all
                                participants benefitting under the plan
                                for the plan year in which the 5-year
                                period described in subparagraph (E)
                                ends, exceeds
                                    ``(II) the total amount determined
                                under section 430(b)(1)(A)(i) for all
                                such participants for such plan year,
                                by using the benefit formula in effect
                                for each such participant for the first
                                plan year in such 5-year period, by
                                more than 50 percent.
                        In the case of a CSEC plan (as defined in
                        section 414(y)), the normal cost of the plan
                        (as determined under section 433(j)(1)(B))
                        shall be used in lieu of the amount determined
                        under section 430(b)(1)(A)(i).
                    ``(H) Treatment as single plan.--For purposes of
                subparagraphs (E) and (G), a plan described in section
                413(c) shall be treated as a single plan rather than as
                separate plans maintained by each participating
                employer.
                    ``(I) Special rules.--For purposes of subparagraphs
                (A)(i) and (B)(iii)(II), the following rules shall
                apply:
                            ``(i) In applying section 410(b)(6)(C), the
                        closing of the class of participants shall not
                        be treated as a significant change in coverage
                        under section 410(b)(6)(C)(i)(II).
                            ``(ii) 2 or more plans shall not fail to be
                        eligible to be aggregated and treated as a
                        single plan solely by reason of having
                        different plan years.
                            ``(iii) Changes in the employee population
                        shall be disregarded to the extent attributable
                        to individuals who become employees or cease to
                        be employees, after the date the class is
                        closed, by reason of a merger, acquisition,
                        divestiture, or similar event.
                            ``(iv) Aggregation and all other testing
                        methodologies otherwise applicable under
                        subsection (a)(4) and section 410(b) may be
                        taken into account.
                The rule of clause (ii) shall also apply for purposes
                of determining whether plans to which subparagraph
                (B)(i) applies may be aggregated and treated as 1 plan
                for purposes of determining whether such plans meet the
                requirements of subsection (a)(4) and section 410(b).
                    ``(J) Spun-off plans.--For purposes of this
                paragraph, if a portion of a defined benefit plan
                described in subparagraph (A) or (B)(iii) is spun off
                to another employer and the spun-off plan continues to
                satisfy the requirements of--
                            ``(i) subparagraph (A)(i) or (B)(iii)(II),
                        whichever is applicable, if the original plan
                        was still within the 3-year period described in
                        such subparagraph at the time of the spin off,
                        and
                            ``(ii) subparagraph (A)(ii) or
                        (B)(iii)(III), whichever is applicable,
                the treatment under subparagraph (A) or (B) of the
                spun-off plan shall continue with respect to such other
                employer.
            ``(2) Testing of defined contribution plans.--
                    ``(A) Testing on a benefits basis.--A defined
                contribution plan shall be permitted to be tested on a
                benefits basis if--
                            ``(i) such defined contribution plan
                        provides make-whole contributions to a closed
                        class of participants whose accruals under a
                        defined benefit plan have been reduced or
                        eliminated,
                            ``(ii) for the plan year of the defined
                        contribution plan as of which the class
                        eligible to receive such make-whole
                        contributions closes and the 2 succeeding plan
                        years, such closed class of participants
                        satisfies the requirements of section
                        410(b)(2)(A)(i) (determined by applying the
                        rules of paragraph (1)(I)),
                            ``(iii) after the date as of which the
                        class was closed, any plan amendment to the
                        defined contribution plan which modifies the
                        closed class or the allocations, benefits,
                        rights, and features provided to such closed
                        class does not discriminate significantly in
                        favor of highly compensated employees, and
                            ``(iv) the class was closed before April 5,
                        2017, or the defined benefit plan under clause
                        (i) is described in paragraph (1)(C) (as
                        applied for purposes of paragraph
                        (1)(B)(iii)(IV)).
                    ``(B) Aggregation with plans including matching
                contributions.--
                            ``(i) In general.--With respect to 1 or
                        more defined contribution plans described in
                        subparagraph (A), for purposes of determining
                        compliance with subsection (a)(4) and section
                        410(b), the portion of such plans which
                        provides make-whole contributions or other
                        nonelective contributions may be aggregated and
                        tested on a benefits basis with the portion of
                        1 or more other defined contribution plans
                        which--
                                    ``(I) provides matching
                                contributions (as defined in subsection
                                (m)(4)(A)),
                                    ``(II) provides annuity contracts
                                described in section 403(b) which are
                                purchased with matching contributions
                                or nonelective contributions, or
                                    ``(III) consists of an employee
                                stock ownership plan (within the
                                meaning of section 4975(e)(7)) or a tax
                                credit employee stock ownership plan
                                (within the meaning of section 409(a)).
                            ``(ii) Special rules for matching
                        contributions.--Rules similar to the rules of
                        paragraph (1)(B)(ii) shall apply for purposes
                        of clause (i).
                    ``(C) Special rules for testing defined
                contribution plan features providing matching
                contributions to certain older, longer service
                participants.--In the case of a defined contribution
                plan which provides benefits, rights, or features to a
                closed class of participants whose accruals under a
                defined benefit plan have been reduced or eliminated,
                the plan shall not fail to satisfy the requirements of
                subsection (a)(4) solely by reason of the composition
                of the closed class or the benefits, rights, or
                features provided to such closed class if the defined
                contribution plan and defined benefit plan otherwise
                meet the requirements of subparagraph (A) but for the
                fact that the make-whole contributions under the
                defined contribution plan are made in whole or in part
                through matching contributions.
                    ``(D) Spun-off plans.--For purposes of this
                paragraph, if a portion of a defined contribution plan
                described in subparagraph (A) or (C) is spun off to
                another employer, the treatment under subparagraph (A)
                or (C) of the spun-off plan shall continue with respect
                to the other employer if such plan continues to comply
                with the requirements of clauses (ii) (if the original
                plan was still within the 3-year period described in
                such clause at the time of the spin off) and (iii) of
                subparagraph (A), as determined for purposes of
                subparagraph (A) or (C), whichever is applicable.
            ``(3) Definitions.--For purposes of this subsection--
                    ``(A) Make-whole contributions.--Except as
                otherwise provided in paragraph (2)(C), the term `make-
                whole contributions' means nonelective allocations for
                each employee in the class which are reasonably
                calculated, in a consistent manner, to replace some or
                all of the retirement benefits which the employee would
                have received under the defined benefit plan and any
                other plan or qualified cash or deferred arrangement
                under subsection (k)(2) if no change had been made to
                such defined benefit plan and such other plan or
                arrangement. For purposes of the preceding sentence,
                consistency shall not be required with respect to
                employees who were subject to different benefit
                formulas under the defined benefit plan.
                    ``(B) References to closed class of participants.--
                References to a closed class of participants and
                similar references to a closed class shall include
                arrangements under which 1 or more classes of
                participants are closed, except that 1 or more classes
                of participants closed on different dates shall not be
                aggregated for purposes of determining the date any
                such class was closed.
                    ``(C) Highly compensated employee.--The term
                `highly compensated employee' has the meaning given
                such term in section 414(q).".''.
    (b) Participation Requirements.--Paragraph (26) of section 401(a)
is amended by adding at the end the following new subparagraph:
                    ``(I) Protected participants.--
                            ``(i) In general.--A plan shall be deemed
                        to satisfy the requirements of subparagraph (A)
                        if--
                                    ``(I) the plan is amended--
                                            ``(aa) to cease all benefit
                                        accruals, or
                                            ``(bb) to provide future
                                        benefit accruals only to a
                                        closed class of participants,
                                    ``(II) the plan satisfies
                                subparagraph (A) (without regard to
                                this subparagraph) as of the effective
                                date of the amendment, and
                                    ``(III) the amendment was adopted
                                before April 5, 2017, or the plan is
                                described in clause (ii).
                            ``(ii) Plans described.--A plan is
                        described in this clause if the plan would be
                        described in subsection (o)(1)(C), as applied
                        for purposes of subsection (o)(1)(B)(iii)(IV)
                        and by treating the effective date of the
                        amendment as the date the class was closed for
                        purposes of subsection (o)(1)(C).
                            ``(iii) Special rules.--For purposes of
                        clause (i)(II), in applying section
                        410(b)(6)(C), the amendments described in
                        clause (i) shall not be treated as a
                        significant change in coverage under section
                        410(b)(6)(C)(i)(II).
                            ``(iv) Spun-off plans.--For purposes of
                        this subparagraph, if a portion of a plan
                        described in clause (i) is spun off to another
                        employer, the treatment under clause (i) of the
                        spun-off plan shall continue with respect to
                        the other employer.''.
    (c) Effective Date.--
            (1) In general.--Except as provided in paragraph (2), the
        amendments made by this section shall take effect on the date
        of the enactment of this Act, without regard to whether any
        plan modifications referred to in such amendments are adopted
        or effective before, on, or after such date of enactment.
            (2) Special rules.--
                    (A) Election of earlier application.--At the
                election of the plan sponsor, the amendments made by
                this section shall apply to plan years beginning after
                December 31, 2013.
                    (B) Closed classes of participants.--For purposes
                of paragraphs (1)(A)(iii), (1)(B)(iii)(IV), and
                (2)(A)(iv) of section 401(o) of the Internal Revenue
                Code of 1986 (as added by this section), a closed class
                of participants shall be treated as being closed before
                April 5, 2017, if the plan sponsor's intention to
                create such closed class is reflected in formal written
                documents and communicated to participants before such
                date.
                    (C) Certain post-enactment plan amendments.--A plan
                shall not be treated as failing to be eligible for the
                application of section 401(o)(1)(A), 401(o)(1)(B)(iii),
                or 401(a)(26) of such Code (as added by this section)
                to such plan solely because in the case of--
                            (i) such section 401(o)(1)(A), the plan was
                        amended before the date of the enactment of
                        this Act to eliminate 1 or more benefits,
                        rights, or features, and is further amended
                        after such date of enactment to provide such
                        previously eliminated benefits, rights, or
                        features to a closed class of participants, or
                            (ii) such section 401(o)(1)(B)(iii) or
                        section 401(a)(26), the plan was amended before
                        the date of the enactment of this Act to cease
                        all benefit accruals, and is further amended
                        after such date of enactment to provide benefit
                        accruals to a closed class of participants. Any
                        such section shall only apply if the plan
                        otherwise meets the requirements of such
                        section and in applying such section, the date
                        the class of participants is closed shall be
                        the effective date of the later amendment.

    Subtitle G--Estate, Gift, and Generation-skipping Transfer Taxes

SEC. 1601. INCREASE IN CREDIT AGAINST ESTATE, GIFT, AND GENERATION-
              SKIPPING TRANSFER TAX.

    (a) In General.--Section 2010(c)(3) is amended by striking
``$5,000,000'' and inserting ``$10,000,000''.
    (b) Effective Date.--The amendments made by this section shall
apply to estates of decedents dying, generation-skipping transfers, and
gifts made, after December 31, 2017.

SEC. 1602. REPEAL OF ESTATE AND GENERATION-SKIPPING TRANSFER TAXES.

    (a) Estate Tax Repeal.--
            (1) In general.--Subchapter C of chapter 11 is amended by
        adding at the end the following new section:

``SEC. 2210. TERMINATION.

    ``(a) In General.--Except as provided in subsection (b), this
chapter shall not apply to the estates of decedents dying after
December 31, 2024.
    ``(b) Certain Distributions From Qualified Domestic Trusts.--In
applying section 2056A with respect to the surviving spouse of a
decedent dying on or before December 31, 2024--
            ``(1) section 2056A(b)(1)(A) shall not apply to
        distributions made after the 10-year period beginning on such
        date, and
            ``(2) section 2056A(b)(1)(B) shall not apply after such
        date.''.
            (2) Conforming amendments.--Section 1014(b) is amended--
                    (A) in paragraph (6), by striking ``was includible
                in determining'' and all that follows through the end
                and inserting ``was includible (or would have been
                includible without regard to section 2210) in
                determining the value of the decedent's gross estate
                under chapter 11 of subtitle B'' ,
                    (B) in paragraph (9), by striking ``required to be
                included'' through ``Code of 1939'' and inserting
                ``required to be included (or would have been required
                to be included without regard to section 2210) in
                determining the value of the decedent's gross estate
                under chapter 11 of subtitle B'', and
                    (C) in paragraph (10), by striking ``Property
                includible in the gross estate'' and inserting
                ``Property includible (or which would have been
                includible without regard to section 2210) in the gross
                estate''.
            (3) Clerical amendment.--The table of sections for
        subchapter C of chapter 11 is amended by adding at the end the
        following new item:

``Sec. 2210. Termination.''.
    (b) Generation-skipping Transfer Tax Repeal.--
            (1) In general.--Subchapter G of chapter 13 of subtitle B
        of such Code is amended by adding at the end the following new
        section:

``SEC. 2664. TERMINATION.

    ``This chapter shall not apply to generation-skipping transfers
after December 31, 2024.''.
            (2) Clerical amendment.--The table of sections for
        subchapter G of chapter 13 of such Code is amended by adding at
        the end the following new item:

``Sec. 2664. Termination.''.
    (c) Conforming Amendments Related to Gift Tax.--
            (1) Computation of gift tax.--Section 2502 is amended by
        adding at the end the following new subsection:
    ``(d) Gifts Made After 2024.--
            ``(1) In general.--In the case of a gift made after
        December 31, 2024, subsection (a) shall be applied by
        substituting `subsection (d)(2)' for `section 2001(c)' and
        `such subsection' for `such section'.
            ``(2) Rate schedule.--


``If the amount with respect to which    The tentative tax is:
 the tentative tax to be computed is:.
Not over $10,000.......................  18% of such amount.
Over $10,000 but not over $20,000......  $1,800, plus 20% of the excess
                                          over $10,000.
Over $20,000 but not over $40,000......  $3,800, plus 22% of the excess
                                          over $20,000.
Over $40,000 but not over $60,000......  $8,200, plus 24% of the excess
                                          over $40,000.
Over $60,000 but not over $80,000......  $13,000, plus 26% of the excess
                                          over $60,000.
Over $80,000 but not over $100,000.....  $18,200, plus 28% of the excess
                                          over $80,000.
Over $100,000 but not over $150,000....  $23,800, plus 30% of the excess
                                          over $100,000.
Over $150,000 but not over $250,000....  $38,800, plus 32% of the excess
                                          of $150,000.
Over $250,000 but not over $500,000....  $70,800, plus 34% of the excess
                                          over $250,000.
Over $500,000..........................  $155,800, plus 35% of the
                                          excess of $500,000.''.


            (2) Lifetime gift exemption.--Section 2505 is amended by
        adding at the end the following new subsection:
    ``(d) Gifts Made After 2024.--
            ``(1) In general.--In the case of a gift made after
        December 31, 2024, subsection (a)(1) shall be applied by
        substituting `the amount of the tentative tax which would be
        determined under the rate schedule set forth in section
        2502(a)(2) if the amount with respect to which such tentative
        tax is to be computed were $10,000,000' for `the applicable
        credit amount in effect under section 2010(c) which would apply
        if the donor died as of the end of the calendar year'.
            ``(2) Inflation adjustment.--
                    ``(A) In general.--In the case of any calendar year
                after 2024, the dollar amount in subsection (a)(1)
                (after application of this subsection) shall be
                increased by an amount equal to--
                            ``(i) such dollar amount, multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under section 1(c)(2)(A) of such
                        calendar year by substituting `calendar year
                        2011' for `calendar year 2016' in clause (ii)
                        thereof.
                    ``(B) Rounding.--If any amount as adjusted under
                paragraph (1) is not a multiple of $10,000, such amount
                shall be rounded to the nearest multiple of $10,000.''.
            (3) Other conforming amendments related to gift tax.--
        Section 2801 is amended by adding at the end the following new
        subsection:
    ``(g) Gifts Received After 2024.--In the case of a gift received
after December 31, 2024, subsection (a)(1) shall be applied by
substituting `section 2502(a)(2)' for `section 2001(c) as in effect on
the date of such receipt'.''.
    (d) Effective Date.--The amendments made by this section shall
apply to estates of decedents dying, generation-skipping transfers, and
gifts made, after December 31, 2024.

                TITLE II--ALTERNATIVE MINIMUM TAX REPEAL

SEC. 2001. REPEAL OF ALTERNATIVE MINIMUM TAX.

    (a) In General.--Subchapter A of chapter 1 is amended by striking
part VI (and by striking the item relating to such part in the table of
parts for subchapter A).
    (b) Credit for Prior Year Minimum Tax Liability.--
            (1) Limitation.--Subsection (c) of section 53 is amended to
        read as follows:
    ``(c) Limitation.--The credit allowable under subsection (a) shall
not exceed the regular tax liability of the taxpayer reduced by the sum
of the credits allowed under subparts A, B, and D.''.
            (2) Credits treated as refundable.--Section 53 is amended
        by adding at the end the following new subsection:
    ``(e) Portion of Credit Treated as Refundable.--
            ``(1) In general.--In the case of any taxable year
        beginning in 2019, 2020, 2021, or 2022, the limitation under
        subsection (c) shall be increased by the AMT refundable credit
        amount for such year.
            ``(2) AMT refundable credit amount.--For purposes of
        paragraph (1), the AMT refundable credit amount is an amount
        equal to 50 percent (100 percent in the case of a taxable year
        beginning in 2022) of the excess (if any) of--
                    ``(A) the minimum tax credit determined under
                subsection (b) for the taxable year, over
                    ``(B) the minimum tax credit allowed under
                subsection (a) for such year (before the application of
                this subsection for such year).
            ``(3) Credit refundable.--For purposes of this title (other
        than this section), the credit allowed by reason of this
        subsection shall be treated as a credit allowed under subpart C
        (and not this subpart).
            ``(4) Short taxable years.--In the case of any taxable year
        of less than 365 days, the AMT refundable credit amount
        determined under paragraph (2) with respect to such taxable
        year shall be the amount which bears the same ratio to such
        amount determined without regard to this paragraph as the
        number of days in such taxable year bears to 365.''.
            (3) Treatment of references.--Section 53(d) is amended by
        adding at the end the following new paragraph:
            ``(3) AMT term references.--Any references in this
        subsection to section 55, 56, or 57 shall be treated as a
        reference to such section as in effect before its repeal by the
        Tax Cuts and Jobs Act.''.
    (c) Conforming Amendments Related to AMT Repeal.--
            (1) Section 2(d) is amended by striking ``sections 1 and
        55'' and inserting ``section 1''.
            (2) Section 5(a) is amended by striking paragraph (4).
            (3) Section 11(d) is amended by striking ``the taxes
        imposed by subsection (a) and section 55'' and inserting ``the
        tax imposed by subsection (a)''.
            (4) Section 12 is amended by striking paragraph (7).
            (5) Section 26(a) is amended to read as follows:
    ``(a) Limitation Based on Amount of Tax.--The aggregate amount of
credits allowed by this subpart for the taxable year shall not exceed
the taxpayer's regular tax liability for the taxable year.''.
            (6) Section 26(b)(2) is amended by striking subparagraph
        (A).
            (7) Section 26 is amended by striking subsection (c).
            (8) Section 38(c) is amended--
                    (A) by striking paragraphs (1) through (5),
                    (B) by redesignating paragraph (6) as paragraph
                (2),
                    (C) by inserting before paragraph (2) (as so
                redesignated) the following new paragraph:
            ``(1) In general.--The credit allowed under subsection (a)
        for any taxable year shall not exceed the excess (if any) of--
                    ``(A) the sum of--
                            ``(i) so much of the regular tax liability
                        as does not exceed $25,000, plus
                            ``(ii) 75 percent of so much of the regular
                        tax liability as exceeds $25,000, over
                    ``(B) the sum of the credits allowable under
                subparts A and B of this part.'', and
                    (D) by striking ``subparagraph (B) of paragraph
                (1)'' each place it appears in paragraph (2) (as so
                redesignated) and inserting ``clauses (i) and (ii) of
                paragraph (1)(A)''.
            (9) Section 39(a) is amended--
                    (A) by striking ``or the eligible small business
                credits'' in paragraph (3)(A), and
                    (B) by striking paragraph (4).
            (10) Section 45D(g)(4)(B) is amended by striking ``or for
        purposes of section 55''.
            (11) Section 54(c)(1) is amended to read as follows:
            ``(1) regular tax liability (as defined in section 26(b)),
        over''.
            (12) Section 54A(c)(1)(A) is amended to read as follows:
                    ``(A) regular tax liability (as defined in section
                26(b)), over''.
            (13) Section 148(b)(3) is amended to read as follows:
            ``(3) Tax-exempt bonds not treated as investment
        property.--The term `investment property' does not include any
        tax-exempt bond.''.
            (14) Section 168(k)(2) is amended by striking subparagraph
        (G).
            (15) Section 168(k) is amended by striking paragraph (4).
            (16) Section 168(k)(5) is amended by striking subparagraph
        (E).
            (17) Section 168(m)(2)(B)(i) is amended by striking
        ``(determined without regard to paragraph (4) thereof)''.
            (18) Section 168(m)(2) is amended by striking subparagraph
        (D).
            (19) Section 173 is amended by striking subsection (b).
            (20) Section 263(c) is amended by striking ``section 59(e)
        or 291'' and inserting ``section 291''.
            (21) Section 263A(c) is amended by striking paragraph (6)
        and by redesignating paragraph (7) (as amended) as paragraph
        (6).
            (22) Section 382(l) is amended by striking paragraph (7)
        and by redesignating paragraph (8) as paragraph (7).
            (23) Section 443 is amended by striking subsection (d) and
        by redesignating subsection (e) as subsection (d).
            (24) Section 616 is amended by striking subsection (e).
            (25) Section 617 is amended by striking subsection (i).
            (26) Section 641(c) is amended--
                    (A) in paragraph (2) by striking subparagraph (B)
                and by redesignating subparagraphs (C) and (D) as
                subparagraphs (B) and (C), respectively, and
                    (B) in paragraph (3), by striking ``paragraph
                (2)(C)'' and inserting ``paragraph (2)(B)''.
            (27) Subsections (b) and (c) of section 666 are each
        amended by striking ``(other than the tax imposed by section
        55)''.
            (28) Section 848 is amended by striking subsection (i).
            (29) Section 860E(a) is amended by striking paragraph (4).
            (30) Section 871(b)(1) is amended by striking ``or 55''.
            (31) Section 882(a)(1) is amended by striking ``55,''.
            (32) Section 897(a) is amended to read as follows:
    ``(a) Treatment as Effectively Connected With United States Trade
or Business.--For purposes of this title, gain or loss of a nonresident
alien individual or a foreign corporation from the disposition of a
United States real property interest shall be taken into account--
            ``(1) in the case of a nonresident alien individual, under
        section 871(b)(1), or
            ``(2) in the case of a foreign corporation, under section
        882(a)(1),
as if the taxpayer were engaged in a trade or business within the
United States during the taxable year and as if such gain or loss were
effectively connected with such trade or business.''.
            (33) Section 904(k) is amended to read as follows:
    ``(k) Cross Reference.--For increase of limitation under subsection
(a) for taxes paid with respect to amounts received which were included
in the gross income of the taxpayer for a prior taxable year as a
United States shareholder with respect to a controlled foreign
corporation, see section 960(b).''.
            (34) Section 911(f) is amended to read as follows:
    ``(f) Determination of Tax Liability.--
            ``(1) In general.--If, for any taxable year, any amount is
        excluded from gross income of a taxpayer under subsection (a),
        then, notwithstanding section 1, if such taxpayer has taxable
        income for such taxable year, the tax imposed by section 1 for
        such taxable year shall be equal to the excess (if any) of--
                    ``(A) the tax which would be imposed by section 1
                for such taxable year if the taxpayer's taxable income
                were increased by the amount excluded under subsection
                (a) for such taxable year, over
                    ``(B) the tax which would be imposed by section 1
                for such taxable year if the taxpayer's taxable income
                were equal to the amount excluded under subsection (a)
                for such taxable year.
        For purposes of this paragraph, the amount excluded under
        subsection (a) shall be reduced by the aggregate amount of any
        deductions or exclusions disallowed under subsection (d)(6)
        with respect to such excluded amount.
            ``(2) Treatment of capital gain excess.--
                    ``(A) In general.--In applying section 1(h) for
                purposes of determining the tax under paragraph (1)(A)
                for any taxable year in which, without regard to this
                subsection, the taxpayer's net capital gain exceeds
                taxable income (hereafter in this subparagraph referred
                to as the capital gain excess)--
                            ``(i) the taxpayer's net capital gain
                        (determined without regard to section 1(h)(11))
                        shall be reduced (but not below zero) by such
                        capital gain excess,
                            ``(ii) the taxpayer's qualified dividend
                        income shall be reduced by so much of such
                        capital gain excess as exceeds the taxpayer's
                        net capital gain (determined without regard to
                        section 1(h)(11) and the reduction under clause
                        (i)), and
                            ``(iii) adjusted net capital gain,
                        unrecaptured section 1250 gain, and 28-percent
                        rate gain shall each be determined after
                        increasing the amount described in section
                        1(h)(4)(B) by such capital gain excess.
                    ``(B) Definitions.--Terms used in this paragraph
                which are also used in section 1(h) shall have the
                respective meanings given such terms by section
                1(h).''.
            (35) Section 962(a)(1) is amended--
                    (A) by striking ``sections 1 and 55'' and inserting
                ``section 1'', and
                    (B) by striking ``sections 11 and 55'' and
                inserting ``section 11''.
            (36) Section 1016(a) is amended by striking paragraph (20).
            (37) Section 1202(a)(4) is amended by inserting ``and'' at
        the end of subparagraph (A), by striking ``, and'' and
        inserting a period at the end of subparagraph (B), and by
        striking subparagraph (C).
            (38) Section 1374(b)(3)(B) is amended by striking the last
        sentence thereof.
            (39) Section 1561(a) is amended--
                    (A) by inserting ``and'' at the end of paragraph
                (1), by striking ``, and'' at the end of paragraph (2)
                and inserting a period, and by striking paragraph (3),
                and
                    (B) by striking the last sentence.
            (40) Section 6015(d)(2)(B) is amended by striking ``or
        55''.
            (41) Section 6211(b)(4)(A) is amended by striking``,
        168(k)(4)''.
            (42) Section 6425(c)(1)(A) is amended to read as follows:
                    ``(A) the tax imposed under section 11 or
                subchapter L of chapter 1, whichever is applicable,
                over''.
            (43) Section 6654(d)(2) is amended--
                    (A) in clause (i) of subparagraph (B), by striking
                ``, alternative minimum taxable income,'', and
                    (B) in clause (i) of subparagraph (C), by striking
                ``, alternative minimum taxable income,''.
            (44) Section 6655(e)(2)(B)(i) is amended by striking ``The
        taxable income and alternative minimum taxable income shall''
        and inserting ``Taxable income shall''.
            (45) Section 6655(g)(1)(A) is amended by adding ``plus'' at
        the end of clause (i), by striking clause (ii), and by
        redesignating clause (iii) as clause (ii).
            (46) Section 6662(e)(3)(C) is amended by striking ``the
        regular tax (as defined in section 55(c))'' and inserting ``the
        regular tax liability (as defined in section 26(b))''.
    (d) Effective Dates.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        taxable years beginning after December 31, 2017.
            (2) Prior elections with respect to certain tax
        preferences.--So much of the amendment made by subsection (a)
        as relates to the repeal of section 59(e) of the Internal
        Revenue Code of 1986 shall apply to amounts paid or incurred
        after December 31, 2017.
            (3) Treatment of net operating loss carrybacks.--For
        purposes of section 56(d) of the Internal Revenue Code of 1986
        (as in effect before its repeal), the amount of any net
        operating loss which may be carried back from a taxable year
        beginning after December 31, 2017, to taxable years beginning
        before January 1, 2018, shall be determined without regard to
        any adjustments under section 56(d)(2)(A) of such Code (as so
        in effect).

                     TITLE III--BUSINESS TAX REFORM

                         Subtitle A--Tax Rates

SEC. 3001. REDUCTION IN CORPORATE TAX RATE.

    (a) In General.--Section 11(b) is amended to read as follows:
    ``(b) Amount of Tax.--
            ``(1) In general.--Except as otherwise provided in this
        subsection, the amount of the tax imposed by subsection (a)
        shall be 20 percent of taxable income.
            ``(2) Special rule for personal service corporations.--
                    ``(A) In general.--In the case of a personal
                service corporation (as defined in section 448(d)(2)),
                the amount of the tax imposed by subsection (a) shall
                be 25 percent of taxable income.
                    ``(B) References to corporate rate.--Any reference
                to the rate imposed under this section or to the
                highest rate in effect under this section (or any
                similar reference) shall be determined without regard
                to the rate imposed with respect to personal service
                corporations (as so defined).''.
    (b) Conforming Amendments.--
            (1)(A) Part I of subchapter P of chapter 1 is amended by
        striking section 1201 (and by striking the item relating to
        such section in the table of sections for such part).
            (B) Section 12 is amended by striking paragraph (4).
            (C) Section 527(b) is amended--
                    (i) by striking paragraph (2), and
                    (ii) by striking all that precedes ``is hereby
                imposed'' and inserting:
    ``(b) Tax Imposed.--A tax''.
            (D) Section 594(a) is amended by striking ``taxes imposed
        by section 11 or 1201(a)'' and inserting ``tax imposed by
        section 11''.
            (E) Section 691(c)(4) is amended by striking ``1201,''.
            (F) Section 801(a) is amended--
                    (i) by striking paragraph (2), and
                    (ii) by striking all that precedes ``is hereby
                imposed'' and inserting:
    ``(a) Tax Imposed.--A tax''.
            (G) Section 831(e) is amended by striking paragraph (1) and
        by redesignating paragraphs (2) and (3) as paragraphs (1) and
        (2), respectively.
            (H) Sections 832(c)(5) and 834(b)(1)(D) are each amended by
        striking ``sec. 1201 and following,''.
            (I) Section 852(b)(3)(A) is amended by striking ``section
        1201(a)'' and inserting ``section 11(b)(1)''.
            (J) Section 857(b)(3) is amended--
                    (i) by striking subparagraph (A) and redesignating
                subparagraphs (B) through (F) as subparagraphs (A)
                through (E), respectively,
                    (ii) in subparagraph (C), as so redesignated--
                            (I) by striking ``subparagraph (A)(ii)'' in
                        clause (i) thereof and inserting ``paragraph
                        (1)'',
                            (II) by striking ``the tax imposed by
                        subparagraph (A)(ii)'' in clauses (ii) and (iv)
                        thereof and inserting ``the tax imposed by
                        paragraph (1) on undistributed capital gain'',
                    (iii) in subparagraph (E), as so redesignated, by
                striking ``subparagraph (B) or (D)'' and inserting
                ``subparagraph (A) or (C)'', and
                    (iv) by adding at the end the following new
                subparagraph:
                    ``(F) Undistributed capital gain.--For purposes of
                this paragraph, the term `undistributed capital gain'
                means the excess of the net capital gain over the
                deduction for dividends paid (as defined in section
                561) determined with reference to capital gain
                dividends only.''.
            (K) Section 882(a)(1) is amended by striking ``, or
        1201(a)''.
            (L) Section 1374(b) is amended by striking paragraph (4).
            (M) Section 1381(b) is amended by striking ``taxes imposed
        by section 11 or 1201'' and inserting ``tax imposed by section
        11''.
            (N) Section 6655(g)(1)(A)(i) is amended by striking ``or
        1201(a),''.
            (O) Section 7518(g)(6)(A) is amended by striking ``or
        1201(a)''.
            (2) Section 1445(e)(1) is amended by striking ``35 percent
        (or, to the extent provided in regulations, 20 percent)'' and
        inserting ``20 percent''.
            (3) Section 1445(e)(2) is amended by striking ``35
        percent'' and inserting ``20 percent''.
            (4) Section 1445(e)(6) is amended by striking ``35 percent
        (or, to the extent provided in regulations, 20 percent)'' and
        inserting ``20 percent''.
            (5)(A) Part I of subchapter B of chapter 5 is amended by
        striking section 1551 (and by striking the item relating to
        such section in the table of sections for such part).
            (B) Section 12 is amended by striking paragraph (6).
            (C) Section 535(c)(5) is amended to read as follows:
            ``(5) Cross reference.--For limitation on credit provided
        in paragraph (2) or (3) in the case of certain controlled
        corporations, see section 1561.''.
            (6)(A) Section 1561, as amended by the preceding provisions
        of this Act, is amended to read as follows:

``SEC. 1561. LIMITATION ON ACCUMULATED EARNINGS CREDIT IN THE CASE OF
              CERTAIN CONTROLLED CORPORATIONS.

    ``(a) In General.--The component members of a controlled group of
corporations on a December 31 shall, for their taxable years which
include such December 31, be limited for purposes of this subtitle to
one $250,000 ($150,000 if any component member is a corporation
described in section 535(c)(2)(B)) amount for purposes of computing the
accumulated earnings credit under section 535(c)(2) and (3). Such
amount shall be divided equally among the component members of such
group on such December 31 unless the Secretary prescribes regulations
permitting an unequal allocation of such amount.
    ``(b) Certain Short Taxable Years.--If a corporation has a short
taxable year which does not include a December 31 and is a component
member of a controlled group of corporations with respect to such
taxable year, then for purposes of this subtitle, the amount to be used
in computing the accumulated earnings credit under section 535(c)(2)
and (3) of such corporation for such taxable year shall be the amount
specified in subsection (a) with respect to such group, divided by the
number of corporations which are component members of such group on the
last day of such taxable year. For purposes of the preceding sentence,
section 1563(b) shall be applied as if such last day were substituted
for December 31.''.
            (B) The table of sections for part II of subchapter B of
        chapter 5 is amended by striking the item relating to section
        1561 and inserting the following new item:

``Sec. 1561. Limitation on accumulated earnings credit in the case of
                            certain controlled corporations.''.
            (7) Section 7518(g)(6)(A) is amended--
                    (A) by striking ``With respect to the portion'' and
                inserting ``In the case of a taxpayer other than a
                corporation, with respect to the portion'', and
                    (B) by striking ``(34 percent in the case of a
                corporation)''.
    (c) Reduction in Dividend Received Deductions to Reflect Lower
Corporate Income Tax Rates.--
            (1) Dividends received by corporations.--
                    (A) In general.--Section 243(a)(1) is amended by
                striking ``70 percent'' and inserting ``50 percent''.
                    (B) Dividends from 20-percent owned corporations.--
                Section 243(c)(1) is amended--
                            (i) by striking ``80 percent'' and
                        inserting ``65 percent'', and
                            (ii) by striking ``70 percent'' and
                        inserting ``50 percent''.
                    (C) Conforming amendment.--The heading for section
                243(c) is amended by striking ``Retention of 80-percent
                Dividend Received Deduction'' and inserting ``Increased
                Percentage''.
            (2) Dividends received from fsc.--Section 245(c)(1)(B) is
        amended--
                    (A) by striking ``70 percent'' and inserting ``50
                percent'', and
                    (B) by striking ``80 percent'' and inserting ``65
                percent''.
            (3) Limitation on aggregate amount of deductions.--Section
        246(b)(3) is amended--
                    (A) by striking ``80 percent'' in subparagraph (A)
                and inserting ``65 percent'', and
                    (B) by striking ``70 percent'' in subparagraph (B)
                and inserting ``50 percent''.
            (4) Reduction in deduction where portfolio stock is debt-
        financed.--Section 246A(a)(1) is amended--
                    (A) by striking ``70 percent'' and inserting ``50
                percent'', and
                    (B) by striking ``80 percent'' and inserting ``65
                percent''.
            (5) Income from sources within the united states.--Section
        861(a)(2) is amended--
                    (A) by striking ``100/70th'' and inserting ``100/
                50th'' in subparagraph (B), and
                    (B) in the flush sentence at the end--
                            (i) by striking ``100/80th'' and inserting
                        ``100/65th'', and
                            (ii) by striking ``100/70th'' and inserting
                        ``100/50th''.
    (d) Effective Date.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        taxable years beginning after December 31, 2017.
            (2) Certain conforming amendments.--The amendments made by
        paragraphs (2), (3), and (4) of subsection (b) shall apply to
        distributions after December 31, 2017.
    (e) Normalization Requirements.--
            (1) In general.--A normalization method of accounting shall
        not be treated as being used with respect to any public utility
        property for purposes of section 167 or 168 of the Internal
        Revenue Code of 1986 if the taxpayer, in computing its cost of
        service for ratemaking purposes and reflecting operating
        results in its regulated books of account, reduces the excess
        tax reserve more rapidly or to a greater extent than such
        reserve would be reduced under the average rate assumption
        method.
            (2) Alternative method for certain taxpayers.--If, as of
        the first day of the taxable year that includes the date of
        enactment of this Act--
                    (A) the taxpayer was required by a regulatory
                agency to compute depreciation for public utility
                property on the basis of an average life or composite
                rate method, and
                    (B) the taxpayer's books and underlying records did
                not contain the vintage account data necessary to apply
                the average rate assumption method,
        the taxpayer will be treated as using a normalization method of
        accounting if, with respect to such jurisdiction, the taxpayer
        uses the alternative method for public utility property that is
        subject to the regulatory authority of that jurisdiction.
            (3) Definitions.--For purposes of this subsection--
                    (A) Excess tax reserve.--The term ``excess tax
                reserve'' means the excess of--
                            (i) the reserve for deferred taxes (as
                        described in section 168(i)(9)(A)(ii) of the
                        Internal Revenue Code of 1986 as in effect on
                        the day before the date of the enactment of
                        this Act), over
                            (ii) the amount which would be the balance
                        in such reserve if the amount of such reserve
                        were determined by assuming that the corporate
                        rate reductions provided in this Act were in
                        effect for all prior periods.
                    (B) Average rate assumption method.--The average
                rate assumption method is the method under which the
                excess in the reserve for deferred taxes is reduced
                over the remaining lives of the property as used in its
                regulated books of account which gave rise to the
                reserve for deferred taxes. Under such method, if
                timing differences for the property reverse, the amount
                of the adjustment to the reserve for the deferred taxes
                is calculated by multiplying--
                            (i) the ratio of the aggregate deferred
                        taxes for the property to the aggregate timing
                        differences for the property as of the
                        beginning of the period in question, by
                            (ii) the amount of the timing differences
                        which reverse during such period.
                    (C) Alternative method.--The ``alternative method''
                is the method in which the taxpayer--
                            (i) computes the excess tax reserve on all
                        public utility property included in the plant
                        account on the basis of the weighted average
                        life or composite rate used to compute
                        depreciation for regulatory purposes, and
                            (ii) reduces the excess tax reserve ratably
                        over the remaining regulatory life of the
                        property.
            (4) Tax increased for normalization violation.--If, for any
        taxable year ending after the date of the enactment of this
        Act, the taxpayer does not use a normalization method of
        accounting, the taxpayer's tax for the taxable year shall be
        increased by the amount by which it reduces its excess tax
        reserve more rapidly than permitted under a normalization
        method of accounting.

                       Subtitle B--Cost Recovery

SEC. 3101. INCREASED EXPENSING.

    (a) 100 Percent Expensing.--Section 168(k)(1)(A) is amended by
striking ``50 percent'' and inserting ``100 percent''.
    (b) Extension Through January 1, 2023.--Section 168(k)(2) is
amended--
            (1) in subparagraph (A)(iii), by striking ``January 1,
        2020'' and inserting ``January 1, 2023'',
            (2) in subparagraph (B)(i)(II), by striking ``January 1,
        2021'' and inserting ``January 1, 2024'',
            (3) in subparagraph (B)(i)(III), by striking ``January 1,
        2020'' and inserting ``January 1, 2023'',
            (4) in subparagraph (B)(ii), by striking ``January 1,
        2020'' in each place it appears and inserting ``January 1,
        2023'', and
            (5) in subparagraph (E)(i), by striking ``January 1, 2020''
        and replacing it with ``January 1, 2023''.
    (c) Application to Used Property.--
            (1) In general.--Section 168(k)(2)(A)(ii) is amended to
        read as follows:
                            ``(ii) the original use of which begins
                        with the taxpayer or the acquisition of which
                        by the taxpayer meets the requirements of
                        clause (ii) of subparagraph (E), and''.
            (2) Acquisition requirements.--Section 168(k)(2)(E)(ii) is
        amended to read as follows:
                            ``(ii) Acquisition requirements.--An
                        acquisition of property meets the requirements
                        of this clause if--
                                    ``(I) such property was not used by
                                the taxpayer at any time prior to such
                                acquisition, and
                                    ``(II) the acquisition of such
                                property meets the requirements of
                                paragraphs (2)(A), (2)(B), (2)(C), and
                                (3) of section 179(d).'',
            (3) Anti-abuse rules.--Section 168(k)(2)(E) is further
        amended by amending clause (iii)(I) to read as follows:
                                    ``(I) property is used by a lessor
                                of such property and such use is the
                                lessor's first use of such property,''.
    (d) Exception for Certain Trades and Businesses Not Subject to
Limitation on Interest Expense.--Section 168(k)(2), as amended by
section 2001, is amended by inserting after subparagraph (F) the
following new subparagraph:
                    ``(G) Exception for property of certain businesses
                not subject to limitation on interest expense.--The
                term `qualified property' shall not include any
                property used in--
                            ``(i) a trade or business described in
                        subparagraph (B) or (C) of section 163(j)(7),
                        or
                            ``(ii) a trade or business that has had
                        floor plan financing indebtedness (as defined
                        in paragraph (9) of section 163(j)), if the
                        floor plan financing interest related to such
                        indebtedness was taken into account under
                        paragraph (1)(C) of such section.''.
    (e) Coordination With Section 280F.--Section 168(k)(2)(F) is
amended--
            (1) by striking ``$8,000'' in clauses (i) and (iii) and
        inserting ``$16,000'', and
            (2) in clause (iii)--
                    (A) by striking ``placed in service by the taxpayer
                after December 31, 2017'' and inserting ``acquired by
                the taxpayer before September 28, 2017, and placed in
                service by the taxpayer after September 27, 2017'', and
                    (B) by redesignating subclauses (I) and (II) as
                subclauses (II) and (III) respectively, and inserting
                before clause (II), as so redesignated, the following
                new subclause:
                                    ``(I) in the case of a passenger
                                automobile placed in service before
                                January 1, 2018, `$8,000',''.
    (f) Conforming Amendments.--
            (1) Section 168(k)(2)(B)(i)(III), as amended, is amended by
        inserting ``binding'' before ``contract''.
            (2) Section 168(k)(5) is amended by--
                    (A) by striking ``January 1, 2020'' in subparagraph
                (A) and inserting ``January 1, 2023'',
                    (B) by striking ``50 percent'' in subparagraph
                (A)(i) and inserting ``100 percent'', and
                    (C) by striking subparagraph (F).
            (3) Section 168(k)(6) is amended to read as follows:
            ``(6) Phase down.--In the case of qualified property
        acquired by the taxpayer before September 28, 2017, and placed
        in service by the taxpayer after September 27, 2017, paragraph
        (1)(A) shall be applied by substituting for `100 percent'--
                    ``(A) `50 percent' in the case of--
                            ``(i) property placed in service before
                        January 1, 2018, and
                            ``(ii) property described in subparagraph
                        (B) or (C) of paragraph (2) which is placed in
                        service in 2018,
                    ``(B) `40 percent' in the case of--
                            ``(i) property placed in service in 2018
                        (other than property described in subparagraph
                        (B) or (C) of paragraph (2)), and
                            ``(ii) property described in subparagraph
                        (B) or (C) of paragraph (2) which is placed in
                        service in 2019, and
                    ``(C) `30 percent' in the case of--
                            ``(i) property placed in service in 2019
                        (other than property described in subparagraph
                        (B) or (C) of paragraph (2)), and
                            ``(ii) property described in subparagraph
                        (B) or (C) of paragraph (2) which is placed in
                        service in 2020.''.
            (4) The heading of section 168(k) is amended by striking
        ``Special Allowance for Certain Property Acquired After
        December 31, 2007, and Before January 1, 2020'' and inserting
        ``Full Expensing of Certain Property''.
            (5) Section 460(c)(6)(B)(ii) is amended by striking
        ``January 1, 2020 (January 1, 2021 in the case of property
        described in section 168(k)(2)(B))'' and inserting ``January 1,
        2023 (January 1, 2024 in the case of property described in
        section 168(k)(2)(B))''.
    (g) Effective Date.--
            (1) In general.--Except at provided by paragraph (2), the
        amendments made by this section shall apply to property which--
                    (A) is acquired after September 27, 2017, and
                    (B) is placed in service after such date.
        For purposes of the preceding sentence, property shall not be
        treated as acquired after the date on which a written binding
        contract is entered into for such acquisition.
            (2) Specified plants.--The amendments made by subsection
        (f)(2) shall apply to specified plants planted or grafted after
        September 27, 2017.
            (3) Transition rule.--In the case of any taxpayer's first
        taxable year ending after September 27, 2017, the taxpayer may
        elect (at such time and in such form and manner as the
        Secretary of the Treasury, or his designee, may provide) to
        apply section 168 of the Internal Revenue Code of 1986 without
        regard to the amendments made by this section.
            (4) Limitation on net operating loss carrybacks
        attributable to full expensing.--In the case of any taxable
        year which includes any portion of the period beginning on
        September 28, 2017, and ending on December 31, 2017, the amount
        of any net operating loss for such taxable year which may be
        treated as a net operating loss carryback (including any such
        carryback attributable to any specified liability loss under
        section 172(b)(1)(C), any corporate equity reduction interest
        loss under section 172(b)(1)(D), any eligible loss under
        section 172(b)(1)(E), and any farming loss under section
        172(b)(1)(F)) shall be determined without regard to the
        amendments made by this section. For purposes of this
        paragraph, terms which are used in section 172 of the Internal
        Revenue Code of 1986 (determined without regard to the
        amendments made by section 3302) shall have the same meaning as
        when used in such section.

                   Subtitle C--Small Business Reforms

SEC. 3201. EXPANSION OF SECTION 179 EXPENSING.

    (a) Increased Dollar Limitations.--
            (1) In general.--Section 179(b) is amended--
                    (A) by inserting ``($5,000,000, in the case of
                taxable years beginning before January 1, 2023)'' after
                ``$500,000'' in paragraph (1), and
                    (B) by inserting ``($20,000,000, in the case of
                taxable years beginning before January 1, 2023)'' after
                ``$2,000,000'' in paragraph (2).
            (2) Inflation adjustment.--Section 179(b)(6) is amended to
        read as follows:
            ``(6) Inflation adjustment.--
                    ``(A) In general.--In the case of a taxable year
                beginning after 2015 (2018 in the case of the
                $5,000,000 and $20,000,000 amounts in subsection (b)),
                each dollar amount in subsection (b) shall be increased
                by an amount equal to such dollar amount multiplied
                by--
                            ``(i) in the case of the $500,000 and
                        $2,000,000 amounts in subsection (b), the cost-
                        of-living adjustment determined under section
                        1(c)(2) for the calendar year in which the
                        taxable year begins, determined by substituting
                        `calendar year 2014' for `calendar year 2016'
                        in subparagraph (A)(ii) thereof, and
                            ``(ii) in the case of the $5,000,000 and
                        $20,000,000 amounts in subsection (b), the
                        cost-of-living adjustment determined under
                        section 1(c)(2) for the calendar year in which
                        the taxable year begins, determined by
                        substituting `calendar year 2017' for `calendar
                        year 2016' in subparagraph (A)(ii) thereof.
                    ``(B) Rounding.--The amount of any increase under
                subparagraph (A) shall be rounded to the nearest
                multiple of $10,000 ($100,000 in the case of the
                $5,000,000 and $20,000,000 amounts in subsection
                (b)).''.
    (b) Application to Qualified Energy Efficient Heating and Air-
conditioning Property.--
            (1) In general.--Section 179(f)(2) is amended by striking
        ``and'' at the end of subparagraph (B), by striking the period
        at the end of subparagraph (C) and inserting ``, and'', and by
        adding at the end the following new subparagraph:
                    ``(D) qualified energy efficient heating and air-
                conditioning property.''.
            (2) Qualified energy efficient heating and air-conditioning
        property.--Section 179(f) is amended by adding at the end the
        following new paragraph:
            ``(3) Qualified energy efficient heating and air-
        conditioning property.--For purposes of this subsection--
                    ``(A) In general.--The term `qualified energy
                efficient heating and air-conditioning property' means
                any section 1250 property--
                            ``(i) with respect to which depreciation
                        (or amortization in lieu of depreciation) is
                        allowable,
                            ``(ii) which is installed as part of a
                        building's heating, cooling, ventilation, or
                        hot water system, and
                            ``(iii) which is within the scope of
                        Standard 90.1-2007 or any successor standard.
                    ``(B) Standard 90.1-2007.--The term `Standard 90.1-
                2007' means Standard 90.1-2007 of the American Society
                of Heating, Refrigerating and Air-Conditioning
                Engineers and the Illuminating Engineering Society of
                North America (as in effect on the day before the date
                of the adoption of Standard 90.1-2010 of such
                Societies).''.
    (c) Effective Date.--
            (1) Increased dollar limitations.--The amendments made by
        subsection (a) shall apply to taxable years beginning after
        December 31, 2017.
            (2) Application to qualified energy efficient heating and
        air-conditioning property.--The amendments made by subsection
        (b) shall apply to property acquired and placed in service
        after November 2, 2017. For purposes of the preceding sentence,
        property shall not be treated as acquired after the date on
        which a written binding contract is entered into for such
        acquisition.

SEC. 3202. SMALL BUSINESS ACCOUNTING METHOD REFORM AND SIMPLIFICATION.

    (a) Modification of Limitation on Cash Method of Accounting.--
            (1) Increased limitation.--So much of section 448(c) as
        precedes paragraph (2) is amended to read as follows:
    ``(c) Gross Receipts Test.--For purposes of this section--
            ``(1) In general.--A corporation or partnership meets the
        gross receipts test of this subsection for any taxable year if
        the average annual gross receipts of such entity for the 3-
        taxable-year period ending with the taxable year which precedes
        such taxable year does not exceed $25,000,000.''.
            (2) Application of exception on annual basis.--Section
        448(b)(3) is amended to read as follows:
            ``(3) Entities which meet gross receipts test.--Paragraphs
        (1) and (2) of subsection (a) shall not apply to any
        corporation or partnership for any taxable year if such entity
        (or any predecessor) meets the gross receipts test of
        subsection (c) for such taxable year.''.
            (3) Inflation adjustment.--Section 448(c) is amended by
        adding at the end the following new paragraph:
            ``(4) Adjustment for inflation.--In the case of any taxable
        year beginning after December 31, 2018, the dollar amount in
        paragraph (1) shall be increased by an amount equal to--
                    ``(A) such dollar amount, multiplied by
                    ``(B) the cost-of-living adjustment determined
                under section 1(c)(2) for the calendar year in which
                the taxable year begins, by substituting `calendar year
                2017' for `calendar year 2016' in subparagraph (A)(ii)
                thereof.
        If any amount as increased under the preceding sentence is not
        a multiple of $1,000,000, such amount shall be rounded to the
        nearest multiple of $1,000,000.''.
            (4) Coordination with section 481.--Section 448(d)(7) is
        amended to read as follows:
            ``(7) Coordination with section 481.--Any change in method
        of accounting made pursuant to this section shall be treated
        for purposes of section 481 as initiated by the taxpayer and
        made with the consent of the Secretary.''.
            (5) Application of exception to corporations engaged in
        farming.--
                    (A) In general.--Section 447(c) is amended--
                            (i) by inserting ``for any taxable year''
                        after ``not being a corporation'' in the matter
                        preceding paragraph (1), and
                            (ii) by amending paragraph (2) to read as
                        follows:
            ``(2) a corporation which meets the gross receipts test of
        section 448(c) for such taxable year.''.
                    (B) Coordination with section 481.--Section 447(f)
                is amended to read as follows:
    ``(f) Coordination With Section 481.--Any change in method of
accounting made pursuant to this section shall be treated for purposes
of section 481 as initiated by the taxpayer and made with the consent
of the Secretary.''.
                    (C) Conforming amendments.--Section 447 is
                amended--
                            (i) by striking subsections (d), (e), (h),
                        and (i), and
                            (ii) by redesignating subsections (f) and
                        (g) (as amended by subparagraph (B)) as
                        subsections (d) and (e), respectively.
    (b) Exemption From UNICAP Requirements.--
            (1) In general.--Section 263A is amended by redesignating
        subsection (i) as subsection (j) and by inserting after
        subsection (h) the following new subsection:
    ``(i) Exemption for Certain Small Businesses.--
            ``(1) In general.--In the case of any taxpayer (other than
        a tax shelter prohibited from using the cash receipts and
        disbursements method of accounting under section 448(a)(3))
        which meets the gross receipts test of section 448(c) for any
        taxable year, this section shall not apply with respect to such
        taxpayer for such taxable year.
            ``(2) Application of gross receipts test to individuals,
        etc.--In the case of any taxpayer which is not a corporation or
        a partnership, the gross receipts test of section 448(c) shall
        be applied in the same manner as if each trade or business of
        such taxpayer were a corporation or partnership.
            ``(3) Coordination with section 481.--Any change in method
        of accounting made pursuant to this subsection shall be treated
        for purposes of section 481 as initiated by the taxpayer and
        made with the consent of the Secretary.''.
            (2) Conforming amendment.--Section 263A(b)(2) is amended to
        read as follows:
            ``(2) Property acquired for resale.--Real or personal
        property described in section 1221(a)(1) which is acquired by
        the taxpayer for resale.''.
    (c) Exemption From Inventories.--Section 471 is amended by
redesignating subsection (c) as subsection (d) and by inserting after
subsection (b) the following new subsection:
    ``(c) Exemption for Certain Small Businesses.--
            ``(1) In general.--In the case of any taxpayer (other than
        a tax shelter prohibited from using the cash receipts and
        disbursements method of accounting under section 448(a)(3))
        which meets the gross receipts test of section 448(c) for any
        taxable year--
                    ``(A) subsection (a) shall not apply with respect
                to such taxpayer for such taxable year, and
                    ``(B) the taxpayer's method of accounting for
                inventory for such taxable year shall not be treated as
                failing to clearly reflect income if such method
                either--
                            ``(i) treats inventory as non-incidental
                        materials and supplies, or
                            ``(ii) conforms to such taxpayer's method
                        of accounting reflected in an applicable
                        financial statement of the taxpayer with
                        respect to such taxable year or, if the
                        taxpayer does not have any applicable financial
                        statement with respect to such taxable year,
                        the books and records of the taxpayer prepared
                        in accordance with the taxpayer's accounting
                        procedures.
            ``(2) Applicable financial statement.--For purposes of this
        subsection, the term `applicable financial statement' means--
                    ``(A) a financial statement which is certified as
                being prepared in accordance with generally accepted
                accounting principles and which is--
                            ``(i) a 10-K (or successor form), or annual
                        statement to shareholders, required to be filed
                        by the taxpayer with the United States
                        Securities and Exchange Commission,
                            ``(ii) an audited financial statement of
                        the taxpayer which is used for--
                                    ``(I) credit purposes,
                                    ``(II) reporting to shareholders,
                                partners, or other proprietors, or to
                                beneficiaries, or
                                    ``(III) any other substantial
                                nontax purpose,
                        but only if there is no statement of the
                        taxpayer described in clause (i), or
                            ``(iii) filed by the taxpayer with any
                        other Federal or State agency for nontax
                        purposes, but only if there is no statement of
                        the taxpayer described in clause (i) or (ii),
                        or
                    ``(B) a financial statement of the taxpayer which--
                            ``(i) is used for a purpose described in
                        subclause (I), (II), or (III) of subparagraph
                        (A)(ii), or
                            ``(ii) filed by the taxpayer with any
                        regulatory or governmental body (whether
                        domestic or foreign) specified by the
                        Secretary,
                but only if there is no statement of the taxpayer
                described in subparagraph (A).
            ``(3) Application of gross receipts test to individuals,
        etc.--In the case of any taxpayer which is not a corporation or
        a partnership, the gross receipts test of section 448(c) shall
        be applied in the same manner as if each trade or business of
        such taxpayer were a corporation or partnership.
            ``(4) Coordination with section 481.--Any change in method
        of accounting made pursuant to this subsection shall be treated
        for purposes of section 481 as initiated by the taxpayer and
        made with the consent of the Secretary.''.
    (d) Exemption From Percentage Completion for Long-term Contracts.--
            (1) In general.--Section 460(e)(1)(B) is amended--
                    (A) by inserting ``(other than a tax shelter
                prohibited from using the cash receipts and
                disbursements method of accounting under section
                448(a)(3))'' after ``taxpayer'' in the matter preceding
                clause (i), and
                    (B) by amending clause (ii) to read as follows:
                            ``(ii) who meets the gross receipts test of
                        section 448(c) for the taxable year in which
                        such contract is entered into.''.
            (2) Conforming amendments.--Section 460(e) is amended by
        striking paragraphs (2) and (3), by redesignating paragraphs
        (4), (5), and (6) as paragraphs (3), (4), and (5),
        respectively, and by inserting after paragraph (1) the
        following new paragraph:
            ``(2) Rules related to gross receipts test.--
                    ``(A) Application of gross receipts test to
                individuals, etc.--For purposes of paragraph
                (1)(B)(ii), in the case of any taxpayer which is not a
                corporation or a partnership, the gross receipts test
                of section 448(c) shall be applied in the same manner
                as if each trade or business of such taxpayer were a
                corporation or partnership.
                    ``(B) Coordination with section 481.--Any change in
                method of accounting made pursuant to paragraph
                (1)(B)(ii) shall be treated as initiated by the
                taxpayer and made with the consent of the Secretary.
                Such change shall be effected on a cut-off basis for
                all similarly classified contracts entered into on or
                after the year of change.''.
    (e) Effective Date.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        taxable years beginning after December 31, 2017.
            (2) Preservation of suspense account rules with respect to
        any existing suspense accounts.--So much of the amendments made
        by subsection (a)(5)(C) as relate to section 447(i) of the
        Internal Revenue Code of 1986 shall not apply with respect to
        any suspense account established under such section before the
        date of the enactment of this Act.
            (3) Exemption from percentage completion for long-term
        contracts.--The amendments made by subsection (d) shall apply
        to contracts entered into after December 31, 2017, in taxable
        years ending after such date.

SEC. 3203. SMALL BUSINESS EXCEPTION FROM LIMITATION ON DEDUCTION OF
              BUSINESS INTEREST.

    (a) In General.--Section 163(j)(2), as amended by section 3301, is
amended to read as follows:
            ``(2) Exemption for certain small businesses.--In the case
        of any taxpayer (other than a tax shelter prohibited from using
        the cash receipts and disbursements method of accounting under
        section 448(a)(3)) which meets the gross receipts test of
        section 448(c) for any taxable year, paragraph (1) shall not
        apply to such taxpayer for such taxable year. In the case of
        any taxpayer which is not a corporation or a partnership, the
        gross receipts test of section 448(c) shall be applied in the
        same manner as if such taxpayer were a corporation or
        partnership.''.
    (b) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

SEC. 3204. MODIFICATION OF TREATMENT OF S CORPORATION CONVERSIONS TO C
              CORPORATIONS.

    (a) Adjustments Attributable to Conversion From S Corporation to C
Corporation.--Section 481 is amended by adding at the end the following
new subsection:
    ``(d) Adjustments Attributable to Conversion From S Corporation to
C Corporation.--
            ``(1) In general.--In the case of an eligible terminated S
        corporation, any adjustment required by subsection (a)(2) which
        is attributable to such corporation's revocation described in
        paragraph (2)(A)(ii) shall be taken into account ratably during
        the 6-taxable year period beginning with the year of change.
            ``(2) Eligible terminated s corporation.--For purposes of
        this subsection, the term `eligible terminated S corporation'
        means any C corporation--
                    ``(A) which--
                            ``(i) was an S corporation on the day
                        before the date of the enactment of the Tax
                        Cuts and Jobs Act, and
                            ``(ii) during the 2-year period beginning
                        on the date of such enactment makes a
                        revocation of its election under section
                        1362(a), and
                    ``(B) the owners of the stock of which, determined
                on the date such revocation is made, are the same
                owners (and in identical proportions) as on the date of
                such enactment.''.
    (b) Cash Distributions Following Post-termination Transition Period
From S Corporation Status.--Section 1371 is amended by adding at the
end the following new subsection:
    ``(f) Cash Distributions Following Post-termination Transition
Period.--In the case of a distribution of money by an eligible
terminated S corporation (as defined in section 481(d)) after the post-
termination transition period, the accumulated adjustments account
shall be allocated to such distribution, and the distribution shall be
chargeable to accumulated earnings and profits, in the same ratio as
the amount of such accumulated adjustments account bears to the amount
of such accumulated earnings and profits.''.

  Subtitle D--Reform of Business-related Exclusions, Deductions, etc.

SEC. 3301. INTEREST.

    (a) In General.--Section 163(j) is amended to read as follows:
    ``(j) Limitation on Business Interest.--
            ``(1) In general.--In the case of any taxpayer for any
        taxable year, the amount allowed as a deduction under this
        chapter for business interest shall not exceed the sum of--
                    ``(A) the business interest income of such taxpayer
                for such taxable year,
                    ``(B) 30 percent of the adjusted taxable income of
                such taxpayer for such taxable year, plus
                    ``(C) the floor plan financing interest of such
                taxpayer for such taxable year.
        The amount determined under subparagraph (B) (after any
        increases in such amount under paragraph (3)(A)(iii)) shall not
        be less than zero.
            ``(2) Exemption for certain small businesses.--For
        exemption for certain small businesses, see the amendment made
        by section 3203 of the Tax Cuts and Jobs Act.
            ``(3) Application to partnerships, etc.--
                    ``(A) In general.--In the case of any partnership--
                            ``(i) this subsection shall be applied at
                        the partnership level and any deduction for
                        business interest shall be taken into account
                        in determining the non-separately stated
                        taxable income or loss of the partnership,
                            ``(ii) the adjusted taxable income of each
                        partner of such partnership shall be determined
                        without regard to such partner's distributive
                        share of the non-separately stated taxable
                        income or loss of such partnership, and
                            ``(iii) the amount determined under
                        paragraph (1)(B) with respect to each partner
                        of such partnership shall be increased by such
                        partner's distributive share of such
                        partnership's excess amount.
                    ``(B) Excess amount.--The term `excess amount'
                means, with respect to any partnership, the excess (if
                any) of--
                            ``(i) 30 percent of the adjusted taxable
                        income of the partnership, over
                            ``(ii) the amount (if any) by which the
                        business interest of the partnership, reduced
                        by floor plan financing interest, exceeds the
                        business interest income of the partnership.
                    ``(C) Application to s corporations.--Rules similar
                to the rules of subparagraphs (A) and (B) shall apply
                with respect to any S corporation and its shareholders.
            ``(4) Business interest.--For purposes of this subsection,
        the term `business interest' means any interest paid or accrued
        on indebtedness properly allocable to a trade or business. Such
        term shall not include investment interest (within the meaning
        of subsection (d)).
            ``(5) Business interest income.--For purposes of this
        subsection, the term `business interest income' means the
        amount of interest includible in the gross income of the
        taxpayer for the taxable year which is properly allocable to a
        trade or business. Such term shall not include investment
        income (within the meaning of subsection (d)).
            ``(6) Adjusted taxable income.--For purposes of this
        subsection, the term `adjusted taxable income' means the
        taxable income of the taxpayer--
                    ``(A) computed without regard to--
                            ``(i) any item of income, gain, deduction,
                        or loss which is not properly allocable to a
                        trade or business,
                            ``(ii) any business interest or business
                        interest income,
                            ``(iii) the amount of any net operating
                        loss deduction under section 172, and
                            ``(iv) any deduction allowable for
                        depreciation, amortization, or depletion, and
                    ``(B) computed with such other adjustments as the
                Secretary may provide.
            ``(7) Trade or business.--For purposes of this subsection,
        the term `trade or business' shall not include--
                    ``(A) the trade or business of performing services
                as an employee,
                    ``(B) a real property trade or business (as such
                term is defined in section 469(c)(7)(C)), or
                    ``(C) the trade or business of the furnishing or
                sale of--
                            ``(i) electrical energy, water, or sewage
                        disposal services,
                            ``(ii) gas or steam through a local
                        distribution system, or
                            ``(iii) transportation of gas or steam by
                        pipeline,
                if the rates for such furnishing or sale, as the case
                may be, have been established or approved by a State or
                political subdivision thereof, by any agency or
                instrumentality of the United States, or by a public
                service or public utility commission or other similar
                body of any State or political subdivision thereof.
            ``(8) Carryforward of disallowed interest.--For
        carryforward of interest disallowed under paragraph (1), see
        subsection (o).
            ``(9) Floor plan financing interest defined.--For purposes
        of this subsection--
                    ``(A) In general.--The term `floor plan financing
                interest' means interest paid or accrued on floor plan
                financing indebtedness.
                    ``(B) Floor plan financing indebtedness.--The term
                `floor plan financing indebtedness' means
                indebtedness--
                            ``(i) used to finance the acquisition of
                        motor vehicles held for sale to retail
                        customers, and
                            ``(ii) secured by the inventory so
                        acquired.
                    ``(C) Motor vehicle.--The term `motor vehicle'
                means a motor vehicle that is any of the following:
                            ``(i) An automobile.
                            ``(ii) A truck.
                            ``(iii) A recreational vehicle.
                            ``(iv) A motorcycle.
                            ``(v) A boat.
                            ``(vi) Farm machinery or equipment.
                            ``(vii) Construction machinery or
                        equipment.''.
    (b) Carryforward of Disallowed Business Interest.--Section 163,
after amendment by section 4302(a) and before amendment by section
4302(b), is amended by inserting after subsection (n) the following new
subsection:
    ``(o) Carryforward of Disallowed Business Interest.--The amount of
any business interest not allowed as a deduction for any taxable year
by reason of subsection (j) shall be treated as business interest paid
or accrued in the succeeding taxable year. Business interest paid or
accrued in any taxable year (determined without regard to the preceding
sentence) shall not be carried past the 5th taxable year following such
taxable year, determined by treating business interest as allowed as a
deduction on a first-in, first-out basis.''.
    (c) Treatment of Carryforward of Disallowed Business Interest in
Certain Corporate Acquisitions.--
            (1) In general.--Section 381(c) is amended by inserting
        after paragraph (19) the following new paragraph:
            ``(20) Carryforward of disallowed interest.--The carryover
        of disallowed interest described in section 163(o) to taxable
        years ending after the date of distribution or transfer.''.
            (2) Application of limitation.--Section 382(d) is amended
        by adding at the end the following new paragraph:
            ``(3) Application to carryforward of disallowed interest.--
        The term `pre-change loss' shall include any carryover of
        disallowed interest described in section 163(o) under rules
        similar to the rules of paragraph (1).''.
            (3) Conforming amendment.--Section 382(k)(1) is amended by
        inserting after the first sentence the following: ``Such term
        shall include any corporation entitled to use a carryforward of
        disallowed interest described in section 381(c)(20).''
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3302. MODIFICATION OF NET OPERATING LOSS DEDUCTION.

    (a) Indefinite Carryforward of Net Operating Losses.--Section
172(b)(1)(A)(ii) is amended by striking ``to each of the 20 taxable
years'' and inserting ``to each taxable year''.
    (b) Repeal of Net Operating Loss Carrybacks Other Than 1-year
Carryback of Eligible Disaster Losses.--
            (1) In general.--Section 172(b)(1)(A)(i) is amended to read
        as follows:
                            ``(i) in the case of any portion of a net
                        operating loss for the taxable year which is an
                        eligible disaster loss with respect to the
                        taxpayer, shall be a net operating loss
                        carryback to the taxable year preceding the
                        taxable year of such loss, and''.
            (2) Conforming amendments.--
                    (A) Section 172(b)(1) is amended by striking
                subparagraphs (B) through (F) and inserting the
                following:
                    ``(B) Eligible disaster loss.--
                            ``(i) In general.--For purposes of
                        subparagraph (A)(i), the term `eligible
                        disaster loss' means--
                                    ``(I) in the case of a taxpayer
                                which is a small business, net
                                operating losses attributable to
                                federally declared disasters (as
                                defined by section 165(i)(5)), and
                                    ``(II) in the case of a taxpayer
                                engaged in the trade or business of
                                farming, net operating losses
                                attributable to such federally declared
                                disasters.
                            ``(ii) Small business.--For purposes of
                        this subparagraph, the term `small business'
                        means a corporation or partnership which meets
                        the gross receipts test of section 448(c)
                        (determined by substituting `$5,000,000' for
                        `$25,000,000' each place it appears therein)
                        for the taxable year in which the loss arose
                        (or, in the case of a sole proprietorship,
                        which would meet such test if such
                        proprietorship were a corporation).
                            ``(iii) Trade or business of farming.--For
                        purposes of this subparagraph, the trade or
                        business of farming shall include the trade or
                        business of--
                                    ``(I) operating a nursery or sod
                                farm, or
                                    ``(II) the raising or harvesting of
                                trees bearing fruit, nuts, or other
                                crops, or ornamental trees.
                        For purposes of subclause (II), an evergreen
                        tree which is more than 6 years old at the time
                        severed from the roots shall not be treated as
                        an ornamental tree.''.
                    (B) Section 172 is amended by striking subsections
                (f), (g), and (h).
    (c) Limitation of Net Operating Loss to 90 Percent of Taxable
Income.--
            (1) In general.--Section 172(a) is amended to read as
        follows:
    ``(a) Deduction Allowed.--There shall be allowed as a deduction for
the taxable year an amount equal to the lesser of--
            ``(1) the aggregate of the net operating loss carryovers to
        such year, plus the net operating loss carrybacks to such year,
        or
            ``(2) 90 percent of taxable income computed without regard
        to the deduction allowable under this section.
For purposes of this subtitle, the term `net operating loss deduction'
means the deduction allowed by this subsection.''.
            (2) Coordination of limitation with carrybacks and
        carryovers.--Section 172(b)(2) is amended by striking ``shall
        be computed--'' and all that follows and inserting ``shall--
                    ``(A) be computed with the modifications specified
                in subsection (d) other than paragraphs (1), (4), and
                (5) thereof, and by determining the amount of the net
                operating loss deduction without regard to the net
                operating loss for the loss year or for any taxable
                year thereafter,
                    ``(B) not be considered to be less than zero, and
                    ``(C) not exceed the amount determined under
                subsection (a)(2) for such prior taxable year.''.
            (3) Conforming amendment.--Section 172(d)(6) is amended by
        striking ``and'' at the end of subparagraph (A), by striking
        the period at the end of subparagraph (B) and inserting ``;
        and'', and by adding at the end the following new subparagraph:
                    ``(C) subsection (a)(2) shall be applied by
                substituting `real estate investment trust taxable
                income (as defined in section 857(b)(2) but without
                regard to the deduction for dividends paid (as defined
                in section 561))' for `taxable income'.''.
    (d) Annual Increase of Indefinite Carryover Amounts.--Section
172(b) is amended by redesignating paragraph (3) as paragraph (4) and
by inserting after paragraph (2) the following new paragraph:
            ``(3) Annual increase of indefinite carryover amounts.--For
        purposes of paragraph (2)--
                    ``(A) the amount of any indefinite net operating
                loss which is carried to the next succeeding taxable
                year after the loss year (within the meaning of
                paragraph (2)) shall be increased by an amount equal
                to--
                            ``(i) the amount of the loss which may be
                        so carried over to such succeeding taxable year
                        (determined without regard to this paragraph),
                        multiplied by
                            ``(ii) the sum of--
                                    ``(I) the annual Federal short-term
                                rate (determined under section 1274(d))
                                for the last month ending before the
                                beginning of such taxable year, plus
                                    ``(II) 4 percentage points, and
                    ``(B) the amount of any indefinite net operating
                loss which is carried to any succeeding taxable year
                (after such next succeeding taxable year) shall be an
                amount equal to--
                            ``(i) the excess of--
                                    ``(I) the amount of the loss
                                carried to the prior taxable year
                                (after any increase under this
                                paragraph with respect to such amount),
                                over
                                    ``(II) the amount by which such
                                loss was reduced under paragraph (2) by
                                reason of the taxable income for such
                                prior taxable year, multiplied by
                            ``(ii) a percentage equal to 100 percent
                        plus the percentage determined under
                        subparagraph (A)(ii) with respect to such
                        succeeding taxable year.
                For purposes of the preceding sentence, the term
                `indefinite net operating loss' means any net operating
                loss arising in a taxable year beginning after December
                31, 2017.''.
    (e) Effective Date.--
            (1) Carryforwards and carrybacks.--The amendments made by
        subsections (a) and (b) shall apply to net operating losses
        arising in taxable years beginning after December 31, 2017.
            (2) Net operating loss limited to 90 percent of taxable
        income.--The amendments made by subsection (c) shall apply to
        taxable years beginning after December 31, 2017.
            (3) Annual increase in carryover amounts.--The amendments
        made by subsection (d) shall apply to amounts carried to
        taxable years beginning after December 31, 2017.
            (4) Special rule for net disaster losses.--Notwithstanding
        paragraph (1), the amendments made by subsection (b) shall not
        apply to the portion of the net operating loss for any taxable
        year which is a net disaster loss to which section 504(b) of
        the Disaster Tax Relief and Airport and Airway Extension Act of
        2017 applies.

SEC. 3303. LIKE-KIND EXCHANGES OF REAL PROPERTY.

    (a) In General.--Section 1031(a)(1) is amended by striking
``property'' each place it appears and inserting ``real property''.
    (b) Conforming Amendments.--
            (1) Paragraph (2) of section 1031(a) is amended to read as
        follows:
            ``(2) Exception for real property held for sale.--This
        subsection shall not apply to any exchange of real property
        held primarily for sale.''.
            (2) Section 1031 is amended by striking subsections (e) and
        (i).
            (3) Section 1031, as amended by paragraph (2), is amended
        by inserting after subsection (d) the following new subsection:
    ``(e) Application to Certain Partnerships.--For purposes of this
section, an interest in a partnership which has in effect a valid
election under section 761(a) to be excluded from the application of
all of subchapter K shall be treated as an interest in each of the
assets of such partnership and not as an interest in a partnership.''.
            (4) Section 1031(h) is amended to read as follows:
    ``(h) Special Rules for Foreign Real Property.--Real property
located in the United States and real property located outside the
United States are not property of a like kind.''.
            (5) The heading of section 1031 is amended by striking
        ``property'' and inserting ``real property''.
            (6) The table of sections for part III of subchapter O of
        chapter 1 is amended by striking the item relating to section
        1031 and inserting the following new item:

``Sec. 1031. Exchange of real property held for productive use or
                            investment.''.
    (c) Effective Date.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        exchanges completed after December 31, 2017.
            (2) Transition rule.--The amendments made by this section
        shall not apply to any exchange if--
                    (A) the property disposed of by the taxpayer in the
                exchange is disposed of on or before December 31 2017,
                or
                    (B) the property received by the taxpayer in the
                exchange is received on or before December 31, 2017.

SEC. 3304. REVISION OF TREATMENT OF CONTRIBUTIONS TO CAPITAL.

    (a) Inclusion of Contributions to Capital.--Part II of subchapter B
of chapter 1 is amended by inserting after section 75 the following new
section:

``SEC. 76. CONTRIBUTIONS TO CAPITAL.

    ``(a) In General.--Gross income includes any contribution to the
capital of any entity.
    ``(b) Treatment of Contributions in Exchange for Stock, etc.--
            ``(1) In general.--In the case of any contribution of money
        or other property to a corporation in exchange for stock of
        such corporation--
                    ``(A) such contribution shall not be treated for
                purposes of subsection (a) as a contribution to the
                capital of such corporation (and shall not be
                includible in the gross income of such corporation),
                and
                    ``(B) no gain or loss shall be recognized to such
                corporation upon the issuance of such stock.
            ``(2) Treatment limited to value of stock.--For purposes of
        this subsection, a contribution of money or other property to a
        corporation shall be treated as being in exchange for stock of
        such corporation only to the extent that the fair market value
        of such money and other property does not exceed the fair
        market value of such stock.
            ``(3) Application to entities other than corporations.--In
        the case of any entity other than a corporation, rules similar
        to the rules of paragraphs (1) and (2) shall apply in the case
        of any contribution of money or other property to such entity
        in exchange for any interest in such entity.
    ``(c) Treasury Stock Treated as Stock.--Any reference in this
section to stock shall be treated as including a reference to treasury
stock.''.
    (b) Basis of Corporation in Contributed Property.--
            (1) Contributions to capital.--Subsection (c) of section
        362 is amended to read as follows:
    ``(c) Contributions to Capital.--If property other than money is
transferred to a corporation as a contribution to the capital of such
corporation (within the meaning of section 76) then the basis of such
property shall be the greater of--
            ``(1) the basis determined in the hands of the transferor,
        increased by the amount of gain recognized to the transferor on
        such transfer, or
            ``(2) the amount included in gross income by such
        corporation under section 76 with respect to such
        contribution.''.
            (2) Contributions in exchange for stock.--Paragraph (2) of
        section 362(a) is amended by striking ``contribution to
        capital'' and inserting ``contribution in exchange for stock of
        such corporation (determined under rules similar to the rules
        of paragraphs (2) and (3) of section 76(b))''.
    (c) Conforming Amendments.--
            (1) Section 108(e) is amended by striking paragraph (6).
            (2) Part III of subchapter B of chapter 1 is amended by
        striking section 118 (and by striking the item relating to such
        section in the table of sections for such part).
            (3) The table of sections for part II of subchapter B of
        chapter 1 is amended by inserting after the item relating to
        section 75 the following new item:

``Sec. 76. Contributions to capital.''.
    (d) Effective Date.--The amendments made by this section shall
apply to contributions made, and transactions entered into, after the
date of the enactment of this Act.

SEC. 3305. REPEAL OF DEDUCTION FOR LOCAL LOBBYING EXPENSES.

    (a) In General.--Section 162(e) is amended by striking paragraphs
(2) and (7) and by redesignating paragraphs (3), (4), (5), (6), and (8)
as paragraphs (2), (3), (4), (5), and (6), respectively.
    (b) Conforming Amendment.--Section 6033(e)(1)(B)(ii) is amended by
striking ``section 162(e)(5)(B)(ii)'' and inserting ``section
162(e)(4)(B)(ii)''.
    (c) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred after December 31, 2017.

SEC. 3306. REPEAL OF DEDUCTION FOR INCOME ATTRIBUTABLE TO DOMESTIC
              PRODUCTION ACTIVITIES.

    (a) In General.--Part VI of subchapter B of chapter 1 is amended by
striking section 199 (and by striking the item relating to such section
in the table of sections for such part).
    (b) Conforming Amendments.--
            (1) Sections 74(d)(2)(B), 86(b)(2)(A), 137(b)(3)(A),
        219(g)(3)(A)(ii), and 246(b)(1) are each amended by striking
        ``199,''.
            (2) Section 170(b)(2)(D), as amended by the preceding
        provisions of this Act, is amended by striking clause (iv), by
        redesignating clause (v) as clause (iv), and by inserting
        ``and'' at the end of clause (iii).
            (3) Section 172(d) is amended by striking paragraph (7).
            (4) Section 613(a) is amended by striking ``and without the
        deduction under section 199''.
            (5) Section 613A(d)(1) is amended by striking subparagraph
        (B) and by redesignating subparagraphs (C), (D), and (E) as
        subparagraphs (B), (C), and (D), respectively.
            (6) Section 1402(a) is amended by adding ``and'' at the end
        of paragraph (15) and by striking paragraph (16).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3307. ENTERTAINMENT, ETC. EXPENSES.

    (a) Denial of Deduction.--Subsection (a) of section 274 is amended
to read as follows:
    ``(a) Entertainment, Amusement, Recreation, and Other Fringe
Benefits .--
            ``(1) In general.--No deduction otherwise allowable under
        this chapter shall be allowed for amounts paid or incurred for
        any of the following items:
                    ``(A) Activity.--With respect to an activity which
                is of a type generally considered to constitute
                entertainment, amusement, or recreation.
                    ``(B) Membership dues.--With respect to membership
                in any club organized for business, pleasure,
                recreation or other social purposes.
                    ``(C) Amenity.--With respect to a de minimis fringe
                (as defined in section 132(e)(1)) that is primarily
                personal in nature and involving property or services
                that are not directly related to the taxpayer's trade
                or business.
                    ``(D) Facility.--With respect to a facility or
                portion thereof used in connection with an activity
                referred to in subparagraph (A), membership dues or
                similar amounts referred to in subparagraph (B), or an
                amenity referred to in subparagraph (C).
                    ``(E) Qualified transportation fringe and parking
                facility.--Which is a qualified transportation fringe
                (as defined in section 132(f)) or which is a parking
                facility used in connection with qualified parking (as
                defined in section 132(f)(5)(C)).
                    ``(F) On-premises athletic facility.--Which is an
                on-premises athletic facility as defined in section
                132(j)(4)(B).
            ``(2) Special rules.--For purposes of applying paragraph
        (1), an activity described in section 212 shall be treated as a
        trade or business.
            ``(3) Regulations.--Under the regulations prescribed to
        carry out this section, the Secretary shall include
        regulations--
                    ``(A) defining entertainment, amenities,
                recreation, amusement, and facilities for purposes of
                this subsection,
                    ``(B) providing for the appropriate allocation of
                depreciation and other costs with respect to facilities
                used for parking or for on-premises athletic
                facilities, and
                    ``(C) specifying arrangements a primary purpose of
                which is the avoidance of this subsection.''.
    (b) Exception for Certain Expenses Includible in Income of
Recipient.--
            (1) Expenses treated as compensation.--Paragraph (2) of
        section 274(e) is amended to read as follows:
            ``(2) Expenses treated as compensation.--Expenses for
        goods, services, and facilities, to the extent that the
        expenses do not exceed the amount of the expenses which are
        treated by the taxpayer, with respect to the recipient of the
        entertainment, amusement, or recreation, as compensation to an
        employee on the taxpayer's return of tax under this chapter and
        as wages to such employee for purposes of chapter 24 (relating
        to withholding of income tax at source on wages).''.
            (2) Expenses includible in income of persons who are not
        employees.--Paragraph (9) of section 274(e) is amended by
        striking ``to the extent that the expenses'' and inserting ``to
        the extent that the expenses do not exceed the amount of the
        expenses that''.
    (c) Exceptions for Reimbursed Expenses.--Paragraph (3) of section
274(e) is amended to read as follows:
            ``(3) Reimbursed expenses.--
                    ``(A) In general.--Expenses paid or incurred by the
                taxpayer, in connection with the performance by him of
                services for another person (whether or not such other
                person is the taxpayer's employer), under a
                reimbursement or other expense allowance arrangement
                with such other person, but this paragraph shall
                apply--
                            ``(i) where the services are performed for
                        an employer, only if the employer has not
                        treated such expenses in the manner provided in
                        paragraph (2), or
                            ``(ii) where the services are performed for
                        a person other than an employer, only if the
                        taxpayer accounts (to the extent provided by
                        subsection (d)) to such person.
                    ``(B) Exception.--Except as provided by the
                Secretary, subparagraph (A) shall not apply--
                            ``(i) in the case of an arrangement in
                        which the person other than the employer is an
                        entity described in section 168(h)(2)(A), or
                            ``(ii) to any other arrangement designated
                        by the Secretary as having the effect of
                        avoiding the limitation under subparagraph
                        (A).''.
    (d) 50 Percent Limitation on Meals and Entertainment Expenses.--
Subsection (n) of section 274 is amended to read as follows:
    ``(n) Limitation on Certain Expenses.--
            ``(1) In general.--The amount allowable as a deduction
        under this chapter for any expense for food or beverages
        (pursuant to subsection (e)(1)) or business meals (pursuant to
        subsection (k)(1)) shall not exceed 50 percent of the amount of
        such expense or item which would (but for this paragraph) be
        allowable as a deduction under this chapter.
            ``(2) Exceptions.--Paragraph (1) shall not apply to any
        expense if--
                    ``(A) such expense is described in paragraph (2),
                (3), (6), (7), or (8) of subsection (e),
                    ``(B) in the case of an expense for food or
                beverages, such expense is excludable from the gross
                income of the recipient under section 132 by reason of
                subsection (e) thereof (relating to de minimis fringes)
                or under section 119 (relating to meals and lodging
                furnished for convenience of employer), or
                    ``(C) in the case of an employer who pays or
                reimburses moving expenses of an employee, such
                expenses are includible in the income of the employee
                under section 82.
            ``(3) Special rule for individuals subject to federal hours
        of service.--In the case of any expenses for food or beverages
        consumed while away from home (within the meaning of section
        162(a)(2)) by an individual during, or incident to, the period
        of duty subject to the hours of service limitations of the
        Department of Transportation, paragraph (1) shall be applied by
        substituting `80 percent' for `50 percent'.''.
    (e) Conforming Amendments.--
            (1) Section 274(d) is amended--
                    (A) by striking paragraph (2) and redesignating
                paragraphs (3) and (4) as paragraphs (2) and (3),
                respectively, and
                    (B) in the flush material following paragraph (3)
                (as so redesignated)--
                            (i) by striking ``, entertainment,
                        amusement, recreation, or'' in item (B), and
                            (ii) by striking ``(D) the business
                        relationship to the taxpayer of persons
                        entertained, using the facility or property, or
                        receiving the gift'' and inserting ``(D) the
                        business relationship to the taxpayer of the
                        person receiving the benefit''.
            (2) Section 274(e) is amended by striking paragraph (4) and
        redesignating paragraphs (5), (6), (7), (8), and (9) as
        paragraphs (4), (5), (6), (7), and (8), respectively.
            (3) Section 274(k)(2)(A) is amended by striking ``(4), (7),
        (8), or (9)'' and inserting ``(6), (7), or (8)''.
            (4) Section 274 is amended by striking subsection (l).
            (5) Section 274(m)(1)(B)(ii) is amended by striking ``(4),
        (7), (8), or (9)'' and inserting ``(6), (7), or (8)''.
    (f) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred after December 31, 2017.

SEC. 3308. UNRELATED BUSINESS TAXABLE INCOME INCREASED BY AMOUNT OF
              CERTAIN FRINGE BENEFIT EXPENSES FOR WHICH DEDUCTION IS
              DISALLOWED.

    (a) In General.--Section 512(a) is amended by adding at the end the
following new paragraph:
            ``(6) Increase in unrelated business taxable income by
        disallowed fringe.--Unrelated business taxable income of an
        organization shall be increased by any amount for which a
        deduction is not allowable under this chapter by reason of
        section 274 and which is paid or incurred by such organization
        for any qualified transportation fringe (as defined in section
        132(f)), any parking facility used in connection with qualified
        parking (as defined in section 132(f)(5)(C)), or any on-
        premises athletic facility (as defined in section
        132(j)(4)(B)). The preceding sentence shall not apply to the
        extent the amount paid or incurred is directly connected with
        an unrelated trade or business which is regularly carried on by
        the organization. The Secretary may issue such regulations or
        other guidance as may be necessary or appropriate to carry out
        the purposes of this paragraph, including regulations or other
        guidance providing for the appropriate allocation of
        depreciation and other costs with respect to facilities used
        for parking or for on-premises athletic facilities.
        ''.
    (b) Effective Date.--The amendment made by this section shall apply
to amounts paid or incurred after December 31, 2017.

SEC. 3309. LIMITATION ON DEDUCTION FOR FDIC PREMIUMS.

    (a) In General.--Section 162 is amended by redesignating subsection
(q) as subsection (r) and by inserting after subsection (p) the
following new subsection:
    ``(q) Disallowance of FDIC Premiums Paid by Certain Large Financial
Institutions.--
            ``(1) In general.--No deduction shall be allowed for the
        applicable percentage of any FDIC premium paid or incurred by
        the taxpayer.
            ``(2) Exception for small institutions.--Paragraph (1)
        shall not apply to any taxpayer for any taxable year if the
        total consolidated assets of such taxpayer (determined as of
        the close of such taxable year) do not exceed $10,000,000,000.
            ``(3) Applicable percentage.--For purposes of this
        subsection, the term `applicable percentage' means, with
        respect to any taxpayer for any taxable year, the ratio
        (expressed as a percentage but not greater than 100 percent)
        which--
                    ``(A) the excess of--
                            ``(i) the total consolidated assets of such
                        taxpayer (determined as of the close of such
                        taxable year), over
                            ``(ii) $10,000,000,000, bears to
                    ``(B) $40,000,000,000.
            ``(4) FDIC premiums.--For purposes of this subsection, the
        term `FDIC premium' means any assessment imposed under section
        7(b) of the Federal Deposit Insurance Act (12 U.S.C. 1817(b)).
            ``(5) Total consolidated assets.--For purposes of this
        subsection, the term `total consolidated assets' has the
        meaning given such term under section 165 of the Dodd-Frank
        Wall Street Reform and Consumer Protection Act (12 U.S.C.
        5365).
            ``(6) Aggregation rule.--
                    ``(A) In general.--Members of an expanded
                affiliated group shall be treated as a single taxpayer
                for purposes of applying this subsection.
                    ``(B) Expanded affiliated group.--For purposes of
                this paragraph, the term `expanded affiliated group'
                means an affiliated group as defined in section
                1504(a), determined--
                            ``(i) by substituting `more than 50
                        percent' for `at least 80 percent' each place
                        it appears, and
                            ``(ii) without regard to paragraphs (2) and
                        (3) of section 1504(b).
                A partnership or any other entity (other than a
                corporation) shall be treated as a member of an
                expanded affiliated group if such entity is controlled
                (within the meaning of section 954(d)(3)) by members of
                such group (including any entity treated as a member of
                such group by reason of this sentence).''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3310. REPEAL OF ROLLOVER OF PUBLICLY TRADED SECURITIES GAIN INTO
              SPECIALIZED SMALL BUSINESS INVESTMENT COMPANIES.

    (a) In General.--Part III of subchapter O of chapter 1 is amended
by striking section 1044 (and by striking the item relating to such
section in the table of sections of such part).
    (b) Conforming Amendments.--Section 1016(a)(23) is amended--
            (1) by striking ``1044,'', and
            (2) by striking ``1044(d),''.
    (c) Effective Date.--The amendments made by this section shall
apply to sales after December 31, 2017.

SEC. 3311. CERTAIN SELF-CREATED PROPERTY NOT TREATED AS A CAPITAL
              ASSET.

    (a) Patents, etc.--Section 1221(a)(3) is amended by inserting ``a
patent, invention, model or design (whether or not patented), a secret
formula or process,'' before ``a copyright''.
    (b) Conforming Amendment.--Section 1231(b)(1)(C) is amended by
inserting ``a patent, invention, model or design (whether or not
patented), a secret formula or process,'' before ``a copyright''.
    (c) Effective Date.--The amendments made by this section shall
apply to dispositions after December 31, 2017.

SEC. 3312. REPEAL OF SPECIAL RULE FOR SALE OR EXCHANGE OF PATENTS.

    (a) In General.--Part IV of subchapter P of chapter 1 is amended by
striking section 1235 (and by striking the item relating to such
section in the table of sections of such part).
    (b) Conforming Amendments.--
            (1) Section 483(d) is amended by striking paragraph (4).
            (2) Section 901(l)(5) is amended by striking ``without
        regard to section 1235 or any similar rule'' and inserting
        ``without regard to any provision which treats a disposition as
        a sale or exchange of a capital asset held for more than 1 year
        or any similar provision''.
            (3) Section 1274(c)(3) is amended by striking subparagraph
        (E) and redesignating subparagraph (F) as subparagraph (E).
    (c) Effective Date.--The amendments made by this section shall
apply to dispositions after December 31, 2017.

SEC. 3313. REPEAL OF TECHNICAL TERMINATION OF PARTNERSHIPS.

    (a) In General.--Paragraph (1) of section 708(b) is amended--
            (1) by striking ``, or'' at the end of subparagraph (A) and
        all that follows and inserting a period, and
            (2) by striking ``only if--'' and all that follows through
        ``no part of any business'' and inserting the following: ``only
        if no part of any business''.
    (b) Effective Date.--The amendments made by this section shall
apply to partnership taxable years beginning after December 31, 2017.

SEC. 3314. RECHARACTERIZATION OF CERTAIN GAINS IN THE CASE OF
              PARTNERSHIP PROFITS INTERESTS HELD IN CONNECTION WITH
              PERFORMANCE OF INVESTMENT SERVICES.

    (a) In General.--Part IV of subchapter O of chapter 1 is amended--
            (1) by redesignating section 1061 as section 1062, and
            (2) by inserting after section 1060 the following new
        section:

``SEC. 1061. PARTNERSHIP INTERESTS HELD IN CONNECTION WITH PERFORMANCE
              OF SERVICES.

    ``(a) In General.--If one or more applicable partnership interests
are held by a taxpayer at any time during the taxable year, the excess
(if any) of--
            ``(1) the taxpayer's net long-term capital gain with
        respect to such interests for such taxable year, over
            ``(2) the taxpayer's net long-term capital gain with
        respect to such interests for such taxable year computed by
        applying paragraphs (3) and (4) of sections 1222 by
        substituting `3 years' for `1 year',
shall be treated as short-term capital gain.
    ``(b) Special Rule.--To the extent provided by the Secretary,
subsection (a) shall not apply to income or gain attributable to any
asset not held for portfolio investment on behalf of third party
investors.
    ``(c) Applicable Partnership Interest.--For purposes of this
section--
            ``(1) In general.--Except as provided in this paragraph or
        paragraph (4), the term `applicable partnership interest' means
        any interest in a partnership which, directly or indirectly, is
        transferred to (or is held by) the taxpayer in connection with
        the performance of substantial services by the taxpayer, or any
        other related person, in any applicable trade or business. The
        previous sentence shall not apply to an interest held by a
        person who is employed by another entity that is conducting a
        trade or business (other than an applicable trade or business)
        and only provides services to such other entity.
            ``(2) Applicable trade or business.--The term `applicable
        trade or business' means any activity conducted on a regular,
        continuous, and substantial basis which, regardless of whether
        the activity is conducted in one or more entities, consists, in
        whole or in part, of--
                    ``(A) raising or returning capital, and
                    ``(B) either--
                            ``(i) investing in (or disposing of)
                        specified assets (or identifying specified
                        assets for such investing or disposition), or
                            ``(ii) developing specified assets.
            ``(3) Specified asset.--The term `specified asset' means
        securities (as defined in section 475(c)(2) without regard to
        the last sentence thereof), commodities (as defined in section
        475(e)(2)), real estate held for rental or investment, cash or
        cash equivalents, options or derivative contracts with respect
        to any of the foregoing, and an interest in a partnership to
        the extent of the partnership's proportionate interest in any
        of the foregoing.
            ``(4) Exceptions.--The term `applicable partnership
        interest' shall not include--
                    ``(A) any interest in a partnership directly or
                indirectly held by a corporation, or
                    ``(B) any capital interest in the partnership which
                provides the taxpayer with a right to share in
                partnership capital commensurate with--
                            ``(i) the amount of capital contributed
                        (determined at the time of receipt of such
                        partnership interest), or
                            ``(ii) the value of such interest subject
                        to tax under section 83 upon the receipt or
                        vesting of such interest.
            ``(5) Third party investor.--The term `third party
        investor' means a person who--
                    ``(A) holds an interest in the partnership which
                does not constitute property held in connection with an
                applicable trade or business; and
                    ``(B) is not (and has not been) actively engaged,
                and is (and was) not related to a person so engaged, in
                (directly or indirectly) providing substantial services
                described in paragraph (1) for such partnership or any
                applicable trade or business.
    ``(d) Transfer of Applicable Partnership Interest to Related
Person.--
            ``(1) In general.--If a taxpayer transfers any applicable
        partnership interest, directly or indirectly, to a person
        related to the taxpayer, the taxpayer shall include in gross
        income (as short term capital gain) the excess (if any) of--
                    ``(A) so much of the taxpayer's long-term capital
                gains with respect to such interest for such taxable
                year attributable to the sale or exchange of any asset
                held for not more than 3 years as is allocable to such
                interest, over
                    ``(B) any amount treated as short term capital gain
                under subsection (a) with respect to the transfer of
                such interest.
            ``(2) Related person.--For purposes of this paragraph, a
        person is related to the taxpayer if--
                    ``(A) the person is a member of the taxpayer's
                family within the meaning of section 318(a)(1), or
                    ``(B) the person performed a service within the
                current calendar year or the preceding three calendar
                years in any applicable trade or business in which or
                for which the taxpayer performed a service.
    ``(e) Reporting.--The Secretary shall require such reporting (at
the time and in the manner prescribed by the Secretary) as is necessary
to carry out the purposes of this section.
    ``(f) Regulations.--The Secretary shall issue such regulations or
other guidance as is necessary or appropriate to carry out the purposes
of this section''.
    (b) Coordination With Section 83.--Subsection (e) of section 83 is
amended by striking ``or'' at the end of paragraph (4), by striking the
period at the end of paragraph (5) and inserting ``, or'', and by
adding at the end the following new paragraph:
            ``(6) a transfer of an applicable partnership interest to
        which section 1061 applies.''.
    (c) Clerical Amendment.--The table of sections for part IV of
subchapter O of chapter 1 is amended by striking the item relating to
1061 and inserting the following new items:

``Sec. 1061. Partnership interests held in connection with performance
                            of services.
``Sec. 1062. Cross references.''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3315. AMORTIZATION OF RESEARCH AND EXPERIMENTAL EXPENDITURES.

    (a) In General.--Section 174 is amended to read as follows:

``SEC. 174. AMORTIZATION OF RESEARCH AND EXPERIMENTAL EXPENDITURES.

    ``(a) In General.--In the case of a taxpayer's specified research
or experimental expenditures for any taxable year--
            ``(1) except as provided in paragraph (2), no deduction
        shall be allowed for such expenditures, and
            ``(2) the taxpayer shall--
                    ``(A) charge such expenditures to capital account,
                and
                    ``(B) be allowed an amortization deduction of such
                expenditures ratably over the 5-year period (15-year
                period in the case of any specified research or
                experimental expenditures which are attributable to
                foreign research (within the meaning of section
                41(d)(4)(F))) beginning with the midpoint of the
                taxable year in which such expenditures are paid or
                incurred.
    ``(b) Specified Research or Experimental Expenditures.--For
purposes of this section, the term `specified research or experimental
expenditures' means, with respect to any taxable year, research or
experimental expenditures which are paid or incurred by the taxpayer
during such taxable year in connection with the taxpayer's trade or
business.
    ``(c) Special Rules.--
            ``(1) Land and other property.--This section shall not
        apply to any expenditure for the acquisition or improvement of
        land, or for the acquisition or improvement of property to be
        used in connection with the research or experimentation and of
        a character which is subject to the allowance under section 167
        (relating to allowance for depreciation, etc.) or section 611
        (relating to allowance for depletion); but for purposes of this
        section allowances under section 167, and allowances under
        section 611, shall be considered as expenditures.
            ``(2) Exploration expenditures.--This section shall not
        apply to any expenditure paid or incurred for the purpose of
        ascertaining the existence, location, extent, or quality of any
        deposit of ore or other mineral (including oil and gas).
            ``(3) Software development.--For purposes of this section,
        any amount paid or incurred in connection with the development
        of any software shall be treated as a research or experimental
        expenditure.
    ``(d) Treatment Upon Disposition, Retirement, or Abandonment.--If
any property with respect to which specified research or experimental
expenditures are paid or incurred is disposed, retired, or abandoned
during the period during which such expenditures are allowed as an
amortization deduction under this section, no deduction shall be
allowed with respect to such expenditures on account of such
disposition, retirement, or abandonment and such amortization deduction
shall continue with respect to such expenditures.''.
    (b) Clerical Amendment.--The table of sections for part VI of
subchapter B of chapter 1 is amended by striking the item relating to
section 174 and inserting the following new item:

``Sec. 174. Amortization of research and experimental expenditures.''.
    (c) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred in taxable years beginning after
December 31, 2022.

SEC. 3316. UNIFORM TREATMENT OF EXPENSES IN CONTINGENCY FEE CASES.

    (a) In General.--Section 162, as amended by the preceding
provisions of this Act, is amended by redesignating subsection (r) as
subsection (s) and by inserting after subsection (q) the following new
subsection:
    ``(r) Expenses in Contingency Fee Cases.--No deduction shall be
allowed under subsection (a) to a taxpayer for any expense--
            ``(1) paid or incurred in the course of the trade or
        business of practicing law, and
            ``(2) resulting from a case for which the taxpayer is
        compensated primarily on a contingent basis,
until such time as such contingency is resolved.''.
    (b) Effective Date.--The amendment made by this section shall apply
to expenses and costs paid or incurred in taxable years beginning after
the date of the enactment of this Act.

                 Subtitle E--Reform of Business Credits

SEC. 3401. REPEAL OF CREDIT FOR CLINICAL TESTING EXPENSES FOR CERTAIN
              DRUGS FOR RARE DISEASES OR CONDITIONS.

    (a) In General.--Subpart D of part IV of subchapter A of chapter 1
is amended by striking section 45C (and by striking the item relating
to such section in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1) Section 38(b) is amended by striking paragraph (12).
            (2) Section 280C is amended by striking subsection (b).
            (3) Section 6501(m) is amended by striking ``45C(d)(4),''.
    (c) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred in taxable years beginning after
December 31, 2017.

SEC. 3402. REPEAL OF EMPLOYER-PROVIDED CHILD CARE CREDIT.

    (a) In General.--Subpart D of part IV of subchapter A of chapter 1
is amended by striking section 45F (and by striking the item relating
to such section in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1) Section 38(b) is amended by striking paragraph (15).
            (2) Section 1016(a) is amended by striking paragraph (28).
    (c) Effective Date.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        taxable years beginning after December 31, 2017.
            (2) Basis adjustments.--The amendment made by subsection
        (b)(2) shall apply to credits determined for taxable years
        beginning after December 31, 2017.

SEC. 3403. REPEAL OF REHABILITATION CREDIT.

    (a) In General.--Subpart E of part IV of subchapter A of chapter 1
is amended by striking section 47 (and by striking the item relating to
such section in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1) Section 170(f)(14)(A) is amended by inserting ``(as in
        effect before its repeal by the Tax Cuts and Jobs Act)'' after
        ``section 47''.
            (2) Section 170(h)(4) is amended--
                    (A) by striking ``(as defined in section
                47(c)(3)(B))'' in subparagraph (C)(ii), and
                    (B) by adding at the end the following new
                subparagraph:
                    ``(D) Registered historic district.--The term
                `registered historic district' means--
                            ``(i) any district listed in the National
                        Register, and
                            ``(ii) any district--
                                    ``(I) which is designated under a
                                statute of the appropriate State or
                                local government, if such statute is
                                certified by the Secretary of the
                                Interior to the Secretary as containing
                                criteria which will substantially
                                achieve the purpose of preserving and
                                rehabilitating buildings of historic
                                significance to the district, and
                                    ``(II) which is certified by the
                                Secretary of the Interior to the
                                Secretary as meeting substantially all
                                of the requirements for the listing of
                                districts in the National Register.''.
            (3) Section 469(i)(3) is amended by striking subparagraph
        (B).
            (4) Section 469(i)(6)(B) is amended--
                    (A) by striking ``in the case of--'' and all that
                follows and inserting ``in the case of any credit
                determined under section 42 for any taxable year.'',
                and
                    (B) by striking ``, rehabilitation credit,'' in the
                heading thereof.
            (5) Section 469(k)(1) is amended by striking ``, or any
        rehabilitation credit determined under section 47,''.
    (c) Effective Date.--
            (1) In general.--Except as provided in paragraph (2), the
        amendments made by this section shall apply to amounts paid or
        incurred after December 31, 2017.
            (2) Transition rule.--In the case of qualified
        rehabilitation expenditures (within the meaning of section 47
        of the Internal Revenue Code of 1986 as in effect before its
        repeal) with respect to any building--
                    (A) owned or leased (as permitted by section 47 of
                the Internal Revenue Code of 1986 as in effect before
                its repeal) by the taxpayer at all times after December
                31, 2017, and
                    (B) with respect to which the 24-month period
                selected by the taxpayer under section 47(c)(1)(C) of
                such Code begins not later than the end of the 180-day
                period beginning on the date of the enactment of this
                Act,
        the amendments made by this section shall apply to such
        expenditures paid or incurred after the end of the taxable year
        in which the 24-month period referred to in subparagraph (B)
        ends.

SEC. 3404. REPEAL OF WORK OPPORTUNITY TAX CREDIT.

    (a) In General.--Subpart F of part IV of subchapter A of chapter 1
is amended by striking section 51 (and by striking the item relating to
such section in the table of sections for such subpart).
    (b) Clerical Amendment.--The heading of such subpart F (and the
item relating to such subpart in the table of subparts for part IV of
subchapter A of chapter 1) are each amended by striking ``Rules for
Computing Work Opportunity Credit'' and inserting ``Special Rules''.
    (c) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred to individuals who begin work for the
employer after December 31, 2017.

SEC. 3405. REPEAL OF DEDUCTION FOR CERTAIN UNUSED BUSINESS CREDITS.

    (a) In General.--Part VI of subchapter B of chapter 1 is amended by
striking section 196 (and by striking the item relating to such section
in the table of sections for such part).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3406. TERMINATION OF NEW MARKETS TAX CREDIT.

    (a) In General.--Section 45D(f) is amended--
            (1) by striking ``2019'' in paragraph (1)(G) and inserting
        ``2017'', and
            (2) by striking ``2024'' in paragraph (3) and inserting
        ``2022''.
    (b) Effective Date.--The amendments made by this section shall
apply to calendar years beginning after December 31, 2017.

SEC. 3407. REPEAL OF CREDIT FOR EXPENDITURES TO PROVIDE ACCESS TO
              DISABLED INDIVIDUALS.

    (a) In General.--Subpart D of part IV of subchapter A of chapter 1
is amended by striking section 44 (and by striking the item relating to
such section in the table of sections for such subpart).
    (b) Conforming Amendment.--Section 38(b) is amended by striking
paragraph (7).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3408. MODIFICATION OF CREDIT FOR PORTION OF EMPLOYER SOCIAL
              SECURITY TAXES PAID WITH RESPECT TO EMPLOYEE TIPS.

    (a) Credit Determined With Respect to Minimum Wage as in Effect.--
Section 45B(b)(1)(B) is amended by striking ``as in effect on January
1, 2007, and''.
    (b) Information Return Requirement.--Section 45B is amended by
redesignating subsections (c) and (d) as subsections (d) and (e),
respectively, and by inserting after subsection (b) the following new
subsection:
    ``(c) Information Return Requirement.--
            ``(1) In general.--No credit shall be determined under
        subsection (a) with respect to any food or beverage
        establishment of any taxpayer for any taxable year unless such
        taxpayer has, with respect to the calendar year which ends in
        or with such taxable year--
                    ``(A) made a report to the Secretary showing the
                information described in section 6053(c)(1) with
                respect to such food or beverage establishment, and
                    ``(B) furnished written statements to each employee
                of such food or beverage establishment showing the
                information described in section 6053(c)(2).
            ``(2) Allocation of 10 percent of gross receipts.--For
        purposes of determining the information referred to in
        subparagraphs (A) and (B), section 6053(c)(3)(A)(i) shall be
        applied by substituting `10 percent' for `8 percent'. For
        purposes of section 6053(c)(5), any reference to section
        6053(c)(3)(B) contained therein shall be treated as including a
        reference to this paragraph.
            ``(3) Food or beverage establishment.--For purposes of this
        subsection, the term `food or beverage establishment' means any
        trade or business (or portion thereof) which would be a large
        food or beverage establishment (as defined in section
        6053(c)(4)) if such section were applied without regard to
        subparagraph (C) thereof.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

                       Subtitle F--Energy Credits

SEC. 3501. MODIFICATIONS TO CREDIT FOR ELECTRICITY PRODUCED FROM
              CERTAIN RENEWABLE RESOURCES.

    (a) Termination of Inflation Adjustment.--Section 45(b)(2) is
amended--
            (1) by striking ``The 1.5 cent amount'' and inserting the
        following:
                    ``(A) In general.--The 1.5 cent amount'', and
            (2) by adding at the end the following new subparagraph:
                    ``(B) Termination.--Subparagraph (A) shall not
                apply with respect to any electricity or refined coal
                produced at a facility the construction of which begins
                after the date of the enactment of this
                subparagraph.''.
    (b) Special Rule for Determination of Beginning of Construction.--
Section 45(e) is amended by adding at the end the following new
paragraph:
            ``(12) Special rule for determining beginning of
        construction.--For purposes of subsection (d), the construction
        of any facility, modification, improvement, addition, or other
        property shall not be treated as beginning before any date
        unless there is a continuous program of construction which
        begins before such date and ends on the date that such property
        is placed in service.''.
    (c) Effective Dates.--
            (1) Termination of inflation adjustment.--The amendments
        made by subsection (a) shall apply to taxable years ending
        after the date of the enactment of this Act.
            (2) Special rule for determination of beginning of
        construction.--The amendment made by subsection (b) shall apply
        to taxable years beginning before, on, or after the date of the
        enactment of this Act.

SEC. 3502. MODIFICATION OF THE ENERGY INVESTMENT TAX CREDIT.

    (a) Extension of Solar Energy Property.--Section 48(a)(3)(A)(ii) is
amended by striking ``periods ending before January 1, 2017'' and
inserting ``property the construction of which begins before January 1,
2022''.
    (b) Extension of Qualified Fuel Cell Property.--Section 48(c)(1)(D)
is amended by striking ``for any period after December 31, 2016'' and
inserting ``the construction of which does not begin before January 1,
2022''.
    (c) Extension of Qualified Microturbine Property.--Section
48(c)(2)(D) is amended by striking ``for any period after December 31,
2016'' and inserting ``the construction of which does not begin before
January 1, 2022''.
    (d) Extension of Combined Heat and Power System Property.--Section
48(c)(3)(A)(iv) is amended by striking ``which is placed in service
before January 1, 2017'' and inserting ``the construction of which
begins before January 1, 2022''.
    (e) Extension of Qualified Small Wind Energy Property.--Section
48(c)(4)(C) is amended by striking ``for any period after December 31,
2016'' and inserting ``the construction of which does not begin before
January 1, 2022''.
    (f) Extension of Thermal Energy Property.--Section 48(a)(3)(A)(vii)
is amended by striking ``periods ending before January 1, 2017'' and
inserting ``property the construction of which begins before January 1,
2022''.
    (g) Phaseout of 30 Percent Credit Rate for Fuel Cell and Small Wind
Energy Property.--Section 48(a) is amended by adding at the end the
following new paragraph:
            ``(7) Phaseout for qualified fuel cell property and
        qualified small wind energy property.--
                    ``(A) In general.--In the case of qualified fuel
                cell property or qualified small wind energy property,
                the construction of which begins before January 1,
                2022, the energy percentage determined under paragraph
                (2) shall be equal to--
                            ``(i) in the case of any property the
                        construction of which begins after December 31,
                        2019, and before January 1, 2021, 26 percent,
                        and
                            ``(ii) in the case of any property the
                        construction of which begins after December 31,
                        2020, and before January 1, 2022, 22 percent.
                    ``(B) Placed in service deadline.--In the case of
                any qualified fuel cell property or qualified small
                wind energy property, the construction of which begins
                before January 1, 2022, and which is not placed in
                service before January 1, 2024, the energy percentage
                determined under paragraph (2) shall be equal to 10
                percent.''.
    (h) Phaseout for Fiber-optic Solar Energy Property.--Subparagraphs
(A) and (B) of section 48(a)(6) are each amended by inserting ``or
(3)(A)(ii)'' after ``paragraph (3)(A)(i)''.
    (i) Termination of Solar Energy Property.--Section 48(a)(3)(A)(i)
is amended by inserting ``, the construction of which begins before
January 1, 2028, and'' after ``equipment''.
    (j) Termination of Geothermal Energy Property.--Section
48(a)(3)(A)(iii) is amended by inserting ``, the construction of which
begins before January 1, 2028, and'' after ``equipment''.
    (k) Special Rule for Determination of Beginning of Construction.--
Section 48(c) is amended by adding at the end the following new
paragraph:
            ``(5) Special rule for determining beginning of
        construction.--The construction of any facility, modification,
        improvement, addition, or other property shall not be treated
        as beginning before any date unless there is a continuous
        program of construction which begins before such date and ends
        on the date that such property is placed in service.''.
    (l) Effective Date.--
            (1) In general.--Except as otherwise provided in this
        subsection, the amendments made by this section shall apply to
        periods after December 31, 2016, under rules similar to the
        rules of section 48(m) of the Internal Revenue Code of 1986 (as
        in effect on the day before the date of the enactment of the
        Revenue Reconciliation Act of 1990).
            (2) Extension of combined heat and power system property.--
        The amendment made by subsection (d) shall apply to property
        placed in service after December 31, 2016.
            (3) Phaseouts and terminations.--The amendments made by
        subsections (g), (h), (i), and (j) shall take effect on the
        date of the enactment of this Act.
            (4) Special rule for determination of beginning of
        construction.--The amendment made by subsection (k) shall apply
        to taxable years beginning before, on, or after the date of the
        enactment of this Act.

SEC. 3503. EXTENSION AND PHASEOUT OF RESIDENTIAL ENERGY EFFICIENT
              PROPERTY.

    (a) Extension.--Section 25D(h) is amended by striking ``December
31, 2016 (December 31, 2021, in the case of any qualified solar
electric property expenditures and qualified solar water heating
property expenditures)'' and inserting ``December 31, 2021''.
    (b) Phaseout.--
            (1) In general.--Paragraphs (3), (4), and (5) of section
        25D(a) are amended by striking ``30 percent'' each place it
        appears and inserting ``the applicable percentage''.
            (2) Conforming amendment.--Section 25D(g) of such Code is
        amended by striking ``paragraphs (1) and (2) of''.
    (c) Effective Date.--The amendments made by this section shall
apply to property placed in service after December 31, 2016.

SEC. 3504. REPEAL OF ENHANCED OIL RECOVERY CREDIT.

    (a) In General.--Subpart D of part IV of subchapter A of chapter 1
is amended by striking section 43 (and by striking the item relating to
such section in the table of sections for such subpart).
    (b) Conforming Amendments.--
            (1) Section 38(b) is amended by striking paragraph (6).
            (2) Section 6501(m) is amended by striking ``43,''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3505. REPEAL OF CREDIT FOR PRODUCING OIL AND GAS FROM MARGINAL
              WELLS.

    (a) In General.--Subpart D of part IV of subchapter A of chapter 1
is amended by striking section 45I (and by striking the item relating
to such section in the table of sections for such subpart).
    (b) Conforming Amendment.--Section 38(b) is amended by striking
paragraph (19).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3506. MODIFICATIONS OF CREDIT FOR PRODUCTION FROM ADVANCED NUCLEAR
              POWER FACILITIES.

    (a) Treatment of Unutilized Limitation Amounts.--Section 45J(b) is
amended--
            (1) in paragraph (4), by inserting ``or any amendment to''
        after ``enactment of''; and
            (2) by adding at the end the following new paragraph:
            ``(5) Allocation of unutilized limitation.--
                    ``(A) In general.--Any unutilized national megawatt
                capacity limitation shall be allocated by the Secretary
                under paragraph (3) as rapidly as is practicable after
                December 31, 2020--
                            ``(i) first to facilities placed in service
                        on or before such date to the extent that such
                        facilities did not receive an allocation equal
                        to their full nameplate capacity; and
                            ``(ii) then to facilities placed in service
                        after such date in the order in which such
                        facilities are placed in service.
                    ``(B) Unutilized national megawatt capacity
                limitation.--The term `unutilized national megawatt
                capacity limitation' means the excess (if any) of--
                            ``(i) 6,000 megawatts, over
                            ``(ii) the aggregate amount of national
                        megawatt capacity limitation allocated by the
                        Secretary before January 1, 2021, reduced by
                        any amount of such limitation which was
                        allocated to a facility which was not placed in
                        service before such date.
                    ``(C) Coordination with other provisions.--In the
                case of any unutilized national megawatt capacity
                limitation allocated by the Secretary pursuant to this
                paragraph--
                            ``(i) such allocation shall be treated for
                        purposes of this section in the same manner as
                        an allocation of national megawatt capacity
                        limitation; and
                            ``(ii) subsection (d)(1)(B) shall not apply
                        to any facility which receives such
                        allocation.''.
    (b) Transfer of Credit by Certain Public Entities.--
            (1) In general.--Section 45J is amended--
                    (A) by redesignating subsection (e) as subsection
                (f); and
                    (B) by inserting after subsection (d) the following
                new subsection:
    ``(e) Transfer of Credit by Certain Public Entities.--
            ``(1) In general.--If, with respect to a credit under
        subsection (a) for any taxable year--
                    ``(A) the taxpayer would be a qualified public
                entity; and
                    ``(B) such entity elects the application of this
                paragraph for such taxable year with respect to all (or
                any portion specified in such election) of such credit,
        the eligible project partner specified in such election (and
        not the qualified public entity) shall be treated as the
        taxpayer for purposes of this title with respect to such credit
        (or such portion thereof).
            ``(2) Definitions.--For purposes of this subsection--
                    ``(A) Qualified public entity.--The term `qualified
                public entity' means--
                            ``(i) a Federal, State, or local government
                        entity, or any political subdivision, agency,
                        or instrumentality thereof;
                            ``(ii) a mutual or cooperative electric
                        company described in section 501(c)(12) or
                        section 1381(a)(2); or
                            ``(iii) a not-for-profit electric utility
                        which has or had received a loan or loan
                        guarantee under the Rural Electrification Act
                        of 1936.
                    ``(B) Eligible project partner.--The term `eligible
                project partner' means--
                            ``(i) any person responsible for, or
                        participating in, the design or construction of
                        the advanced nuclear power facility to which
                        the credit under subsection (a) relates;
                            ``(ii) any person who participates in the
                        provision of the nuclear steam supply system to
                        the advanced nuclear power facility to which
                        the credit under subsection (a) relates;
                            ``(iii) any person who participates in the
                        provision of nuclear fuel to the advanced
                        nuclear power facility to which the credit
                        under subsection (a) relates; or
                            ``(iv) any person who has an ownership
                        interest in such facility.
            ``(3) Special rules.--
                    ``(A) Application to partnerships.--In the case of
                a credit under subsection (a) which is determined at
                the partnership level--
                            ``(i) for purposes of paragraph (1)(A), a
                        qualified public entity shall be treated as the
                        taxpayer with respect to such entity's
                        distributive share of such credit; and
                            ``(ii) the term `eligible project partner'
                        shall include any partner of the partnership.
                    ``(B) Taxable year in which credit taken into
                account.--In the case of any credit (or portion
                thereof) with respect to which an election is made
                under paragraph (1), such credit shall be taken into
                account in the first taxable year of the eligible
                project partner ending with, or after, the qualified
                public entity's taxable year with respect to which the
                credit was determined.
                    ``(C) Treatment of transfer under private use
                rules.--For purposes of section 141(b)(1), any benefit
                derived by an eligible project partner in connection
                with an election under this subsection shall not be
                taken into account as a private business use.''.
            (2) Special rule for proceeds of transfers for mutual or
        cooperative electric companies.--Section 501(c)(12) of such
        Code is amended by adding at the end the following new
        subparagraph:
                    ``(I) In the case of a mutual or cooperative
                electric company described in this paragraph or an
                organization described in section 1381(a)(2), income
                received or accrued in connection with an election
                under section 45J(e)(1) shall be treated as an amount
                collected from members for the sole purpose of meeting
                losses and expenses.''.
    (c) Effective Dates.--
            (1) Treatment of unutilized limitation amounts.--The
        amendment made by subsection (a) shall take effect on the date
        of the enactment of this Act.
            (2) Transfer of credit by certain public entities.--The
        amendments made by subsection (b) shall apply to taxable years
        beginning after the date of the enactment of this Act.

                        Subtitle G--Bond Reforms

SEC. 3601. TERMINATION OF PRIVATE ACTIVITY BONDS.

    (a) In General.--Paragraph (1) of section 103(b) is amended--
            (1) by striking ``which is not a qualified bond (within the
        meaning of section 141)'', and
            (2) by striking ``which is not a qualified bond'' in the
        heading thereof.
    (b) Conforming Amendments.--
            (1) Subpart A of part IV of subchapter B of chapter 1 is
        amended by striking sections 142, 143, 144, 145, 146, and 147
        (and by striking each of the items relating to such sections in
        the table of sections for such subpart).
            (2) Section 25 is amended by adding at the end the
        following new subsection:
    ``(j) Coordination With Repeal of Private Activity Bonds.--Any
reference to section 143, 144, or 146 shall be treated as a reference
to such section as in effect before its repeal by the Tax Cuts and Jobs
Act.''.
            (3) Section 26(b)(2) is amended by striking subparagraph
        (D).
            (4) Section 141(b) is amended by striking paragraphs (5)
        and (9).
            (5) Section 141(d) is amended by striking paragraph (5).
            (6) Section 141 is amended by striking subsection (e).
            (7) Section 148(f)(4) is amended--
                    (A) by striking ``(determined in accordance with
                section 147(b)(2)(A))'' in the flush matter following
                subparagraph (A)(ii) and inserting ``(determined by
                taking into account the respective issue prices of the
                bonds issued as part of the issue)'', and
                    (B) by striking the last sentence of subparagraph
                (D)(v).
            (8) Clause (iv) of section 148(f)(4)(C) is amended to read
        as follows:
                            ``(iv) Construction issue.--For purposes of
                        this subparagraph--
                                    ``(I) In general.--The term
                                `construction issue' means any issue if
                                at least 75 percent of the available
                                construction proceeds of such issue are
                                to be used for construction
                                expenditures.
                                    ``(II) Construction.--The term
                                `construction' includes reconstruction
                                and rehabilitation.''.
            (9) Section 149(b)(3) is amended by striking subparagraph
        (C).
            (10) Section 149(e)(2) is amended--
                    (A) by striking subparagraphs (C), (D), and (F) and
                by redesignating subparagraphs (E) and (G) as
                subparagraphs (C) and (D), respectively, and
                    (B) by striking the second sentence.
            (11) Section 149(f)(6) is amended--
                    (A) by striking subparagraph (B), and
                    (B) by striking ``For purposes of this subsection''
                and all that follows through ``The term'' and inserting
                the following: ``For purposes of this subsection, the
                term''.
            (12) Section 150(e)(3) is amended to read as follows:
            ``(3) Public approval requirement.--A bond shall not be
        treated as part of an issue which meets the requirements of
        paragraph (1) unless such bond satisfies the requirements of
        section 147(f)(2) (as in effect before its repeal by the Tax
        Cuts and Jobs Act).''.
            (13) Section 269A(b)(3) is amended by striking
        ``144(a)(3)'' and inserting ``414(n)(6)(A)''.
            (14) Section 414(m)(5) is amended by striking ``section
        144(a)(3)'' and inserting ``subsection (n)(6)(A)''.
            (15) Section 414(n)(6)(A) is amended to read as follows:
                    ``(A) Related persons.--A person is a related
                person to another person if--
                            ``(i) the relationship between such persons
                        would result in a disallowance of losses under
                        section 267 or 707(b), or
                            ``(ii) such persons are members of the same
                        controlled group of corporations (as defined in
                        section 1563(a), except that `more than 50
                        percent' shall be substituted for `at least 80
                        percent' each place it appears therein).''.
            (16) Section 6045(e)(4)(B) is amended by inserting ``(as in
        effect before its repeal by the Tax Cuts and Jobs Act)'' after
        ``section 143(m)(3)''.
            (17) Section 6654(f)(1) is amended by inserting ``(as in
        effect before its repeal by the Tax Cuts and Jobs Act)'' after
        ``section 143(m)''.
            (18) Section 7871(c) is amended--
                    (A) by striking paragraphs (2) and (3), and
                    (B) by striking ``Tax-exempt Bonds.--'' and all
                that follows through ``Subsection (a) of section 103''
                and inserting the following: ``Tax-exempt Bonds.--
                Subsection (a) of section 103''.
    (c) Effective Date.--The amendments made by this section shall
apply to bonds issued after December 31, 2017.

SEC. 3602. REPEAL OF ADVANCE REFUNDING BONDS.

    (a) In General.--Paragraph (1) of section 149(d) is amended by
striking ``as part of an issue described in paragraph (2), (3), or
(4).'' and inserting ``to advance refund another bond.''.
    (b) Conforming Amendments.--
            (1) Section 149(d) is amended by striking paragraphs (2),
        (3), (4), and (6) and by redesignating paragraphs (5) and (7)
        as paragraphs (2) and (3).
            (2) Section 148(f)(4)(C) is amended by striking clause
        (xiv) and by redesignating clauses (xv) to (xvii) as clauses
        (xiv) to (xvi).
    (c) Effective Date.--The amendments made by this section shall
apply to advance refunding bonds issued after December 31, 2017.

SEC. 3603. REPEAL OF TAX CREDIT BONDS.

    (a) In General.--Part IV of subchapter A of chapter 1 is amended by
striking subparts H, I, and J (and by striking the items relating to
such subparts in the table of subparts for such part).
    (b) Payments to Issuers.--Subchapter B of chapter 65 is amended by
striking section 6431 (and by striking the item relating to such
section in the table of sections for such subchapter).
    (c) Conforming Amendments.--
            (1) Part IV of subchapter U of chapter 1 is amended by
        striking section 1397E (and by striking the item relating to
        such section in the table of sections for such part).
            (2) Section 54(l)(3)(B) is amended by inserting ``(as in
        effect before its repeal by the Tax Cuts and Jobs Act)'' after
        ``section 1397E(I)''.
            (3) Section 6211(b)(4)(A) is amended by striking ``, and
        6431'' and inserting ``and'' before ``36B''.
            (4) Section 6401(b)(1) is amended by striking ``G, H, I,
        and J'' and inserting ``and G''.
    (d) Effective Date.--The amendments made by this section shall
apply to bonds issued after December 31, 2017.

SEC. 3604. NO TAX EXEMPT BONDS FOR PROFESSIONAL STADIUMS.

    (a) In General.--Section 103(b), as amended by this Act, is further
amended by adding at the end the following new paragraph:
            ``(4) Professional stadium bond.--Any professional stadium
        bond.''.
    (b) Professional Stadium Bond Defined.--Subsection (c) of section
103 is amended by adding at the end the following new paragraph:
            ``(3) Professional stadium bond.--The term `professional
        stadium bond' means any bond issued as part of an issue any
        proceeds of which are used to finance or refinance capital
        expenditures allocable to a facility (or appurtenant real
        property) which, during at least 5 days during any calendar
        year, is used as a stadium or arena for professional sports
        exhibitions, games, or training.''.
    (c) Effective Date.--The amendments made by this section shall
apply to bonds issued after November 2, 2017.

                         Subtitle H--Insurance

SEC. 3701. NET OPERATING LOSSES OF LIFE INSURANCE COMPANIES.

    (a) In General.--Section 805(b) is amended by striking paragraph
(4) and by redesignating paragraph (5) as paragraph (4).
    (b) Conforming Amendments.--
            (1) Part I of subchapter L of chapter 1 is amended by
        striking section 810 (and by striking the item relating to such
        section in the table of sections for such part).
            (2) Part III of subchapter L of chapter 1 is amended by
        striking section 844 (and by striking the item relating to such
        section in the table of sections for such part).
            (3) Section 381 is amended by striking subsection (d).
            (4) Section 805(a)(4)(B)(ii) is amended to read as follows:
                            ``(ii) the deduction allowed under section
                        172,''.
            (5) Section 805(a) is amended by striking paragraph (5).
            (6) Section 953(b)(1)(B) is amended to read as follows:
                    ``(B) So much of section 805(a)(8) as relates to
                the deduction allowed under section 172.''.
    (c) Effective Date.--The amendments made by this section shall
apply to losses arising in taxable years beginning after December 31,
2017.

SEC. 3702. REPEAL OF SMALL LIFE INSURANCE COMPANY DEDUCTION.

    (a) In General.--Part I of subchapter L of chapter 1 is amended by
striking section 806 (and by striking the item relating to such section
in the table of sections for such part).
    (b) Conforming Amendments.--
            (1) Section 453B(e) is amended--
                    (A) by striking ``(as defined in section
                806(b)(3))'' in paragraph (2)(B), and
                    (B) by adding at the end the following new
                paragraph:
            ``(3) Noninsurance business.--
                    ``(A) In general.--For purposes of this subsection,
                the term `noninsurance business' means any activity
                which is not an insurance business.
                    ``(B) Certain activities treated as insurance
                businesses.--For purposes of subparagraph (A), any
                activity which is not an insurance business shall be
                treated as an insurance business if--
                            ``(i) it is of a type traditionally carried
                        on by life insurance companies for investment
                        purposes, but only if the carrying on of such
                        activity (other than in the case of real
                        estate) does not constitute the active conduct
                        of a trade or business, or
                            ``(ii) it involves the performance of
                        administrative services in connection with
                        plans providing life insurance, pension, or
                        accident and health benefits.''.
            (2) Section 465(c)(7)(D)(v)(II) is amended by striking
        ``section 806(b)(3)'' and inserting ``section 453B(e)(3)''.
            (3) Section 801(a)(2) is amended by striking subparagraph
        (C).
            (4) Section 804 is amended by striking ``means--'' and all
        that follows and inserting ``means the general deductions
        provided in section 805.''.
            (5) Section 805(a)(4)(B), as amended by section 3701, is
        amended by striking clause (i) and by redesignating clauses
        (ii), (iii), and (iv) as clauses (i), (ii), and (iii),
        respectively.
            (6) Section 805(b)(2)(A) is amended by striking clause
        (iii) and by redesignating clauses (iv) and (v) as clauses
        (iii) and (iv), respectively.
            (7) Section 842(c) is amended by striking paragraph (1) and
        by redesignating paragraphs (2) and (3) as paragraphs (1) and
        (2), respectively.
            (8) Section 953(b)(1), as amended by section 3701, is
        amended by striking subparagraph (A) and by redesignating
        subparagraphs (B) and (C) as subparagraphs (A) and (B),
        respectively.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3703. SURTAX ON LIFE INSURANCE COMPANY TAXABLE INCOME.

    (a) In General.--Section 801(a)(1) is amended--
            (1) by striking ``consist of a tax'' and insert ``consist
        of the sum of--
                    ``(A) a tax'', and
            (2) by striking the period at the end and inserting ``,
        and'', and
            (3) by adding at the end the following new subparagraph:
                    ``(B) a tax equal to 8 percent of the life
                insurance company taxable income.''.

SEC. 3704. ADJUSTMENT FOR CHANGE IN COMPUTING RESERVES.

    (a) In General.--Paragraph (1) of section 807(f) is amended to read
as follows:
            ``(1) Treatment as change in method of accounting.--If the
        basis for determining any item referred to in subsection (c) as
        of the close of any taxable year differs from the basis for
        such determination as of the close of the preceding taxable
        year, then so much of the difference between--
                    ``(A) the amount of the item at the close of the
                taxable year, computed on the new basis, and
                    ``(B) the amount of the item at the close of the
                taxable year, computed on the old basis,
        as is attributable to contracts issued before the taxable year
        shall be taken into account under section 481 as adjustments
        attributable to a change in method of accounting initiated by
        the taxpayer and made with the consent of the Secretary.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3705. REPEAL OF SPECIAL RULE FOR DISTRIBUTIONS TO SHAREHOLDERS
              FROM PRE-1984 POLICYHOLDERS SURPLUS ACCOUNT.

    (a) In General.--Subpart D of part I of subchapter L is amended by
striking section 815 (and by striking the item relating to such section
in the table of sections for such subpart).
    (b) Conforming Amendment.--Section 801 is amended by striking
subsection (c).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.
    (d) Phased Inclusion of Remaining Balance of Policyholders Surplus
Accounts.--In the case of any stock life insurance company which has a
balance (determined as of the close of such company's last taxable year
beginning before January 1, 2018) in an existing policyholders surplus
account (as defined in section 815 of the Internal Revenue Code of
1986, as in effect before its repeal), the tax imposed by section 801
of such Code for the first 8 taxable years beginning after December 31,
2017, shall be the amount which would be imposed by such section for
such year on the sum of--
            (1) life insurance company taxable income for such year
        (within the meaning of such section 801 but not less than
        zero), plus
            (2) \1/8\ of such balance.

SEC. 3706. MODIFICATION OF PRORATION RULES FOR PROPERTY AND CASUALTY
              INSURANCE COMPANIES.

    (a) In General.--Section 832(b)(5)(B) is amended by striking ``15
percent'' and inserting ``26.25 percent''.
    (b) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

SEC. 3707. MODIFICATION OF DISCOUNTING RULES FOR PROPERTY AND CASUALTY
              INSURANCE COMPANIES.

    (a) Modification of Rate of Interest Used to Discount Unpaid
Losses.--Paragraph (2) of section 846(c) is amended to read as follows:
            ``(2) Determination of annual rate.--The annual rate
        determined by the Secretary under this paragraph for any
        calendar year shall be a rate determined on the basis of the
        corporate bond yield curve (as defined in section
        430(h)(2)(D)(i)).''.
    (b) Modification of Computational Rules for Loss Payment
Patterns.--Section 846(d)(3) is amended by striking subparagraphs (B)
through (G) and inserting the following new subparagraphs:
                    ``(B) Treatment of certain losses.--Losses which
                would have been treated as paid in the last year of the
                period applicable under subparagraph (A)(i) or (A)(ii)
                shall be treated as paid in the following manner:
                            ``(i) 3-year loss payment pattern.--
                                    ``(I) In general.--The period taken
                                into account under subparagraph (A)(i)
                                shall be extended to the extent
                                required under subclause (II).
                                    ``(II) Computation of extension.--
                                The amount of losses which would have
                                been treated as paid in the 3d year
                                after the accident year shall be
                                treated as paid in such 3d year and
                                each subsequent year in an amount equal
                                to the average of the losses treated as
                                paid in the 1st and 2d years after the
                                accident year (or, if lesser, the
                                portion of the unpaid losses not
                                theretofore taken into account). To the
                                extent such unpaid losses have not been
                                treated as paid before the 18th year
                                after the accident year, they shall be
                                treated as paid in such 18th year.
                            ``(ii) 10-year loss payment pattern.--
                                    ``(I) In general.--The period taken
                                into account under subparagraph (A)(ii)
                                shall be extended to the extent
                                required under subclause (II).
                                    ``(II) Computation of extension.--
                                The amount of losses which would have
                                been treated as paid in the 10th year
                                after the accident year shall be
                                treated as paid in such 10th year and
                                each subsequent year in an amount equal
                                to the amount of the average of the
                                losses treated as paid in the 7th, 8th,
                                and 9th years after the accident year
                                (or, if lesser, the portion of the
                                unpaid losses not theretofore taken
                                into account). To the extent such
                                unpaid losses have not been treated as
                                paid before the 25th year after the
                                accident year, they shall be treated as
                                paid in such 25th year.''.
    (c) Repeal of Historical Payment Pattern Election.--Section 846 is
amended by striking subsection (e) and by redesignating subsections (f)
and (g) as subsections (e) and (f), respectively.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.
    (e) Transitional Rule.--For the first taxable year beginning after
December 31, 2017--
            (1) the unpaid losses and the expenses unpaid (as defined
        in paragraphs (5)(B) and (6) of section 832(b) of the Internal
        Revenue Code of 1986) at the end of the preceding taxable year,
        and
            (2) the unpaid losses as defined in sections 807(c)(2) and
        805(a)(1) of such Code at the end of the preceding taxable
        year,
shall be determined as if the amendments made by this section had
applied to such unpaid losses and expenses unpaid in the preceding
taxable year and by using the interest rate and loss payment patterns
applicable to accident years ending with calendar year 2018, and any
adjustment shall be taken into account ratably in such first taxable
year and the 7 succeeding taxable years. For subsequent taxable years,
such amendments shall be applied with respect to such unpaid losses and
expenses unpaid by using the interest rate and loss payment patterns
applicable to accident years ending with calendar year 2018.

SEC. 3708. REPEAL OF SPECIAL ESTIMATED TAX PAYMENTS.

    (a) In General.--Part III of subchapter L of chapter 1 is amended
by striking section 847 (and by striking the item relating to such
section in the table of sections for such part).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

                        Subtitle I--Compensation

SEC. 3801. MODIFICATION OF LIMITATION ON EXCESSIVE EMPLOYEE
              REMUNERATION.

    (a) Repeal of Performance-based Compensation and Commission
Exceptions for Limitation on Excessive Employee Remuneration.--
            (1) In general.--Section 162(m)(4) is amended by striking
        subparagraphs (B) and (C) and by redesignating subparagraphs
        (D), (E), (F), and (G) as subparagraphs (B), (C), (D), and (E),
        respectively.
            (2) Conforming amendments.--
                    (A) Paragraphs (5)(E) and (6)(D) of section 162(m)
                are each amended by striking ``subparagraphs (B), (C),
                and (D)'' and inserting ``subparagraph (B)''.
                    (B) Paragraphs (5)(G) and (6)(G) of section 162(m)
                are each amended by striking ``(F) and (G)'' and
                inserting ``(D) and (E)''.
    (b) Expansion of Applicable Employer.--Section 162(m)(2) is amended
to read as follows:
            ``(2) Publicly held corporation.--For purposes of this
        subsection, the term `publicly held corporation' means any
        corporation which is an issuer (as defined in section 3 of the
        Securities Exchange Act of 1934 (15 U.S.C. 78c))--
                    ``(A) the securities of which are required to be
                registered under section 12 of such Act (15 U.S.C.
                78l), or
                    ``(B) that is required to file reports under
                section 15(d) of such Act (15 U.S.C. 78o(d)).''.
    (c) Modification of Definition of Covered Employees.--Section
162(m)(3) is amended--
            (1) in subparagraph (A), by striking ``as of the close of
        the taxable year, such employee is the chief executive officer
        of the taxpayer or is'' and inserting ``such employee is the
        principal executive officer or principal financial officer of
        the taxpayer at any time during the taxable year, or was'',
            (2) in subparagraph (B)--
                    (A) by striking ``4'' and inserting ``3'', and
                    (B) by striking ``(other than the chief executive
                officer)'' and inserting ``(other than the principal
                executive officer or principal financial officer)'',
                and
            (3) by striking ``or'' at the end of subparagraph (A), by
        striking the period at the end of subparagraph (B) and
        inserting ``, or'', and by adding at the end the following:
                    ``(C) was a covered employee of the taxpayer (or
                any predecessor) for any preceding taxable year
                beginning after December 31, 2016.
        Such term shall include any employee who would be described in
        subparagraph (B) if the reporting described in such
        subparagraph were required as so described.''.
    (d) Special Rule for Remuneration Paid to Beneficiaries, etc.--
Section 162(m)(4), as amended by subsection (a), is amended by adding
at the end the following new subparagraph:
                    ``(F) Special rule for remuneration paid to
                beneficiaries, etc.--Remuneration shall not fail to be
                applicable employee remuneration merely because it is
                includible in the income of, or paid to, a person other
                than the covered employee, including after the death of
                the covered employee.''.
    (e) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3802. EXCISE TAX ON EXCESS TAX-EXEMPT ORGANIZATION EXECUTIVE
              COMPENSATION.

    (a) In General.--Subchapter D of chapter 42 is amended by adding at
the end the following new section:

``SEC. 4960. TAX ON EXCESS TAX-EXEMPT ORGANIZATION EXECUTIVE
              COMPENSATION.

    ``(a) Tax Imposed.--There is hereby imposed a tax equal to 20
percent of the sum of--
            ``(1) so much of the remuneration paid (other than any
        excess parachute payment) by an applicable tax-exempt
        organization for the taxable year with respect to employment of
        any covered employee in excess of $1,000,000, plus
            ``(2) any excess parachute payment paid by such an
        organization to any covered employee.
    ``(b) Liability for Tax.--The employer shall be liable for the tax
imposed under subsection (a).
    ``(c) Definitions and Special Rules.--For purposes of this
section--
            ``(1) Applicable tax-exempt organization.--The term
        `applicable tax-exempt organization' means any organization
        that for the taxable year--
                    ``(A) is exempt from taxation under section 501(a),
                    ``(B) is a farmers' cooperative organization
                described in section 521(b)(1),
                    ``(C) has income excluded from taxation under
                section 115(1), or
                    ``(D) is a political organization described in
                section 527(e)(1).
            ``(2) Covered employee.--For purposes of this section, the
        term `covered employee' means any employee (including any
        former employee) of an applicable tax-exempt organization if
        the employee--
                    ``(A) is one of the 5 highest compensated employees
                of the organization for the taxable year, or
                    ``(B) was a covered employee of the organization
                (or any predecessor) for any preceding taxable year
                beginning after December 31, 2016.
            ``(3) Remuneration.--For purposes of this section, the term
        `remuneration' means wages (as defined in section 3401(a)),
        except that such term shall not include any designated Roth
        contribution (as defined in section 402A(c)).
            ``(4) Remuneration from related organizations.--
                    ``(A) In general.--Remuneration of a covered
                employee paid by an applicable tax-exempt organization
                shall include any remuneration paid with respect to
                employment of such employee by any related person or
                governmental entity.
                    ``(B) Related organizations.--A person or
                governmental entity shall be treated as related to an
                applicable tax-exempt organization if such person or
                governmental entity--
                            ``(i) controls, or is controlled by, the
                        organization,
                            ``(ii) is controlled by one or more persons
                        that control the organization,
                            ``(iii) is a supported organization (as
                        defined in section 509(f)(2)) during the
                        taxable year with respect to the organization,
                            ``(iv) is a supporting organization
                        described in section 509(a)(3) during the
                        taxable year with respect to the organization,
                        or
                            ``(v) in the case of an organization that
                        is a voluntary employees' beneficiary
                        association described in section 501(a)(9),
                        establishes, maintains, or makes contributions
                        to such voluntary employees' beneficiary
                        association.
                    ``(C) Liability for tax.--In any case in which
                remuneration from more than one employer is taken into
                account under this paragraph in determining the tax
                imposed by subsection (a), each such employer shall be
                liable for such tax in an amount which bears the same
                ratio to the total tax determined under subsection (a)
                with respect to such remuneration as--
                            ``(i) the amount of remuneration paid by
                        such employer with respect to such employee,
                        bears to
                            ``(ii) the amount of remuneration paid by
                        all such employers to such employee.
            ``(5) Excess parachute payment.--For purposes determining
        the tax imposed by subsection (a)(2)--
                    ``(A) In general.--The term `excess parachute
                payment' means an amount equal to the excess of any
                parachute payment over the portion of the base amount
                allocated to such payment.
                    ``(B) Parachute payment.--The term `parachute
                payment' means any payment in the nature of
                compensation to (or for the benefit of) a covered
                employee if--
                            ``(i) such payment is contingent on such
                        employee's separation from employment with the
                        employer, and
                            ``(ii) the aggregate present value of the
                        payments in the nature of compensation to (or
                        for the benefit of) such individual which are
                        contingent on such separation equals or exceeds
                        an amount equal to 3 times the base amount.
                Such term does not include any payment described in
                section 280G(b)(6) (relating to exemption for payments
                under qualified plans) or any payment made under or to
                an annuity contract described in section 403(b) or a
                plan described in section 457(b).
                    ``(C) Base amount.--Rules similar to the rules of
                280G(b)(3) shall apply for purposes of determining the
                base amount.
                    ``(D) Property transfers; present value.--Rules
                similar to the rules of paragraphs (3) and (4) of
                section 280G(d) shall apply.
            ``(6) Coordination with deduction limitation.--Remuneration
        the deduction for which is not allowed by reason of section
        162(m) shall not be taken into account for purposes of this
        section.
    ``(d) Regulations.--The Secretary shall prescribe such regulations
as may be necessary to prevent avoidance of the purposes of this
section through the performance of services other than as an
employee.''.
    (b) Clerical Amendment.--The table of sections for subchapter D of
chapter 42 is amended by adding at the end the following new item:

``Sec. 4960. Tax on excess exempt organization executive
                            compensation.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 3803. TREATMENT OF QUALIFIED EQUITY GRANTS.

    (a) In General.--
            (1) Election to defer income.--Section 83 is amended by
        adding at the end the following new subsection:
    ``(i) Qualified Equity Grants.--
            ``(1) In general.--For purposes of this subtitle, if
        qualified stock is transferred to a qualified employee who
        makes an election with respect to such stock under this
        subsection--
                    ``(A) except as provided in subparagraph (B), no
                amount shall be included in income under subsection (a)
                for the first taxable year in which the rights of the
                employee in such stock are transferable or are not
                subject to a substantial risk of forfeiture, whichever
                is applicable, and
                    ``(B) an amount equal to the amount which would be
                included in income of the employee under subsection (a)
                (determined without regard to this subsection) shall be
                included in income for the taxable year of the employee
                which includes the earliest of--
                            ``(i) the first date such qualified stock
                        becomes transferable (including transferable to
                        the employer),
                            ``(ii) the date the employee first becomes
                        an excluded employee,
                            ``(iii) the first date on which any stock
                        of the corporation which issued the qualified
                        stock becomes readily tradable on an
                        established securities market (as determined by
                        the Secretary, but not including any market
                        unless such market is recognized as an
                        established securities market by the Secretary
                        for purposes of a provision of this title other
                        than this subsection),
                            ``(iv) the date that is 5 years after the
                        first date the rights of the employee in such
                        stock are transferable or are not subject to a
                        substantial risk of forfeiture, whichever
                        occurs earlier, or
                            ``(v) the date on which the employee
                        revokes (at such time and in such manner as the
                        Secretary may provide) the election under this
                        subsection with respect to such stock.
            ``(2) Qualified stock.--
                    ``(A) In general.--For purposes of this subsection,
                the term `qualified stock' means, with respect to any
                qualified employee, any stock in a corporation which is
                the employer of such employee, if--
                            ``(i) such stock is received--
                                    ``(I) in connection with the
                                exercise of an option, or
                                    ``(II) in settlement of a
                                restricted stock unit, and
                            ``(ii) such option or restricted stock unit
                        was provided by the corporation--
                                    ``(I) in connection with the
                                performance of services as an employee,
                                and
                                    ``(II) during a calendar year in
                                which such corporation was an eligible
                                corporation.
                    ``(B) Limitation.--The term `qualified stock' shall
                not include any stock if the employee may sell such
                stock to, or otherwise receive cash in lieu of stock
                from, the corporation at the time that the rights of
                the employee in such stock first become transferable or
                not subject to a substantial risk of forfeiture.
                    ``(C) Eligible corporation.--For purposes of
                subparagraph (A)(ii)(II)--
                            ``(i) In general.--The term `eligible
                        corporation' means, with respect to any
                        calendar year, any corporation if--
                                    ``(I) no stock of such corporation
                                (or any predecessor of such
                                corporation) is readily tradable on an
                                established securities market (as
                                determined under paragraph (1)(B)(iii))
                                during any preceding calendar year, and
                                    ``(II) such corporation has a
                                written plan under which, in such
                                calendar year, not less than 80 percent
                                of all employees who provide services
                                to such corporation in the United
                                States (or any possession of the United
                                States) are granted stock options, or
                                restricted stock units, with the same
                                rights and privileges to receive
                                qualified stock.
                            ``(ii) Same rights and privileges.--For
                        purposes of clause (i)(II)--
                                    ``(I) except as provided in
                                subclauses (II) and (III), the
                                determination of rights and privileges
                                with respect to stock shall be
                                determined in a similar manner as
                                provided under section 423(b)(5),
                                    ``(II) employees shall not fail to
                                be treated as having the same rights
                                and privileges to receive qualified
                                stock solely because the number of
                                shares available to all employees is
                                not equal in amount, so long as the
                                number of shares available to each
                                employee is more than a de minimis
                                amount, and
                                    ``(III) rights and privileges with
                                respect to the exercise of an option
                                shall not be treated as the same as
                                rights and privileges with respect to
                                the settlement of a restricted stock
                                unit.
                            ``(iii) Employee.--For purposes of clause
                        (i)(II), the term `employee' shall not include
                        any employee described in section 4980E(d)(4)
                        or any excluded employee.
                            ``(iv) Special rule for calendar years
                        before 2018.--In the case of any calendar year
                        beginning before January 1, 2018, clause
                        (i)(II) shall be applied without regard to
                        whether the rights and privileges with respect
                        to the qualified stock are the same.
            ``(3) Qualified employee; excluded employee.--For purposes
        of this subsection--
                    ``(A) In general.--The term `qualified employee'
                means any individual who--
                            ``(i) is not an excluded employee, and
                            ``(ii) agrees in the election made under
                        this subsection to meet such requirements as
                        determined by the Secretary to be necessary to
                        ensure that the withholding requirements of the
                        corporation under chapter 24 with respect to
                        the qualified stock are met.
                    ``(B) Excluded employee.--The term `excluded
                employee' means, with respect to any corporation, any
                individual--
                            ``(i) who was a 1-percent owner (within the
                        meaning of section 416(i)(1)(B)(ii)) at any
                        time during the 10 preceding calendar years,
                            ``(ii) who is or has been at any prior
                        time--
                                    ``(I) the chief executive officer
                                of such corporation or an individual
                                acting in such a capacity, or
                                    ``(II) the chief financial officer
                                of such corporation or an individual
                                acting in such a capacity,
                            ``(iii) who bears a relationship described
                        in section 318(a)(1) to any individual
                        described in subclause (I) or (II) of clause
                        (ii), or
                            ``(iv) who has been for any of the 10
                        preceding taxable years one of the 4 highest
                        compensated officers of such corporation
                        determined with respect to each such taxable
                        year on the basis of the shareholder disclosure
                        rules for compensation under the Securities
                        Exchange Act of 1934 (as if such rules applied
                        to such corporation).
            ``(4) Election.--
                    ``(A) Time for making election.--An election with
                respect to qualified stock shall be made under this
                subsection no later than 30 days after the first time
                the rights of the employee in such stock are
                transferable or are not subject to a substantial risk
                of forfeiture, whichever occurs earlier, and shall be
                made in a manner similar to the manner in which an
                election is made under subsection (b).
                    ``(B) Limitations.--No election may be made under
                this section with respect to any qualified stock if--
                            ``(i) the qualified employee has made an
                        election under subsection (b) with respect to
                        such qualified stock,
                            ``(ii) any stock of the corporation which
                        issued the qualified stock is readily tradable
                        on an established securities market (as
                        determined under paragraph (1)(B)(iii)) at any
                        time before the election is made, or
                            ``(iii) such corporation purchased any of
                        its outstanding stock in the calendar year
                        preceding the calendar year which includes the
                        first time the rights of the employee in such
                        stock are transferable or are not subject to a
                        substantial risk of forfeiture, unless--
                                    ``(I) not less than 25 percent of
                                the total dollar amount of the stock so
                                purchased is deferral stock, and
                                    ``(II) the determination of which
                                individuals from whom deferral stock is
                                purchased is made on a reasonable
                                basis.
                    ``(C) Definitions and special rules related to
                limitation on stock redemptions.--
                            ``(i) Deferral stock.--For purposes of this
                        paragraph, the term `deferral stock' means
                        stock with respect to which an election is in
                        effect under this subsection.
                            ``(ii) Deferral stock with respect to any
                        individual not taken into account if individual
                        holds deferral stock with longer deferral
                        period.--Stock purchased by a corporation from
                        any individual shall not be treated as deferral
                        stock for purposes of clause (iii) if such
                        individual (immediately after such purchase)
                        holds any deferral stock with respect to which
                        an election has been in effect under this
                        subsection for a longer period than the
                        election with respect to the stock so
                        purchased.
                            ``(iii) Purchase of all outstanding
                        deferral stock.--The requirements of subclauses
                        (I) and (II) of subparagraph (B)(iii) shall be
                        treated as met if the stock so purchased
                        includes all of the corporation's outstanding
                        deferral stock.
                            ``(iv) Reporting.--Any corporation which
                        has outstanding deferral stock as of the
                        beginning of any calendar year and which
                        purchases any of its outstanding stock during
                        such calendar year shall include on its return
                        of tax for the taxable year in which, or with
                        which, such calendar year ends the total dollar
                        amount of its outstanding stock so purchased
                        during such calendar year and such other
                        information as the Secretary may require for
                        purposes of administering this paragraph.
            ``(5) Controlled groups.--For purposes of this subsection,
        all corporations which are members of the same controlled group
        of corporations (as defined in section 1563(a)) shall be
        treated as one corporation.
            ``(6) Notice requirement.--Any corporation that transfers
        qualified stock to a qualified employee shall, at the time that
        (or a reasonable period before) an amount attributable to such
        stock would (but for this subsection) first be includible in
        the gross income of such employee--
                    ``(A) certify to such employee that such stock is
                qualified stock, and
                    ``(B) notify such employee--
                            ``(i) that the employee may elect to defer
                        income on such stock under this subsection, and
                            ``(ii) that, if the employee makes such an
                        election--
                                    ``(I) the amount of income
                                recognized at the end of the deferral
                                period will be based on the value of
                                the stock at the time at which the
                                rights of the employee in such stock
                                first become transferable or not
                                subject to substantial risk of
                                forfeiture, notwithstanding whether the
                                value of the stock has declined during
                                the deferral period,
                                    ``(II) the amount of such income
                                recognized at the end of the deferral
                                period will be subject to withholding
                                under section 3401(i) at the rate
                                determined under section 3402(t), and
                                    ``(III) the responsibilities of the
                                employee (as determined by the
                                Secretary under paragraph (3)(A)(ii))
                                with respect to such withholding.
            ``(7) Restricted stock units.--This section (other than
        this subsection), including any election under subsection (b),
        shall not apply to restricted stock units.''.
            (2) Deduction by employer.--Subsection (h) of section 83 is
        amended by striking ``or (d)(2)'' and inserting ``(d)(2), or
        (i)''.
    (b) Withholding.--
            (1) Time of withholding.--Section 3401 is amended by adding
        at the end the following new subsection:
    ``(i) Qualified Stock for Which an Election Is in Effect Under
Section 83(i).--For purposes of subsection (a), qualified stock (as
defined in section 83(i)) with respect to which an election is made
under section 83(i) shall be treated as wages--
            ``(1) received on the earliest date described in section
        83(i)(1)(B), and
            ``(2) in an amount equal to the amount included in income
        under section 83 for the taxable year which includes such
        date.''.
            (2) Amount of withholding.--Section 3402 is amended by
        adding at the end the following new subsection:
    ``(t) Rate of Withholding for Certain Stock.--In the case of any
qualified stock (as defined in section 83(i)) with respect to which an
election is made under section 83(i)--
            ``(1) the rate of tax under subsection (a) shall not be
        less than the maximum rate of tax in effect under section 1,
        and
            ``(2) such stock shall be treated for purposes of section
        3501(b) in the same manner as a non-cash fringe benefit.''.
    (c) Coordination With Other Deferred Compensation Rules.--
            (1) Election to apply deferral to statutory options.--
                    (A) Incentive stock options.--Section 422(b) is
                amended by adding at the end the following: ``Such term
                shall not include any option if an election is made
                under section 83(i) with respect to the stock received
                in connection with the exercise of such option.''.
                    (B) Employee stock purchase plans.--Section 423(a)
                is amended by adding at the end the following flush
                sentence:
``The preceding sentence shall not apply to any share of stock with
respect to which an election is made under section 83(i).''.
            (2) Exclusion from definition of nonqualified deferred
        compensation plan.--Subsection (d) of section 409A is amended
        by adding at the end the following new paragraph:
            ``(7) Treatment of qualified stock.--An arrangement under
        which an employee may receive qualified stock (as defined in
        section 83(i)(2)) shall not be treated as a nonqualified
        deferred compensation plan solely because of an employee's
        election, or ability to make an election, to defer recognition
        of income under section 83(i).''.
    (d) Information Reporting.--Section 6051(a) is amended by striking
``and'' at the end of paragraph (13), by striking the period at the end
of paragraph (14) and inserting a comma, and by inserting after
paragraph (14) the following new paragraphs:
            ``(15) the amount excludable from gross income under
        subparagraph (A) of section 83(i)(1),
            ``(16) the amount includible in gross income under
        subparagraph (B) of section 83(i)(1) with respect to an event
        described in such subparagraph which occurs in such calendar
        year, and
            ``(17) the aggregate amount of income which is being
        deferred pursuant to elections under section 83(i), determined
        as of the close of the calendar year.''.
    (e) Penalty for Failure of Employer To Provide Notice of Tax
Consequences.--Section 6652 is amended by adding at the end the
following new subsection:
    ``(o) Failure to Provide Notice Under Section 83(i).--In the case
of each failure to provide a notice as required by section 83(i)(6), at
the time prescribed therefor, unless it is shown that such failure is
due to reasonable cause and not to willful neglect, there shall be
paid, on notice and demand of the Secretary and in the same manner as
tax, by the person failing to provide such notice, an amount equal to
$100 for each such failure, but the total amount imposed on such person
for all such failures during any calendar year shall not exceed
$50,000.''.
    (f) Effective Dates.--
            (1) In general.--Except as provided in paragraph (2), the
        amendments made by this section shall apply to stock
        attributable to options exercised, or restricted stock units
        settled, after December 31, 2017.
            (2) Requirement to provide notice.--The amendments made by
        subsection (e) shall apply to failures after December 31, 2017.
    (g) Transition Rule.--Until such time as the Secretary (or the
Secretary's delegate) issue regulations or other guidance for purposes
of implementing the requirements of paragraph (2)(C)(i)(II) of section
83(i) of the Internal Revenue Code of 1986 (as added by this section),
or the requirements of paragraph (6) of such section, a corporation
shall be treated as being in compliance with such requirements
(respectively) if such corporation complies with a reasonable good
faith interpretation of such requirements.

        TITLE IV--TAXATION OF FOREIGN INCOME AND FOREIGN PERSONS

    Subtitle A--Establishment of Participation Exemption System for
                       Taxation of Foreign Income

SEC. 4001. DEDUCTION FOR FOREIGN-SOURCE PORTION OF DIVIDENDS RECEIVED
              BY DOMESTIC CORPORATIONS FROM SPECIFIED 10-PERCENT OWNED
              FOREIGN CORPORATIONS.

    (a) In General.--Part VIII of subchapter B of chapter 1 is amended
by inserting after section 245 the following new section:

``SEC. 245A. DEDUCTION FOR FOREIGN-SOURCE PORTION OF DIVIDENDS RECEIVED
              BY DOMESTIC CORPORATIONS FROM SPECIFIED 10-PERCENT OWNED
              FOREIGN CORPORATIONS.

    ``(a) In General.--In the case of any dividend received from a
specified 10-percent owned foreign corporation by a domestic
corporation which is a United States shareholder with respect to such
foreign corporation, there shall be allowed as a deduction an amount
equal to the foreign-source portion of such dividend.
    ``(b) Specified 10-percent Owned Foreign Corporation.--For purposes
of this section, the term `specified 10-percent owned foreign
corporation' means any foreign corporation with respect to which any
domestic corporation is a United States shareholder. Such term shall
not include any passive foreign investment company (within the meaning
of subpart D of part VI of subchapter P) that is not a controlled
foreign corporation.
    ``(c) Foreign-source Portion.--For purposes of this section--
            ``(1) In general.--The foreign-source portion of any
        dividend is an amount which bears the same ratio to such
        dividend as--
                    ``(A) the post-1986 undistributed foreign earnings
                of the specified 10-percent owned foreign corporation,
                bears to
                    ``(B) the total post-1986 undistributed earnings of
                such foreign corporation.
            ``(2) Post-1986 undistributed earnings.--The term `post-
        1986 undistributed earnings' means the amount of the earnings
        and profits of the specified 10-percent owned foreign
        corporation (computed in accordance with sections 964(a) and
        986) accumulated in taxable years beginning after December 31,
        1986--
                    ``(A) as of the close of the taxable year of the
                specified 10-percent owned foreign corporation in which
                the dividend is distributed, and
                    ``(B) without diminution by reason of dividends
                distributed during such taxable year.
            ``(3) Post-1986 undistributed foreign earnings.--The term
        `post-1986 undistributed foreign earnings' means the portion of
        the post-1986 undistributed earnings which is attributable to
        neither--
                    ``(A) income described in subparagraph (A) of
                section 245(a)(5), nor
                    ``(B) dividends described in subparagraph (B) of
                such section (determined without regard to section
                245(a)(12)).
            ``(4) Treatment of distributions from earnings before
        1987.--
                    ``(A) In general.--In the case of any dividend paid
                out of earnings and profits of the specified 10-percent
                owned foreign corporation (computed in accordance with
                sections 964(a) and 986) accumulated in taxable years
                beginning before January 1, 1987--
                            ``(i) paragraphs (1), (2), and (3) shall be
                        applied without regard to the phrase `post-
                        1986' each place it appears, and
                            ``(ii) paragraph (2) shall be applied by
                        substituting `after the date specified in
                        section 316(a)(1)' for `in taxable years
                        beginning after December 31, 1986'.
                    ``(B) Dividends paid first out of post-1986
                earnings.--Dividends shall be treated as paid out of
                post-1986 undistributed earnings to the extent thereof.
            ``(5) Treatment of certain dividends in excess of
        undistributed earnings.--In the case of any dividend from the
        specified 10-percent owned foreign corporation which is in
        excess of undistributed earnings (as determined under paragraph
        (2) after taking into account the modifications described in
        clauses (i) and (ii) of paragraph (4)(A)), the foreign-source
        portion of such dividend is an amount which bears the same
        ratio to such dividend as--
                    ``(A) the portion of the earnings and profits
                described in subparagraph (B) which is attributable to
                neither income described in paragraph (3)(A) nor
                dividends described in paragraph (3)(B), bears to
                    ``(B) the earnings and profits of such corporation
                for the taxable year in which such distribution is made
                (computed as of the close of the taxable year without
                diminution by reason of any distributions made during
                the taxable year).
    ``(d) Disallowance of Foreign Tax Credit, etc.--
            ``(1) In general.--No credit shall be allowed under section
        901 for any taxes paid or accrued (or treated as paid or
        accrued) with respect to any dividend for which a deduction is
        allowed under this section.
            ``(2) Denial of deduction.--No deduction shall be allowed
        under this chapter for any tax for which credit is not
        allowable under section 901 by reason of paragraph (1)
        (determined by treating the taxpayer as having elected the
        benefits of subpart A of part III of subchapter N).
    ``(e) Regulations.--The Secretary may prescribe such regulations or
other guidance as may be necessary or appropriate to carry out the
provisions of this section.''.
    (b) Application of Holding Period Requirement.--Section 246(c) is
amended--
            (1) by striking ``or 245'' in paragraph (1) and inserting
        ``245, or 245A'', and
            (2) by adding at the end the following new paragraph:
            ``(5) Special rules for foreign source portion of dividends
        received from specified 10-percent owned foreign
        corporations.--
                    ``(A) 6-month holding period requirement.--For
                purposes of section 245A--
                            ``(i) paragraph (1)(A) shall be applied--
                                    ``(I) by substituting `180 days'
                                for `45 days'each place it appears, and
                                    ``(II) by substituting `361-day
                                period' for `91-day period', and
                            ``(ii) paragraph (2) shall not apply.
                    ``(B) Status must be maintained during holding
                period.--For purposes of applying paragraph (1) with
                respect to section 245A, the taxpayer shall be treated
                as holding the stock referred to in paragraph (1) for
                any period only if--
                            ``(i) the specified 10-percent owned
                        foreign corporation referred to in section
                        245A(a) is a specified 10-percent owned foreign
                        corporation for such period, and
                            ``(ii) the taxpayer is a United States
                        shareholder with respect to such specified 10-
                        percent owned foreign corporation for such
                        period.''.
    (c) Application of Rules Generally Applicable to Deductions for
Dividends Received.--
            (1) Treatment of dividends from certain corporations.--
        Section 246(a)(1) is amended by striking ``and 245'' and
        inserting ``245, and 245A''.
            (2) Coordination with section 1059.--Section 1059(b)(2)(B)
        is amended by striking ``or 245'' and inserting ``245, or
        245A''.
    (d) Coordination With Foreign Tax Credit Limitation.--Section
904(b) is amended by adding at the end the following new paragraph:
            ``(5) Treatment of dividends for which deduction is allowed
        under section 245a.--For purposes of subsection (a), in the
        case of a United States shareholder with respect to a specified
        10-percent owned foreign corporation, such shareholder's
        taxable income from sources without the United States (and
        entire taxable income) shall be determined without regard to--
                    ``(A) the foreign-source portion of any dividend
                received from such foreign corporation, and
                    ``(B) any deductions properly allocable or
                apportioned to--
                            ``(i) income (other than subpart F income
                        (as defined in section 952) and foreign high
                        return amounts (as defined in section 951A(b))
                        with respect to stock of such specified 10-
                        percent owned foreign corporation, or
                            ``(ii) such stock (to the extent income
                        with respect to such stock is other than
                        subpart F income (as so defined) or foreign
                        high return amounts (as so defined)).
        Any term which is used in section 245A and in this paragraph
        shall have the same meaning for purposes of this paragraph as
        when used in such section.''.
    (e) Conforming Amendments.--
            (1) Section 245(a)(4) is amended by striking ``section
        902(c)(1)'' and inserting ``section 245A(c)(2) applied by
        substituting `qualified 10-percent owned foreign corporation'
        for `specified 10-percent owned foreign corporation' each place
        it appears''.
            (2) Section 951(b) is amended by striking ``subpart'' and
        inserting ``title''.
            (3) Section 957(a) is amended by striking ``subpart'' in
        the matter preceding paragraph (1) and inserting ``title''.
            (4) The table of sections for part VIII of subchapter B of
        chapter 1 is amended by inserting after section 245 the
        following new item:

``Sec. 245A. Deduction for foreign-source portion of dividends received
                            by domestic corporations from specified 10-
                            percent owned foreign corporations.''.
    (f) Effective Date.--The amendments made by this section shall
apply to distributions made after (and, in the case of the amendments
made by subsection (d), deductions with respect to taxable years ending
after) December 31, 2017.

SEC. 4002. APPLICATION OF PARTICIPATION EXEMPTION TO INVESTMENTS IN
              UNITED STATES PROPERTY.

    (a) In General.--Section 956(a) is amended in the matter preceding
paragraph (1) by inserting ``(other than a corporation)'' after
``United States shareholder''.
    (b) Regulatory Authority to Prevent Abuse.--Section 956(e) is
amended by striking ``including regulations to prevent'' and inserting
``including regulations--
            ``(1) to address United States shareholders that are
        partnerships with corporate partners, and
            ``(2) to prevent''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017.

SEC. 4003. LIMITATION ON LOSSES WITH RESPECT TO SPECIFIED 10-PERCENT
              OWNED FOREIGN CORPORATIONS.

    (a) Basis in Specified 10-percent Owned Foreign Corporation Reduced
by Nontaxed Portion of Dividend for Purposes of Determining Loss.--
            (1) In general.--Section 961 is amended by adding at the
        end the following new subsection:
    ``(d) Basis in Specified 10-percent Owned Foreign Corporation
Reduced by Nontaxed Portion of Dividend for Purposes of Determining
Loss.--If a domestic corporation received a dividend from a specified
10-percent owned foreign corporation (as defined in section 245A) in
any taxable year, solely for purposes of determining loss on any
disposition of stock of such foreign corporation in such taxable year
or any subsequent taxable year, the basis of such domestic corporation
in such stock shall be reduced (but not below zero) by the amount of
any deduction allowable to such domestic corporation under section 245A
with respect to such stock except to the extent such basis was reduced
under section 1059 by reason of a dividend for which such a deduction
was allowable.''.
            (2) Effective date.--The amendments made by this subsection
        shall apply to distributions made after December 31, 2017.
    (b) Treatment of Foreign Branch Losses Transferred to Specified 10-
percent Owned Foreign Corporations.--
            (1) In general.--Part II of subchapter B of chapter 1 is
        amended by adding at the end the following new section:

``SEC. 91. CERTAIN FOREIGN BRANCH LOSSES TRANSFERRED TO SPECIFIED 10-
              PERCENT OWNED FOREIGN CORPORATIONS.

    ``(a) In General.--If a domestic corporation transfers
substantially all of the assets of a foreign branch (within the meaning
of section 367(a)(3)(C)) to a specified 10-percent owned foreign
corporation (as defined in section 245A) with respect to which it is a
United States shareholder after such transfer, such domestic
corporation shall include in gross income for the taxable year which
includes such transfer an amount equal to the transferred loss amount
with respect to such transfer.
    ``(b) Transferred Loss Amount.--For purposes of this section, the
term `transferred loss amount' means, with respect to any transfer of
substantially all of the assets of a foreign branch, the excess (if
any) of--
            ``(1) the sum of losses--
                    ``(A) which were incurred by the foreign branch
                after December 31, 2017, and before the transfer, and
                    ``(B) with respect to which a deduction was allowed
                to the taxpayer, over
            ``(2) the sum of--
                    ``(A) any taxable income of such branch for a
                taxable year after the taxable year in which the loss
                was incurred and through the close of the taxable year
                of the transfer, and
                    ``(B) any amount which is recognized under section
                904(f)(3) on account of the transfer.
    ``(c) Reduction for Recognized Gains.--
            ``(1) In general.--In the case of a transfer not described
        in section 367(a)(3)(C), the transferred loss amount shall be
        reduced (but not below zero) by the amount of gain recognized
        by the taxpayer on account of the transfer (other than amounts
        taken into account under subsection (c)(2)(B)).
            ``(2) Coordination with recognition under section 367.--In
        the case of a transfer described in section 367(a)(3)(C), the
        transferred loss amount shall not exceed the excess (if any)
        of--
                    ``(A) the excess of the amount described in section
                367(a)(3)(C)(i) over the amount described in section
                367(a)(3)(C)(ii) with respect to such transfer, over
                    ``(B) the amount of gain recognized under section
                367(a)(3)(C) with respect to such transfer.
    ``(d) Source of Income.--Amounts included in gross income under
this section shall be treated as derived from sources within the United
States.
    ``(e) Basis Adjustments.--Consistent with such regulations or other
guidance as the Secretary may prescribe, proper adjustments shall be
made in the adjusted basis of the taxpayer's stock in the specified 10-
percent owned foreign corporation to which the transfer is made, and in
the transferee's adjusted basis in the property transferred, to reflect
amounts included in gross income under this section.''.
            (2) Amounts recognized under section 367 on transfer of
        foreign branch with previously deducted losses treated as
        united states source.--Section 367(a)(3)(C) is amended by
        striking ``outside'' in the last sentence and inserting
        ``within''.
            (3) Clerical amendment.--The table of sections for part II
        of subchapter B of chapter 1 is amended by adding at the end
        the following new item:

``Sec. 91. Certain foreign branch losses transferred to specified 10-
                            percent owned foreign corporations.''.
            (4) Effective date.--The amendments made by this subsection
        shall apply to transfers after December 31, 2017.

SEC. 4004. TREATMENT OF DEFERRED FOREIGN INCOME UPON TRANSITION TO
              PARTICIPATION EXEMPTION SYSTEM OF TAXATION.

    (a) In General.--Section 965 is amended to read as follows:

``SEC. 965. TREATMENT OF DEFERRED FOREIGN INCOME UPON TRANSITION TO
              PARTICIPATION EXEMPTION SYSTEM OF TAXATION.

    ``(a) Treatment of Deferred Foreign Income as Subpart F Income.--In
the case of the last taxable year of a deferred foreign income
corporation which begins before January 1, 2018, the subpart F income
of such foreign corporation (as otherwise determined for such taxable
year under section 952) shall be increased by the greater of--
            ``(1) the accumulated post-1986 deferred foreign income of
        such corporation determined as of November 2, 2017, or
            ``(2) the accumulated post-1986 deferred foreign income of
        such corporation determined as of December 31, 2017.
    ``(b) Reduction in Amounts Included in Gross Income of United
States Shareholders of Specified Foreign Corporations With Deficits in
Earnings and Profits.--
            ``(1) In general.--In the case of a taxpayer which is a
        United States shareholder with respect to at least one deferred
        foreign income corporation and at least one E&P deficit foreign
        corporation, the amount which would (but for this subsection)
        be taken into account under section 951(a)(1) by reason of
        subsection (a) as such United States shareholder's pro rata
        share of the subpart F income of each deferred foreign income
        corporation shall be reduced (but not below zero) by the amount
        of such United States shareholder's aggregate foreign E&P
        deficit which is allocated under paragraph (2) to such deferred
        foreign income corporation.
            ``(2) Allocation of aggregate foreign e&p deficit.--The
        aggregate foreign E&P deficit of any United States shareholder
        shall be allocated among the deferred foreign income
        corporations of such United States shareholder in an amount
        which bears the same proportion to such aggregate as--
                    ``(A) such United States shareholder's pro rata
                share of the accumulated post-1986 deferred foreign
                income of each such deferred foreign income
                corporation, bears to
                    ``(B) the aggregate of such United States
                shareholder's pro rata share of the accumulated post-
                1986 deferred foreign income of all deferred foreign
                income corporations of such United States shareholder.
            ``(3) Definitions related to e&p deficits.--For purposes of
        this subsection--
                    ``(A) Aggregate foreign e&p deficit.--The term
                `aggregate foreign E&P deficit' means, with respect to
                any United States shareholder, the aggregate of such
                shareholder's pro rata shares of the specified E&P
                deficits of the E&P deficit foreign corporations of
                such shareholder.
                    ``(B) E&P deficit foreign corporation.--The term
                `E&P deficit foreign corporation' means, with respect
                to any taxpayer, any specified foreign corporation with
                respect to which such taxpayer is a United States
                shareholder, if--
                            ``(i) such specified foreign corporation
                        has a deficit in post-1986 earnings and
                        profits, and
                            ``(ii) as of November 2, 2017--
                                    ``(I) such corporation was a
                                specified foreign corporation, and
                                    ``(II) such taxpayer was a United
                                States shareholder of such corporation.
                    ``(C) Specified e&p deficit.--The term `specified
                E&P deficit' means, with respect to any E&P deficit
                foreign corporation, the amount of the deficit referred
                to in subparagraph (B).
            ``(4) Netting among united states shareholders in same
        affiliated group.--
                    ``(A) In general.--In the case of any affiliated
                group which includes at least one E&P net surplus
                shareholder and one E&P net deficit shareholder, the
                amount which would (but for this paragraph) be taken
                into account under section 951(a)(1) by reason of
                subsection (a) by each such E&P net surplus shareholder
                shall be reduced (but not below zero) by such
                shareholder's applicable share of the affiliated
                group's aggregate unused E&P deficit.
                    ``(B) E&P net surplus shareholder.--For purposes of
                this paragraph, the term `E&P net surplus shareholder'
                means any United States shareholder which would
                (determined without regard to this paragraph) take into
                account an amount greater than zero under section
                951(a)(1) by reason of subsection (a).
                    ``(C) E&P net deficit shareholder.--For purposes of
                this paragraph, the term `E&P net deficit shareholder'
                means any United States shareholder if--
                            ``(i) the aggregate foreign E&P deficit
                        with respect to such shareholder (as defined in
                        paragraph (3)(A)), exceeds
                            ``(ii) the amount which would (but for this
                        subsection) be taken into account by such
                        shareholder under section 951(a)(1) by reason
                        of subsection (a).
                    ``(D) Aggregate unused e&p deficit.--For purposes
                of this paragraph--
                            ``(i) In general.--The term `aggregate
                        unused E&P deficit' means, with respect to any
                        affiliated group, the lesser of--
                                    ``(I) the sum of the excesses
                                described in subparagraph (C),
                                determined with respect to each E&P net
                                deficit shareholder in such group, or
                                    ``(II) the amount determined under
                                subparagraph (E)(ii).
                            ``(ii) Reduction with respect to e&p net
                        deficit shareholders which are not wholly owned
                        by the affiliated group.--If the group
                        ownership percentage of any E&P net deficit
                        shareholder is less than 100 percent, the
                        amount of the excess described in subparagraph
                        (C) which is taken into account under clause
                        (i)(I) with respect to such E&P net deficit
                        shareholder shall be such group ownership
                        percentage of such amount.
                    ``(E) Applicable share.--For purposes of this
                paragraph, the term `applicable share' means, with
                respect to any E&P net surplus shareholder in any
                affiliated group, the amount which bears the same
                proportion to such group's aggregate unused E&P deficit
                as--
                            ``(i) the product of--
                                    ``(I) such shareholder's group
                                ownership percentage, multiplied by
                                    ``(II) the amount which would (but
                                for this paragraph) be taken into
                                account under section 951(a)(1) by
                                reason of subsection (a) by such
                                shareholder, bears to
                            ``(ii) the aggregate amount determined
                        under clause (i) with respect to all E&P net
                        surplus shareholders in such group.
                    ``(F) Group ownership percentage.--For purposes of
                this paragraph, the term `group ownership percentage'
                means, with respect to any United States shareholder in
                any affiliated group, the percentage of the value of
                the stock of such United States shareholder which is
                held by other includible corporations in such
                affiliated group. Notwithstanding the preceding
                sentence, the group ownership percentage of the common
                parent of the affiliated group is 100 percent. Any term
                used in this subparagraph which is also used in section
                1504 shall have the same meaning as when used in such
                section.
    ``(c) Application of Participation Exemption to Included Income.--
            ``(1) In general.--In the case of a United States
        shareholder of a deferred foreign income corporation, there
        shall be allowed as a deduction for the taxable year in which
        an amount is included in the gross income of such United States
        shareholder under section 951(a)(1) by reason of this section
        an amount equal to the sum of--
                    ``(A) the United States shareholder's 7 percent
                rate equivalent percentage of the excess (if any) of--
                            ``(i) the amount so included as gross
                        income, over
                            ``(ii) the amount of such United States
                        shareholder's aggregate foreign cash position,
                        plus
                    ``(B) the United States shareholder's 14 percent
                rate equivalent percentage of so much of the amount
                described in subparagraph (A)(ii) as does not exceed
                the amount described in subparagraph (A)(i).
            ``(2) 7 and 14 percent rate equivalent percentages.--For
        purposes of this subsection--
                    ``(A) 7 percent rate equivalent percentage.--The
                term `7 percent rate equivalent percentage' means, with
                respect to any United States shareholder for any
                taxable year, the percentage which would result in the
                amount to which such percentage applies being subject
                to a 7 percent rate of tax determined by only taking
                into account a deduction equal to such percentage of
                such amount and the highest rate of tax specified in
                section 11 for such taxable year. In the case of any
                taxable year of a United States shareholder to which
                section 15 applies, the highest rate of tax under
                section 11 before the effective date of the change in
                rates and the highest rate of tax under section 11
                after the effective date of such change shall each be
                taken into account under the preceding sentence in the
                same proportions as the portion of such taxable year
                which is before and after such effective date,
                respectively.
                    ``(B) 14 percent rate equivalent percentage.--The
                term `14 percent rate equivalent percentage' means,
                with respect to any United States shareholder for any
                taxable year, the percentage determined under
                subparagraph (A) applied by substituting `14 percent
                rate of tax' for `7 percent rate of tax'.
            ``(3) Aggregate foreign cash position.--For purposes of
        this subsection--
                    ``(A) In general.--The term `aggregate foreign cash
                position' means, with respect to any United States
                shareholder, one-third of the sum of--
                            ``(i) the aggregate of such United States
                        shareholder's pro rata share of the cash
                        position of each specified foreign corporation
                        of such United States shareholder determined as
                        of November 2, 2017,
                            ``(ii) the aggregate described in clause
                        (i) determined as of the close of the last
                        taxable year of each such specified foreign
                        corporation which ends before November 2, 2017,
                        and
                            ``(iii) the aggregate described in clause
                        (i) determined as of the close of the taxable
                        year of each such specified foreign corporation
                        which precedes the taxable year referred to in
                        clause (ii).
                In the case of any foreign corporation which did not
                exist as of the determination date described in clause
                (ii) or (iii), this subparagraph shall be applied
                separately to such foreign corporation by not taking
                into account such clause and by substituting `one-half
                (100 percent in the case that both clauses (ii) and
                (iii) are disregarded)' for `one-third'.
                    ``(B) Cash position.--For purposes of this
                paragraph, the cash position of any specified foreign
                corporation is the sum of--
                            ``(i) cash held by such foreign
                        corporation,
                            ``(ii) the net accounts receivable of such
                        foreign corporation, plus
                            ``(iii) the fair market value of the
                        following assets held by such corporation:
                                    ``(I) Actively traded personal
                                property for which there is an
                                established financial market.
                                    ``(II) Commercial paper,
                                certificates of deposit, the securities
                                of the Federal government and of any
                                State or foreign government.
                                    ``(III) Any foreign currency.
                                    ``(IV) Any obligation with a term
                                of less than one year.
                                    ``(V) Any asset which the Secretary
                                identifies as being economically
                                equivalent to any asset described in
                                this subparagraph.
                    ``(C) Net accounts receivable.--For purposes of
                this paragraph, the term `net accounts receivable'
                means, with respect to any specified foreign
                corporation, the excess (if any) of--
                            ``(i) such corporation's accounts
                        receivable, over
                            ``(ii) such corporation's accounts payable
                        (determined consistent with the rules of
                        section 461).
                    ``(D) Prevention of double counting.--
                            ``(i) In general.--The applicable
                        percentage of each specified cash position of a
                        specified foreign corporation shall not be
                        taken into account by--
                                    ``(I) the United States shareholder
                                referred to in clause (ii) with respect
                                to such position, or
                                    ``(II) any United States
                                shareholder which is an includible
                                corporation in the same affiliated
                                group as such United States shareholder
                                referred to in clause (ii).
                            ``(ii) Specified cash position.--For
                        purposes of this subparagraph, the term
                        `specified cash position' means--
                                    ``(I) amounts described in
                                subparagraph (B)(ii) to the extent such
                                amounts are receivable from another
                                specified foreign corporation with
                                respect to any United States
                                shareholder,
                                    ``(II) amounts described in
                                subparagraph (B)(iii)(I) to the extent
                                such amounts consist of an equity
                                interest in another specified foreign
                                corporation with respect to any United
                                States shareholder, and
                                    ``(III) amounts described in
                                subparagraph (B)(iii)(IV) to the extent
                                that another specified foreign
                                corporation with respect to any United
                                States shareholder is obligated to
                                repay such amount.
                            ``(iii) Applicable percentage.--For
                        purposes of this subparagraph, the term
                        `applicable percentage' means--
                                    ``(I) with respect to each
                                specified cash position described in
                                subclause (I) or (III) of clause (ii),
                                the pro rata share of the United States
                                shareholder referred to in clause (ii)
                                with respect to the specified foreign
                                corporation referred to in such clause,
                                and
                                    ``(II) with respect to each
                                specified cash position described in
                                clause (ii)(II), the ratio (expressed
                                as a percentage and not in excess of
                                100 percent) of the United States
                                shareholder's pro rata share of the
                                cash position of the specified foreign
                                corporation referred to in such clause
                                divided by the amount of such specified
                                cash position.
                        For purposes of this subparagraph, a separate
                        applicable percentage shall be determined under
                        each of subclauses (I) and (II) with respect to
                        each specified foreign corporation referred to
                        in clause (ii) with respect to which a
                        specified cash position is determined for the
                        specified foreign corporation referred to in
                        clause (i).
                            ``(iv) Reduction with respect to affiliated
                        group members not wholly owned by the
                        affiliated group.--For purposes of clause
                        (i)(II), in the case of an includible
                        corporation the group ownership percentage of
                        which is less than 100 percent (as determined
                        under subsection (b)(4)(F)), the amount not
                        take into account by reason of such clause
                        shall be the group ownership percentage of such
                        amount (determined without regard to this
                        clause).
                    ``(E) Certain blocked assets not taken into
                account.--A cash position of a specified foreign
                corporation shall not be taken into account under
                subparagraph (A) if such position could not (as of the
                date that it would otherwise have been taken into
                account under clause (i), (ii), or (iii) of
                subparagraph (A)) have been distributed by such
                specified foreign corporation to United States
                shareholders of such specified foreign corporation
                because of currency or other restrictions or
                limitations imposed under the laws of any foreign
                country (within the meaning of section 964(b)).
                    ``(F) Cash positions of certain non-corporate
                entities taken into account.--An entity (other than a
                domestic corporation) shall be treated as a specified
                foreign corporation of a United States shareholder for
                purposes of determining such United States
                shareholder's aggregate foreign cash position if any
                interest in such entity is held by a specified foreign
                corporation of such United States shareholder
                (determined after application of this subparagraph) and
                such entity would be a specified foreign corporation of
                such United States shareholder if such entity were a
                foreign corporation.
                    ``(G) Time of certain determinations.--For purposes
                of this paragraph, the determination of whether a
                person is a United States shareholder, whether a person
                is a specified foreign corporation, and the pro rata
                share of a United States shareholder with respect to a
                specified foreign corporation, shall be determined as
                of the end of the taxable year described in subsection
                (a).
                    ``(H) Anti-abuse.--If the Secretary determines that
                the principal purpose of any transaction was to reduce
                the aggregate foreign cash position taken into account
                under this subsection, such transaction shall be
                disregarded for purposes of this subsection.
    ``(d) Deferred Foreign Income Corporation; Accumulated Post-1986
Deferred Foreign Income.--For purposes of this section--
            ``(1) Deferred foreign income corporation.--The term
        `deferred foreign income corporation' means, with respect to
        any United States shareholder, any specified foreign
        corporation of such United States shareholder which has
        accumulated post-1986 deferred foreign income (as of the date
        referred to in paragraph (1) or (2) of subsection (a),
        whichever is applicable with respect to such foreign
        corporation) greater than zero.
            ``(2) Accumulated post-1986 deferred foreign income.--The
        term `accumulated post-1986 deferred foreign income' means the
        post-1986 earnings and profits except to the extent such
        earnings--
                    ``(A) are attributable to income of the specified
                foreign corporation which is effectively connected with
                the conduct of a trade or business within the United
                States and subject to tax under this chapter, or
                    ``(B) if distributed, would be excluded from the
                gross income of a United States shareholder under
                section 959.
        To the extent provided in regulations or other guidance
        prescribed by the Secretary, in the case of any controlled
        foreign corporation which has shareholders which are not United
        States shareholders, accumulated post-1986 deferred foreign
        income shall be appropriately reduced by amounts which would be
        described in subparagraph (B) if such shareholders were United
        States shareholders.
            ``(3) Post-1986 earnings and profits.--The term `post-1986
        earnings and profits' means the earnings and profits of the
        foreign corporation (computed in accordance with sections
        964(a) and 986) accumulated in taxable years beginning after
        December 31, 1986, and determined--
                    ``(A) as of the date referred to in paragraph (1)
                or (2) of subsection (a), whichever is applicable with
                respect to such foreign corporation,
                    ``(B) without diminution by reason of dividends
                distributed during the taxable year ending with or
                including such date, and
                    ``(C) increased by the amount of any qualified
                deficit (within the meaning of section
                952(c)(1)(B)(ii)) arising before January 1, 2018, which
                is treated as a qualified deficit (within the meaning
                of such section as amended by the Tax Cuts and Jobs
                Act) for purposes of such foreign corporation's first
                taxable year beginning after December 31, 2017.
    ``(e) Specified Foreign Corporation.--
            ``(1) In general.--For purposes of this section, the term
        `specified foreign corporation' means--
                    ``(A) any controlled foreign corporation, and
                    ``(B) any foreign corporation with respect to which
                one or more domestic corporations is a United States
                shareholder (determined without regard to section
                958(b)(4)).
            ``(2) Application to certain foreign corporations.--For
        purposes of sections 951 and 961, a foreign corporation
        described in paragraph (1)(B) shall be treated as a controlled
        foreign corporation solely for purposes of taking into account
        the subpart F income of such corporation under subsection (a)
        (and for purposes of applying subsection (f)).
            ``(3) Exception for passive foreign investment companies.--
        The term `specified foreign corporation' shall not include any
        passive foreign investment company (within the meaning of
        subpart D of part VI of subchapter P) that is not a controlled
        foreign corporation.
    ``(f) Determinations of Pro Rata Share.--For purposes of this
section, the determination of any United States shareholder's pro rata
share of any amount with respect to any specified foreign corporation
shall be determined under rules similar to the rules of section
951(a)(2) by treating such amount in the same manner as subpart F
income (and by treating such specified foreign corporation as a
controlled foreign corporation).
    ``(g) Disallowance of Foreign Tax Credit, etc.--
            ``(1) In general.--No credit shall be allowed under section
        901 for the applicable percentage of any taxes paid or accrued
        (or treated as paid or accrued) with respect to any amount for
        which a deduction is allowed under this section.
            ``(2) Applicable percentage.--For purposes of this
        subsection, the term `applicable percentage' means the amount
        (expressed as a percentage) equal to the sum of--
                    ``(A) 80 percent of the ratio of--
                            ``(i) the excess to which subsection
                        (c)(1)(A) applies, divided by
                            ``(ii) the sum of such excess plus the
                        amount to which subsection (c)(1)(B) applies,
                        plus
                    ``(B) 60 percent of the ratio of--
                            ``(i) the amount to which subsection
                        (c)(1)(B) applies, divided by
                            ``(ii) the sum described in subparagraph
                        (A)(ii).
            ``(3) Denial of deduction.--No deduction shall be allowed
        under this chapter for any tax for which credit is not
        allowable under section 901 by reason of paragraph (1)
        (determined by treating the taxpayer as having elected the
        benefits of subpart A of part III of subchapter N).
            ``(4) Coordination with section 78.--With respect to the
        taxes treated as paid or accrued by a domestic corporation with
        respect to amounts which are includible in gross income of such
        domestic corporation by reason of this section, section 78
        shall apply only to so much of such taxes as bears the same
        proportion to the amount of such taxes as--
                    ``(A) the excess of--
                            ``(i) the amounts which are includible in
                        gross income of such domestic corporation by
                        reason of this section, over
                            ``(ii) the deduction allowable under
                        subsection (c) with respect to such amounts,
                        bears to
                    ``(B) such amounts.
            ``(5) Extension of foreign tax credit carryover period.--
        With respect to any taxes paid or accrued (or treated as paid
        or accrued) with respect to any amount for which a deduction is
        allowed under this section, section 904(c) shall be applied by
        substituting `first 20 succeeding taxable years' for `first 10
        succeeding taxable years'.
    ``(h) Election to Pay Liability in Installments.--
            ``(1) In general.--In the case of a United States
        shareholder of a deferred foreign income corporation, such
        United States shareholder may elect to pay the net tax
        liability under this section in 8 equal installments.
            ``(2) Date for payment of installments.--If an election is
        made under paragraph (1), the first installment shall be paid
        on the due date (determined without regard to any extension of
        time for filing the return) for the return of tax for the
        taxable year described in subsection (a) and each succeeding
        installment shall be paid on the due date (as so determined)
        for the return of tax for the taxable year following the
        taxable year with respect to which the preceding installment
        was made.
            ``(3) Acceleration of payment.--If there is an addition to
        tax for failure to timely pay any installment required under
        this subsection, a liquidation or sale of substantially all the
        assets of the taxpayer (including in a title 11 or similar
        case), a cessation of business by the taxpayer, or any similar
        circumstance, then the unpaid portion of all remaining
        installments shall be due on the date of such event (or in the
        case of a title 11 or similar case, the day before the petition
        is filed). The preceding sentence shall not apply to the sale
        of substantially all the assets of a taxpayer to a buyer if
        such buyer enters into an agreement with the Secretary under
        which such buyer is liable for the remaining installments due
        under this subsection in the same manner as if such buyer were
        the taxpayer.
            ``(4) Proration of deficiency to installments.--If an
        election is made under paragraph (1) to pay the net tax
        liability under this section in installments and a deficiency
        has been assessed with respect to such net tax liability, the
        deficiency shall be prorated to the installments payable under
        paragraph (1). The part of the deficiency so prorated to any
        installment the date for payment of which has not arrived shall
        be collected at the same time as, and as a part of, such
        installment. The part of the deficiency so prorated to any
        installment the date for payment of which has arrived shall be
        paid upon notice and demand from the Secretary. This subsection
        shall not apply if the deficiency is due to negligence, to
        intentional disregard of rules and regulations, or to fraud
        with intent to evade tax.
            ``(5) Election.--Any election under paragraph (1) shall be
        made not later than the due date for the return of tax for the
        taxable year described in subsection (a) and shall be made in
        such manner as the Secretary may provide.
            ``(6) Net tax liability under this section.--For purposes
        of this subsection--
                    ``(A) In general.--The net tax liability under this
                section with respect to any United States shareholder
                is the excess (if any) of--
                            ``(i) such taxpayer's net income tax for
                        the taxable year in which an amount is included
                        in the gross income of such United States
                        shareholder under section 951(a)(1) by reason
                        of this section, over
                            ``(ii) such taxpayer's net income tax for
                        such taxable year determined--
                                    ``(I) without regard to this
                                section, and
                                    ``(II) without regard to any
                                income, deduction, or credit, properly
                                attributable to a dividend received by
                                such United States shareholder from any
                                deferred foreign income corporation.
                    ``(B) Net income tax.--The term `net income tax'
                means the regular tax liability reduced by the credits
                allowed under subparts A, B, and D of part IV of
                subchapter A.
    ``(i) Special Rules for S Corporation Shareholders.--
            ``(1) In general.--In the case of any S corporation which
        is a United States shareholder of a deferred foreign income
        corporation, each shareholder of such S corporation may elect
        to defer payment of such shareholder's net tax liability under
        this section with respect to such S corporation until the
        shareholder's taxable year which includes the triggering event
        with respect to such liability. Any net tax liability payment
        of which is deferred under the preceding sentence shall be
        assessed on the return as an addition to tax in the
        shareholder's taxable year which includes such triggering
        event.
            ``(2) Triggering event.--
                    ``(A) In general.--In the case of any shareholder's
                net tax liability under this section with respect to
                any S corporation, the triggering event with respect to
                such liability is whichever of the following occurs
                first:
                            ``(i) Such corporation ceases to be an S
                        corporation (determined as of the first day of
                        the first taxable year that such corporation is
                        not an S corporation).
                            ``(ii) A liquidation or sale of
                        substantially all the assets of such S
                        corporation (including in a title 11 or similar
                        case), a cessation of business by such S
                        corporation, such S corporation ceases to
                        exist, or any similar circumstance.
                            ``(iii) A transfer of any share of stock in
                        such S corporation by the taxpayer (including
                        by reason of death, or otherwise).
                    ``(B) Partial transfers of stock.--In the case of a
                transfer of less than all of the taxpayer's shares of
                stock in the S corporation, such transfer shall only be
                a triggering event with respect to so much of the
                taxpayer's net tax liability under this section with
                respect to such S corporation as is properly allocable
                to such stock.
                    ``(C) Transfer of liability.--A transfer described
                in clause (iii) shall not be treated as a triggering
                event if the transferee enters into an agreement with
                the Secretary under which such transferee is liable for
                net tax liability with respect to such stock in the
                same manner as if such transferee were the taxpayer.
            ``(3) Net tax liability.--A shareholder's net tax liability
        under this section with respect to any S corporation is the net
        tax liability under this section which would be determined
        under subsection (h)(6) if the only subpart F income taken into
        account by such shareholder by reason of this section were
        allocations from such S corporation.
            ``(4) Election to pay deferred liability in installments.--
        In the case of a taxpayer which elects to defer payment under
        paragraph (1)--
                    ``(A) subsection (h) shall be applied separately
                with respect to the liability to which such election
                applies,
                    ``(B) an election under subsection (h) with respect
                to such liability shall be treated as timely made if
                made not later than the due date for the return of tax
                for the taxable year in which the triggering event with
                respect to such liability occurs,
                    ``(C) the first installment under subsection (h)
                with respect to such liability shall be paid not later
                than such due date (but determined without regard to
                any extension of time for filing the return), and
                    ``(D) if the triggering event with respect to any
                net tax liability is described in paragraph (2)(A)(ii),
                an election under subsection (h) with respect to such
                liability may be made only with the consent of the
                Secretary.
            ``(5) Joint and several liability of s corporation.--If any
        shareholder of an S corporation elects to defer payment under
        paragraph (1), such S corporation shall be jointly and
        severally liable for such payment and any penalty, addition to
        tax, or additional amount attributable thereto.
            ``(6) Extension of limitation on collection.--
        Notwithstanding any other provision of law, any limitation on
        the time period for the collection of a liability deferred
        under this subsection shall not be treated as beginning before
        the date of the triggering event with respect to such
        liability.
            ``(7) Annual reporting of net tax liability.--
                    ``(A) In general.--Any shareholder of an S
                corporation which makes an election under paragraph (1)
                shall report the amount of such shareholder's deferred
                net tax liability on such shareholder's return of tax
                for the taxable year for which such election is made
                and on the return of tax for each taxable year
                thereafter until such amount has been fully assessed on
                such returns.
                    ``(B) Deferred net tax liability.--For purposes of
                this paragraph, the term `deferred net tax liability'
                means, with respect to any taxable year, the amount of
                net tax liability payment of which has been deferred
                under paragraph (1) and which has not been assessed on
                a return of tax for any prior taxable year.
                    ``(C) Failure to report.--In the case of any
                failure to report any amount required to be reported
                under subparagraph (A) with respect to any taxable year
                before the due date for the return of tax for such
                taxable year, there shall be assessed on such return as
                an addition to tax 5 percent of such amount.
            ``(8) Election.--Any election under paragraph (1)--
                    ``(A) shall be made by the shareholder of the S
                corporation not later than the due date for such
                shareholder's return of tax for the taxable year which
                includes the close of the taxable year of such S
                corporation in which the amount described in subsection
                (a) is taken into account, and
                    ``(B) shall be made in such manner as the Secretary
                may provide.
    ``(j) Reporting by S Corporation.--Each S corporation which is a
United States shareholder of a deferred foreign income corporation
shall report in its return of tax under section 6037(a) the amount
includible in its gross income for such taxable year by reason of this
section and the amount of the deduction allowable by subsection (c).
Any copy provided to a shareholder under section 6037(b) shall include
a statement of such shareholder's pro rata share of such amounts.
    ``(k) Inclusion of Deferred Foreign Income Under This Section Not
to Trigger Recapture of Overall Foreign Loss, etc.--For purposes of
sections 904(f)(1) and 907(c)(4), in the case of a United States
shareholder of a deferred foreign income corporation, such United
States shareholder's taxable income from sources without the United
States and combined foreign oil and gas income shall be determined
without regard to this section.
    ``(l) Regulations.--The Secretary may prescribe such regulations or
other guidance as may be necessary or appropriate to carry out the
provisions of this section.''.
    (b) Clerical Amendment.--The table of sections for subpart F of
part III of subchapter N of chapter 1 is amended by striking the item
relating to section 965 and inserting the following:

``Sec. 965. Treatment of deferred foreign income upon transition to
                            participation exemption system of
                            taxation.''.

     Subtitle B--Modifications Related to Foreign Tax Credit System

SEC. 4101. REPEAL OF SECTION 902 INDIRECT FOREIGN TAX CREDITS;
              DETERMINATION OF SECTION 960 CREDIT ON CURRENT YEAR
              BASIS.

    (a) Repeal of Section 902 Indirect Foreign Tax Credits.--Subpart A
of part III of subchapter N of chapter 1 is amended by striking section
902.
    (b) Determination of Section 960 Credit on Current Year Basis.--
Section 960 is amended--
            (1) by striking subsection (c), by redesignating subsection
        (b) as subsection (c), by striking all that precedes subsection
        (c) (as so redesignated) and inserting the following:

``SEC. 960. DEEMED PAID CREDIT FOR SUBPART F INCLUSIONS.

    ``(a) In General.--For purposes of this subpart, if there is
included in the gross income of a domestic corporation any item of
income under section 951(a)(1) with respect to any controlled foreign
corporation with respect to which such domestic corporation is a United
States shareholder, such domestic corporation shall be deemed to have
paid so much of such foreign corporation's foreign income taxes as are
properly attributable to such item of income.
    ``(b) Special Rules for Distributions From Previously Taxed
Earnings and Profits.--For purposes of this subpart--
            ``(1) In general.--If any portion of a distribution from a
        controlled foreign corporation to a domestic corporation which
        is a United States shareholder with respect to such controlled
        foreign corporation is excluded from gross income under section
        959(a), such domestic corporation shall be deemed to have paid
        so much of such foreign corporation's foreign income taxes as--
                    ``(A) are properly attributable to such portion,
                and
                    ``(B) have not been deemed to have to been paid by
                such domestic corporation under this section for the
                taxable year or any prior taxable year.
            ``(2) Tiered controlled foreign corporations.--If section
        959(b) applies to any portion of a distribution from a
        controlled foreign corporation to another controlled foreign
        corporation, such controlled foreign corporation shall be
        deemed to have paid so much of such other controlled foreign
        corporation's foreign income taxes as--
                    ``(A) are properly attributable to such portion,
                and
                    ``(B) have not been deemed to have been paid by a
                domestic corporation under this section for the taxable
                year or any prior taxable year.'',
            (2) and by adding after subsection (c) (as so redesignated)
        the following new subsections:
    ``(d) Foreign Income Taxes.--The term `foreign income taxes' means
any income, war profits, or excess profits taxes paid or accrued to any
foreign country or possession of the United States.
    ``(e) Regulations.--The Secretary may prescribe such regulations or
other guidance as may be necessary or appropriate to carry out the
provisions of this section.''.
    (c) Conforming Amendments.--
            (1) Section 78 is amended to read as follows:

``SEC. 78. GROSS UP FOR DEEMED PAID FOREIGN TAX CREDIT.

    ``If a domestic corporation chooses to have the benefits of subpart
A of part III of subchapter N (relating to foreign tax credit) for any
taxable year, an amount equal to the taxes deemed to be paid by such
corporation under subsections (a) and (b) of section 960 for such
taxable year shall be treated for purposes of this title (other than
sections 959, 960, and 961) as an item of income required to be
included in the gross income of such domestic corporation under section
951(a) for such taxable year.''.
            (2) Section 245(a)(10)(C) is amended by striking ``sections
        902, 907, and 960'' and inserting ``sections 907 and 960''.
            (3) Sections 535(b)(1) and 545(b)(1) are each amended by
        striking ``section 902(a) or 960(a)(1)'' and inserting
        ``section 960''.
            (4) Section 814(f)(1) is amended--
                    (A) by striking subparagraph (B), and
                    (B) by striking all that precedes ``No income'' and
                inserting the following:
            ``(1) Treatment of foreign taxes.--''.
            (5) Section 865(h)(1)(B) is amended by striking ``sections
        902, 907, and 960'' and inserting ``sections 907 and 960''.
            (6) Section 901(a) is amended by striking ``sections 902
        and 960'' and inserting ``section 960''.
            (7) Section 901(e)(2) is amended by striking ``but is not
        limited to--'' and all that follows through ``that portion''
        and inserting ``but is not limited to, that portion''.
            (8) Section 901(f) is amended by striking ``sections 902
        and 960'' and inserting ``section 960''.
            (9) Section 901(j)(1)(A) is amended by striking ``902 or''.
            (10) Section 901(j)(1)(B) is amended by striking ``sections
        902 and 960'' and inserting ``section 960''.
            (11) Section 901(k)(2) is amended by striking ``section
        853, 902, or 960'' and inserting ``section 853 or 960''.
            (12) Section 901(k)(6) is amended by striking ``902 or''.
            (13) Section 901(m)(1) is amended by striking ``relevant
        foreign assets--'' and all that follows and inserting
        ``relevant foreign assets shall not be taken into account in
        determining the credit allowed under subsection (a).''.
            (14) Section 904(d)(1) is amended by striking ``sections
        902, 907, and 960'' and inserting ``sections 907 and 960''.
            (15) Section 904(d)(6)(A) is amended by striking ``sections
        902, 907, and 960'' and inserting ``sections 907 and 960''.
            (16) Section 904(h)(10)(A) is amended by striking
        ``sections 902, 907, and 960'' and inserting ``sections 907 and
        960''.
            (17) Section 904 is amended by striking subsection (k).
            (18) Section 905(c)(1) is amended by striking the last
        sentence.
            (19) Section 905(c)(2)(B)(i) is amended to read as follows:
                            ``(i) shall be taken into account for the
                        taxable year to which such taxes relate, and''.
            (20) Section 906(a) is amended by striking ``(or deemed,
        under section 902, paid or accrued during the taxable year)''.
            (21) Section 906(b) is amended by striking paragraphs (4)
        and (5).
            (22) Section 907(b)(2)(B) is amended by striking ``902
        or''.
            (23) Section 907(c)(3) is amended--
                    (A) by striking subparagraph (A) and redesignating
                subparagraphs (B) and (C) as subparagraphs (A) and (B),
                respectively, and
                    (B) by striking ``section 960(a)'' in subparagraph
                (A) (as so redesignated) and inserting ``section 960''.
            (24) Section 907(c)(5) is amended by striking ``902 or''.
            (25) Section 907(f)(2)(B)(i) is amended by striking ``902
        or''.
            (26) Section 908(a) is amended by striking ``902 or''.
            (27) Section 909(b) is amended--
                    (A) by striking ``section 902 corporation'' in the
                matter preceding paragraph (1) and inserting ``10/50
                corporation'',
                    (B) by striking ``902 or'' in paragraph (1),
                    (C) by striking ``by such section 902 corporation''
                and all that follows in the matter following paragraph
                (2) and inserting ``by such 10/50 corporation or a
                domestic corporation which is a United States
                shareholder with respect to such 10/50 corporation.'',
                and
                    (D) by striking ``Section 902 Corporations'' in the
                heading thereof and inserting ``10/50 Corporations''.
            (28) Section 909(d)(5) is amended to read as follows:
            ``(5) 10/50 corporation.--The term `10/50 corporation'
        means any foreign corporation with respect to which one or more
        domestic corporations is a United States shareholder.''.
            (29) Section 958(a)(1) is amended by striking ``960(a)(1)''
        and inserting ``960''.
            (30) Section 959(d) is amended by striking ``Except as
        provided in section 960(a)(3), any'' and inserting ``Any''.
            (31) Section 959(e) is amended by striking ``section
        960(b)'' and inserting ``section 960(c)''.
            (32) Section 1291(g)(2)(A) is amended by striking ``any
        distribution--'' and all that follows through ``but only if''
        and inserting ``any distribution, any withholding tax imposed
        with respect to such distribution, but only if''.
            (33) Section 6038(c)(1)(B) is amended by striking
        ``sections 902 (relating to foreign tax credit for corporate
        stockholder in foreign corporation) and 960 (relating to
        special rules for foreign tax credit)'' and inserting ``section
        960''.
            (34) Section 6038(c)(4) is amended by striking subparagraph
        (C).
            (35) The table of sections for subpart A of part III of
        subchapter N of chapter 1 is amended by striking the item
        relating to section 902.
            (36) The table of sections for subpart F of part III of
        subchapter N of chapter 1 is amended by striking the item
        relating to section 960 and inserting the following:

``Sec. 960. Deemed paid credit for subpart F inclusions.''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 4102. SOURCE OF INCOME FROM SALES OF INVENTORY DETERMINED SOLELY
              ON BASIS OF PRODUCTION ACTIVITIES.

    (a) In General.--Section 863(b) is amended by adding at the end the
following: ``Gains, profits, and income from the sale or exchange of
inventory property described in paragraph (2) shall be allocated and
apportioned between sources within and without the United States solely
on the basis of the production activities with respect to the
property.''.
    (b) Effective Date.--The amendment made by this section shall apply
to taxable years beginning after December 31, 2017.

            Subtitle C--Modification of Subpart F Provisions

SEC. 4201. REPEAL OF INCLUSION BASED ON WITHDRAWAL OF PREVIOUSLY
              EXCLUDED SUBPART F INCOME FROM QUALIFIED INVESTMENT.

    (a) In General.--Subpart F of part III of subchapter N of chapter 1
is amended by striking section 955.
    (b) Conforming Amendments.--
            (1)(A) Section 951(a)(1)(A) is amended to read as follows:
                    ``(A) his pro rata share (determined under
                paragraph (2)) of the corporation's subpart F income
                for such year, and''.
            (B) Section 851(b)(3) is amended by striking ``section
        951(a)(1)(A)(i)'' in the flush language at the end and
        inserting ``section 951(a)(1)(A)''.
            (C) Section 952(c)(1)(B)(i) is amended by striking
        ``section 951(a)(1)(A)(i)'' and inserting ``section
        951(a)(1)(A)''.
            (D) Section 953(c)(1)(C) is amended by striking ``section
        951(a)(1)(A)(i)'' and inserting ``section 951(a)(1)(A)''.
            (2) Section 951(a) is amended by striking paragraph (3).
            (3) Section 953(d)(4)(B)(iv)(II) is amended by striking
        ``or amounts referred to in clause (ii) or (iii) of section
        951(a)(1)(A)''.
            (4) Section 964(b) is amended by striking ``, 955,''.
            (5) Section 970 is amended by striking subsection (b).
            (6) The table of sections for subpart F of part III of
        subchapter N of chapter 1 is amended by striking the item
        relating to section 955.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4202. REPEAL OF TREATMENT OF FOREIGN BASE COMPANY OIL RELATED
              INCOME AS SUBPART F INCOME.

    (a) In General.--Section 954(a) is amended by striking paragraph
(5), by striking the comma at the end of paragraph (3) and inserting a
period, and by inserting ``and'' at the end of paragraph (2).
    (b) Conforming Amendments.--
            (1) Section 952(c)(1)(B)(iii) is amended by striking
        subclause (I) and by redesignating subclauses (II) through (V)
        as subclauses (I) through (IV), respectively.
            (2) Section 954(b)(4) is amended by striking the last
        sentence.
            (3) Section 954(b)(5) is amended by striking ``the foreign
        base company services income, and the foreign base company oil
        related income'' and inserting ``and the foreign base company
        services income''.
            (4) Section 954(b) is amended by striking paragraph (6).
            (5) Section 954 is amended by striking subsection (g).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4203. INFLATION ADJUSTMENT OF DE MINIMIS EXCEPTION FOR FOREIGN
              BASE COMPANY INCOME.

    (a) In General.--Section 954(b)(3) is amended by adding at the end
the following new subparagraph:
                    ``(D) Inflation adjustment.--In the case of any
                taxable year beginning after 2017, the dollar amount in
                subparagraph (A)(ii) shall be increased by an amount
                equal to--
                            ``(i) such dollar amount, multiplied by
                            ``(ii) the cost-of-living adjustment
                        determined under section 1(c)(2)(A) for the
                        calendar year in which the taxable year begins.
                Any increase determined under the preceding sentence
                shall be rounded to the nearest multiple of $50,000.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4204. LOOK-THRU RULE FOR RELATED CONTROLLED FOREIGN CORPORATIONS
              MADE PERMANENT.

    (a) In General.--Paragraph (6) of section 954(c) is amended by
striking subparagraph (C).
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2019, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4205. MODIFICATION OF STOCK ATTRIBUTION RULES FOR DETERMINING
              STATUS AS A CONTROLLED FOREIGN CORPORATION.

    (a) In General.--Section 958(b) is amended--
            (1) by striking paragraph (4), and
            (2) by striking ``Paragraphs (1) and (4)'' in the last
        sentence and inserting ``Paragraph (1)''.
    (b) Application of Certain Reporting Requirements.--Section
6038(e)(2) is amended by striking ``except that--'' and all that
follows through ``in applying subparagraph (C)'' and inserting ``except
that in applying subparagraph (C)''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4206. ELIMINATION OF REQUIREMENT THAT CORPORATION MUST BE
              CONTROLLED FOR 30 DAYS BEFORE SUBPART F INCLUSIONS APPLY.

    (a) In General.--Section 951(a)(1) is amended by striking ``for an
uninterrupted period of 30 days or more'' and inserting ``at any
time''.
    (b) Effective Date.--The amendment made by this section shall apply
to taxable years of foreign corporations beginning after December 31,
2017, and to taxable years of United States shareholders with or within
which such taxable years of foreign corporations end.

                 Subtitle D--Prevention of Base Erosion

SEC. 4301. CURRENT YEAR INCLUSION BY UNITED STATES SHAREHOLDERS WITH
              FOREIGN HIGH RETURNS.

    (a) In General.--Subpart F of part III of subchapter N of chapter 1
is amended by inserting after section 951 the following new section:

``SEC. 951A. FOREIGN HIGH RETURN AMOUNT INCLUDED IN GROSS INCOME OF
              UNITED STATES SHAREHOLDERS.

    ``(a) In General.--Each person who is a United States shareholder
of any controlled foreign corporation for any taxable year of such
United States shareholder shall include in gross income for such
taxable year 50 percent of such shareholder's foreign high return
amount for such taxable year.
    ``(b) Foreign High Return Amount.--For purposes of this section--
            ``(1) In general.--The term `foreign high return amount'
        means, with respect to any United States shareholder for any
        taxable year of such United States shareholder, the excess (if
        any) of--
                    ``(A) such shareholder's net CFC tested income for
                such taxable year, over
                    ``(B) the excess (if any) of--
                            ``(i) the applicable percentage of the
                        aggregate of such shareholder's pro rata share
                        of the qualified business asset investment of
                        each controlled foreign corporation with
                        respect to which such shareholder is a United
                        States shareholder for such taxable year
                        (determined for each taxable year of each such
                        controlled foreign corporation which ends in or
                        with such taxable year of such United States
                        shareholder), over
                            ``(ii) the amount of interest expense taken
                        into account under subsection (c)(2)(A)(ii) in
                        determining the shareholder's net CFC tested
                        income for the taxable year.
            ``(2) Applicable percentage.--The term `applicable
        percentage' means, with respect to any taxable year, the
        Federal short-term rate (determined under section 1274(d) for
        the month in which or with which such taxable year ends) plus 7
        percentage points.
    ``(c) Net CFC Tested Income.--For purposes of this section--
            ``(1) In general.--The term `net CFC tested income' means,
        with respect to any United States shareholder for any taxable
        year of such United States shareholder, the excess (if any)
        of--
                    ``(A) the aggregate of such shareholder's pro rata
                share of the tested income of each controlled foreign
                corporation with respect to which such shareholder is a
                United States shareholder for such taxable year of such
                United States shareholder (determined for each taxable
                year of such controlled foreign corporation which ends
                in or with such taxable year of such United States
                shareholder), over
                    ``(B) the aggregate of such shareholder's pro rata
                share of the tested loss of each controlled foreign
                corporation with respect to which such shareholder is a
                United States shareholder for such taxable year of such
                United States shareholder (determined for each taxable
                year of such controlled foreign corporation which ends
                in or with such taxable year of such United States
                shareholder).
            ``(2) Tested income; tested loss.--For purposes of this
        section--
                    ``(A) Tested income.--The term `tested income'
                means, with respect to any controlled foreign
                corporation for any taxable year of such controlled
                foreign corporation, the excess (if any) of--
                            ``(i) the gross income of such corporation
                        determined without regard to--
                                    ``(I) any item of income which is
                                effectively connected with the conduct
                                by such corporation of a trade or
                                business within the United States if
                                subject to tax under this chapter,
                                    ``(II) any gross income taken into
                                account in determining the subpart F
                                income of such corporation,
                                    ``(III) except as otherwise
                                provided by the Secretary, any amount
                                excluded from the foreign personal
                                holding company income (as defined in
                                section 954) of such corporation by
                                reason of section 954(c)(6) but only to
                                the extent that any deduction allowable
                                for the payment or accrual of such
                                amount does not result in a reduction
                                in the foreign high return amount of
                                any United States shareholder
                                (determined without regard to this
                                subclause),
                                    ``(IV) any gross income excluded
                                from the foreign personal holding
                                company income (as defined in section
                                954) of such corporation by reason of
                                subsection (c)(2)(C), (h), or (i) of
                                section 954,
                                    ``(V) any gross income excluded
                                from the insurance income (as defined
                                in section 953) of such corporation by
                                reason of section 953(a)(2),
                                    ``(VI) any gross income excluded
                                from foreign base company income (as
                                defined in section 954) or insurance
                                income (as defined in section 953) of
                                such corporation by reason of section
                                954(b)(4),
                                    ``(VII) any dividend received from
                                a related person (as defined in section
                                954(d)(3)), and
                                    ``(VIII) any commodities gross
                                income of such corporation, over
                            ``(ii) the deductions (including taxes)
                        properly allocable to such gross income under
                        rules similar to the rules of section 954(b)(5)
                        (or which would be so properly allocable if
                        such corporation had such gross income).
                    ``(B) Tested loss.--The term `tested loss' means,
                with respect to any controlled foreign corporation for
                any taxable year of such controlled foreign
                corporation, the excess (if any) of the amount
                described in subparagraph (A)(ii) over the amount
                described in subparagraph (A)(i).
    ``(d) Qualified Business Asset Investment.--For purposes of this
section--
            ``(1) In general.--The term `qualified business asset
        investment' means, with respect to any controlled foreign
        corporation for any taxable year of such controlled foreign
        corporation, the aggregate of the corporation's adjusted bases
        (determined as of the close of such taxable year and after any
        adjustments with respect to such taxable year) in specified
        tangible property--
                    ``(A) used in a trade or business of the
                corporation, and
                    ``(B) of a type with respect to which a deduction
                is allowable under section 168.
            ``(2) Specified tangible property.--The term `specified
        tangible property' means any tangible property to the extent
        such property is used in the production of tested income or
        tested loss.
            ``(3) Partnership property.--For purposes of this
        subsection, if a controlled foreign corporation holds an
        interest in a partnership at the close of such taxable year of
        the controlled foreign corporation, such controlled foreign
        corporation shall take into account under paragraph (1) the
        controlled foreign corporation's distributive share of the
        aggregate of the partnership's adjusted bases (determined as of
        such date in the hands of the partnership) in tangible property
        held by such partnership to the extent such property--
                    ``(A) is used in the trade or business of the
                partnership,
                    ``(B) is of a type with respect to which a
                deduction is allowable under section 168, and
                    ``(C) is used in the production of tested income or
                tested loss (determined with respect to such controlled
                foreign corporation's distributive share of income or
                loss with respect to such property).
        For purposes of this paragraph, the controlled foreign
        corporation's distributive share of the adjusted basis of any
        property shall be the controlled foreign corporation's
        distributive share of income and loss with respect to such
        property.
            ``(4) Determination of adjusted basis.--For purposes of
        this subsection, the adjusted basis in any property shall be
        determined without regard to any provision of this title (or
        any other provision of law) which is enacted after the date of
        the enactment of this section.
            ``(5) Regulations.--The Secretary shall issue such
        regulations or other guidance as the Secretary determines
        appropriate to prevent the avoidance of the purposes of this
        subsection, including regulations or other guidance which
        provide for the treatment of property if--
                    ``(A) such property is transferred, or held,
                temporarily, or
                    ``(B) the avoidance of the purposes of this
                paragraph is a factor in the transfer or holding of
                such property.
    ``(e) Commodities Gross Income.--For purposes of this section--
            ``(1) Commodities gross income.--The term `commodities
        gross income' means, with respect to any corporation--
                    ``(A) gross income of such corporation from the
                disposition of commodities which are produced or
                extracted by such corporation (or a partnership in
                which such corporation is a partner), and
                    ``(B) gross income of such corporation from the
                disposition of property which gives rise to income
                described in subparagraph (A).
            ``(2) Commodity.--The term `commodity' means any commodity
        described in section 475(e)(2)(A) or section 475(e)(2)(D)
        (determined without regard to clause (i) thereof and by
        substituting `a commodity described in subparagraph (A)' for
        `such a commodity' in clause (ii) thereof).
    ``(f) Taxable Years for Which Persons Are Treated as United States
Shareholders of Controlled Foreign Corporations.--For purposes of this
section--
            ``(1) In general.--A United States shareholder of a
        controlled foreign corporation shall be treated as a United
        States shareholder of such controlled foreign corporation for
        any taxable year of such United States shareholder if--
                    ``(A) a taxable year of such controlled foreign
                corporation ends in or with such taxable year of such
                person, and
                    ``(B) such person owns (within the meaning of
                section 958(a)) stock in such controlled foreign
                corporation on the last day, in such taxable year of
                such foreign corporation, on which the foreign
                corporation is a controlled foreign corporation.
            ``(2) Treatment as a controlled foreign corporation.--
        Except for purposes of paragraph (1)(B) and the application of
        section 951(a)(2) to this section pursuant to subsection (g), a
        foreign corporation shall be treated as a controlled foreign
        corporation for any taxable year of such foreign corporation if
        such foreign corporation is a controlled foreign corporation at
        any time during such taxable year.
    ``(g) Determination of Pro Rata Share.--For purposes of this
section, pro rata shares shall be determined under the rules of section
951(a)(2) in the same manner as such section applies to subpart F
income.
    ``(h) Coordination With Subpart F.--
            ``(1) Treatment as subpart f income for certain purposes.--
        Except as otherwise provided by the Secretary any foreign high
        return amount included in gross income under subsection (a)
        shall be treated in the same manner as an amount included under
        section 951(a)(1)(A) for purposes of applying sections
        168(h)(2)(B), 535(b)(10), 851(b), 904(h)(1), 959, 961, 962,
        993(a)(1)(E), 996(f)(1), 1248(b)(1), 1248(d)(1), 6501(e)(1)(C),
        6654(d)(2)(D), and 6655(e)(4).
            ``(2) Entire foreign high return amount taken into account
        for purposes of certain sections.--For purposes of applying
        paragraph (1) with respect to sections 168(h)(2)(B), 851(b),
        959, 961, 962, 1248(b)(1), and 1248(d)(1), the foreign high
        return amount included in gross income under subsection (a)
        shall be determined by substituting `100 percent' for `50
        percent' in such subsection.
            ``(3) Allocation of foreign high return amount to
        controlled foreign corporations.--For purposes of the sections
        referred to in paragraph (1), with respect to any controlled
        foreign corporation any pro rata amount from which is taken
        into account in determining the foreign high return amount
        included in gross income of a United States shareholder under
        subsection (a), the portion of such foreign high return amount
        which is treated as being with respect to such controlled
        foreign corporation is--
                    ``(A) in the case of a controlled foreign
                corporation with tested loss, zero, and
                    ``(B) in the case of a controlled foreign
                corporation with tested income, the portion of such
                foreign high return amount which bears the same ratio
                to such foreign high return amount as--
                            ``(i) such United States shareholder's pro
                        rata amount of the tested income of such
                        controlled foreign corporation, bears to
                            ``(ii) the aggregate amount determined
                        under subsection (c)(1)(A) with respect to such
                        United States shareholder.
            ``(4) Coordination with subpart f to deny double benefit of
        losses.--In the case of any United States shareholder of any
        controlled foreign corporation, the amount included in gross
        income under section 951(a)(1)(A) shall be determined by
        increasing the earnings and profits of such controlled foreign
        corporation (solely for purposes of determining such amount) by
        an amount that bears the same ratio (not greater than 1) to
        such shareholder's pro rata share of the tested loss of such
        controlled foreign corporation as--
                    ``(A) the aggregate amount determined under
                subsection (c)(1)(A) with respect to such shareholder,
                bears to
                    ``(B) the aggregate amount determined under
                subsection (c)(1)(B) with respect to such
                shareholder.''.
    (b) Foreign Tax Credit.--
            (1) Application of deemed paid foreign tax credit.--Section
        960, as amended by the preceding provisions of this Act, is
        amended by redesignating subsections (d) and (e) as subsections
        (e) and (f), respectively, and by inserting after subsection
        (c) the following new subsection:
    ``(d) Deemed Paid Credit for Taxes Properly Attributable to Tested
Income.--
            ``(1) In general.--For purposes of this subpart, if any
        amount is includible in the gross income of a domestic
        corporation under section 951A, such domestic corporation shall
        be deemed to have paid foreign income taxes equal to 80 percent
        of--
                    ``(A) such domestic corporation's foreign high
                return percentage, multiplied by
                    ``(B) the aggregate tested foreign income taxes
                paid or accrued by controlled foreign corporations with
                respect to which such domestic corporation is a United
                States shareholder.
            ``(2) Foreign high return percentage.--For purposes of
        paragraph (1), the term `foreign high return percentage' means,
        with respect to any domestic corporation, the ratio (expressed
        as a percentage) of--
                    ``(A) such corporation's foreign high return amount
                (as defined in section 951A(b)), divided by
                    ``(B) the aggregate amount determined under section
                951A(c)(1)(A) with respect to such corporation.
            ``(3) Tested foreign income taxes.--For purposes of
        paragraph (1), the term `tested foreign income taxes' means,
        with respect to any domestic corporation which is a United
        States shareholder of a controlled foreign corporation, the
        foreign income taxes paid or accrued by such foreign
        corporation which are properly attributable to gross income
        described in section 951A(c)(2)(A)(i).''.
            (2) Application of foreign tax credit limitation.--
                    (A) Separate basket for foreign high return
                amount.--Section 904(d)(1) is amended by redesignating
                subparagraphs (A) and (B) as subparagraphs (B) and (C),
                respectively, and by inserting before subparagraph (B)
                (as so redesignated) the following new subparagraph:
                    ``(A) any amount includible in gross income under
                section 951A,''.
                    (B) No carryover of excess taxes.--Section 904(c)
                is amended by adding at the end the following: ``This
                subsection shall not apply to taxes paid or accrued
                with respect to amounts described in subsection
                (d)(1)(A).''
            (3) Gross up for deemed paid foreign tax credit.--Section
        78, as amended by the preceding provisions of this Act, is
        amended--
                    (A) by striking ``any taxable year, an amount'' and
                inserting ``any taxable year--
            ``(1) an amount'', and
                    (B) by striking the period at the end and inserting
                ``, and
            ``(2) an amount equal to the taxes deemed to be paid by
        such corporation under section 960(d) for such taxable year
        (determined by substituting `100 percent' for `80 percent' in
        such section) shall be treated for purposes of this title
        (other than sections 959, 960, and 961) as an increase in the
        foreign high return amount of such domestic corporation under
        section 951A for such taxable year.''.
    (c) Conforming Amendments.--
            (1) Section 170(b)(2)(D) is amended by striking ``computed
        without regard to'' and all that follows and inserting
        ``computed--
                            ``(i) without regard to--
                                    ``(I) this section,
                                    ``(II) part VIII (except section
                                248),
                                    ``(III) any net operating loss
                                carryback to the taxable year under
                                section 172,
                                    ``(IV) any capital loss carryback
                                to the taxable year under section
                                1212(a)(1), and
                            ``(ii) by substituting `100 percent' for
                        `50 percent' in section 951A(a).''.
            (2) Section 246(b)(1) is amended by--
                    (A) striking ``and without regard to'' and
                inserting ``without regard to'', and
                    (B) by striking the period at the end and inserting
                ``, and by substituting `100 percent' for `50 percent'
                in section 951A(a).''.
            (3) Section 469(i)(3)(F) is amended by striking
        ``determined without regard to'' and all that follows and
        inserting ``determined--
                            ``(i) without regard to--
                                    ``(I) any amount includible in
                                gross income under section 86,
                                    ``(II) the amounts allowable as a
                                deduction under section 219, and
                                    ``(III) any passive activity loss
                                or any loss allowable by reason of
                                subsection (c)(7), and
                            ``(ii) by substituting `100 percent' for
                        `50 percent' in section 951A(a).''.
            (4) Section 856(c)(2) is amended by striking ``and'' at the
        end of subparagraph (H), by adding ``and'' at the end of
        subparagraph (I), and by inserting after subparagraph (I) the
        following new subparagraph:
                    ``(J) amounts includible in gross income under
                section 951A(a);''.
            (5) Section 856(c)(3)(D) is amended by striking ``dividends
        or other distributions on, and gain'' and inserting
        ``dividends, other distributions on, amounts includible in
        gross income under section 951A(a) with respect to, and gain''.
            (6) The table of sections for subpart F of part III of
        subchapter N of chapter 1 is amended by inserting after the
        item relating to section 951 the following new item:

``Sec. 951A. Foreign high return amount included in gross income of
                            United States shareholders.''.
    (d) Effective Date.--The amendments made by this section shall
apply to taxable years of foreign corporations beginning after December
31, 2017, and to taxable years of United States shareholders in which
or with which such taxable years of foreign corporations end.

SEC. 4302. LIMITATION ON DEDUCTION OF INTEREST BY DOMESTIC CORPORATIONS
              WHICH ARE MEMBERS OF AN INTERNATIONAL FINANCIAL REPORTING
              GROUP.

    (a) In General.--Section 163 is amended by redesignating subsection
(n) as subsection (p) and by inserting after subsection (m) the
following new subsection:
    ``(n) Limitation on Deduction of Interest by Domestic Corporations
in International Financial Reporting Groups.--
            ``(1) In general.--In the case of any domestic corporation
        which is a member of any international financial reporting
        group, the deduction under this chapter for interest paid or
        accrued during the taxable year shall not exceed the sum of--
                    ``(A) the allowable percentage of 110 percent of
                the excess (if any) of --
                            ``(i) the amount of such interest so paid
                        or accrued, over
                            ``(ii) the amount described in subparagraph
                        (B), plus
                    ``(B) the amount of interest includible in gross
                income of such corporation for such taxable year.
            ``(2) International financial reporting group.--
                    ``(A) For purposes of this subsection, the term
                `international financial reporting group' means, with
                respect to any reporting year, any group of entities
                which--
                            ``(i) includes--
                                    ``(I) at least one foreign
                                corporation engaged in a trade or
                                business within the United States, or
                                    ``(II) at least one domestic
                                corporation and one foreign
                                corporation,
                            ``(ii) prepares consolidated financial
                        statements with respect to such year, and
                            ``(iii) reports in such statements average
                        annual gross receipts (determined in the
                        aggregate with respect to all entities which
                        are part of such group) for the 3-reporting-
                        year period ending with such reporting year in
                        excess of $100,000,000.
                    ``(B) Rules relating to determination of average
                gross receipts.--For purposes of subparagraph (A)(iii),
                rules similar to the rules of section 448(c)(3) shall
                apply.
            ``(3) Allowable percentage.--For purposes of this
        subsection--
                    ``(A) In general.--The term `allowable percentage'
                means, with respect to any domestic corporation for any
                taxable year, the ratio (expressed as a percentage and
                not greater than 100 percent) of--
                            ``(i) such corporation's allocable share of
                        the international financial reporting group's
                        reported net interest expense for the reporting
                        year of such group which ends in or with such
                        taxable year of such corporation, over
                            ``(ii) such corporation's reported net
                        interest expense for such reporting year of
                        such group.
                    ``(B) Reported net interest expense.--The term
                `reported net interest expense' means--
                            ``(i) with respect to any international
                        financial reporting group for any reporting
                        year, the excess of--
                                    ``(I) the aggregate amount of
                                interest expense reported in such
                                group's consolidated financial
                                statements for such taxable year, over
                                    ``(II) the aggregate amount of
                                interest income reported in such
                                group's consolidated financial
                                statements for such taxable year, and
                            ``(ii) with respect to any domestic
                        corporation for any reporting year, the excess
                        of--
                                    ``(I) the amount of interest
                                expense of such corporation reported in
                                the books and records of the
                                international financial reporting group
                                which are used in preparing such
                                group's consolidated financial
                                statements for such taxable year, over
                                    ``(II) the amount of interest
                                income of such corporation reported in
                                such books and records.
                    ``(C) Allocable share of reported net interest
                expense.--With respect to any domestic corporation
                which is a member of any international financial
                reporting group, such corporation's allocable share of
                such group's reported net interest expense for any
                reporting year is the portion of such expense which
                bears the same ratio to such expense as--
                            ``(i) the EBITDA of such corporation for
                        such reporting year, bears to
                            ``(ii) the EBITDA of such group for such
                        reporting year.
                    ``(D) EBITDA.--
                            ``(i) In general.--The term `EBITDA' means,
                        with respect to any reporting year, earnings
                        before interest, taxes, depreciation, and
                        amortization--
                                    ``(I) as determined in the
                                international financial reporting
                                group's consolidated financial
                                statements for such year, or
                                    ``(II) for purposes of subparagraph
                                (A)(i), as determined in the books and
                                records of the international financial
                                reporting group which are used in
                                preparing such statements if not
                                determined in such statements.
                            ``(ii) Treatment of disregarded entities.--
                        The EBITDA of any domestic corporation shall
                        not fail to include the EBITDA of any entity
                        which is disregarded for purposes of this
                        chapter.
                            ``(iii) Treatment of intra-group
                        distributions.--The EBITDA of any domestic
                        corporation shall be determined without regard
                        to any distribution received by such
                        corporation from any other member of the
                        international financial reporting group.
                    ``(E) Special rules for non-positive ebitda.--
                            ``(i) Non-positive group ebitda.--In the
                        case of any international financial reporting
                        group the EBITDA of which is zero or less,
                        paragraph (1) shall not apply to any member of
                        such group the EBITDA of which is above zero.
                            ``(ii) Non-positive entity ebitda.--In the
                        case of any group member the EBITDA of which is
                        zero or less, paragraph (1) shall be applied
                        without regard to subparagraph (A) thereof.
            ``(4) Consolidated financial statement.--For purposes of
        this subsection, the term `consolidated financial statement'
        means any consolidated financial statement described in
        paragraph (2)(A)(ii) if such statement is--
                    ``(A) a financial statement which is certified as
                being prepared in accordance with generally accepted
                accounting principles, international financial
                reporting standards, or any other comparable method of
                accounting identified by the Secretary, and which is--
                            ``(i) a 10-K (or successor form), or annual
                        statement to shareholders, required to be filed
                        with the United States Securities and Exchange
                        Commission,
                            ``(ii) an audited financial statement which
                        is used for--
                                    ``(I) credit purposes,
                                    ``(II) reporting to shareholders,
                                partners, or other proprietors, or to
                                beneficiaries, or
                                    ``(III) any other substantial
                                nontax purpose,
                        but only if there is no statement described in
                        clause (i), or
                            ``(iii) filed with any other Federal or
                        State agency for nontax purposes, but only if
                        there is no statement described in clause (i)
                        or (ii), or
                    ``(B) a financial statement which--
                            ``(i) is used for a purpose described in
                        subclause (I), (II), or (III) of subparagraph
                        (A)(ii), or
                            ``(ii) filed with any regulatory or
                        governmental body (whether domestic or foreign)
                        specified by the Secretary,
                but only if there is no statement described in
                subparagraph (A).
            ``(5) Reporting year.--For purposes of this subsection, the
        term `reporting year' means, with respect to any international
        financial reporting group, the year with respect to which the
        consolidated financial statements are prepared.
            ``(6) Application to certain entities.--
                    ``(A) Partnerships.--Except as otherwise provided
                by the Secretary in paragraph (7), this subsection
                shall apply to any partnership which is a member of any
                international financial reporting group under rules
                similar to the rules of section 163(j)(3).
                    ``(B) Foreign corporations engaged in trade or
                business within the united states.--Except as otherwise
                provided by the Secretary in paragraph (8), any
                deduction for interest paid or accrued by a foreign
                corporation engaged in a trade or business within the
                United States shall be limited in a manner consistent
                with the principles of this subsection.
                    ``(C) Consolidated groups.--For purposes of this
                subsection, the members of any group that file (or are
                required to file) a consolidated return with respect to
                the tax imposed by chapter 1 for a taxable year shall
                be treated as a single corporation.
            ``(7) Regulations.--The Secretary may issue such
        regulations or other guidance as are necessary or appropriate
        to carry out the purposes of this subsection.''.
    (b) Carryforward of Disallowed Interest.--
            (1) In general.--Section 163(o) is amended to read as
        follows:
    ``(o) Carryforward of Certain Disallowed Interest.--The amount of
any interest not allowed as a deduction for any taxable year by reason
of subsection (j)(1) or (n)(1) (whichever imposes the lower limitation
with respect to such taxable year) shall be treated as interest (and as
business interest for purposes of subsection (j)(1)) paid or accrued in
the succeeding taxable year. Interest paid or accrued in any taxable
year (determined without regard to the preceding sentence) shall not be
carried past the 5th taxable year following such taxable year,
determined by treating interest as allowed as a deduction on a first-
in, first-out basis.''.
            (2) Treatment of carryforward of disallowed interest in
        certain corporate acquisitions.--For rules related to the
        carryforward of disallowed interest in certain corporate
        acquisitions, see the amendments made by section 3301(c).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 4303. EXCISE TAX ON CERTAIN PAYMENTS FROM DOMESTIC CORPORATIONS TO
              RELATED FOREIGN CORPORATIONS; ELECTION TO TREAT SUCH
              PAYMENTS AS EFFECTIVELY CONNECTED INCOME.

    (a) Excise Tax on Certain Amounts From Domestic Corporations to
Foreign Affiliates.--
            (1) In general.--Chapter 36 is amended by adding at the end
        the following new subchapter:

      ``Subchapter E--Tax on Certain Amounts to Foreign Affiliates

``Sec. 4491. Imposition of tax on certain amounts from domestic
                            corporations to foreign affiliates.

``SEC. 4491. IMPOSITION OF TAX ON CERTAIN AMOUNTS FROM DOMESTIC
              CORPORATIONS TO FOREIGN AFFILIATES.

    ``(a) In General.--There is hereby imposed on each specified amount
paid or incurred by a domestic corporation to a foreign corporation
which is a member of the same international financial reporting group
as such domestic corporation a tax equal to the highest rate of tax in
effect under section 11 multiplied by such amount.
    ``(b) By Whom Paid.--The tax imposed by subsection (a) shall be
paid by the domestic corporation described in such subsection.
    ``(c) Exception for Effectively Connected Income.--Subsection (a)
shall not apply to so much of any specified amount as is effectively
connected with the conduct of a trade or business within the United
States if such amount is subject to tax under chapter 1. In the case of
any amount which is treated as effectively connected with the conduct
of a trade or business within the United States by reason of section
882(g), the preceding sentence shall apply to such amount only if the
domestic corporation provides to the Secretary (at such time and in
such form and manner as the Secretary may provide) a copy of the
election made under section 882(g) by the foreign corporation referred
to in subsection (a).
    ``(d) Definitions and Special Rules.--Terms used in this section
that are also used in section 882(g) shall have the same meaning as
when used in such section and rules similar to the rules of paragraphs
(5) and (6) of such section shall apply for purposes of this
section.''.
            (2) Denial of deduction for tax imposed.--Section 275(a) is
        amended by inserting after paragraph (6) the following new
        paragraph:
            ``(7) Taxes imposed by section 4491.''.
            (3) Clerical amendment.--The table of subchapters for
        chapter 36 is amended by adding at the end the following new
        item:

    ``subchapter e. tax on certain amounts to foreign affiliates.''.

    (b) Election to Treat Certain Payments From Domestic Corporations
to Related Foreign Corporations as Effectively Connected Income.--
Section 882 is amended by adding at the end the following new
subsection:
    ``(g) Election to Treat Certain Payments From Domestic Corporations
to Related Foreign Corporations as Effectively Connected Income.--
            ``(1) In general.--In the case of any specified amount paid
        or incurred by a domestic corporation to a foreign corporation
        which is a member of the same international financial reporting
        group as such domestic corporation and which has elected to be
        subject to the provisions of this subsection--
                    ``(A) such amount shall be taken into account
                (other than for purposes of sections 245, 245A, and
                881) in the taxable year of such foreign corporation
                during which such amount is paid or incurred as if--
                            ``(i) such foreign corporation were engaged
                        in a trade or business within the United
                        States,
                            ``(ii) such foreign corporation had a
                        permanent establishment in the United States
                        during the taxable year, and
                            ``(iii) such payment were effectively
                        connected with the conduct of a trade or
                        business within the United States and were
                        attributable to such permanent establishment,
                    ``(B) for purposes of subsection (c)(1)(A), no
                deduction shall be allowed with respect to such amount
                and such subsection shall be applied without regard to
                such amount, and
                    ``(C) the foreign corporation shall be allowed a
                deduction (for the taxable year referred to in
                subparagraph (A)) equal to the deemed expenses with
                respect to such amount.
            ``(2) Specified amount.--For purposes of this subsection--
                    ``(A) In general.--The term `specified amount'
                means any amount which is, with respect to the payor,
                allowable as a deduction or includible in costs of
                goods sold, inventory, or the basis of a depreciable or
                amortizable asset.
                    ``(B) Exceptions.--The term `specified amount'
                shall not include--
                            ``(i) interest,
                            ``(ii) any amount paid or incurred for the
                        acquisition of any security described in
                        section 475(c)(2) (determined without regard to
                        the last sentence thereof) or any commodity
                        described in section 475(e)(2),
                            ``(iii) except as provided in subparagraph
                        (C), any amount with respect to which tax is
                        imposed under section 881(a), and
                            ``(iv) in the case of a payor which has
                        elected to use a services cost method for
                        purposes of section 482, any amount paid or
                        incurred for services if such amount is the
                        total services cost with no markup.
                    ``(C) Amounts not treated as effectively connected
                to extent of gross-basis tax.--Subparagraph (B)(iii)
                shall only apply to so much of any specified amount as
                bears the proportion to such amount as--
                            ``(i) the rate of tax imposed under section
                        881(a) with respect to such amount, bears to
                            ``(ii) 30 percent.
            ``(3) Deemed expenses.--
                    ``(A) In general.--The deemed expenses with respect
                to any specified amount received by a foreign
                corporation during any reporting year is the amount of
                expenses such that the net income ratio of such foreign
                corporation with respect to such amount (taking into
                account only such specified amount and such deemed
                expenses) is equal to the net income ratio of the
                international financial reporting group determined for
                such reporting year with respect to the product line to
                which the specified amount relates.
                    ``(B) Net income ratio.--For purposes of this
                paragraph, the term `net income ratio' means the ratio
                of--
                            ``(i) net income determined without regard
                        to interest income, interest expense, and
                        income taxes, divided by
                            ``(ii) revenues.
                    ``(C) Method of determination.--Amounts described
                in subparagraph (B) shall be determined with respect to
                the international financial reporting group on the
                basis of the consolidated financial statements referred
                to in paragraph (4)(A)(i) and the books and records of
                the members of the international financial reporting
                group which are used in preparing such statements,
                taking into account only revenues and expenses of the
                members of such group (other than the members of such
                group which are (or are treated as) a domestic
                corporation for purposes of this subsection) derived
                from, or incurred with respect to--
                            ``(i) persons who are not members of such
                        group, and
                            ``(ii) members of such group which are (or
                        are treated as) a domestic corporation for
                        purposes of this subsection.
            ``(4) International financial reporting group.--For
        purposes of this subsection--
                    ``(A) In general.--The term `international
                financial reporting group' means any group of entities,
                with respect to any specified amount, if such amount is
                paid or incurred during a reporting year of such group
                with respect to which--
                            ``(i) such group prepares consolidated
                        financial statements (within the meaning of
                        section 163(n)(4)) with respect to such year,
                        and
                            ``(ii) the average annual aggregate payment
                        amount of such group for the 3-reporting-year
                        period ending with such reporting year exceeds
                        $100,000,000.
                    ``(B) Annual aggregate payment amount.--The term
                `annual aggregate payment amount' means, with respect
                to any reporting year of the group referred to in
                subparagraph (A)(i), the aggregate specified amounts to
                which paragraph (1) applies (or would apply if such
                group were an international financial reporting group).
                    ``(C) Application of certain rules.--Rules similar
                to the rules of subparagraphs (A), (B), and (D) of
                section 448(c)(3) shall apply for purposes of this
                paragraph.
            ``(5) Treatment of partnerships.--Any specified amount
        paid, incurred, or received by a partnership which is a member
        of any international financial reporting group (and any amount
        treated as paid, incurred, or received by a partnership under
        this paragraph) shall be treated for purposes of this
        subsection as amounts paid, incurred, or received,
        respectively, by each partner of such partnership in an amount
        equal to such partner's distributive share of the items of
        income, gain, deduction, or loss to which such amounts relate.
            ``(6) Treatment of amounts in connection with united states
        trade or business.--Any specified amount paid, incurred, or
        received by a foreign corporation in connection with the
        conduct of a trade or business within the United States (other
        than a trade or business it is deemed to conduct pursuant to
        this subsection) shall be treated for purposes of this
        subsection as an amount paid, incurred, or received,
        respectively, by a domestic corporation. For purposes of the
        preceding sentence, a foreign corporation shall be deemed to
        pay, incur, and receive amounts with respect to a trade or
        business it conducts within the United States (other than a
        trade or business it is deemed to conduct pursuant to this
        subsection) to the extent such foreign corporation would be
        treated as paying, incurring, or receiving such amounts from
        such trade or business if such trade or business were a
        domestic corporation.
            ``(7) Joint and several liability of members of internal
        financial reporting group.--In the case of any underpayment
        with respect to any taxable year of a foreign corporation which
        is a member of an international financial accounting group,
        each domestic corporation which is a member of such group at
        any time during such taxable year shall be jointly and
        severally liable for--
                    ``(A) so much of such underpayment as does not
                exceed the excess (if any) of such underpayment over
                the amount of such underpayment determined without
                regard to this subsection, and
                    ``(B) any penalty, addition to tax, or additional
                amount attributable to the amount described in
                subparagraph (A).
            ``(8) Foreign tax credit allowed.--The credit allowed under
        section 906(a) with respect to amounts taken into account in
        income under paragraph (1)(A) shall be limited to 80 percent of
        the amount of taxes paid or accrued and determined without
        regard to section 906(b)(1).
            ``(9) Election.--Any election under paragraph (1)--
                    ``(A) shall be made at such time and in such form
                and manner as the Secretary may provide, and
                    ``(B) shall apply for the taxable year for which
                made and all subsequent taxable years unless revoked
                with the consent of the Secretary.
            ``(10) Regulations.--The Secretary may issue such
        regulations or other guidance as are necessary or appropriate
        to carry out the purposes of this subsection, including
        regulations or other guidance--
                    ``(A) to provide for the proper determination of
                product lines, and
                    ``(B) to prevent the avoidance of the purposes of
                this subsection through the use of conduit transactions
                or by other means.''.
    (c) Reporting Requirements.--
            (1) Reporting by foreign corporation.--Section 6038C(b) is
        amended to read as follows:
    ``(b) Required Information.--
            ``(1) In general.--The information described in this
        subsection is--
                    ``(A) the information described in section
                6038A(b), and
                    ``(B) such other information as the Secretary may
                prescribe by regulations relating to any item not
                directly connected with a transaction for which
                information is required under subparagraph (A).
            ``(2) Certain payments from related domestic
        corporations.--
                    ``(A) In general.--In the case of any reporting
                corporation that receives during the taxable year any
                amount to which section 882(g)(1) applies, the
                information described in this subsection shall include,
                with respect to each member of the international
                financial reporting group from which any such amount is
                received--
                            ``(i) the name and taxpayer identification
                        number of such member,
                            ``(ii) the aggregate amounts received from
                        such member,
                            ``(iii) the product lines to which such
                        amounts relate, the aggregate amounts relating
                        to each such product line, and the net income
                        ratio for each such product line (determined
                        under section 882(g)(3)(B) with respect to the
                        international financial reporting group), and
                            ``(iv) a summary of any changes in
                        financial accounting methods that affect the
                        computation of any net income ratio described
                        in clause (iii).
                    ``(B) Definitions and special rules.--Terms used in
                this paragraph that are also used in section 882(g)
                shall have the same meaning as when used in such
                section and rules similar to the rules of paragraphs
                (5) and (6) of such section shall apply for purposes of
                this paragraph.''.
            (2) Reporting by domestic group members.--
                    (A) In general .--Subpart A of part III of
                subchapter A of chapter 61 is amended by inserting
                after section 6038D the following new section:

``SEC. 6038E. INFORMATION WITH RESPECT TO CERTAIN PAYMENTS FROM
              DOMESTIC CORPORATIONS TO RELATED FOREIGN CORPORATIONS.

    ``(a) In General.--In the case of any domestic corporation which
pays or incurs any amount to which section 882(g)(1) applies, such
person shall--
            ``(1) make a return according to the forms and regulations
        prescribed the Secretary, setting forth the information
        described in subsection (b), and
            ``(2) maintain (at the location, in the manner, and to the
        extent prescribed in regulations) such records as may be
        appropriate to determine liability for tax pursuant to
        paragraphs (1) and (7) of section 882(g).
    ``(b) Required Information.--The information described in this
subsection is--
            ``(1) the name and taxpayer identification number of the
        common parent of the international financial reporting group in
        which such domestic corporation is a member, and
            ``(2) with respect to any person who receives an amount
        described in subsection (a) from such domestic corporation--
                    ``(A) the name and taxpayer identification number
                of such person,
                    ``(B) the aggregate amounts received by such
                person,
                    ``(C) the product lines to which such amounts
                relate, the aggregate amounts relating to each such
                product line, and the net income ratio for each such
                product line (determined under section 882(g)(3)(B)
                with respect to the international financial reporting
                group), and
                    ``(D) a summary of any changes in financial
                accounting methods that affect the computation of any
                net income ratios described in subparagraph (C).
    ``(c) Definitions and Special Rules.--Terms used in this paragraph
that are also used in section 882(g) shall have the same meaning as
when used in such section and rules similar to the rules of paragraphs
(5) and (6) of such section shall apply for purposes of this
paragraph.''.
                    (B) Clerical amendment.--The table of sections for
                subpart A of part III of subchapter A of chapter 61 is
                amended by inserting after the item relating to section
                6038D the following new item:

``Sec. 6038E. Information with respect to certain payments from
                            domestic corporations to related foreign
                            corporations.''.
    (d) Effective Date.--The amendments made by this section shall
apply to amounts paid or incurred after December 31, 2018.

   Subtitle E--Provisions Related to Possessions of the United States

SEC. 4401. EXTENSION OF DEDUCTION ALLOWABLE WITH RESPECT TO INCOME
              ATTRIBUTABLE TO DOMESTIC PRODUCTION ACTIVITIES IN PUERTO
              RICO.

    (a) In General.--Section 199(d)(8)(C), prior to its repeal by this
Act, is amended--
            (1) by striking ``first 11 taxable years'' and inserting
        ``first 12 taxable years'', and
            (2) by striking ``January 1, 2017'' and inserting ``January
        1, 2018''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2016.

SEC. 4402. EXTENSION OF TEMPORARY INCREASE IN LIMIT ON COVER OVER OF
              RUM EXCISE TAXES TO PUERTO RICO AND THE VIRGIN ISLANDS.

    (a) In General.--Section 7652(f)(1) is amended by striking
``January 1, 2017'' and inserting ``January 1, 2023''.
    (b) Effective Date.--The amendment made by this section shall apply
to distilled spirits brought into the United States after December 31,
2016.

SEC. 4403. EXTENSION OF AMERICAN SAMOA ECONOMIC DEVELOPMENT CREDIT.

    (a) In General.--Section 119(d) of division A of the Tax Relief and
Health Care Act of 2006 is amended--
            (1) by striking ``January 1, 2017'' each place it appears
        and inserting ``January 1, 2023'',
            (2) by striking ``first 11 taxable years'' in paragraph (1)
        and inserting ``first 17 taxable years'', and
            (3) by striking ``first 5 taxable years'' in paragraph (2)
        and inserting ``first 11 taxable years''.
    (b) Treatment of Certain References.--Section 119(e) of division A
of the Tax Relief and Health Care Act of 2006 is amended by adding at
the end the following: ``References in this subsection to section 199
of the Internal Revenue Code of 1986 shall be treated as references to
such section as in effect before its repeal by the Tax Cuts and Jobs
Act.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2016.

                Subtitle F--Other International Reforms

SEC. 4501. RESTRICTION ON INSURANCE BUSINESS EXCEPTION TO PASSIVE
              FOREIGN INVESTMENT COMPANY RULES.

    (a) In General.--Section 1297(b)(2)(B) is amended to read as
follows:
                    ``(B) derived in the active conduct of an insurance
                business by a qualifying insurance corporation (as
                defined in subsection (f)),''.
    (b) Qualifying Insurance Corporation Defined.--Section 1297 is
amended by adding at the end the following new subsection:
    ``(f) Qualifying Insurance Corporation.--For purposes of subsection
(b)(2)(B)--
            ``(1) In general.--The term `qualifying insurance
        corporation' means, with respect to any taxable year, a foreign
        corporation--
                    ``(A) which would be subject to tax under
                subchapter L if such corporation were a domestic
                corporation, and
                    ``(B) the applicable insurance liabilities of which
                constitute more than 25 percent of its total assets,
                determined on the basis of such liabilities and assets
                as reported on the corporation's applicable financial
                statement for the last year ending with or within the
                taxable year.
            ``(2) Alternative facts and circumstances test for certain
        corporations.--If a corporation fails to qualify as a qualified
        insurance corporation under paragraph (1) solely because the
        percentage determined under paragraph (1)(B) is 25 percent or
        less, a United States person that owns stock in such
        corporation may elect to treat such stock as stock of a
        qualifying insurance corporation if--
                    ``(A) the percentage so determined for the
                corporation is at least 10 percent, and
                    ``(B) under regulations provided by the Secretary,
                based on the applicable facts and circumstances--
                            ``(i) the corporation is predominantly
                        engaged in an insurance business, and
                            ``(ii) such failure is due solely to
                        runoff-related or rating-related circumstances
                        involving such insurance business.
            ``(3) Applicable insurance liabilities.--For purposes of
        this subsection--
                    ``(A) In general.--The term `applicable insurance
                liabilities' means, with respect to any life or
                property and casualty insurance business--
                            ``(i) loss and loss adjustment expenses,
                        and
                            ``(ii) reserves (other than deficiency,
                        contingency, or unearned premium reserves) for
                        life and health insurance risks and life and
                        health insurance claims with respect to
                        contracts providing coverage for mortality or
                        morbidity risks.
                    ``(B) Limitations on amount of liabilities.--Any
                amount determined under clause (i) or (ii) of
                subparagraph (A) shall not exceed the lesser of such
                amount--
                            ``(i) as reported to the applicable
                        insurance regulatory body in the applicable
                        financial statement described in paragraph
                        (4)(A) (or, if less, the amount required by
                        applicable law or regulation), or
                            ``(ii) as determined under regulations
                        prescribed by the Secretary.
            ``(4) Other definitions and rules.--For purposes of this
        subsection--
                    ``(A) Applicable financial statement.--The term
                `applicable financial statement' means a statement for
                financial reporting purposes which--
                            ``(i) is made on the basis of generally
                        accepted accounting principles,
                            ``(ii) is made on the basis of
                        international financial reporting standards,
                        but only if there is no statement that meets
                        the requirement of clause (i), or
                            ``(iii) except as otherwise provided by the
                        Secretary in regulations, is the annual
                        statement which is required to be filed with
                        the applicable insurance regulatory body, but
                        only if there is no statement which meets the
                        requirements of clause (i) or (ii).
                    ``(B) Applicable insurance regulatory body.--The
                term `applicable insurance regulatory body' means, with
                respect to any insurance business, the entity
                established by law to license, authorize, or regulate
                such business and to which the statement described in
                subparagraph (A) is provided.''.
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

                     TITLE V--EXEMPT ORGANIZATIONS

               Subtitle A--Unrelated Business Income Tax

SEC. 5001. CLARIFICATION OF UNRELATED BUSINESS INCOME TAX TREATMENT OF
              ENTITIES TREATED AS EXEMPT FROM TAXATION UNDER SECTION
              501(A).

    (a) In General.--Section 511 is amended by adding at the end the
following new subsection:
    ``(d) Organizations and Trusts Exempt From Taxation Not Solely by
Reason of Section 501(a).--For purposes of subsections (a)(2) and
(b)(2), an organization or trust shall not fail to be treated as exempt
from taxation under this subtitle by reason of section 501(a) solely
because such organization is also so exempt, or excludes amounts from
gross income, by reason of any other provision of this title.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 5002. EXCLUSION OF RESEARCH INCOME LIMITED TO PUBLICLY AVAILABLE
              RESEARCH.

    (a) In General.--Section 512(b)(9) is amended by striking ``from
research'' and inserting ``from such research''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

                        Subtitle B--Excise Taxes

SEC. 5101. SIMPLIFICATION OF EXCISE TAX ON PRIVATE FOUNDATION
              INVESTMENT INCOME.

    (a) Rate Reduction.--Section 4940(a) is amended by striking ``2
percent'' and inserting ``1.4 percent''.
    (b) Repeal of Special Rules for Certain Private Foundations.--
Section 4940 is amended by striking subsection (e).
    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 5102. PRIVATE OPERATING FOUNDATION REQUIREMENTS RELATING TO
              OPERATION OF ART MUSEUM.

    (a) In General.--Section 4942(j) is amended by adding at the end
the following new paragraph:
            ``(6) Organization operating art museum.--For purposes of
        this section, the term `operating foundation' shall not include
        an organization which operates an art museum as a substantial
        activity unless such museum is open during normal business
        hours to the public for at least 1,000 hours during the taxable
        year.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 5103. EXCISE TAX BASED ON INVESTMENT INCOME OF PRIVATE COLLEGES
              AND UNIVERSITIES.

    (a) In General.--Chapter 42 is amended by adding at the end the
following new subchapter:

   ``Subchapter H--Excise Tax Based on Investment Income of Private
                       Colleges and Universities

``Sec. 4969. Excise tax based on investment income of private colleges
                            and universities.

``SEC. 4969. EXCISE TAX BASED ON INVESTMENT INCOME OF PRIVATE COLLEGES
              AND UNIVERSITIES.

    ``(a) Tax Imposed.--There is hereby imposed on each applicable
educational institution for the taxable year a tax equal to 1.4 percent
of the net investment income of such institution for the taxable year.
    ``(b) Applicable Educational Institution.--For purposes of this
subchapter--
            ``(1) In general.--The term `applicable educational
        institution' means an eligible educational institution (as
        defined in section 25A(e)(3))--
                    ``(A) which has at least 500 students during the
                preceding taxable year,
                    ``(B) which is not described in the first sentence
                of section 511(a)(2)(B), and
                    ``(C) the aggregate fair market value of the assets
                of which at the end of the preceding taxable year
                (other than those assets which are used directly in
                carrying out the institution's exempt purpose) is at
                least $250,000 per student of the institution.
            ``(2) Students.--For purposes of paragraph (1), the number
        of students of an institution shall be based on the daily
        average number of full-time students attending such institution
        (with part-time students taken into account on a full-time
        student equivalent basis).
    ``(c) Net Investment Income.--For purposes of this section, net
investment income shall be determined under rules similar to the rules
of section 4940(c).
    ``(d) Assets and Net Investment Income of Related Organizations.--
            ``(1) In general.--For purposes of subsections (b)(1)(C)
        and (c), the assets and net investment income of any related
        organization shall be treated as the assets and net investment
        income of the eligible educational institution.
            ``(2) Related organization.--For purposes of this
        subsection, the term `related organization' means, with respect
        to an eligible educational institution, any organization
        which--
                    ``(A) controls, or is controlled by, such
                institution,
                    ``(B) is controlled by one or more persons that
                control such institution, or
                    ``(C) is a supported organization (as defined in
                section 509(f)(3)), or an organization described in
                section 509(a)(3), during the taxable year with respect
                to such institution.''.
    (b) Clerical Amendment.--The table of subchapters for chapter 42 is
amended by adding at the end the following new item:

   ``subchapter h--excise tax based on investment income of private
                      colleges and universities''.

    (c) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

SEC. 5104. EXCEPTION FROM PRIVATE FOUNDATION EXCESS BUSINESS HOLDING
              TAX FOR INDEPENDENTLY-OPERATED PHILANTHROPIC BUSINESS
              HOLDINGS.

    (a) In General.--Section 4943 is amended by adding at the end the
following new subsection:
    ``(g) Exception for Certain Holdings Limited to Independently-
operated Philanthropic Business.--
            ``(1) In general.--Subsection (a) shall not apply with
        respect to the holdings of a private foundation in any business
        enterprise which for the taxable year meets--
                    ``(A) the ownership requirements of paragraph (2),
                    ``(B) the all profits to charity distribution
                requirement of paragraph (3), and
                    ``(C) the independent operation requirements of
                paragraph (4).
            ``(2) Ownership.--The ownership requirements of this
        paragraph are met if--
                    ``(A) 100 percent of the voting stock in the
                business enterprise is held by the private foundation
                at all times during the taxable year, and
                    ``(B) all the private foundation's ownership
                interests in the business enterprise were acquired not
                by purchase.
            ``(3) All profits to charity.--
                    ``(A) In general.--The all profits to charity
                distribution requirement of this paragraph is met if
                the business enterprise, not later than 120 days after
                the close of the taxable year, distributes an amount
                equal to its net operating income for such taxable year
                to the private foundation.
                    ``(B) Net operating income.--For purposes of this
                paragraph, the net operating income of any business
                enterprise for any taxable year is an amount equal to
                the gross income of the business enterprise for the
                taxable year, reduced by the sum of--
                            ``(i) the deductions allowed by chapter 1
                        for the taxable year which are directly
                        connected with the production of such income,
                            ``(ii) the tax imposed by chapter 1 on the
                        business enterprise for the taxable year, and
                            ``(iii) an amount for a reasonable reserve
                        for working capital and other business needs of
                        the business enterprise.
            ``(4) Independent operation.--The independent operation
        requirements of this paragraph are met if, at all times during
        the taxable year--
                    ``(A) no substantial contributor (as defined in
                section 4958(c)(3)(C)) to the private foundation, or
                family member of such a contributor (determined under
                section 4958(f)(4)) is a director, officer, trustee,
                manager, employee, or contractor of the business
                enterprise (or an individual having powers or
                responsibilities similar to any of the foregoing),
                    ``(B) at least a majority of the board of directors
                of the private foundation are not--
                            ``(i) also directors or officers of the
                        business enterprise, or
                            ``(ii) members of the family (determined
                        under section 4958(f)(4)) of a substantial
                        contributor (as defined in section
                        4958(c)(3)(C)) to the private foundation, and
                    ``(C) there is no loan outstanding from the
                business enterprise to a substantial contributor (as so
                defined) to the private foundation or a family member
                of such contributor (as so determined).
            ``(5) Certain deemed private foundations excluded.--This
        subsection shall not apply to--
                    ``(A) any fund or organization treated as a private
                foundation for purposes of this section by reason of
                subsection (e) or (f),
                    ``(B) any trust described in section 4947(a)(1)
                (relating to charitable trusts), and
                    ``(C) any trust described in section 4947(a)(2)
                (relating to split-interest trusts).''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2017.

       Subtitle C--Requirements for Organizations Exempt From Tax

SEC. 5201. 501(C)(3) ORGANIZATIONS PERMITTED TO MAKE STATEMENTS
              RELATING TO POLITICAL CAMPAIGN IN ORDINARY COURSE OF
              ACTIVITIES.

    (a) In General.--Section 501 is amended by adding at the end the
following new subsection:
    ``(s) Special Rule Relating to Political Campaign Statements of
Organizations Described in Subsection (c)(3).--
            ``(1) In general.--For purposes of subsection (c)(3) and
        sections 170(c)(2), 2055, 2106, 2522, and 4955, an organization
        shall not fail to be treated as organized and operated
        exclusively for a purpose described in subsection (c)(3), nor
        shall it be deemed to have participated in, or intervened in
        any political campaign on behalf of (or in opposition to) any
        candidate for public office, solely because of the content of
        any statement which--
                    ``(A) is made in the ordinary course of the
                organization's regular and customary activities in
                carrying out its exempt purpose, and
                    ``(B) results in the organization incurring not
                more than de minimis incremental expenses.
            ``(2) Termination.--Paragraph (1) shall not apply to
        taxable years beginning after December 31, 2023.''.
    (b) Effective Date.--The amendments made by this section shall
apply to taxable years beginning after December 31, 2018.

SEC. 5202. ADDITIONAL REPORTING REQUIREMENTS FOR DONOR ADVISED FUND
              SPONSORING ORGANIZATIONS.

    (a) In General.--Section 6033(k) is amended by striking ``and'' at
the end of paragraph (2), by striking the period at the end of
paragraph (3), and by adding at the end the following new paragraphs:
            ``(4) indicate the average amount of grants made from such
        funds during such taxable year (expressed as a percentage of
        the value of assets held in such funds at the beginning of such
        taxable year), and
            ``(5) indicate whether the organization has a policy with
        respect to donor advised funds (as so defined) for frequency
        and minimum level of distributions.
Such organization shall include with such return a copy of any policy
described in paragraph (5).''.
    (b) Effective Date.--The amendment made by this section shall apply
for returns filed for taxable years beginning after December 31, 2017.

            Passed the House of Representatives November 16, 2017.

            Attest:

                                                 KAREN L. HAAS,

                                                                 Clerk.
                                                       Calendar No. 266

115th CONGRESS

  1st Session

                                H. R. 1

_______________________________________________________________________

                                 AN ACT

   To provide for reconciliation pursuant to titles II and V of the
       concurrent resolution on the budget for fiscal year 2018.

_______________________________________________________________________

                           November 28, 2017

            Read the second time and placed on the calendar
