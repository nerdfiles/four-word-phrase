In September of last year, Donald Trump released his first tax plan, but now he has made another go of it. Over the past couple of months, he has released an overhaul that changes rates and includes newly announced child care deductions. The revised plan would still cost the government trillions in revenues, according to a new analysis, though not as much as his last plan.

The right-leaning Tax Foundation released an analysis Monday that said Trump's campaign would shrink federal revenues by as much as $5.9 trillion over 10 years. His former plan would have done so by up to $12.3 trillion.

The new plan adopts individual tax brackets from the House Republicans' tax plan, creating three brackets of 12, 25, and 33 percent (down from the current seven brackets). It would also lower the corporate tax rate to 15 percent, eliminate the estate tax and increase the standard deduction, among other things. Altogether, over 10 years, it would reduce federal revenue by around $4.4 trillion to $5.9 trillion.

One huge question, still unanswered

That $1.5 trillion gap between $4.4 and $5.9 trillion exists because one big aspect of the Trump tax plan has been unclear: how to tax "pass-through" businesses. In these firms, the owners pay taxes on earnings at individual income tax rates.

In his old plan, Trump had said pass-through businesses would be taxed at 15 percent, the same as the corporate tax rate he is proposing. However, since Trump released the new version of this plan, his campaign has given two different explanations about how that income would be taxed: one saying it would be 15 percent; the other saying it would be at individual rates, as the New York Times' Binyamin Appelbaum has reported.

That makes a huge difference when it comes to revenue. Given the uncertainty, the Tax Foundation created two estimates of what the plan could do. If pass-through income is taxed at the lower rate, revenue would shrink by $5.9 trillion over 10 years on a static basis (that is, not including the effects of economic growth). If it were taxed at higher, individual income tax rates, the revenue cut would be nearly $4.4 trillion.

(Monday, Politico reported a new development: "the Trump camp says that pass-throughs could claim that 15 percent rate, but would also have to join corporations in taking on a second layer of tax — a 20 percent hit — when owners take money out of the business.")

And economic growth wouldn't close those holes. Even taking economic growth into effect, in what are known as dynamic estimates, the foundation estimates that the plan would reduce revenues by $2.6 trillion to $3.9 trillion. Dynamic estimates take more factors into account than static estimates, but economists don't all agree on the growth effects of different tax changes.

A less progressive vode, aiming for lofty goals

The plan would also make the tax code less progressive. While it would increase incomes across the income spectrum, it would increase them by much more for the richest Americans. The bottom four quintiles of income groups would see their incomes go up by 0.8 to 1.9 percent.

But people between the 80th and 100th percentiles would see higher after-tax incomes by 4.4 to 6.5 percent (on a static basis, again depending on how that pass-through rate ends up). In that top 1 percent, the income growth is particularly high — 10.2 to 16 percent.

Then there's economic growth, which wouldn't be nearly as impressive under this plan as Trump would like. He said last week that he thinks he could get the economy to grow by 3.5 percent or 4 percent per year.

The Tax Foundation tells NPR that it estimates Trump's plan would add around 0.5 percentage points to economic growth each year.

Currently, members of the Federal Reserve's Open Market Committee say they expect annual GDP growth rates of 2 percent in the long run. An extra half-point each year would still be far from Trump's goals.


Moreover, 3.5 to 4 percent is a growth rate the U.S. economy hasn't seen in a long time, as NPR's Scott Horsley recently reported.

Likewise, Trump had said his economic policies could produce 25 million jobs over 10 years. This plan would create up to 2.2 million jobs beyond current projections, the Tax Foundation found, leaving a massive gap between these projections and Trump's goal.

"It is fair to say that if his plan — if he enacts this plan, pays for it, all that stuff, and he gets to this growth rate [that Tax Foundation predicts], it's still a lot of work for his regulatory and other policy proposals to do," said Kyle Pomerleau, director of federal projects at the Tax Foundation. "They've got a lot of work to do to get to the millions more jobs."

Indeed, this tax plan isn't the Trump campaign's only economic proposal. The Tax Foundation points to Trump's trade policies as an example: If he raises tariffs, it would reduce growth but raise revenue, they write.

Taken together, Trump's plans could easily damage the economy. According to one analysis from Moody's Economics, Trump's policies taken at "face value" would create only 1.4 percent average growth per year over the next decade and lead to several years of negative employment growth.
