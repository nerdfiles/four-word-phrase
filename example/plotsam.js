var pos = require('pos');
var _ = require('lodash')
var fs = require('fs');

String.prototype.preposition = function() {
  var self = this;
  var terms = [
    'according to',
    'because of',
    'by way of',
    'in addition to',
    'in front of',
    'in place of',
    'in regard to',
    'in spite of',
    'instead of',
    'on account of',
    'out of',
    'beside',
    'immanent to',
    'with respect to',
    'through',
    'by',
    'down',
    'enduring',
    'except',
    'for',
    'from',
    'in',
    'inside',
    'into',
    'like',
    'near',
    'of',
    'off',
    'on',
    'out'
  ];
  var id = Math.floor(Math.random() * terms.length);
  // console.log(terms[id]);
  var sep = ' ';
  return [sep, [ self, terms[id] ].join(' '), sep].join('');
};

String.prototype.satellite = function() {
  var sep = ' ';
  var self = this;
  var terms = [
    'the moon',
    'the sky',
    'the ether',
    'the nature'
  ];
  var id = (Math.floor(Math.random() * terms.length));
  return [sep, [ self, terms[id].toString() ].join(' '), sep.preposition()].join('');
};

String.prototype.__de_re__ = function() {
  var self = this;
};
String.prototype.__de_se__ = function() {};
String.prototype.__de_te__ = function() {};
String.prototype.__de_dicto__ = function() {};

String.prototype.prefix = function() {
  var self = this;
  // self = '/' + self;
  self = ' ' + self;
  return self;
};

const natural = require('natural');
const tokenizer = new natural.WordTokenizer()
const NGrams = natural.NGrams

/**
 * @name postagger
 * @param filename
 * @returns {undefined}
 */
function postagger(filename) {

  return new Promise(function(resolve, reject) {

    var lang = [];

    if (!filename) {
      return reject(Error('!'))
    }

    fs.readFile(filename, 'utf-8', function(error, data) {

      var sentences = data.split('.');

      sentences.forEach(function(sentence) {

        var words = new pos.Lexer().lex(sentence);

        var tags = new pos.Tagger()
          .tag(words)
          .map(function(tag) {
            return tag[0].preposition();
          });

//         _.each(tags, function(item) {
//           lang.push(item);
//         });

        var _tags = new pos.Tagger()
          .tag(words)
          .map(function(tag) {
            return tag[0].preposition();
          });

//         _.each(_tags, function(item) {
//           lang.push(item);
//         });

        var sats = new pos.Tagger()
          .tag(words)
          .map(function(tag) {
            return tag[0].satellite();
          });
        _.each(sats, function(item) {
          lang.push(item);
        });
      });

      lang = lang.sort(function(a, b) {
        return b - a;
      });

      resolve(lang);
    });

  });

}

postagger('./badiou--bataille.md').then(function(res) {

  var res1 = function() {
    return res[Math.floor(Math.random() * 20)];
  }

  var u = [

    res1(),    res1(),    res1(),
    res1(),    res1(),    res1(),
    res1(),    res1(),    res1()

  ].join(' ');

  var words = new pos.Lexer().lex(u);
  var tags = new pos.Tagger()
    .tag(words)
    .map(function(tag) {
      // introspect some term from fwp
      if (tag[1] === ':')
        return '';
      if (tag[1] === 'JJ')
        return '';
      if (tag[1] === 'NN')
        return ' '.preposition() + tag[0] + ' ';
      if (tag[1] === 'NNS')
        return tag[0] + ' is '.satellite();
      return tag[0].prefix() + ' ';
    })
    .join('\n');
  console.log(tags);

}, function(e) {
});
