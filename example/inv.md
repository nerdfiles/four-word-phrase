
Topics
Reference
Advisors
Markets
Simulator
NEW Academy

Search News, Symbols, Terms

Interest Rate Parity + SUBSCRIBE
  SHARE

What is the 'Interest Rate Parity'
Interest rate parity is a theory in which the interest rate differential between two countries is equal to the differential between the forward exchange rate and the spot exchange rate. Interest rate parity plays an essential role in foreign exchange markets, connecting interest rates, spot exchange rates and foreign exchange rates.
BREAKING DOWN 'Interest Rate Parity'
If one country offers a higher risk-free rate of return in one currency than that of another, the country that offers the higher risk-free rate of return will be exchanged at a more expensive future price than the current spot price. In other words, the interest rate parity presents an idea that there is no arbitrage in the foreign exchange markets. Investors cannot lock in the current exchange rate in one currency for a lower price and then purchase another currency from a country offering a higher interest rate.
Covered vs. Uncovered Interest Rate Parity
The interest rate parity is said to be covered when the no-arbitrage condition could be satisfied through the use of forward contracts in an attempt to hedge against foreign exchange risk. Conversely, the interest rate parity is said to be uncovered when the no-arbitrage condition could be satisfied without the use of forward contracts to hedge against foreign exchange risk.

Options of Converting Currencies
The relationship can be seen in the two methods an investor may take to convert foreign currency into U.S. dollars.

One option an investor may take would be to invest the foreign currency locally at the foreign risk-free rate for a specific time period. The investor would then simultaneously enter into a forward rate agreement to convert the proceeds from the investment into U.S. dollars, using a forward exchange rate, at the end of the investing period.

The second option would be to convert the foreign currency to U.S. dollars at the spot exchange rate, then invest the dollars for the same amount of time as in option A, at the local (U.S.) risk-free rate. When no arbitrage opportunities exist, the cash flows from both options are equal.

Covered Interest Rate Parity Example
For example, assume Australian Treasury bills are offering an annual interest rate of 1.75%, while U.S. Treasury bills are offering an annual interest rate of 0.5%. If an investor in the United States seeks to take advantage of the interest rates in Australia, the investor would have to translate U.S. dollars to Australian dollars to purchase the Treasury bill. Thereafter, the investor would have to sell a one-year forward contract on the Australian dollar. However, under the covered interest rate parity, the transaction would only have a return of 0.5%, or else the no-arbitrage condition would be violated.

Trading Center
NEXT UP
Interest Rate Parity Uncovered Interest Rate Parity ...  Parity Price  Forward Premium  Risk Parity  Conversion Parity Price  Interest Rate Differential - IRD  Foreign Exchange  Currency Exchange  Forward Discount
Uncovered Interest Rate Parity - UIP + SUBSCRIBE
  SHARE
Video Definition

00:0000:00
The uncovered interest rate parity (UIP) is a parity condition stating that the difference in interest rates between two countries is equal to the expected change in exchange rates between the countries' currencies. If this parity does not exist, there is an opportunity to make a risk-free profit using arbitrage techniques.

BREAKING DOWN 'Uncovered Interest Rate Parity - UIP'
Assuming foreign exchange equilibrium, interest rate parity implies that the expected return of a domestic asset will equal the expected return of a foreign asset once adjusted for exchange rates. There are two types of interest rate parity: covered interest rate parity and uncovered interest rate parity. When this no-arbitrage condition exists without the use of forward contracts, which are used to hedge foreign currency risk, it is called uncovered interest rate parity.

Read More +

Search Investopedia

DICTIONARY: # A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
CONTENT LIBRARY
Articles Terms Videos Guides Slideshows FAQs Calculators Chart Advisor Stock Analysis Stock Simulator FXtrader Exam Prep Quizzer Net Worth Calculator
CONNECT WITH INVESTOPEDIA

WORK WITH INVESTOPEDIA
About Us Advertise With Us Contact Us Careers
GET FREE NEWSLETTERS
Newsletters
© 2017, Investopedia, LLC. All Rights Reserved Terms Of Use Privacy Policy
