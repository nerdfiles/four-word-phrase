It is clear that the world is purely parodic, in other words, that each thing seen is the parody of another, or is the same thing in a deceptive form.

Ever since sentences started to circulate in brains devoted to reflection, an effort at total identification has been made, because with the aid of a copula each sentence ties one thing to another; all things would be visibly connected if one could discover at a single glance and in its totality the tracings of Ariadne’s thread leading thought into its own labyrinth.

But the copula of terms is no less irritating than the copulation of bodies. And when I scream I AM THE SUN an integral erection results, because the verb to be is the vehicle of amorous frenzy.

Everyone is aware that life is parodic and that it lacks an interpretation. Thus lead is the parody of gold. Air is the parody of water. The brain is the parody of the equator. Coitus is the parody of crime.

Gold, water, the equator, or crime can each be put forward as the principle of things.

And if the origin of things is not like the ground of the planet that seems to be the base, but like the circular movement that the planet describes around a mobile center, then a car, a clock, or a sewing machine could equally be accepted as the generative principle.

The two primary motions are rotation and sexual movement, whose combination is expressed by the locomotive’s wheels and pistons.

These two motions are reciprocally transformed, the one into the other.

Thus one notes that the earth, by turning, makes animals and men have coitus, and (because the result is as much the cause as that which provokes it) that animals and men make the earth turn by having coitus.

It is the mechanical combination or transformation of these movements that the alchemists sought as the philosopher’s stone.

It is through the use of this magically valued combination that one can determine the present position of men in the midst of the elements.

An abandoned shoe, a rotten tooth, a snub nose, the cook spitting in the soup of his masters are to love what a battle flag is to nationality.

An umbrella, a sexagenarian, a seminarian, the smell of rotten eggs, the hollow eyes of judges are the roots that nourish love.

A dog devouring the stomach of a goose, a drunken vomiting woman, a slobbering accountant, a jar of mustard represent the confusion that serves as the vehicle of love.

A man who finds himself among others is irritated because he does not know why he is not one of the others.

In bed next to a girl he loves, he forgets that he does not know why he is himself instead of the body he touches.

Without knowing it, he suffers from the mental darkness that keeps him from screaming that he himself is the girl who forgets his presence while shuddering in his arms.

Love or infantile rage, or a provincial dowager’s vanity, or clerical pornography, or the diamond of a soprano bewilder individuals forgotten in dusty apartments.

They can very well try to find each other; they will never find anything but parodic images, and they will fall asleep as empty as mirrors.

The absent and inert girl hanging dreamless from my arms is no more foreign to me than the door or window through which I can look or pass.

I rediscover indifference (allowing her to leave me) when I fall asleep, through an inability to love what happens.

It is impossible for her to know whom she will discover when I hold her, because she obstinately attains a complete forgetting.

The planetary systems that turn in space like rapid disks, and whose centers also move, describing an infinitely larger circle, only move away continuously from their own position in order to return it, completing their rotation.

Movement is a figure of love, incapable of stopping at a particular being, and rapidly passing from one to another.

But the forgetting that determines it in this way is only a subterfuge of memory.

A man gets up as brusquely as a specter in a coffin and falls in the same way.

He gets up a few hours later and then he falls again, and the same thing happens every day; this great coitus with the celestial atmosphere is regulated by the terrestrial rotation around the sun.

Thus even though terrestrial life moves to the rhythm of this rotation, the image of this movement is not turning earth, but the male shaft penetrating the female and almost entirely emerging, in order to reenter.

Love and life appear to be separate only because everything on earth is broken apart by vibrations of various amplitudes and durations.

However, there are no vibrations that are not conjugated with a continuous circular movement; in the same way, a locomotive rolling on the surface of the earth is the image of continuous metamorphosis.

Beings only die to be born, in the manner of phalluses that leave bodies in order to enter them.

Plants rise in the direction of the sun and then collapse in the direction of the ground.

Trees bristle the ground with a vast quantity of flowered shafts raised up to the sun.

The trees that forcefully soar end up burned by lightning, chopped down, or uprooted. Returned to the ground, they come back up in another form.

But their polymorphous coitus is a function of uniform terrestrial rotation.

The simplest image of organic life united with rotation is the tide. From the movement of the sea, uniform coitus of the earth with the moon, comes the polymorphous and organic coitus of the earth with the sun.

But the first form of solar love is a cloud raised up over the liquid element. The erotic cloud sometimes becomes a storm and falls back to earth in the form of rain, while lightning staves in the layers of the atmosphere.

The rain is soon raised up again in the form of an immobile plant.

Animal life comes entirely from the movement of the seas and, inside bodies, life continues to come from salt water.

The sea, then, has played the role of the female organ that liquefies under the excitation of the penis.

The sea continuously jerks off.

Solid elements, contained and brewed in water animated by erotic movement, shoot out in the form of flying fish.

The erection and the sun scandalize, in the same way as the cadaver and the darkness of cellars.

Vegetation is uniformly directed towards the sun; human beings, on the other hand, even though phalloid like trees, in opposition to other animals, necessarily avert their eyes.

Human eyes tolerate neither sun, coitus, cadavers, nor obscurity, but with different reactions.

When my face is flushed with blood, it becomes red and obscene.

It betrays at the same time, through morbid reflexes, a bloody erection and a demanding thirst for indecency and criminal debauchery.

For that reason I am not afraid to affirm that my face is a scandal and that my passions are expressed only by the JESUVE.

The terrestrial globe is covered with volcanoes, which serve as its anus.

Although this globe eats nothing, it often violently ejects the contents of its entrails.

Those contents shoot out with a racket and fall back, streaming down the sides of the Jesuve, spreading death and terror everywhere.

In fact, the erotic movements of the ground are not fertile like those of the water, but they are far more rapid.

The earth sometimes jerks off in a frenzy, and everything collapses on its surface.

The Jesuve is thus the image of an erotic movement that burglarizes the ideas contained in the mind, giving them the force a scandalous eruption.

This eruptive force accumulates in those who are necessarily situated below.

Communist workers appear to the bourgeois to be as ugly and dirty as hairy sexual organs, or lower parts; sooner or later there will be a scandalous eruption in the course of which the asexual noble heads of the bourgeois will be chopped off.

Disasters, revolutions, and volcanoes do not make love with the stars.

The erotic revolutionary and volcanic deflagrations antagonize the heavens.

As in the case of violent love, they take place beyond the constraints of fecundity.

In opposition to celestial fertility there are terrestrial disasters, the image of terrestrial love without condition, erection without escape and without rule, scandal, and terror.

Love then screams in my own throat; I am the Jesuve, the filthy parody of the torrid and blinding sun.

I want to have my throat slashed while violating the girl to whom I will have been able to say: you are the night.

The Sun exclusively loves the Night and directs its luminous violence, its ignoble shaft, toward the earth, but finds itself incapable of reaching the gaze or the night, even though the nocturnal terrestrial expanses head continuously toward the indecency of the solar ray.

The solar annulus is the intact anus of her body at eighteen years to which nothing sufficiently blinding can be compared except the sun, even though the anus is night.


