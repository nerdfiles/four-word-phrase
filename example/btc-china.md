China is reaching consensus on blockchain technology

Demo day at International Blockchain Week in Shanghai was kicked off by IBM and Hyperledger Project.

Consensus seems to come naturally to a lot of Chinese peopleReuters

Consensus would seem to come naturally to the Chinese, a civilisation which has somehow retained its unity against all the odds for thousands of years. As International Blockchain Week in Shanghai backed by the famous Wanxiang Blockchain Labs got underway, expressions like "cross-industry consensus" were being repeatedly used as developers showed how shared ledgers could be given the Chinese mega-platform treatment.

To start the Demo Day, IBM gave an introduction and demonstration of its latest trade finance and IoT industrial blockchain use cases. Talking up its proliferation with the Hyperledger Project, John Wolpert, IBM's blockchain offering director, said: "Don't think of IBM as technology arms dealer, think of it as a viral networking company."

He added that Hyperledger is the fastest growing open source project ever; from 17 to 80-plus companies in past year. It's not about one platform over another of course, he said – a vaguely familiar party line.

Trade finance is undoubtedly one of the busiest spaces in blockchain. IBM demoed its internal finance blockchain initiative, aka shadowchain, as well as trade logistics, car leasing and aircraft maintenance. The trade finance blockchain showed a container consignment of flowers was being moved around. One of the key points being that participants can only see data relevant to them. One would imagine the R3 trade finance and banking implementation does something similar.

IBM senior research staff member Sheng Huang who showed the app, said that to issue letters of credit and bills needs full party consensus, but who does this and when it's done, is privy only to certain people on the chain. "All the events, such as the transport company loading flowers to the container port and ship are all recorded on the blockchain. After arriving at the next port, the customs declaration is checked and that's another event – but again, customs can only see the relevant information."

Similarly, a car leasing use case tracked everything from manufacture of the vehicle to transfer of ownership and leasing through to finally being scrapped. "It reduces fraud risk and lots of intermediary costs," said research scientist Chang Chen.

The aircraft industry has actually been using fault tolerance algorithms for decades so it's interesting to see blockchain use cases there. Research scientist Bo Liang Chen explained that data about the number of soft and hard landings, G-force during take-offs and such like, incorporating IoT at the airport, could all be processed. This could then be used to trigger a smart contract that maintenance was needed.

"There's quite a lot of data points, which also includes weather conditions for instance. The blockchain tracks the life cycle of the aircraft's tyres. The smart contract will say these need checked and the plane will not be allowed to take off until that has been done."

The standard of firms and startups presenting was extremely high. A major announcement was the first public outing of Cosmos, the "internet of blockchains" released by Tendermint. Tendermint, which is used by Eris Industries, is a free and open source consensus middleware, related to the classic Practical Byzantine Fault Tolerance (PBFT) algorithm, released in 1999.

Presenting, Jae Kwon, co-founder of Tendermint, said Cosmos is a public network of blockchains, which he called "zones" because the can have their own style of governance, their own economic models and tokens. "They are like economic zones," he said.

As blockchains plug into a Cosmos hub, it can send coins from one to another, while upgrades can be done in a nice way without the need to hard fork.

"We are most excited about distributed exchange functionality. There are a lot of decentralised exchange ideas out there, but none like this this. It can create order books and set limits – and it's fast too," said Kwon.

"We see it as a world wide web for tokens and blockchains, a solution to lots of longstanding problems in the blockchain world." He added that seven companies have approached him regarding Cosmos during the last three days at DevCon2.

BubiChain, an interesting Chinese blockchain, began by saying there is a centralisation problem with all the data that is currently held by favourite apps like Alibaba and WeChat.

Presenting, Jack Yang said: "What we have is data monopolies; what we want is democratisation of data."

Yang went on to say this personal data can be handled on Bubi's BaaS platform instead, where the nodes that would take on that job are "partners, agencies, institutions and users".

Unfortunately those manning the Bubi stand could not elaborate any further on exactly who would store data and how – but it seemed to be worth asking, because Bubi currently has close to two million users on its blockchain. The platform handles 250,000 transactions per week, a figure which has risen exponentially in the past several months; in other words, a Chinese mega-platform blockchain.

Also presenting was Singapore's Digix Global, the gold tokenising system using IPFS and Ethereum. The group is one of the most serious fintech blockchain players around, with heavyweight partners in the bullion and precious metal vaulting ecosystem.

Another buzzing app is Decentralised Capital (DC), which creates DC tokens to use on Ethereum that are tied to fiat currency, to mitigate crypto volatility. Alex Wearn said: "Blockchains face an adoption problem, and that adoption problem is about stability." He also hinted that the app might be used by the likes of Banco Santander, which made a big announcement about connecting ether with bank accounts earlier this week.

Also interesting was Shanghai's own Vechain, which is using blockchain and hardware in the form of small chipsets, to tackle the problem of counterfeiting luxury goods – a black market in China that's probably larger than the GDPs of some countries.

While Bestowit founder Mary Davies recounted how she came to see blockchain as a use case for succession planning and the management of wills after her mother died suddenly. "Everything changes when someone dies," she said.

Despite being traumatised by the death, Davies found that all her mother assets were frozen until the will could be produced. "I found myself looking for this paper document and asking why am I doing this in the 21st century. Death is taboo. But there are lots of blockchain applications and the will is one of them."
