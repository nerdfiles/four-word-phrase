Every mental health problem has a biological dimension. How could it not? All our experience and behaviour, normal or abnormal, is founded on our neurobiology.

Researchers have taken great strides towards understanding these foundations and the public has taken note. Increasingly, we explain our problems as products of heredity, brain disease and chemical imbalance, rather than life experiences, adversities and ways of thinking.

Regrettably, these scientific advances have a dark side. As a recent review shows, people who hold biogenetic (biological and genetic) explanations of mental health disorders tend to have some negative perceptions of those who experience them. They view these people as relatively dangerous, unpredictable and unlikely to recover, and seek greater distance from them.

The consequences of these perceptions extend beyond stigma; they also have troubling implications for treatment.

The “therapeutic alliance” between clinician and client is a key ingredient in successful treatment, responsible for better clinical outcomes and lower rates of dropout. Biogenetic explanations held by clinicians can impair the therapeutic relationship and those held by clients can impede their recovery.

Studies by Matt Lebowitz and Woo-kyoung Ahn at Yale University, for example, found that biogenetic explanations reduce empathy among clinicians.

Clinicians read descriptions of people suffering from mental health conditions, whose problems were given biogenetic or psycho-social (psychological and social) explanations. An anxious client’s troubles, for instance, were attributed either to neurochemical imbalances and genes, or to bullying and parental neglect. The clinicians consistently reported feeling less empathy for clients whose problems were ascribed to biogenetic causes.

Empathy was lower among clinicians who had undergone medical training.

In Lebowitz’s later work, clients judged clinicians who espoused biogenetic views of mental health disorders as less warm than those who espoused psycho-social views. If clinicians feel less empathy for clients, and clients see them as lacking warmth, the therapeutic relationship has two strikes against it.

Like any other members of the public, clients vary in the extent to which they believe that genes, chemical imbalances and brain abnormalities cause their problems. These explanations influence how they approach treatment.

People who believe their problems have biogenetic causes tend to opt for biomedical treatments. Although psychological treatments are effective alternatives for most mental health disorders, and often have fewer side effects, people who believe their troubles are rooted in their biology tend to disfavour these treatments.

One study, by the University of Wollongong’s Brett Deacon and colleagues, gave a bogus cheek swab test to a sample of depressed people. Half were told that the test revealed a serotonin deficiency and the other half that their serotonin levels were normal. People who believed they had a chemical imbalance preferred medication over psychotherapy (talk-based therapy).

This phenomenon is playing out on a societal scale. In recent decades, lay people’s explanations of mental health problems have become increasingly biogenetic. This is contributing to steep increases in the use of psychiatric medication and declining rates of psychotherapy.

Clients who attribute their mental health problems to biology tend to believe that they have limited control over their problems. They are also relatively pessimistic about their prognosis. In the cheek swab study, depressed people who were told they had a chemical imbalance were less confident they could regulate their negative moods and less optimistic about recovery.

These twin perceptions can hamper successful treatment. Believing one has limited control over one’s difficulties may weaken active engagement in overcoming them. Similarly, pessimism undermines the positive expectations that promote successful treatment and underlie the powerful placebo effect.

Biogenetic explanations have some worrying implications for treatment. So what can be done about this medicalisation of mental illness?

We cannot ignore the scientific evidence that neurobiological and genetic factors contribute to mental health problems. To do so would be to throw the psychiatric baby out with its bathwater. But we need to be mindful that biogenetic explanations have a dark side when it comes to treatment, as they do for public stigma.

The challenge for clinicians and clients alike is to overcome the tendency to view suffering individuals as broken mechanisms. Mental health problems have a biological component, but that fact does not dissolve the person’s subjective experience and individuality.
