What America needs right now is a good villain. Not a mustache twirling comic book adaptation, but a real life bad guy. We are currently so divided as a country that the only way to bring us back together again is to find a person that deserves our collectively hatred and contempt.  And man, we might have discovered one in Martin Shkreli.

Martin Shkreli, the pharmaceutical company CEO who became infamous for increasing the price of an AIDS drug by more than 5,000 percent has now resorted to trolling much more likable people on Twitter. His next target, beloved actor

Chris Evans.

Shkreli went after Chris Evans when the actor publicly backed comedian

Patton Oswalt in his Twitter feud with Shkreli.

@pattonoswalt owning @MartinShkreli is my favorite thing on Twitter right now.

— Chris Evans (@ChrisEvans) September 7, 2016
Shkreli didn't take to kindly to Chris being awesome, because villains hate it when you insult them. So, Shkreli responded to Chris Evans in multiple, very douche-y tweets.

Comedian gets a pass because wife died but you, @ChrisEvans, I'll fucking destroy. Pretty boy, shit-for-brains. Think your thoughts matter?

— Martin Shkreli (@MartinShkreli) September 7, 2016
Comedian gets a pass because wife died but you, @ChrisEvans, I'll fucking destroy. Pretty boy, shit-for-brains. Think your thoughts matter?

— Martin Shkreli (@MartinShkreli) September 7, 2016
Everything you need to know about Shkreli can be summed up in those two tweets. All the credit in the world should go to Chris Evans for not responding.

If you want to know more about Martin Shkreli, (and why wouldn't you?) >Vanity Fair did an interesting piece on him. During the interview, Shrekil was asked how he feels about the public's outraged after he gauged the price of Daraprim, the AIDS medication. 

“The attempt to public shame is interesting,” Shkreli told Vanity Fair. “Because everything we've done is legal.” In fact, Shrekil says, he wishes he had raised the price higher. “My investors expect me to maximize profits.”

Of course, what Shkreli is failing to see is hiking the price of a drug that could save millions of lives may not be illegal, but it is sure as heck unethical. 

Even Donald Trump doesn't seem to like Shkreli. The Republican candidate said Shkreli “looks like a spoiled brat.” And you know you're a terrible person when everyone can agree with what Donald Trump has to say about you.
