# Manual Steps

1. Replace '.comby' with '.com' (re-run ##Automated Steps 1) (71 instances in first list)
2. Skeptical of .hk (Hong Kong) domains
3. Remove e-mails with multiple dots (https://serverfault.com/questions/395766/are-two-periods-allowed-in-the-local-part-of-an-email-address)  
   * '...'
   * '....'
   * '.....'
   * And more than 4 consecutive dots.
4. Remove all '.comdelete' and '.comremove'
5. Remove all names with 'delete' or 'remove'
6. Keep subdomain e-mails with 'www'
7. Replace '..com' with '.com'
8. Replace 'com..thanks' with '.com'
9. Replace '..' with '.'
10. Remove all e-mails with only periods in handle
11. Remove all 'asdf' emails
12. Remove xxxxxx@xxxxxxxx.com, and the like
13. Replace '.com.{video_format}' with '.com'
14. Replace '.com(arbitrary_word)' with '.com'
15. (email)(email) changed to (email)(newline)(email)
16. (email)(domain) changed to (email)
17. Remove /yahoo/ @ email address
18. Remove all "aol.com@..."
19. Remove all "gmail.com@..."
20. Remove all "hotmail.com@..."
21. Remove all "yandex.com@..."
22. Remove all "ymail..."

## Automated Steps

1. Use dictionary generator script to de-dupe e-mails matching the pattern:  
   (email)(hard-return or new-line)
2. Use white list domains to create subset (will weed out things like 'asd.net' and 'xxx@ccc.com', etc. but this will be a completely separate list so that you can choose)
