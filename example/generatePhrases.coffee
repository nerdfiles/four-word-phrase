"""
  @fileOverview
  Generate pseudo-random word sequences using Moby Dick as the source for the
  dictionary.
"""

async       = require 'async'
fs          = require 'fs'
path        = require 'path'
fwp         = require '..'
CryptoJS    = require 'crypto-js'
pry         = require 'pryjs'


# @jsdoc
# @description
generateKey = (p, args={}) ->

  ratio = if args and args.ratio then args.ratio else (128 / 8)
  keySize = if args and args.keySize then args.keySize else (512 / 32)

  salt = CryptoJS.lib.WordArray.random(ratio)

  config = {
    keySize: keySize,
    iterations: if args and args.iterations then args.iterations else 1000
  }

  CryptoJS.PBKDF2(p, salt, config)


__interface = (new fwp.util.Interface).phrases()

filename = __interface.filename
seed = __interface.seed

generatedKey = generateKey('VsfCKKSkwrsLjOLbQHcWSBeQeok=', null)

eval(pry.it)
generator = new fwp.generator.MemoryGenerator {
  baseSeed: if seed then seed else generatedKey
}

filePath = path.join __dirname, filename
readStream = fs.createReadStream filePath

importer = new fwp.importer.ReadStreamImporter generator

importer.import {
  readStream: readStream
}, (error) ->
  if (error)
    return console.error error

  phraseLength = 12

  async.times 10, (index, asyncCallback) ->
    generator.nextPhrase(phraseLength, asyncCallback)
  , (error, phrases) ->
    if (error)
      return console.error(error)
    #console.log phrases
    phrases.forEach((phrase, index) ->
      console.log('Phrase ' + (index + 1) + ': ' + phrase.join(' '))
    )
