Albert Einstein

( 1879 - 1955 )

A Selection of Quotes

<<<<<<<< CONTENTS >>>>>>>>

The teaching of Relativity

The formulation of Relativity - and the child ...

On the Moon being there without Looking at It ...

Intelligence and the ultimate & fundamental ends

The goals of science & religion, and our understanding of life

The mystic emotion, knowledge, and religious sentiment

The cosmic religious experience

The Temple of Science, and the Scientific Assembly

Human beings and their circle of compassion

The student, success, service, and the task of educators

Imagination and Knowledge

The Tree of Life

Two Goals of Freedom

Spiritual Development of Individuals

Morals and Emotions

The Difficulty of the Sages

Belief and Knowledge

The Nature of Authority and Fundamental Ends

The Christian Religion and the Human Goal

The Definition of Science

Predicting the Weather ...

Living Things

The first step in the setting of a 'real external world'

Experience: Personal and Cosmic ...

A collection of "One Liners" from various UseNet posts ...


----------------------------------------------------------------------------

[Image] The teaching of Relativity

   * "Relativity teaches us the connection between the different descriptions of one and the same reality".

----------------------------------------------------------------------------

[Image] The formulation of Relativity

   * "I sometimes ask myself how it came about that I was the one to develop the theory of relativity.
   * The reason, I think, is that a normal adult never stops to think about problems of space and time.
   * These are things which he has thought about as a child.
   * But my intellectual development was retarded,as a result of which I began to wonder about space and time only when I had already grown up".

----------------------------------------------------------------------------

[Image] On the Moon being there without Looking at It ...

   * "I think that a particle must have a separate reality independent of the measurements.
   * That is an electron has spin, location and so forth even when it is not being measured.
   * I like to think that the moon is there even if I am not looking at it.".

----------------------------------------------------------------------------
[Image]Intelligence and the ultimate & fundamental ends

   * "Intelligence makes clear to us the interrelationship of means and ends.
   * But mere thinking cannot give us a sense of the ultimate and fundamental ends.
   * To make clear these fundamental ends and valuations and to set them fast in the emotional life of the individual, seems to me precisely the most important function which religion has to form in the social life of man".

----------------------------------------------------------------------------

[Image] The goals of science & religion, and our understanding of life

   * "The more a man is imbued with the ordered regularity of all events the firmer becomes his conviction that there is no room left by the side of this ordered regularity for causes of a different nature.
   * For him neither the rule of human nor the rule of divine will exist as an independent cause of natural events.
   * To be sure, the doctrine of a personal God interfering with the natural events could never be refuted, in the real sense, by science, for this doctrine can always take refuge in those domains in which scientific knowledge has not yet been able to set foot.
   * But I am persuaded that such behaviour on the part of the representatives of religion would not only be unworthy but also fatal.
   * For a doctrine which is able to maintain itself not in clear light but only in the dark, will of necessity lose its effect on mankind, with incalculable harm to human progress ....
   * If it is one of the goals of religions to liberate maknind as far as possible from the bondage of egocentric cravings, desires, and fears, scientific reasoning can aid religion in another sense.
   * Although it is true that it is the goal of science to discover (the) rules which permit the association and foretelling of facts, this is not its only aim.
   * It also seeks to reduce the connections discovered to the smallest possible number of mutually independent conceptual elements.
   * It is in this striving after the rational unification of the manifold that it encounters its greatest successes, even though it is precisely this attempt which causes it to run the greatest risk of falling a prey to illusion.
   * But whoever has undergone the intense experience of successful advances made in this domain, is moved by the profound reverence for the rationality made manifest in existence.
   * By way of the understanding he achieves a far reaching emancipation from the shackles of personal hopes and desires, and thereby attains that humble attitude of mind toward the grandeur of reason, incarnate in existence, and which, in its profoundest depths, is inaccessible to man.
   * This attitude, however, appears to me to be religious in the highest sense of the word.
   * And so it seems to me that science not only purifies the religious imulse of the dross of its anthropomorphism but also contibutes to a religious spiritualisation of our understanding of life".

----------------------------------------------------------------------------

[Image] The mystic emotion, knowledge, and religious sentiment

   * "The finest emotion of which we are capable is the mystic emotion.
   * Herein lies the germ of all art and all true science.
   * Anyone to whom this feeling is alien, who is no longer capable of wonderment and lives in a state of fear is a dead man.
   * To know that what is impenatrable for us really exists and manifests itself as the highest wisdom and the most radiant beauty, whose gross forms alone are intelligible to our poor faculties - this knowledge, this feeling ... that is the core of the true religious sentiment.
   * In this sense, and in this sense alone, I rank myself amoung profoundly religious men."

----------------------------------------------------------------------------
[Image]The Temple of Science, and the Scientific Assembly ...

   * "In the temple of science are many mansions, and various indeed are they that dwell therein and the motives that have led them thither.
   * Many take to science out of a joyful sense of superior intellectual power; science is their own special sport to which they look for vivid experience and the satisfaction of ambition;
   * many others are to be found in the temple who have offered the products of their brains on this altar for purely utilitarian purposes.
   * Were an angel of the Lord to come and drive all the people belonging to these two categories out of the temple, the assemblage would be seriously depleted, but there would still be some men, of both present and past times, left inside"

----------------------------------------------------------------------------
[Image] The cosmic religious experience

   * "The cosmic religious experience is the strongest and noblest driving force behind scientific research.
   * No one who does not appreciate the terrific exertions and above all, the devotion without which pioneer creations in scientific thought cannot come into being, can judge the strength of the feeling out of which alone such work, turned away as it is from immediate practical life, can grow.
   * What a deep faith in the rationality of the world and its structure and what a longing to understand even the smallest glimpses of the reason revealed in the world there must have been in Kepler and Newton ..."

----------------------------------------------------------------------------
[Image] Human beings and their circle of compassion

   * "A human being is part of the whole called by us universe , a part limited in time and space.
   * We experience ourselves, our thoughts and feelings as something separate from the rest.
   * A kind of optical delusion of consciousness.
   * This delusion is a kind of prison for us, restricting us to our personal desires and to affection for a few persons nearest to us.
   * Our task must be to free ourselves from the prison by widening our circle of compassion to embrace all living creatures and the whole of nature in its beauty...
   * We shall require a substantially new manner of thinking if mankind is to survive."

----------------------------------------------------------------------------

[Image] The student, success, service, and the task of educators

   * "One should guard against inculcating a young man {or woman} with the idea that success is the aim of life, for a successful man normally receives from his peers an incomparibly greater portion than than the services he has been able to render them deserve.
   * The value of a man resides in what he gives and not in what he is capable of receiving.
   * The most important motive for study at school, at the university, and in life is the pleasure of working and thereby obtaining results which will serve the community.
   * The most important task for our educators is to awaken and encourage these psychological forces in a young man {or woman}. Such a basis alone can lead to the joy of possessing one of the most precious assets in the world - knowledge or artistic skill."

----------------------------------------------------------------------------

[Image] Imagination and Knowledge ...

   * "I am enough of an artist to draw freely upon my imagination.
   * Imagination is more important than knowledge. Knowledge is limited.
     Imagination encircles the world."

----------------------------------------------------------------------------

[Image] The Tree of Life ...

   * "All religions, arts and sciences are branches of the same tree.
   * All these aspirations are direected toward ennobling man's life, lifting it from the sphere of mere physical existence and leading the individual towards freedom."

----------------------------------------------------------------------------

[Image] Two Goals of Freedom ....

   * "(1) Those instrumental goods which should serve to maintain the life and health of all human beings should be produced by the least possible labour of all.
   * (2) The satisfaction of physical needs is indeed the indespensible precondition of a satisfactory existence, but in itself is not enough.
   * In order to be content men must also have the possibility of developing their intellectual and artistic powers to whatever extent accord with their personal characteristics and abilities."

----------------------------------------------------------------------------

[Image] Spiritual Development of Individuals ....

   * "If the possibility of the spiritual development of all individuals is to be secured, a second kind of outward freedom is necessary.
   * The development of science and of the creative activities of the spirit in general requires still another kind of freedom, which may be characterised as inward freedom.
   * It is this freedom of the spirit which consists in the interdependence of thought from the restrictions of authoritarian and social prejudices as well as from unphilosophical routinizing and habit in general.
   * This inward freedom is an infrequent gift of nature and a worthy object for the individual."

----------------------------------------------------------------------------

[Image] Morals and Emotions ....

   * "We all know, from what we experience with and within ourselves, that our conscious acts spring from our desires and our fears.
   * Intuition tells us that that is true also of our fellows and of the higher animals.
   * We all try to escape pain and death, while we seek what is pleasant.
   * We are all ruled in what we do by impulses; and these impulses are so organised that our actions in general serve for our self preservation and that of the race.
   * Hunger, love, pain, fear are some of those inner forces which rule the individual's instinct for self preservation.
   * At the same time, as social beings, we are moved in the relations with our fellow beings by such feelings as sympathy, pride, hate, need for power, pity, and so on.
   * All these primary impulses, not easily described in words, are the springs of man's actions.
   * All such action would cease if those powerful elemental forces were to cease stirring within us.
   * Though our conduct seems so very different from that of the higher animals, the primary instincts are much aloke in them and in us.
   * The most evident difference springs from the important part which is played in man by a relatively strong power of imagination and by the capacity to think, aided as it is by language and other symbolical devices.
   * Thought is the organising factor in man, intersected between the causal primary instincts and the resulting actions.
   * In that way imagination and intelligence enter into our existence in the part of servants of the primary instincts.
   * But their intervention makes our acts to serve ever less merely the immediate claims of our instincts."

----------------------------------------------------------------------------

[Image] The Difficulty of the Sages ...

   * "The real difficulty, the difficulty which has baffled the sages of all times, is rather this: how can we make our teaching so potent in the motional life of man, that its influence should withstand the pressure of the elemental psychic forces in the individual?"

----------------------------------------------------------------------------

[Image] Belief and Knowledge (Newton - 1900's) ...

   * " During the last century, and part of the one before, it was widely held that there was an unreconcilable conflict between knowledge and belief.
   * The opinion prevailed amoung advanced minds that it was time that belief should be replaced increasingly by knowledge; belief that did not itself rest on knowledge was superstition, and as such had to be opposed.
   * According to this conception, the sole function of education was to open the way to thinking and knowing, and the school, as the outstanding organ for the people's education, must serve that end exclusively."

----------------------------------------------------------------------------

[Image] The Nature and Authority of Fundamental Ends ...

   * "Knowledge of what is does not open the door directly to what should be.
   * If one asks the whence derives the authority of fundamental ends, since they cannot be stated and justifed merely by reason, one can only answer: they exist in a healthy society as powerful traditions, which act upon the conduct and aspirations and judgements of the individuals; they are there, that is, as something living, without its being necessary to find justification for their existence.
   * They come into being not through demonstration but through revelation, through the medium of powerful personalities.
   * One must not attempt to justify them, but rather to sense their nature simply and clearly."

----------------------------------------------------------------------------

[Image] The Christian Religion and the Human Goal ...

   * "The highest principles for our aspirations and judgements are given to us in the Jewish-Christian religious tradition.
   * It is a very high goal which, with our weak powers, we can reach only very inadequately, but which gives a sure foundation to our aspirations and valuations.
   * If one were to take that goal out of out of its religious form and look merely at its purely human side, one might state it perhaps thus: free and responsible development of the individual, so that he may place his powers freely and gladly in the service of all mankind.
   * ... it is only to the individual that a soul is given.
   * And the high destiny of the individual is to serve rather than to rule, or to impose himself in any otherway."

----------------------------------------------------------------------------

[Image] Definition of Science ...

   * "Science is the century-old endeavour to bring together by means of systematic thought the perceptible phenomena of this world into as thorough-going an association as possible.
   * To put it boldly, it is the attempt at a posterior reconstruction of existence by the process of conceptualisation.
   * Science can only ascertain what is, but not what should be, and outside of its domain value judgements of all kinds remain necessary."

----------------------------------------------------------------------------

[Image] Weather Prediction ...

   * "When the number of factors coming into play in a phenomenological complex is too large scientific method in most cases fails.
   * One need only think of the weather, in which case the prediction even for a few days ahead is impossible.
   * Neverthess, noone doubts that we are confronted with a causal connection whose causal components are in the main known to us.
   * Occurrences in this domain are beyond the reach of exact perdiction because of the variety of factors in operation, not because of any lack of order in nature."

----------------------------------------------------------------------------

[Image] Living Things ...

   * "We have penetrated far less deeply into the regularities obtaining within the realm of living things, but deeply enough nevertheless to sense at least the rule of fixed necessity ..... what is still lacking here is a grasp of the connections of profound generality, but not a knowledge of order itself.

----------------------------------------------------------------------------

[Image] The first step in the setting of a 'real external world' ...

   * "I believe that the first step in the setting of a 'real external world' is the formation of the concept of bodily objects and of bodily objects of various kinds.
   * Out of the multitude of our sense experiences we take, mentally and arbitrarily, certain repeatedly occurring complexes of sense impression (partly in conjunction with sense impressions which areinterpreted as signs for sense experiences of others), and we attribute to them a meaning the meaning of the bodily object.
   * Considered logically this concept is not identical with the totality of sense impressions referred to; but it is an arbitrary creation of the human (or animal) mind.
   * On the other hand, the concept owes its meaning and its justification exclusively to the totality of the sense impressions which we associate with it."

----------------------------------------------------------------------------

[Image] Experience: Personal and Cosmic ...

   * Man tries to make for himself in the fashion that suits him best a simplified and intelligible picture of the world; he then tries to some extent to substitute this cosmos of his for the world of experience, and thus to overcome it.
   * This is what the painter, the poet, the speculative philosopher, and the natural scientists do, each in his own fashion.
   * Each makes this cosmos and its construction the pivot of his emotional life, in order to find in this way peace and security which he can not find in the narrow whirlpool of personal experience.

----------------------------------------------------------------------------
From a UseNetPost by Colin_Douthwaite@equinox.gen.nz (Colin Douthwaite), 27
Mar 1995 05:06:19 GMT

[Image] A collection of One Liners ....

   * "Great spirits have always encountered violent opposition from mediocre minds"

   * "I do not know with what weapons World War 3 will be fought, but World War 4 will be fought with sticks and stones."

   * "Science without religion is lame, religion without science is blind."

   * "God does not play dice with the universe."

   * "Common sense is the collection of prejudices acquired by age 18."

   * "Nothing will benefit human health and increase the chances for survival of life on Earth
     as much as the evolution to a vegetarian diet"

   * "Only two things are infinite, the universe and human stupidity, and I'm not sure about the former."

   * "Problems cannot be solved at the same level of awareness that created them."

   * "Few are those who see with their own eyes and feel with their own hearts."

   * "Peace cannot be achieved through violence, it can only be attained through understanding."

   * "When the solution is simple, God is answering."

   * "Where the world ceases to be the scene of our personal hopes and wishes, where we face it as free beings admiring, asking and observing, there we enter the realm of Art and Science"

   * "Watch the stars, and from them learn. To the Master's honor all must turn, each in its track, without a sound, forever tracing Newton's ground."

   * "Things should be made as simple as possible, but not any simpler."

   * "Put your hand on a hot stove for a minute, and it seems like an hour. Sit with a pretty girl for an hour, and it seems like a minute. THAT's relativity."

   * "Gravitation can not be held resposible for people falling in love"

----------------------------------------------------------------------------

