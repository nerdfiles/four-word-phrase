TED CRUZ
Barack Obama issued a decree "demanding that every public school now allow grown men and boys into the little girls’ bathroom."  

Clinton and Trump don't see eye-to-eye
Says both Donald Trump and Hillary Clinton say Planned Parenthood is "terrific and that it should keep taxpayer funding." 

Distorting Trump's bathroom views
Says Donald Trump thinks "a grown man pretending to be a woman (should) be allowed to use the women's restroom."

Senator's claim about the polls is still 
"The polling shows over and over again that, unlike Donald Trump, that with me as the nominee, we beat Hillary Clinton."

Short by several polls
"Poll after poll after poll shows me beating Hillary."

Time to ditch this talking point
"The EPA has tried to define a puddle or a drainage ditch on your farm to be navigable waters and thus subject to massive environmental regulations."  

Trend long predates Obama
Says President Barack Obama has "been presiding over our jobs going overseas for seven years."

Trump had Cruz's back -- then
Says Donald Trump and Marco Rubio opposed Cruz’s "efforts to defund Planned Parenthood."

Of late? The 'worst'
Donald Trump has "described Hillary Clinton as one of the best secretaries of state in history."

Can't account for drop of 'hundreds of millions'
"Spending on welfare, on prisons, and education, all of those have dropped by hundreds of millions of dollars" because of Arizona’s immigration laws that drove out undocumented immigrants.

Trump says he was kidding
Says "the one person (Donald Trump) has suggested that would make a good justice is his sister, who is a court of appeals judge appointed by Bill Clinton. She is a hardcore pro-abortion liberal judge."

"During eight years under Ronald Reagan, African-American median income rose by about $5,000."

Cruz has had some tough talk
"I have not insulted Donald personally."

Sequestration a pox on both parties
"Barack Obama, right now, No. 1, over seven years has dramatically degraded our military."  

You've disappointed your constitutional law professor, Ted
"The Democrats in the Senate last year introduced a constitutional amendment to repeal the free speech protections of the First Amendment."

Not quite what he said
"The head of the FBI has told Congress they cannot vet those (Syrian) refugees."

Statement cherry-picks, twists an academic study
"Here’s the simple and undeniable fact: The overwhelming majority of violent criminals are Democrats."

Parties may be larger than they appear
The "Democratic Party is getting smaller and smaller and smaller."

After a historically long delay
"Republican leadership took the lead confirming Loretta Lynch as attorney general."

Includes some serious logical flaws
