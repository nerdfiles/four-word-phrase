###
@fileOverview ./example/lang.coffee
@example
$ sudo t update "$(node lang.js filename phraseNo)"
@usage
    -----------------
    -----------------
    ------x----------
    --x--------------
    -----------------
    ----x------------
    -x---------------
    ---------x-------
###

fs = require('fs')
natural = require('natural')
tokenizer = new natural.WordTokenizer()
_ = require('lodash')
NGrams = natural.NGrams
pry = require('pryjs')

argies = []

process.argv.forEach((val, index, array) ->
  if index > 1
    argies.push val
)

fs.readFile(argies[0], 'utf8', (error, data) ->

  _data = data.split('\n')
  spans = []

  for s in [0.._data.length] by 1
    if _data[s]
      spans.push _data[s].replace(/Phrase \d\d?\:\s/g, '')

  tokens = tokenizer.tokenize(spans.join(' '))
  trigrams = NGrams.ngrams(tokens, 3)
  phrases = []

  for p in [0..trigrams.length] by 1
    if trigrams[p]
      phrases.push trigrams[p].join ' '

  polished_words = _.union(_.join(phrases, ' ').split(' '))
  p = polished_words.join(' ')
  uniques = []
  counter = 0

  for index, character of p
    z = p.slice(counter, counter + 140)
    if z.length
      uniques.push z
    counter += 140

  console.log(uniques)

)


