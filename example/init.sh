function fwp() {
  node ./generatePhrases.js ./discourse-on-method.txt > ./discourse-on-method.fwp.txt
}

function trigram()  {
  node ./lang.js ./discourse-on-method.fwp.txt > ./discourse-on-method.tri.txt
}

function proofbot() {
  node ~/Projects/proofbot/src/index.js $1 ./discourse-on-method.tri.txt
}
