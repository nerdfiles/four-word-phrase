# Improve the Affordable Care Act, Don’t Repeal It

Republicans are poised to use their control of the presidency and both
houses of Congress to repeal major parts of the Affordable Care Act. But
a careful review reveals that significant parts of the ACA are actually
working. Specifically, it has allowed for significant expansion of
insurance coverage and it is accelerating the move from a financing
system that pays for the volume of services provided to one that rewards
care providers for delivering higher-quality care. For these reasons,
the Republicans should try to improve the ACA, not largely abandon it.

True health care reform will happen when care is redesigned around the
patient, not the doctor or hospital; when the financial incentives
reward better health outcomes rather than hospital beds filled; and when
the consumer has access to information to make good choices.

#### Insight Center

During the debate in 2009, before the [Affordable Care
Act](https://www.hhs.gov/healthcare/about-the-law/read-the-law/) (aka
“Obamacare”) was passed, I
[argued](http://www.healthcarefinancenews.com/news/healthcare-debate-fatally-flawed)
that health care would continue to be unaffordable unless America
addressed the root cause of its high costs. With the huge increases in
premiums this year on the insurance exchanges, we can safely say the
root cause has not been addressed. Most of the cost of health care is in
the delivery of that care.

The Institute of Medicine estimates that 30%–40% of that care is waste.
In other words, 30%–40% of the activities of care delivery are
essentially unnecessary to the health outcomes of patients. [These
wastes](http://journals.lww.com/journalpatientsafety/fulltext/2013/09000/a_new,_evidence_based_%20%20%20estimate_of_patient_harms.2.aspx)
include unnecessary procedures, waiting for tests and appointments,
duplicated services, and medical errors. There are parts of the
Affordable Care Act that were specifically designed to attack waste.
Some of those activities are just now coming to fruition and should be
retained.

The following is a rundown of what I believe should be kept, what should
be discarded, and why.

### **What’s Working**

**Payment system changes.** Most everyone in the industry believes the
present fee-for-service system rewards the wrong behavior. It is a
system that focuses on treating sickness and does not reward providers
for keeping people healthy and out of the hospital. Hospitalization
drives 80% of the overall cost of care. Reducing it can lead to large
overall reductions in cost for caring for populations

The ACA legislation addressed this by establishing different financing
models. These new mechanisms have been aimed at paying for value, not
volume of services. Administered by the recently created Center for
Medicare and Medicaid Innovation (CMMI), the models have created rewards
for care-delivery organizations that deliver better health outcomes for
populations of patients.

CMMI has introduced many payment model changes, including [accountable
care
organizations](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/ACO/index.html?redirect)
(ACOs), [medical homes](https://www.pcpcc.org/about/medical-home),
[bundled payments](/2016/07/how-to-pay-for-health-care), the
[Comprehensive Primary Care
Initiative](https://innovation.cms.gov/initiatives/comprehensive-primary-care-initiative/),
and the [Comprehensive End Stage Renal Disease
Initiative](https://innovation.cms.gov/initiatives/comprehensive-esrd-care/).
Other payment-model changes are on the way.

ACOs have grown rapidly, with over 800 now registered with CMMI. They
are designed to bring hospitals and physicians together in a structure
that delivers more-coordinated, less-wasteful care. The program is early
in its life cycle, but the results are showing improved quality with a
small but meaningful reduction in the overall cost of care. They are
extremely important because providers are paid to avoid unnecessary
care, reduce errors, and keep Medicare beneficiaries out of the hospital
— ACOs share in savings created over a pre-established target.

CMMI is now introducing full-risk-sharing models with providers in the
[Next Generation ACO
Model](https://innovation.cms.gov/initiatives/Next-Generation-ACO-Model/).
This can include a per member, per month payment for large populations
of Medicare beneficiaries. Allowing providers to have the payment up
front will remove the fee-for-service world completely and unleash the
creativity of provider systems to design radically new care models that
are patient centered.

If CMMI is dismantled, as some have recently suggested, it’s imperative
this pay-for-value work continues. The bipartisan Medicare Access and
CHIP Reauthorization Act of 2015 (MACRA) legislation is a good example
of how both the Republican and Democratic parties can work together in
improving the care delivery system. MACRA set up a [Physician-Focused
Payment Model Technical Advisory
Committee](https://aspe.hhs.gov/ptac-physician-focused-payment-model-technical-advisory-committee).
Perhaps this committee can play an important role in threading the
needle to show how both parties can work together to make the incentives
work.

**Public reporting of quality performance data.** This is a powerful way
to improve care. As a direct result of the ACA provision to publicly
report hospital performance, the Centers for Medicare and Medicaid
Services (CMS) released overall rating systems for hospitals in July of
this year. As [its
announcement](https://www.cms.gov/Newsroom/MediaReleaseDatabase/Fact-sheets/2016-Fact-sheets-items/2016-07-27.html)
explained, “The new Overall Hospital Quality Star Rating summarizes data
from existing quality measures, publicly reported on Hospital Compare,
into a single star rating for each hospital, making it easier for
consumers to compare hospitals and interpret complex quality
information.”

MACRA also established similar public reporting activities for
physicians. By releasing the Medicare data to certain qualified entities
in states, the public has access to comparative reports on individual
physicians. In addition, [this
act](https://www.cms.gov/Medicare/Quality-Initiatives-Patient-Assessment-Instruments/Value-Based-Programs/MACRA-MIPS-and-APMs/MACRA-MIPS-and-APMs.html) gives
physicians incentives to move away from fee-for-service to alternative
payment models like those described above. This aligns physicians and
hospitals to deliver higher-quality, lower-cost care for the populations
of patients they serve.

**Focus on improvement.** Collaborative learning networks have been
established throughout the country and are producing encouraging
results. CMS has facilitated these networks by committing resources to
establish them. An example is the [Hospital Engagement
Network](https://partnershipforpatients.cms.gov/p4p_resources/archivedmaterials/archivedmaterials.html)
(HENs). Seventeen HENs were created in 2011, which include more than
3,000 hospitals. Together, they worked to reduce the rates of 10 types
of harms, such as patient falls and pressure ulcers. This was part of
CMS’s [Partnership for Patients
Initiative](https://partnershipforpatients.cms.gov/about-the-partnership/aboutthepartnershipforpatients.html),
which was designed to improve the quality performance of hospitals. It
was a partnership between the government and the private sector,
including many consumer groups.

The U.S. Department of Health and Human Services
has [touted](http://www.hhs.gov/about/news/2014/12/02/efforts-improve-patient-safety-result-1-3-million-fewer-patient-harms-50000-lives-saved-and-12-billion-in-health-spending-avoided.html)
the program’s overall success. For example, it stated that 50,000 fewer
patients died, 1.3 million adverse events were avoided, and \$12 billion
was saved at hospitals because of reductions in hospital-acquired
conditions from 2010 to 2013. There has been some controversy as to the
accuracy of these results, as there was some variation in measurement
from hospital to hospital, but overall this improvement initiative
delivered real results. The CMS renewed the effort in 2015 for one year.

Similar collaboratives were established for physicians — for example,
the [Transforming Clinical Practice
Initiative](https://innovation.cms.gov/initiatives/Transforming-Clinical-Practices/),
which was designed to support clinician practices through nationwide
collaborative, and peer-based learning networks that facilitate practice
transformation.

**Eliminating pre-existing conditions as a barrier to coverage.** There

are two elements of the ACA that appear to have clear bipartisan
support. One is allowing people to purchase insurance even if they have
a pre-existing condition. This has reduced the insurance rates for this
group of people considerably. The other is allowing children up to the
age of 26 to stay on their parents’ insurance plan. Of course, the irony
is that without a risk pool that includes everyone (discussed below),
the uninsurable become so expensive that insurance premiums skyrocket
— something that just happened for people who purchase plans on the
exchanges.

### **What Needs to Change**

**The exchanges.** The most attacked provision of the ACA is the
insurance exchanges. The cost of insurance is driven by the actuarial
risk of the insured. The fundamental flaw of the ACA insurance exchanges
was that people could easily opt in and out.

For example, healthy young people are allowed to opt out of the
requirement to pay for insurance by paying a penalty that was extremely
low compared to the actual cost of purchasing health insurance — even
with government subsidies and the relatively low original premium rates.
At this point, there are millions of healthy young people, and others,
who prefer to take their chances without insurance. They then jump into
the exchange at the long open-enrollment periods if they get sick. This
skews the overall risk pool and is leading to some of the exorbitant
premium increases this year.

A [Rand
study](http://www.rand.org/pubs/external_publications/EP50692.html)
recently found that between 2013 and 2015, 22.8 million people gained
coverage and 5.9 million people lost coverage, resulting in a net
increase of 16.9 million people with insurance. The number of uninsured
Americans fell from 42.7 million to 25.8 million. We don’t want to go
backward; therefore, we must address the risk pool issue. Here are three
possible alternatives.

The first option is to continue the exchanges in their current form but
make the penalties close or equal to what the least expensive policy on
the exchange costs. This will reduce the number of healthy people who
don’t sign up. Shorten the enrollment period and make it mandatory that
people stay in the pool for at least a year. This will tighten the risk
pool and help insurers better predict the cost of coverage each year.

The second option, which has been [discussed by House Speaker Paul
Ryan](https://paulryan.house.gov/uploadedfiles/pcasummary2p.pdf), would
allow states to create and manage insurance exchanges but also give
states “the ability to band together in regional pooling arrangements,
as well as the creation of robust high risk pools, reinsurance markets,
or risk adjustment mechanisms to cover those deemed ‘uninsurable.'”

The third option is Medicare for all. Some have referred to it as the
“public option.” Designed for those who don’t have employer-sponsored
health insurance, it would be administered much as Medicare is
administered today. Employers could opt in to the plan for their
employees by paying a fee that covers most of the cost of their
employees, but they wouldn’t be mandated to do so. Some have argued this
would allow the government to negotiate better prices. Having a single
administrator would also reduce administrative expenses.

**Medicaid expansion.** To put the exchanges in perspective, over 4
million of the newly insured people enrolled through the exchanges, but
6.5 million of those with coverage were due to Medicaid expansion. There
are 20 states that have opted to not accept federal funding to expand
Medicaid. It
is [estimated](http://healthaffairs.org/blog/2016/03/21/the-economics-of-medicaid-expansion/)
that 3 million more people could be covered if these states would expand
Medicaid. The arguments about whether to expand rage on.

One option is for the CMS to provide block grants to states for
Medicaid, with the states taking control of managing the population.
This would only work if states don’t increase eligibility requirements.
Whether this option or others emerge as the winner, it’s important to
get everyone eligible for Medicaid covered if we want to keep the
rate of the uninsured at the historically low level it is at today.

### **In Conclusion**

Because of the complexity of all these issues, it is very easy to miss
the forest for the trees. In fact, no alternative is financially viable
without the core reform processes that I outlined in the beginning of
this article.

Payment systems must reward the efficient delivery of care. They must
reward care providers for keeping patients healthy and out of expensive
hospitals. CMS has led the way on this but has no control over whether
commercial insurers create incentives for providers to become more
efficient. If commercial insurers continue to pay on a fee-for-service
basis, the total cost of care will continue to rise for employers who
offer health insurance and for individuals who purchase insurance on the
exchanges.

In addition, when patients do need care, the processes of care delivery
must be redesigned to radically improve the patient experience and
reduce errors; [250,000 people dying every year from medical error is
unacceptable.](https://hub.jhu.edu/2016/05/03/medical-errors-third-leading-cause-of-death/)
Finally, individual hospital and provider outcomes must be made public
to the consumer so patient choice can play a role in reform.

Parts of the ACA are clearly on the right track to address health care
reform. Other parts need to be significantly improved. The focus should
be on improving the affordability of health care for every American.
This should be the number one priority of the new government.
