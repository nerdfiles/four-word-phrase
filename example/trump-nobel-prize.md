<div class="container">

<div class="site-branding row">

<div class="col-md-4 logo">

[![GarretGalland.com](//s3.amazonaws.com/ggc-gg-images/global/main-site-logo.gif)](http://www.garretgalland.com/)

</div>

<div class="col-md-5 header-top-right">

<span class="indent-right"><span class="tealcolor">Independent</span>
Accurate</span>\
Unbiased <span class="tealcolor">Disciplined</span>

</div>

</div>

<div class="main-navigation row">

<div class="col-md-12">

<div class="navbar-header">

<div class="alignleft">

MENU

</div>

<div class="alignright">

<span class="sr-only">Toggle</span> <span class="icon-bar"></span> <span
class="icon-bar"></span> <span class="icon-bar"></span>

</div>

</div>

<div class="collapse navbar-collapse">

-   [Home](http://www.garretgalland.com/)
-   [About Us <span class="caret"></span>](){.dropdown-toggle}
    -   [Company Overview](/about-us)
    -   [Our Team](/about-us/our-team)
    -   [Our Mandate](/about-us/our-mandate)
    -   [Our Process](/about-us/our-process)
    -   [Our Investment Policies](/about-us/our-investment-policies)
-   [Free Research <span class="caret"></span>](#){.dropdown-toggle}
    -   [Our Free Research](/passing-parade/overview)
    -   [The Passing Parade](/passing-parade)
    -   [Articles](/articles)
-   [Premium Research <span class="caret"></span>](#){.dropdown-toggle}
    -   [Our Premium Research](/premium-research)
    -   [Compelling Investments
        Quantified](/compelling-investments/overview)
    -   [The Tangible Investor](/the-tangible-investor/overview)
-   [Testimonials](/testimonials)
-   [Contact Us](/contact)
-   <div id="/login-nav">

    </div>

    [Members Area](/members)

</div>

</div>

</div>

<div id="entry-content" class="col-md-9">

![The Passing
Parade](//s3.amazonaws.com/ggc-gg-images/passing-parade/masthead-online-passing-parade.png){.masthead
.img-responsive width="825"}\
<div class="pull-right" style="margin-right:35px;">

<div class="addthis-wrapper">

<div class="addthis_sharing_toolbox text-center">

<div class="rss-share">

[![rss](//s3.amazonaws.com/ggc-gg-images/global/icon-rss.png){width="32px"}](http://feeds.feedburner.com/garretgalland/BFWT)

</div>

</div>

</div>

</div>

Markets Award Trump Nobel Prize in Economics
============================================

By [David Galland](/about-us/david-galland) | [The Passing
Parade](/passing-parade/archive) | December 16, 2016

Dear Parader,

As today we are headed north to spend Christmas with the family, Jake
Weber, the invaluable senior researcher of our premium [**Compelling
Investments *Quantified*
(CIQ)**](http://www.garretgalland.com/go/v34gdd/GGR) service has kindly
offered to fill in for this edition.

In his article, he delves into the economic and political challenges
President Trump is facing and scans for any red flags flapping in the
breeze.

On the topic of CIQ, we maintain a laser-like focus on finding great
companies that sell below their intrinsic value—the only proven path to
successful long-term investment.

In fact, of the six stocks we recommended since launching in June of
2016, only one is in the red, down 8%. However, as we are long-term
investors and our reasons for owning the high-yielding stock have not
changed, its short-term pullback doesn’t concern us.

<div class="inline-signup-form">

 

</div>

The other stocks in our CIQ portfolio have returned 1%, 16%, 22%, and
31%. Our most recent recommendation, made at the end of November, is
already up 15%.

If you’re looking for a more predictable approach to investing, one that
takes emotion out of the equation and ensures you’ll sleep well at
night, you owe it to yourself to take a risk-free trial subscription to
**Compelling Investments *Quantified***—[click here for
details](http://www.garretgalland.com/go/v34gdd/GGR).

And with that biased but fully guaranteed recommendation, I’ll now turn
the page over to Jake.

*David Galland*

<span style="color:#0a4b7d;">Markets Award Trump Nobel Prize in Economics</span>
--------------------------------------------------------------------------------

**By Jake Weber**

It took only nine days in office for President Obama to be nominated for
the Nobel Peace Prize.

Less than nine months later, the Norwegian Nobel Committee followed
through and awarded Obama the Nobel Prize for “his extraordinary efforts
to strengthen international diplomacy.” That, despite the fact Obama had
no significant diplomatic accomplishments.

Likewise, the markets seem to have prematurely greeted Trumponomics as
an outstanding success. US stock indices continue to make all-time
highs, the dollar is charging ahead, and bond markets are being trashed.

Furthermore, the latest Fund Manager Survey by Bank of America shows
growth and inflation expectations at the second-highest level since
2004.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_1_20161216_TPP.jpg)

In short, US equity markets are already factoring in Trump’s draining of
the swamp, his implementation of regulatory reforms, and the rollback of
poor policies that trip up the American economy, while bond markets are
pricing in higher inflation.

There’s just one thing: Trump hasn’t done anything yet. We’re still
weeks away from his inauguration, and details of his economic plans
remain scarce.

No matter, any potential positives for economic growth (lower taxes,
deregulation, and fiscal stimulus) are being accepted at face value,
while the potential negatives (soaring deficits, tariffs, and trade
wars) are being ignored.

After eight years and 100,000 pages of new regulations during the Obama
administration, it’s understandable why the markets are cheering for
change. There are reasons to be optimistic. But as an investor, I’m not
ready to give Trump the Nobel Prize for Economics just yet.  

### The Art of the Deal

There’s little doubt Trump will shake up the status quo of DC politics.
For evidence, look no further than the @realDonaldTrump Twitter feed.
But he’s going to have to use all of his skills in the *art of the deal*
to get things done.

For example, earlier this year the Republican-majority House passed a
budget blueprint calling for deficit reductions of \$7 trillion over the
next 10 years, primarily through spending cuts.

Against that blueprint, we have Trump’s proposed policy changes. The
Committee for a Responsible Federal Budget, a nonpartisan think tank,
estimates those changes, in particular tax cuts, could increase the
deficit by \$5.3 trillion over the next decade.

Speaker of the House Paul Ryan and Senate Majority Leader Mitch
McConnell have both indicated that tax reform should be “deficit
neutral.” Moving forward with the tax cuts will therefore require the
assumption that they will trigger enough economic growth to offset the
projected budgetary hit.

Trump’s team will have to successfully argue that point.

Deregulation is perhaps the most encouraging, pro-growth policy priority
Trump and most Republicans can agree on. But it’s far harder to repeal a
law than it is to write a new one. The process of repealing Obamacare
could take years and, according to the Congressional Budget Office
(CBO), could add \$353 billion to the deficit over the next decade.

It’s also going to be hard to get deficit hawks on board with a
trillion-dollar infrastructure plan, even if it is for the most part
privately financed and includes significant tax incentives.

Then there’s Trump’s steady threats of imposing tariffs. History has
shown that tariffs are inflationary as they raise prices of imported
goods, allowing domestic industries to follow suit. Of course, should
the new president follow through with tariffs, it will almost certainly
lead to a tit-for-tat trade war with trading partners.

Trump the businessman was a huge fan of using debt to build his empire,
and President-elect Trump appears to disregard the deficit entirely.
Every economic policy proposal put forth by Trump seems to have
inflationary implications.

In that light, the bond market’s sell-off makes perfect sense. After
all, inflation is the number-one enemy of bonds.

However, it looks like the markets assume that Trump will be able to
force his policies through Congress, and do so without consequences.

### Trump’s Mountain

The consensus view is that the era of boosting growth with loose
monetary policy is giving way to a new era of expansive fiscal policy.
Most of the proposed changes should boost inflation and maybe growth
too, which should send interest rates higher. But higher interest rates
pose a major problem to the mountain of debt overhanging the US economy.
 

Federal debt is already \$20 trillion—excluding the massive unfunded
entitlement obligations that will balloon over the next decade as more
Baby Boomers reach retirement age. 6% of the 2015 federal budget was
used to pay interest on federal debt. That’s with historically low
interest rates and the Federal Reserve owning 20% of the outstanding
debt (interest paid to the Fed is remitted back to the Treasury).

If the interest rate on the 3-month Treasury bill rose from 0.5% today
to 3.2% over the next 10 years, and the 10-Year Treasury bond from 2.6%
to 4.1% over the same period, the CBO projects the interest expense
could more than double—to 13.1% of federal spending.

That projection was made before taking into consideration any of Trump’s
plans to boost federal spending while cutting taxes.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_2_20161216_TPP.jpg)

Then there are the state and local governments, which collectively are
\$3 trillion in debt—excluding trillions more in unfunded pension
obligations.

Household debt sits at \$14.6 trillion and business debt at \$13.4
trillion, a 25% increase from 2008. Private debt alone accounts for 150%
of GDP, a dangerous level.

American households have not deleveraged since the financial crisis, and
corporate America has taken advantage of low interest rates to add
significantly more leverage. In many cases, companies tacked on more
debt for share buybacks to juice earnings rather than make productive
capital investment.

An early-2016 report by Standard & Poor’s estimated that US companies
have \$4.1 trillion of debt coming due in the four short years between
2016 and the end of 2020. If the recent trend for higher interest rates
continues, rolling over that corporate debt will be a challenge.

Stock market bulls are placing firm bets that tax cuts and stimulus
spending will boost earnings more than higher interest rates will lower
them. And interest rates are on the rise in anticipation of economic
growth and inflation.

Yet there is still a lot of ground to be covered before tax cuts and
stimulus spending are passed into law and begin to have their
(hopefully) positive impact on the economy.

### Too Far, Too Fast

Inflation *expectations* have been the main driver behind the spike in
rates—not actual inflation.

The Fed’s preferred inflation gauge is the annual change in the price
index for personal consumption expenditures (PCE), which is still
running below the 2% inflation target.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_3_20161216_TPP.jpg)

As you can see in the chart, PCE has been on the rise, but that is
mostly a result of year-over-year increases in still historically cheap
oil prices. Core PCE, which excludes the more volatile food and energy
prices, has had a much smaller increase this year and is also running
below the Fed’s target.

On the other hand, inflation expectations (the light-blue line in the
chart below) have spiked above 2%. Following suit, the 10-Year Treasury
bond yield has jumped as well.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_4_20161216_TPP.jpg)

Inflation expectations may be justified as Trump appears intent on
jumpstarting inflation. However, interest rates may have gone too far,
too fast. If rates stay elevated before the tax cuts and stimulus
spending kicks in, the US could slip back into a recession. In which
case the 35-year secular bull market in bonds may not be over.

Either way, Trump is going to face many challenging decisions in his
quest to stimulate growth. Given the massive overhang of debt and his
plans to add more, the headwinds from higher interest rates pose a
significant threat.

### Catch-22 Monetary Policy

The Federal Reserve hiked rates by a quarter point this week, and the
projected path was raised from two rate hikes to three by the end of
2017. That projection needs to be taken with a grain of salt. At the
beginning of both 2015 and 2016, the Fed projected four hikes, but
ultimately only hiked once during each year, in December.

Significantly, by projecting three rather than four hikes in its latest
meeting, the Fed is actually entering 2017 more dovish than it has been
at the start of the last two years.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_5_20161216_TPP.jpg)

Interestingly, the Fed only modestly adjusted real GDP growth up by 0.1%
in 2017 and 2019. And its projections for inflation and core inflation
were unchanged from 2017 onward.

For years, the Fed has begged for fiscal stimulus to complement its
loose monetary policy. With the economy finally poised to receive a dose
of Keynesian government spending, the Fed is itching to normalize rates.

The Fed wants higher rates so that when the next recession threatens,
it’ll have enough ammo in its war chest to fend it off. However, even a
modest rise in interest rates could be the thing that triggers a
recession. It’s a catch-22.

Of course, the Fed can only control shorter-term interest rates with its
normal operations. There are many determinants of longer-term interest
rates, but growth and inflation expectations are probably the most
important.

Right now, the euphoric markets think both will be instantaneous with
Trump’s inauguration. Until we see something more tangible, cautious
optimism is probably the more appropriate stance for investors to take
on Trumponomics.

### A Few Concluding Portfolio Thoughts

If you’re holding long bonds, your portfolio has already experienced a
lot of pain since the election. Now that the December Fed hike is out of
the way, I wouldn’t be surprised to see interest rates pull back from
here. It’s probably a good idea to use any pullback to reduce your
exposure to long rates as we wait to see what Trump can actually do.

If Trump can quickly push through his economic agenda and cooler heads
prevail on the trade front, then the path of least resistance will be
higher for interest rates.

In this optimistic view, strong economic growth could overcome the
headwinds from the debt overhang. The Fed would still probably err on
the side of caution. Allowing inflation to run ahead of 2% would help
relinquish some of the national debt burden.

However, it’s more likely that Trump’s agenda faces pushback from both
sides of the aisle. If the last eight years have taught us anything,
it’s that there are many creative ways to hold up action in DC. That’s
not always a bad thing as government meddling is usually
counterproductive. But I have a sneaking suspicion that we haven’t heard
the word “filibuster” for the last time.

In that case, the stock market rally and the bond market rout have both
probably overshot.

Of course, no one can predict how this will play out. The best we can do
is wait and see. Having ample cash on the sidelines may be the best
strategy.

![](http://ggc-gg-images.s3.amazonaws.com/uploads/newsletters/Image_6_20161216_TPP.jpg)

Trends in interest rates tend to stay in motion for years if not
decades, so there will be enough time to switch if a new secular bear
market in interest rates emerges in earnest.

(No investment strategy is bulletproof, but you can tilt the odds in
your favor by focusing your attention on buying great companies when
they fall to a level of deep undervaluation. How to do just that is
explained in some detail in our free report, ***Dimes for Dollars—The 3
Most Powerful Strategies to Buy Low and Sell High***. **[Click here to
get your free copy now](http://www.garretgalland.com/go/v34gd9/GGR)**.)

<span style="color:#0a4b7d;">Here Come the Clowns</span>
--------------------------------------------------------

**Next Time, Try Handing the Terrorist a Participation Medal**. The
modern-day separationist movements continue trying to outdo themselves
in the Olympics of inanity… or maybe even insanity. In the latest
attempt at the gold, the Ohio State University *Coalition for Black
~~Separation~~ Liberation* held a rally at which they read out a list of
non-white men killed by the police.

Among the victims on the list was poor **Abdul Razak Ali Artan**, the
jihadist who just a couple of weeks ago plowed his car into a group of
students, then embarked on a stabbing spree, wounding 11 people.

Fortunately, or I guess “unfortunately” if you belong to the
aforementioned coalition, a fast-thinking university police officer
committed the politically incorrect act of sending Abdul off to meet his
72 virgins.

All I can do when [reading this sort of
thing](http://dailycaller.com/2016/12/08/osu-activists-complain-terrorist-was-wrongly-shot-by-police/)
is shake my head and thank my lucky stars that my life path led me to a
quaint pueblo in the Argentine outback where both terrorism and
political correctness are nowhere to be found.

And with that, I will sign off until next week and resume preparing for
the trip back to the frigid North for the holidays. Thank you for
reading.

![David
Galland](//ggc-mauldin-images.s3.amazonaws.com/uploads/DGSig.jpg?v=1461680500097)\
David Galland\
Managing Editor, *The Passing Parade*

<div id="inline-tpp-signup">

<div id="inline-tpp-signup" class="free-newsletter widget inline">

### Get *The Passing Parade* delivered directly to your inbox every Friday. It's FREE:

<div class="row">

<div class="col-md-8">

<div class="g-recaptcha"
style="transform-origin: 0 0;-webkit-transform-origin: 0 0;transform: scale(.7);-webkit-transform: scale(.7);"
data-sitekey="6Lc9LA0UAAAAAFl5Da8mz2cW7KdSs4ta_6Tjioqj">

</div>

</div>

<div class="col-md-4">

</div>

</div>

**The Passing Parade** (No obligation. Unsubscribe at any time).

</div>

</div>

If you enjoyed this read, we’d love you to Share on:

<div class="addthis-wrapper">

<div class="addthis_sharing_toolbox text-center">

<div class="rss-share">

[![rss](//s3.amazonaws.com/ggc-gg-images/global/icon-rss.png){width="32px"}](http://feeds.feedburner.com/garretgalland/BFWT)

</div>

</div>

</div>

\
<div id="disqus_thread">

</div>

Please enable JavaScript to view the [comments powered by
Disqus.](https://disqus.com/?ref_noscript)

</div>

<div id="sidebar" class="col-md-3">

<div class="section article-list">

Recent Articles
---------------

-   [Markets Award Trump Nobel Prize in
    Economics](/passing-parade/markets-award-trump-nobel-prize-in-economics)

[Show Full Archive](/passing-parade/archive){.btn}

</div>

------------------------------------------------------------------------

\
[![](//s3.amazonaws.com/ggc-gg-images/compelling-investments-quantified/ciq-banner-300x250.png)](/compelling-investments/overview)

</div>

</div>

<div id="disclaimer">

<div class="container">

<div class="col-md-12">

**Important Disclosures** ****\
Use of this content, the Garret/Galland Research website and related
websites and applications, is provided pursuant to the Garret/Galland
Research Terms & Conditions of Use.

**Unauthorized Disclosure Prohibited** ****\
The information provided in this publication is private, privileged, and
confidential information, licensed for your sole individual use as a
subscriber. Garret/Galland Research reserves all rights to the content
of this publication and related materials. Forwarding, copying,
disseminating, or distributing this report in whole or in part,
including substantial quotation of any portion the publication or any
release of specific investment recommendations, is strictly prohibited.

Participation in such activity is grounds for immediate termination of
all subscriptions of registered subscribers deemed to be involved at
Garret/Galland Research’s sole discretion, may violate the copyright
laws of the United States, and may subject the violator to legal
prosecution. Garret/Galland Research reserves the right to monitor the
use of this publication without disclosure by any electronic means it
deems necessary and may change those means without notice at any time.
If you have received this publication and are not the intended
subscriber, please contact <support@garretgalland.com>.

**Disclaimers**\
The Garret/Galland Research website, *Compelling Investments Quantified
and The Passing Parade; The Tangible Investor and The New Abnormal *are
published by Garret/Galland Research. Information contained in such
publications is obtained from sources believed to be reliable, but its
accuracy cannot be guaranteed. The information contained in such
publications is not intended to constitute individual investment advice
and is not designed to meet your personal financial situation. The
opinions expressed in such publications are those of the publisher and
are subject to change without notice. The information in such
publications may become outdated and there is no obligation to update
any such information. You are advised to discuss with your financial
advisers your investment options and whether any investment is suitable
for your specific needs prior to making any investments.

Olivier Garret, David Galland, Garret/Galland Research, and other
entities in which they have an interest, employees, officers, family,
and associates, may from time to time have positions in the securities
or commodities covered in these publications or website. Corporate
policies are in effect that attempt to avoid potential conflicts of
interest and resolve conflicts of interest that do arise in a timely
fashion.

Garret/Galland Research reserves the right to cancel any subscription at
any time, and if it does so, it will promptly refund to the subscriber
the amount of the subscription payment previously received relating to
the remaining subscription period. Cancellation of a subscription may
result from any unauthorized use or reproduction or rebroadcast of any
Garret/Galland Research publication or website, any infringement or
misappropriation of Garret/Galland Research’s intellectual property or
other proprietary rights, or any other reason determined in the sole
discretion of Garret/Galland Research.

**Affiliate Notice**\
Garret/Galland Research has affiliate agreements in place that may
include fee sharing. Likewise, from time to time Garret/Galland Research
may engage in affiliate programs offered by other companies, though
corporate policy firmly dictates that such agreements will have no
influence on any product or service recommendations, nor alter the
pricing that would otherwise be available in absence of such an
agreement. As always, it is important that you do your own due diligence
before transacting any business with any firm, for any product or
service.

**Legal Disclosures:**\
All material presented herein is believed to be reliable, but we cannot
attest to its accuracy. Opinions expressed in these reports may change
without prior notice.  Olivier Garret, David Galland, and Garret/Galland
Research and/or its staff may invest in recommendations covered by
Garret/Galland Research’s publications when providing adequate
disclosure to subscribers.  In order to avoid potential conflicts of
interests, investment in any covered recommendations will follow the
terms of Garret/Galland Research’s Ethics and Trading Policy.

This website has links to other agencies and organizations. When you go
to another site through the links, you are no longer on our site and are
subject to the terms and conditions of the new sites.

Please review the privacy policies on those websites to understand their
personal information handling practices. We make no representations
concerning the privacy policies of these third party websites.\
Unauthorized attempts to upload information and/or change information on
any portion of this site are strictly prohibited and are subject to
prosecution under the Computer Fraud and Abuse Act of 1986 and the
National Information and Infrastructure Protection Act of 1996 (see
Title 18 U.S.C. §§ 1001 and 1030).

**Please note:** Changes to this legal statement or to the privacy
policy will be documented here. Check back periodically for the latest
updates.

</div>

</div>

</div>

<div id="footer-columns">

<div class="container">

<div class="col-md-4">

##### Our Services: {#our-services .widget-title}

-   [The Passing Parade](/passing-parade)
-   [Compelling Investments
    Quantified](/compelling-investments/overview)
-   [The Tangible Investor](/the-tangible-investor/overview)

</div>

<div class="col-md-3">

##### Company Info: {#company-info .widget-title}

-   [Company Overview](/about-us)
-   [Our Team](/about-us/our-team)
-   [Our Mandate](/about-us/our-mandate)
-   [Our Process](/about-us/our-process)
-   [Our Investment Policies](/about-us/our-investment-policies)

</div>

<div class="col-md-3">

##### Contact Info:

-   ** (844) 848-8835 Toll-Free
-   ** (602) 883-1999 From Outside the US
-   ** support@garretgalland.com
-   ** 673 South Main Street | PO Box 1423 | Stowe, VT 05672 | USA

</div>

<div class="col-md-2 pull-right">

##### Stay Connected:

-   **
    [Facebook](https://www.facebook.com/Garret-Galland-Research-1684652238465431/)
-   ** [Twitter](https://twitter.com/Garret_Galland)
-   ** [Google+](https://plus.google.com/109993558525096212706/about)
-   **
    [LinkedIn](%0Ahttps://www.linkedin.com/company/garret-galland-research)

</div>

</div>

</div>

<div class="container">

<div id="copyright">

<div class="col-md-8">

Copyright © 2016 GarretGalland.com. All Rights Reserved.

</div>

<div class="col-md-4 text-right">

[Important Disclosures](/disclosures)[Terms of Use](/terms-of-use)
[Privacy Policy](/privacy-policy)

</div>

</div>

</div>

<div id="ouibounce-modal">

<div class="underlay">

</div>

<div class="modal2">

<div class="modal-title">

### Before you go...

</div>

<div class="modal-body">

<div class="row">

<div class="col-sm-12">

...please take a moment to download our most recent FREE special report,
***Dimes for Dollars:** The 3 Most Powerful Strategies to Buy Low and
Sell High*.

Enter your email address below to receive this report and learn...

![Dimes for
Dollars](//s3.amazonaws.com/ggc-gg-images/landing/dimes-for-dollars/dimes-report.png){.pull-right
width="210"}
-   The REAL enemy all retail investors are facing… and what you need to
    do to protect yourself

-   Stop losing money on stocks! With this little-known investment
    strategy, you can effectively eliminate your risk of making two
    common but deadly investment mistakes

-   Three value plays every investor needs to understand to find winning
    investments

-   And much more!

<div id="subscriber-popup-field">

<div class="g-recaptcha"
style="display: inline-block;transform-origin: 0 0;-webkit-transform-origin: 0 0;transform: scale(.7);-webkit-transform: scale(.7);"
data-sitekey="6Lc9LA0UAAAAAFl5Da8mz2cW7KdSs4ta_6Tjioqj">

</div>

Yes! I also want my free subscription to David Galland's insightful (and
free) newsletter "The Passing Parade."

</div>

</div>

</div>

</div>

<div id="roundtable-modal-footer" class="modal-footer">

No Thanks

</div>

</div>

</div>

<span style="display:none;"></span>
