Can Clinton stop Trumpism?

The pressure to vote for Hillary Clinton is becoming ever more intense. But the left has to resist the political logic of lesser evilism that the Democrats rely on every four years.

Donald Trump and Hillary Clinton

FIRST, IT was the shaming. Now it's fear--pure, raw terror at the prospect of Donald Trump in the White House.

Polling expert Nate Silver tweeted the scary numbers on September 14: "[T]here's no margin for error. Lose any one of NH/PA/WI/MI/CO/VA and Trump is POTUS." Silver's FiveThirtyEight.org website estimates the chances of Hillary Clinton winning the presidency at less than 60 percent today--down from nearly 90 percent a month ago.

Those statistics are the start of the one-two punch in a tidal wave of e-mails, social media posts and articles: One, Trump could win. So two, you better vote for Clinton--at least where it matters--or we're all doomed.

The idea of Donald Trump in the White House will sicken any reader of Socialist Worker. It's depressing that this election is even close--and a sign of how awful a candidate Hillary Clinton is. Trump's xenophobic, anti-Muslim rhetoric has emboldened those on the right. It's hard to guess from his erratic campaign exactly what he would try to do as president, except that whatever it is would be aimed at helping the 1 Percent and making the lives of everyone else worse.

But the real question isn't whether Trump is dangerous. Of course he is. It's whether supporting Hillary Clinton will stop the danger of Trump or Trumpism.

Will having a Democrat in the White House make it easier for the left and social movements to organize? What's happened in the past? How will pressuring left-wing people to vote for Clinton--who stands for none of the principles they do--affect the movements we know need to be built to push back against what Trump stands for?

These are serious questions that deserve serious discussion. But that discussion won't take place if the starting point is that it's game over--for the left, for social movements, for any form of political freedom, for the future of the planet--if Trump becomes president.

The doomsday scenarios being used to demand a vote for Hillary Clinton overstate both Trump's potential power and how different he actually is from Clinton. But more importantly, they reduce the large majority of people who oppose much of what both candidates stand for to passive victims who will simply roll over and be crushed by the Trump juggernaut.

A Trump presidency will be a disaster if those people--including the left, as an important and catalyzing part of a potential opposition--don't rise to the challenge of building resistance. But it's also true that a Clinton presidency will be a disaster if those same people don't rise to a different, though related, set of challenges.

Put another way, it's not just who's sitting in the White House that matters, but who's sitting in, as the people's historian Howard Zinn often said.

And if that's true, then the next question is whether it helps the people who will need to do the sitting in to advise them to vote for Hillary Clinton as the "lesser of two evils."

Our answer is no. The struggle to stop Trump and especially Trumpism won't be advanced by giving in to the lesser-evil panic--because that confuses and obscures something our side needs to know to take on Trumpism: that the lesser evil isn't opposed to the greater evil, but is part of a system that allows both kinds of evil to thrive, unless they are confronted from below.

- - - - - - - - - - - - - - - -

AS THE election gets closer and the alarm bells--make that the eardrum-splitting civil defense sirens--about the Trump threat grow louder, the pressure is on not just to vote for Clinton, but to suspend all criticism of the Democratic candidate.

Clinton's former rival Bernie Sanders made this clear in an interview for MSNBC's Morning Joe:

I would say to those people out there who are thinking of the protest vote, think about what the country looks like and whether you're comfortable with four years of a Trump presidency...And I would suggest to those people, let us elect Hillary Clinton as president, and that day after, let us mobilize millions of people around the progressive agenda which was passed in the Democratic platform.

Sanders at least suggested that people who want change should, at some point, start mobilizing again. Other doomsayers have been content to heap abuse on anyone who says wants to vote for a candidate they want, like Jill Stein of the Green Party.

New York Times columnist Paul Krugman, for example, chastised millennials with an op-ed titled "Vote As If It Matters." While most of his fire was aimed at supporters of Libertarian Gary Johnson, who is by no stretch of the imagination a left-wing candidate, his condescending message to young voters dissatisfied with the status quo from the left was: "[D]on't vote for a minor-party candidate to make a statement. Nobody cares."

Krugman has a lot of company, and not only among the usual suspects like organized labor and mainstream liberal organizations.

Independent journalist Arun Gupta made a startling case on Facebook on the fifth anniversary of Occupy Wall Street, a movement he helped give a voice to with the publication he founded, the Occupied Wall Street Journal. In a long post directed at leftists who are "obsessed with how terrible Hillary Clinton is" and who supposedly refuse to take the Trump threat seriously, Gupta wrote:

This election is a choice between two movements. Do you want to see movements like Black Lives Matter, Climate Justice, low-wage workers, immigrant rights, and other left social forces continue to grow and develop. Or do you want to see Neo-Nazis, the Klan, the Alt-Right on the offense and backed by a Trump administration?"

- - - - - - - - - - - - - - - -

LET'S SET aside the routine denigrating of the left as out-of-touch purists who just don't get the high stakes of a Trump win. It's an unfair accusation, especially when directed at people who helped organize protests against Trump campaign events--protests that were criticized or outright condemned by Democrats like Clinton, by the way.

The heart of the Gupta's argument is the belief that if Trump wins, it will be too late to organize resistance--because "there will be little left of movements in the streets."

That's a frightening and provocative image. But is it true?

In 2006, the Republican right threw its weight behind anti-immigrant legislation sponsored by Rep. James Sensenbrenner, which proposed to criminalize all undocumented immigrants in the U.S. With the White House and both houses of Congress in GOP hands, momentum seemed to be with the Sensenbrenner bill.

But the response from immigrants and their supporters was some of the largest demonstrations in the history of the U.S.--mega-marches of millions of people across the country, made possible because large numbers of people participated in one-day general strikes. Within months, HR 4377 was doomed--even Senate Republicans and the Bush administration turned against it.

Fast forward a decade. Barack Obama--who promised during his 2008 campaign that he would pursue comprehensive immigration reform as a top priority--has deported well over 2 million people during his two terms in office, far more than his predecessor George W. Bush. The "lesser evil" has proven to be a greater threat to a vulnerable oppressed group than the one-time "greater evil" was.

Yet the response to Obama was not mass mobilizations of the "movement in the streets." There have been important struggles--for example, to win temporary and qualified legal status for some immigrant youth--during the Obama years. But by and large, the immigrant rights movement has been absent from the streets, largely because liberal organizations have been reluctant to mobilize against their "ally" in the White House.

There's more to this story--but also more historical examples of some of the most important and explosive struggles in U.S. history erupting against Republican presidents and the right wingers who looked to them.

- - - - - - - - - - - - - - - -

BUT, GOES the counter-argument, Donald Trump isn't any ordinary Republican--he represents a unique threat.

Run-of-the-mill Republican, Trump is not--though anyone who thinks he is uniquely dangerous should remember what it was like to live under the Bush regime, managed by monsters like Dick Cheney and Donald Rumsfeld.

While far-right groups have gravitated to the Trump campaign, there is still a difference between what exists in the U.S. and the kind of organization among many far-right political parties in Europe, with bases of hard-core supporters that use systematic violence against oppressed communities and the left.

Nevertheless, it is certainly true that "Neo-Nazis, the Klan [and] the Alt-Right" would be emboldened by a Trump victory. The hate and violence on display at Trump rallies is quite enough evidence of a right-wing threat that needs to be challenged.

But how? Any challenge to the right has to include putting forward a political alternative that can win masses of people away from the politics of despair and scapegoating that a political candidate like Trump and far-right organizations both thrive upon.

Calling for a vote for Hillary Clinton is the opposite of putting forward a political alternative. It is putting forward the status quo--which is exactly what allows the likes of Trump to pose as "populist" outsiders who stand up for ordinary people.

There is intense bitterness in U.S. society about the growing gap between rich and poor, the corruption of the political system, inaction about climate change and many more issues. Yet Hillary Clinton and the Democrats have positioned themselves as the "complacency candidates," as left-wing author Thomas Frank put it.

That leaves the field open--during the election and after--for Trump and the right-wingers operating in his shadow to pose as the people with a solution. The left needs an answer to that challenge, not a plea to vote for the lesser evil.

- - - - - - - - - - - - - - - -

IT SHOULD go without saying--though sadly, judging from the discussion especially on social media, it doesn't--that no one thinks the left would be better off under a Trump presidency.

Trump's ideas are abhorrent, and the policies and proposals a Trump administration would put forward are equally so. If he wins, it would give confidence to the right-wingers, organized and not, who rallied to his campaign. Those on the left would suffer a corresponding blow to their confidence--exactly at a moment when there is a radical awakening, especially among young people.

Our point, however, is that the left would not be better off if Clinton is elected either--for different reasons, but no less valid ones.

The Democrats in power have been able to rely on their influence within official liberalism to get struggles for change demobilized or blunted. The left would continue to face the task of exposing the fraud that the Democratic Party is more responsive to what the mass of working people want.

The neoliberal, pro-war agenda of the bipartisan Washington establishment is the prime cause of the political crisis that has allowed Trump and Trumpism to thrive. It doesn't help to say that the Democrats' version of that agenda is preferable to Trump's. Both versions have to be fought together and at the same time, or both will be able to gain strength.

A left-wing writer named Arun Gupta summarized the challenges for the left very clearly in an article for Telesur published after the Democratic National Convention:

[C]limbing on the Clinton train means muting criticism of her right-wing policies. It would hobble the left going into four years of more war, more free trade, more oil and gas drilling under Clinton. And that's exactly what the Wall Street Democrats want.

The left should concentrate on what it does best: laying the groundwork for new movements such as the antiwar and global justice movements, Occupy Wall Street, union, immigrant, and low-wage worker organizing, and Black Lives Matter. Clinton has bankers and liberals, pundits and billionaires, hawks and Republicans all advocating for her. Someone needs to advocate for people.

The closer we get to November 8, the pressure to vote for Clinton to stop Trump will become enormous. But it's important for socialists and the left to resist it.

The left, social movements, the mass of working people--they need a political alternative that stands outside the two-party box. Making concessions to the logic of lesser evilism that's intended to keep them inside that box won't help build an alternative.
