A

abduct

abducted

abduction

abductions of

abductions of Japanese

abolish terrorist sanctuaries

access to building

access to port

access to school

access to weapons

abnormal incident

according to an official from

according to court record (s)

according to FAA records

according to information

according to investigative reports

according to officials

according to opinion polls

according to sources

according to those familiar with the situation

acid attack

acquiring information

acres burned

across border (s)

across half the state

across large parts

across the region

across state

act of terrorism

act of war

acted immediately

active in the region

active investigation

active mission (s)

active shooter

active shooter protocol

actively monitored

activity is likely

acts of terrorism perpetrated by

acts of violence

accused of

accused of having sex with a student

accused of smuggling

acute symptoms

acute symptoms from radiation exposure

adaptive enemy (ies)

adaptive tactics

additional security measure (s)

advance fee fraud

advanced encryption technology

advanced persistent threats

advanced threat

advanced threat analytics

adversary organizations

adversary’s vulnerabilities

adverse weather conditions

advising against all travel

aerial assassinations

aerial attacks

aerial bomb

aerial bombardment

air campaign

aerial canopy

aerial firefighting

affected areas

affects drinking water

affiliated organizations

affray (UK)

Afghan lily pad

after a bomb exploded

after clash between

after knife attack

after the attack

after the massacre

after the shooting started

against U.S. interests

agency

agency outpost

agency recently disclosed

aggregate data

aggression

aggressor

agitator

agriculture safety

agriculture threat

agro terrorism

aid agencies

aid agencies report

air attack

air cargo threat

air controllers strike

air drop – fire

air marshal

air operations

air piracy

air rage

air sea battle

air show

air strike

air strikes

air tactical group supervisor

air traffic

air traffic cancellations

air traffic cancellations across Europe

air trafficking

air travel market

air tanker – f

aircraft

aircraft diverted

aircraft hijacking

airfield

airfreight transport

airplane

airline bomb threat

airport attack

airport evacuation

airport security

airport security checkpoints

airport security incidents

airspace

airspace surveillance

airspace violation

AK-47 assault rifle

alarm

alarm sounded

alarming statement (s)

all flights grounded

allegation (s) of

allegation (s) of fraud

alleged cyber-hacking

alleged members

alleged members of terrorist group

alleged plot

allegedly shot

allegedly stole proceeds

alert

alert for American citizens

alliance

allied foreign troops

allies

alligator attack

along border

along northern border

along southern border

along the bank

along the river

along the road

alternative chemicals

al Qaeda

Al-Qaeda

al-Qa’ida

al-Qaeda affiliated group

al-Qaeda attack

Al-Qaeda hit list

al-Qaeda leader

al Qaeda linked group

Al-Qaeda seizes

al-Qaeda training camp

amatol

ambush

ambushed him from

ambushed him from behind

American killed

American missing

American wounded

AMERIPOL

amid claims

amid confusion

amid fighting between

amid heightened tensions

ammonia nitrate

ammunition

Amtrak suspends

an autopsy conducted

an autopsy will be performed

anarchy

anarchists

anchor point – f

angles of security cameras

angry citizens

angry employees

angry locals

angry workers

anguish

animal health emergency

animal infected with

annihilate

announcement by the justice department

anticoup demonstrators

anti-American sentiment

anti-foreigner sentiment

anti-foreigner violence

anti-government protesters

anti-human trafficking police

anti-immigrant

anti-immigrant marches

anti-Islam protests

anti-Islam protests turn violent

anti-Japanese sentiment

anti-money laundering

anti-Muslim attacks

anti-Muslim terrorism

anti-Nato activists

anti-Nato activists protest

anti-Semite incident (s)

anti-Semitism problem

anti-terrorism

anti-terrorism police

anti-US terrorism

anticipate a bombing attempt

antibiotic resistant bacteria

apartheid

apartment placed on lockdown

apprehensions decreased

apprehensions increased

AR-15 resistance

Arab (s) attack

Arab attack on

Arab riot

area of operation

areas for hiding

areas for hiding hostages

Arizona border defenders

Arizona border recon

armament

armament programs

armed assailant

armed assault

armed and dangerous

armed attack

armed combat

armed conflict

armed forces

armed forces deployed

armed groups

armed men broke into

armed robbers

armed drone

armed guards have been placed

armed insurgence

armed insurgency

armed man

armed rebel coalition

armed rebellion

armed rebels

armed robbery (ies)

armed standoff

armed suspect

armed terrorist (s)

armed woman

army checkpoint

Army Operations Special Command

armored vehicle

armory

arms

arms control

arms embargo

army

army prepares

army preparing

army preparing to

army thwarts attack

arrest

arrest made

arrest made after

arrested

arrested at airport

arrested in

arrested in connection with shooting death

arrested in connection with stabbing death

arrested in prostitution ring

arrested in raid

arrested in raids

arrested in shooting

arrested in shooting death

arrested on warrant (s)

arrested last week

arrests made abroad

arrivals by sea

arsenal

arsenal of homemade explosives

arson

arson attack

arson suspect

arson suspect arrested

arson suspected

arsonist arrested

arsonist detained

arson suspect arrested

artful dodger

artic blast

artillery

as a matter of policy

Asia – pacific security

asked for intelligence

assailants dressed in military uniforms

assassin

assassin weapons

assassinate

assassination

assassination attempt

assault

assault by

assaulted after

assess terrorist threats

assessment of terrorist threats

assistant secretary of state for diplomatic security

as night fell

asylum

seekers

asymmetric challenge

asymmetric warfare

asymmetrical warfare

asymmetrical weapons

ATF

ATF International Response Team

ATM cloning device (s)

ATM cloning device found at

atoll – geography

atrocity

at the time of the attack

automatic assault weapon

attack

attack against oil pipeline

attack by

attack by former lover

attack by x-lover

attack came from

attack from

attack imminent

attack occurred

attack (s) on American soil

attack on checkpoint in

attack on airline

attack on facilities

attack on tourist (s)

attack on U.S. soil

attack (ed) pedestrian (s)

attack plot

attack that occurred

attack took place in

attack with car bombs

attacked by

attacked while jogging

attacks in recent days

attacks in recent months

attacks in recent weeks

attacks increase

attacks (ed) NATO convoy

attacks on

attacks on foreign nationals

attacks on ISIS

attempted assassination

attempted coup

attempted terrorist attack

attempted to detonate

attempts to assassinate

attempting to sell stolen work

attempting to use a weapon of mass destruction

attrition

authenticity of the document

authoritarian regime

authorities

authorities cracked down

authorities have detected

authorities issued an alert

authorities not release the names

authorities raided homes

authorities reported

authorities said

authorities say

authorities seize

authority

automated web tools

automatic

autopsy results

aviation security

avoid detection

avoid law enforcement

B

baby death

baby’s death

backfire – f

background check

background notes

back off virus malware

backpackers

backpacker district

bacterial infections

bacterial outbreak

backup generator

backup systems failed

bagman (bagmen)

bag snatching

bail in

baits customer

bambi bucket – f

bandits in ski masks

bank bombing

bank heist

bank holiday

bank holdup

bank robbery

banking Trojan

banned all air traffic

banned from entering

banned all air traffic

banned from traveling

barrage

barrels packed with explosives

barricade – f

base – f

barricades outside U.S. embassy

battalion

battle

battlefield

beach closed

beach closure

beach contaminated

bear attack

bears killed

begins ground offensive

beheaded

behind attack

behind enemy lines

believed to be armed

belligerent

belt bomb

berm – f

betray

big data

bilateral accord

bilateral negotiations

bio weapon

biodiversity crime

biohazard

biological hazard

biological research

biological threats and terrorism

biological weapon (s)

biometric (s)

biometric information

biometric passport

biosecurity

bioterror threat

bioterrorism

bird flu

bird flu threat

bird smuggler (s)

bitcoin

black bag job

black bag operation

black bloc (civil unrest)

black box

black chamber – NSA

black dollar scam

black ice

blackline – f

black market

black market exploits

black money

black money scam

black hat conference

black market documents

black op

black operation

blackmail

black travel alert – Hong Kong Security Bureau

blanket the region

blasphemy police

blast

blasting time fuse

blindside

blistering agents

blizzard

blocking accounts

blog

bloodletting

bloodstain evidence

bloody

bloody assault

blow to the head

blow to the ribs

blowup – f

blow up a commuter train

board aircraft

boat capsized

boating accident

bodies found

bodies found in

bodies of missing

bodies recovered

body found

body found hidden in

boil water advisory

bomb

bombardment

bomb detonated in

bomb attack at

bomb attack in

bomb blast

bomb blast in

bomb blast injures

bomb blast kills

bomb defused

bomb exploded

bomb explodes

bomb explosion

bomb explosion in

bomb found

bomb found in

bomb making expert

bomb on board

bomb parts

bomb planted in

bomb plot

bomb plot thwarted

bomb rocks

bomb run

bomb scare

bomb shelter

bomb site

bomb squad

bomb threat

bombing

bombing campaign

bombing victim

bombings of US embassies

booby trap (s)

boost security

border

border aviation security

border clash

border closed

border closing

border conflict (s)

border control

border control agencies

border crime

border crisis

border crossing (s)

border difficulties

border force (UK)

border issues

border militarization

border policy

border reopens

border security

border skirmish

booster pump – f

booster reel – f

botched procedure

both gunmen

bought by terrorists

bouts of vomiting and diarrhea

bound with duct tape

bounty hunter

botched mission

botched rescue

botulism

brand protection

brazen attack

breach

breach of trust

breach protocol

break away faction

break encryption

bribery

bribery extortion

bribery of government officials

bribery scandal

bribery scheme

bride scam (s)

bridge collapse

bring the men to justice

brink of civil war

brink of war

British holiday maker

British tourist

British tourist killed

broke into

brown out

bruises to the head

brush fire

brush fire broke out

brush blade – f

brush hook – f

brush truck – f

brutal

brutal attack

brutal attack against

brutal attack by

brutality

brute force

brute force attack

building closure

building damaged

building destroyed

building evacuated

building explosion

building fire

buildings evacuated

building fire

buildings evacuated

bullet

bulletproof

bullets sprayed

bulletproof Kevlar gear

bump up – f

bunker

bureaucratic resistance

burn

burning

burning cars

burning flesh

burning index – f

burning period – f

burn out – f

bury

bus accident

bus assaulted

bus hijacked

bushfire – f

busiest intersections

bus plunge

bus plunged

bus plunges

bus plunges off cliff

business scam

C

C4 plastic explosive

cache

cache of illegal arms

cache of weapons

cadaver

cadaver bags

California Earthquake Authority

caliphate

call in a bomb squad

call to arms

called the attack a vindication

called the attacks a product of

camouflage

campaign

campus shooting

capital controls

capstun

cap-stun

captive

captor

capture

campus police

cancelled due to weather conditions

cancelled flights

cancelled passports

candle – f

capability to attack

CAPPS II – Computer Assisted Passenger Prescreening System II

car bomb attack

car bomb exploded

car bomb explodes

car bomb kills

car bombing attempt

car bombing kills

careen

cargo

cargo extortion

cargo disruption threats

cargo inspection (s)

cargo security

cargo thieves

cargo security threats

cargo threat

cargo train derailed

carjacking

carnage

carried out an order of

carrier

carry out an attack

carry out an attack against the United States

carry out another attack

carrying assault rifles

cartels smuggling

case reopened

cases of

cases of measles

cases of measles in

cases of police brutality

cases of polio

cases of syphilis

cases reported

cash courier

cash dealer

cash stolen

casualties

casualty figures

cataclysm

catastrophic

catastrophic accident (s)

catastrophic circumstances

catastrophic event (s)

catastrophic incidents

catastrophic insurance

catastrophic natural disaster

category A bioterror threat

cause and origin of fire

cause of death

cause of fire

cause serious injury or death

caused multiple traffic accidents

causes heavy damage to

causing complete service interruptions

cautionary

cautious

CBRN devices

CBRN weapons

CDC current outbreak list

CDC watch list

cease fire breach (es)

cease fire extended

cell

cell phone services went dark

Center for Disease Control (CDC)

center for security policy

CERT Coordination Center (CERT/CC)

chagas disease

chain reaction crash

chance of rain

change of tactics

changes in rainfall

changing sim cards

changing their locations

changing world

chaos

charge

charged in murder

charged in plot to murder

charged in shooting

charged with

charged with wire fraud

charged with money laundering

charging document

charged in court

charged in plot

charges of embezzling

charges of plotting

charges were filed

charred

charter flight (s)

charter flight (s) to get out

chat rooms

chatter

chatter indicating

Chechen female suicide bombers

checkered past

checkpoint (s)

checkpoint miles

checkpoint refusal

chemical facilities

chemical leak (s)

chemical plant fire

chemical time bomb

chemical warfare plant

chemical weapon (s)

chief of mission authority

chik virus

child abducted

child bomber

child predators

child suicide bombers

child traffickers

China dumping goods

China plays a role

China’s intelligence service

China’s military strategy

Chinese aggression

Chinese cyber campaign

Chinese foreign minister

Chinese hackers

Chinese hacking group

Chinese military movement

Chinese spies

chopper

chopper crashed

chronic disease (s)

church shooting

CI center – think thank

CI team (competitive intelligence)

CIA internal review

CIA operated drone program

CIF – cyber threat intelligence management system

citing law enforcement sources

citizen

citizen weather observer program

citizens not directly involved in a civil disorder

citizens traveling to or residing in

citywide lockdown

civil aviation

civil aviation authorities

civil disorder

civil disturbance

civil liberties

civil resistance

civil rights

civil rights violation

civil strife

civil unrest

civil unrest, riots

civilian abducted

civilian airspace

civilian border patrols

civilian deaths

civilian population

civilian drone incidents

civilian drones

civilian target (s)

claims life

claims more than 100 lives

claims more than 1000 lives

claims responsibility for attack

claims responsibility for bomb attack

clamor

clandestine

clandestine airfields

clandestine airstrip

clandestine attack

clandestine brothel

clandestine electronic surveillance

clandestine grave (s)

clandestine laboratory

clandestine means

clandestine migration

clandestine military base

clandestine military operations

clandestine sources

clandestine U.S. Army camp

clandestine warfare

clans

clash

clashes

clashes in

clashes occurring in

classified document (s)

classified information

classified leak

classified leak of information

clean operative

clear and present danger

clear evidence

clear landmine (s)

clear threat

clear warning

climate change

climate engineering

Climatic Prediction Center (CPC)

climber dies

climber killed

climbing accident (s)

climbing death (s)

clogged checkpoints

close to the border

close to the entrance

closed area

closed border

closer to military action

closure extended

closure extended embassy

closure extended consulate

closure extended road (s)

cluster bomb (s)

coalition

coast guard reported

coastal flooding

coastal town

cobalt – 60

cocaine

cocaine coast India

cocaine found hidden in

cockpit smoke

cockpit voice recorder

code word (s)

cold case murder

cold wave

cold weather operations

cold weather temperatures

collapse

collect intelligence

collective intelligence framework

combat

combat crime

combat Islamic extremism

combat terrorism

combat zone deployment

combined forces

command

command center

commando forces

commando forces India

commandos

commercial flights have been temporarily suspended

commit violent crime (s)

commit wire fraud

common border

communal riots

communication channels

communication (s) interception

communication node

compact missile launcher

complaint states

complex bombed

compliance – terrorism

components of risk

comprehensive plan

compromised operations

compromised server

compromised target (s)

computer and identity fraud

Computer Assisted Passenger Prescreening System (CAPPS) (counter-terrorism)

conceal

computer glitch

concealed in

concealment techniques

concentration

concentration of camps

concentration of troops

concussion

condemn all acts of terrorism

condemned attack

condemned terrorist attack

conditional threat – crime

conduct covert operation (s)

conduct Jihad

conduct surveillance

conducted an operation

conducted raids

confidential report

confine a fire

confirmed case

confirmed case of

confirmed cases

confirmed report (s)

conflict

conflict between

conflict could spark

conflict zone

conflict zone widening

conflicts across the Middle East

confrontation

confrontational

confusion

conquer

consequences

conservation cuts

consolidate

conspiracy

conspiracy theory (ies)

conspire

constitute a danger

consular database

consumption girl scam

control measures

consulate

consulate attack

consulate attacked

consulate attacked in

consulate bombed

consulate bombings

consulate closed

consulate closure (s)

consulate evacuated

consulate threat

consumer defrauded

contacted authorities

contagion

contagious bioweapons

contagious bioweapons research

contagious viruses

contain a fire

contaminated

contaminated drinking water

contaminated food

contaminated water

contested areas

contingency plan

continuity of government (COG)

continued attack (s)

continued gunfire

continued to monitor activities

continued violence in

contraband

contraband alcohol

contract assassination (s)

control

control line – f

controlled burn – f

conventional explosive

conventional munitions

conventional military forces

conventional weapons

convoy

coordinate (s)

coordinated attacks

cops

cops injured

cordoned off a large area

corona virus

coroner says

coroner’s autopsy

corporate espionage

corporate fraud

corporate tax haven

corps

corpse

corruption

corruption is widespread

corrupt officials

counterattack

counterattack launched

counterfeit

counterfeit alcohol

counterfeit and illegal goods

counterfeit and illegal products

counterfeit baseball caps

counterfeit cigarettes

counterfeit clothes

counterfeit consumer products

counterfeit currency

counterfeit detection

counterfeit document

counterfeit drugs

counterfeit DVD’s

counterfeit golf merchandise

counterfeit goods

counterfeit goods from China

counterfeit goods seized

counterfeit insulin

counterfeit insulin needles

counterfeit insulin pens

counterfeit medications

counterfeit merchandise

counterfeit money

counterfeit movies

counterfeit purses

counterfeit toys

counterfeit watches

countermand (m)

counter messaging

counterpart

counterparts

counter extremism

counter narcotics efforts

counter-piracy (shipping)

counter piracy mission

counter surveillance techniques

counter terrorism

counter terrorism database

counter terrorism effort

counter terrorism programs

counter terrorist operation

counter terrorist police

country sponsored terrorism

coup attempt

coup attempt failed

coup d’ état

coup plotters

coup plotters arrested

coup risk

courageous

courier

court records indicate

court records sealed

court shooting

coveOps - military

cover-up

cover-up probe

covert action

covert activities

covert activity

covert battlefield

covert communications

covert military action

covert operation (s)

covert ops

covert relations

covert tradecraft

covertly

coyote tactics

craigslist scam (s)

crash

crash involving

crash remains under investigation

crash scene

crash shuts down highway

crashed into

crashed into a vehicle

CRE – infection  – Carbapenem resistant Enterobacteriaceae

credible

credible witness

credible eyewitness

credible fear application – immigration request U.S.

credible report (s)

credible sighting (s)

credible source

credible threat

credible threats

credibility of alert (s)

creeping fire –f

crew (s) fight fire

crime

crime activity

crime against humanity

crime and violence are serious problems

crime blotter

crime decrease

crime increase

crime lord

crime lord arrested

crime prevention

crime record

crime related

crime ridden

crime scene

crime scene investigation

crime statistics

crime trend (s)

crimes unreported

crimeware

criminal activity (ies)

criminal charges filed

criminal charges pending

criminal corruption charges

criminal crime scene

criminal cyber-security intrusion

criminal endeavor

criminal enforcement actions

criminal gangs

criminal investigation

criminal investigation continues

criminal investigations section (CIS)

criminal scam (s)

criminal software

criminal mischief

criminal network (s)

criminal prosecution (s)

criminal record (s)

crisis

crisis at the border

crisis averted

crisis continues

crisis mapping

crisis negotiation unit (FBI)

crisis negotiator

critical

critical condition

critical electric-grid sites

critical emergency

critical facilities

critical infrastructure

critical services

critical situation

critical stage

critically hurt

crop failure

cross-border attack

cross-border tensions

cross border terrorism

cross-hairs

crowd

crowd control

crowd of people

crowd seeding

crowd sourcing

crown out – f

cryptanalysis

cryptic currency

culpability

cultural threat

culture of terrorism

curb illegal activity

current cyber threat

current information suggests

current international crisis

current international law

current international news

current outbreak

current security situation

current security threat (s)

current security threat (s) and pattern (s)

current threat level (s)

currently being detained

currently detained

customs officer seize

customs seize

cut off water supply

cut with a hacksaw

cyanide poisoning

cyber attack

cyber attack bank

cyber attack China

cyber attack government

cyber attack North Korea

cyber attack power grid

cyber attackers

cyber – bullied

cyber capabilities

cyber command

cyber crime

cyber espionage

cyber exploitation

cyber incident (s)

cyber intrusion

cyber mercenary

cyber mercenaries

cyber security

cyber security and electronic terrorism

cyber security breach

cyber security intrusion

cyber security mission

cyber security risk

cyber security strategy

cyber security worries grow

cyber spying campaign

cyber surveillance

cyber targeting

cyber terrorism

cyber threat (s)

cyber tool (s)

cyber war

cyber warfare

cyber weapon

cybercriminal ring

cyclospora

cyclospora outbreak

cylinder bomb (s)

D

dabiq magazine (ISIS magazine)

Daesh – terrorist group ISIS

damage

danger

dangerous

dangerous behavior

dangerous encounters

dangerous foreign policy

dangerous situation

dangerously close

dark world

darknet

DARPA

data breach

data breach update

data compromised

data from studies show

data mining program

data mining techniques

data monitoring capability

day after the attack

daytime assault

DDOS services

decision making authority

dead

dead after clash between

dead at a local hospital

dead bodies

dead child

dead children

dead in car

dead in home

dead man

dead man zone - f

dead men

dead out – f

dead people

dead persons

dead woman

dead women

deadly

deadly attack (s)

deadly bacterium

deadly blast

deadly campaign

deadly casualties

deadly disease

deadly explosion

deadly flood (s)

deadly flooding

deadly ice storms

deadly infectious disease

deadly storm (s)

deadly unrest

deadly weather

deadly tractor-trailer crash

deadly training missions

deadly tropical storm

death

death list

death on cruise ship

death threat

death threat (s)

death toll

deaths

death (s) to livestock

death (s) to wildlife

decentralized terrorist movement

deception

deception practices

decision making authority

declaration of holy war

declare war

declared emergency

declared insane by the courts

declared intent

declared outbreak

declared state of emergency

declares state of emergency

declassified

declassified email (s)

declassified information

declined comment

declined to speak publicly

deep in the jungle

deep packet inspection

deep penetration agents

deepens ties

defeat counterterrorism measures

defense capabilities

defense intelligence agency (DIA)

defer non-essential travel

deferred prosecution

degrade and destroy

deliberate brushfire (s)

deliberate damage

delicate talks

delivering nuclear weapons

demand for ivory

demand for water

demob – f

demobilization

demolish a terrorist organization

demonstration (s)

demonstration (s) against

demonstration (s) against the coup

dengue

dengue fever

dengue outbreak

denial of service attack

denied persons list

dense rainforest – ivory

department of defense

department of defense report

deploy

deploy (ed) troops

deployed in conjunction

deployed soldiers

deployment

deployment of military

deployment of police

deployment of troops

deployment of soldiers

deployment of weapons of mass destruction

deportation proceedings

destabilization

destabilize a region

destabilize government

detailed information

detect threat (s)

detention hearing

detailed chronology

detailed information

detained by authorities

detained by police

detained in

detect IED

detect terrorist activity

detection of

detection of crime

detectives are continuing to investigate

deteriorating security situation

detonate (d)

detonate a bomb

detonate an explosive device

detonated suicide bombs

devaluation of currency

devastating attack

devastating attacks

devastating disaster

devastating mudslide

devastating storm (s)

device discovered in

device exploded

device found

device found in

device was discovered in

DHS – Department of Homeland Security

DHS assets

DHS blue program (human trafficking)

DIA annual threat assessment

diagonosed with the disease

did not detonate

died as a result of injuries

died at the scene

died at the scene of the accident

died in the attack

died from complications

died suddenly

dies after

dies after near drowning in

dies after nearly drowning

dies in custody

dies in police custody

digital documentation

digital surveillance

digital wiretap

diplomatic asset

diplomatic efforts

diplomatic immunity

diplomatic incident

diplomatic mission

diplomatic post

diplomatic security

dire threat

direct attack

direct hit

direct military action

direct threat – crime

dirty bomb

dirty war

disability fraud

disability scam

disable communications

disaster (s)

disaster assistance

disaster management

disaster medical assistance team (DMAT)

disaster response

disaster response coordination

disaster response team

disaster unemployment assistance

disease control

disease outbreak

disguised as

dispatch from

dispatched from

dispatched marines

dispatched troops

disrupted travel

disrupted travel for commuters

disappeared

disappearance

disaster (s)

disaster assistance

disaster mitigation

disaster plan

disaster relief

disasters man made or natural

discovered evidence

disputed border

disputed waters

disrupt terrorist plans

disrupted

dissident (s)

diversion

diversion tactics

diversionary tactics

diving accident

DNA hacking

DNDO (Domestic Nuclear Detection Office)

do not board list

do not fly list

document fraud

DOD statement

dog attack (ed)

dogs attack pedestrian (s)

doggy door burglar

domestic intelligence

domestic nuclear detection

domestic security

domestic spies

domestic terrorism

domestic terrorist attacks

domestic threat

domestic violence

domestic violence death (s)

domestic violence increase

domino effect

doomed flight

door breach

dork query

dormant operative

downing of drone

dozens arrested in

dozens arrested during

dozens detained

dozens hurt

dozens injured

dozens killed

dozer line – f

dragnet included

dramatic increase in

drawdown

dressed in black tactical gear

drew record numbers

DRIDEX – malware

drink spiking

drip torch – f

drive-by shooting

driven the unemployment rate

drivers fatigue

drone attack

drone base

drone hacking

drone shot down

drone sighting

drone sightings

drone strike (s)

drone strikes in

drones crashed

drought

drowns in boating accident

drug bust

drug corridor

drug (s) found hidden in

drug related violence

drug resistant bacteria

drug shortages

drug trafficking

drug trafficking hub

drug trafficking maps

drug trafficking organizations

drug trafficking route (s)

drug trafficking route (s) Africa

drug trafficking route (s) Asia

dual mandates

duff – f

during a news conference

during a riot

during the search

Dutch police arrested

dynamite





E

early warning

early warning signs

early warning systems

earthquake

earthquake damage

earthquake magnitude

earthquake shakes

earthquake swarm (s)

easily spread

eavesdropping

Ebola watch list

echelon system

eco terrorism

ecocide

ecological risk

economic chaos

economic crisis

economic disruption

economic event

economic stagnation

economic threat

ecoterrorism

e-currency

effect on agriculture

effect on population

electric grid

electric grid vulnerable

electric grid vulnerability

electric outage

electric substation

electrical substation

electromagnetic energy

electronic barrier

electronic consignment security declaration (eCSD)

electronic surveillance

electronic warfare systems

elevated alert level

elevated alert

eliminate al-Qaeda

elite fighting squads

elite paramilitary unit

elite units

elude authorities

email (s)

emails hacked

emails indicate

emails reveal (ed)

embassy

embassy attacked

embassy attacked in

embassy bombing (s)

embassy breach (ed)

embassy closed

embassy closure (s)

embassy evacuated

embassy halts service

embassy has received unconfirmed threat (s)

embassy implemented enhanced security

embassy shuts down

embassy threat

embassy warning (s)

emergence of small groups

emergency action

emergency alert (s)

emergency and disaster information

emergency broadcast system

emergency command

emergency command post

emergency declared

emergency landing

emergency management

emergency phone number for the U.S. embassy

emergency phone number for the U.S. consulate

emergency preparedness

emergency protest

emergency protocol (s)

emergency relief

emergency report

emergency rescue

emergency response

emergency response team

emergency services

emergency services say

emergency session (s)

emergency warning (s)

emerging deadly virus (es)

emerging disease

emerging economy

emerging infection (s)

emerging infectious diseases

emerging new state

emerging risks

emerging security threat

emerging threat

emerging threats

encased in metal piping

encrypt

encryption

encryption techniques

endangered species act

endemic

ends military operation in

enemy combatant (s)

enemy’s vulnerabilities

engage in

engine – f

engine crew – f

entered the airport

entered the building

entered the hotel

entered locked area

entered secure area

entered with fake passport (s)

enticed child

enticed girl

environmental emergency

environmental terrorism

epidemic

epidemic diseases

epidemic of violence

epidemiological bulletin

equatorial rainforests – ivory

equipment failure alarm

eradication efforts

erosion

errant bomb

erroneously reported

escalate threat (s)

escape (d)

escape (d) – fire

escaped convict

escaped inmate

ethical breach

ethnic and religious tension

ethnic cleansing

European airports closed

Europol

euros seized

evacuated

evacuated after threat

evacuation (s)

evacuation center

evacuation flight (s)

evacuation (s) ordered

evacuation route (s)

evade authorities

evade police

evade tracking

event cancelled

event occurred in

everyman for himself

execution carried out

exercise caution

exile (s)

exiled groups

existing military pacts

exists in

exotic pet smuggling

expanded recalls

expatriate (s)(expat)

expatriate (s)(expat) arrested

expatriate (s)(expat) detained

expatriate (s)(expat) killed

expatriate (s)(expat) missing

expatriate (s)(expat) murdered

expatriate (s)(expat) raped

expatriate (s)(expat) robbed

expel militants

experience delays

experts warn of potential disaster

exploding global population

exploit data links

explosion

explosion in

explosion near mosque (s)

explosion occurred

explosion outside consulate

explosion outside embassy

explosion outside U.S. consulate

explosion outside U.S. embassy

explosion proof

explosion reported in

explosion shakes

explosive (s)

explosive breach

explosive charge

explosive detective engineering

explosive device

explosive ordnance

explosive precursors

explosive storage magazines

explosive weapon (s)

explosives stolen

explosives threat

explosives went off near

expressed misgivings

extended attack – f

extends ban on

extends ban on guns

extends ban on sale of

extensive investigation

external subversion

extortion

extortion scam

extra security measures

extracting information

extreme drought (s)

extreme far-right movement (s)

extreme violence

extreme weather

extreme weather events

extremism

extremist (s)

extremist exploitation

extremist forum

extremist group

extremist regime of the Taliban

F

FAA investigates plane crash

FAA issued warning

FAA issues warning

Facebook scam

face terror charges

face-to-face combat

faces charges

faces extradition

faces murder charge (s)

facilitate information sharing

facilitator

fact finding mission

factories shut down

failed coup attempt

failed mission

failed state

failed suicide attack

fake birth certificate

fake bomb

fake degree

fake document (s)

fake driving licence (UK)

fake drugs

fake goods

fake id

fake id’s

fake IRS agents

fake logos

fake medicines

fake merchandise

fake passport (s)

fake passport industry

fake pharmaceuticals

fake websites

false bomb threat

false documents

false flag attack (s)

false flag (covert operation)

false imprisonment

false narrative

family influenced violent extremist

famine

fascist idealism

fastest growing crimes

fastest immigrant population

fatal attack

fatal bear attacks

fatal crash

fatal police shooting (s)

fatal shot

fatalities

fatality

fatally injured

fatally shot

fatally shot by

fatally stabbed

fatally stabbed by

father charged in

FBI

FBI agents arrested

FBI alert

FBI cywatch

fears of more attacks

felony computer abuse and conspiracy

Ferguson effect

fertilizer

fertilizer bomb

fetal homicide

fewer causalities

fight against terrorism

fight between

fight broke out

fight with militant group

fighters ambushed

fighters from

fighting force

filariasis

file charges against the suspect

financial backer of al-Qaeda operations

financial crime (s)

financial crisis

financial fraud

financial intelligence units

financial threat

fire

fire at chemical plant

fire behavior

fire bomb (firebomb)

fire broke-out

fire camp

fire cycle

fire danger

fire department officials say

fire destroyed a

fire displaces

fire ecology

fire edge

fire fighting foam

fire in

fire in plant

fire is under control

fire lookout

fire lookout tower

fire officials suspected

fire risk

fire rockets

fire shelter

fire starters

fire threat

fire trail

firebombs

firebreak

fire line

fire storm

fired three shots into the air

fired shots into the air

first responder

first responder network

first warning

flanks of a fire

flare mules (UK)

flare up

flash flood warning

flash flood watch

flash fuels – f

flashover – f

flawed decision

flawed intelligence

fleeing in

fleeing internal violence

flight (s)

flight (s) between

flight cancellation (s)

flight (s) cancelled

flight conditions

flight data recorder

flight delay

flight delays

flight diverted

flight forced to

flight made emergency landing

flight makes emergency landing

flight plan filed

flight seat reduction

flights added

flood (s)

flood barrier

flood barrier at risk

flood damage

flood radar

flood stressed

flood threat continues

flood toll rises

flood zone

flooding

flooding in

floods in

floods kill

flow of drug money

flu deaths

flu outbreak (s)

foia request

foiled attack

fog of war

foggy weather

following heavy flooding

following heavy flooding and rains

following heavy rains

following the coup

food airlifts

food borne disease outbreak

food borne illness

food defense

food insecurity

food poisoning

food security

food shortage

food source

food supply

food supply risk

food tampering

food terrorism

foot soldier

force concentration – m

force deployment

force protection level

forced emergency landing

forced landing

forced work stoppage

forces evacuation

forces shelled

foreclosure fraud

foreign affairs spokesman

foreign animal disease

foreign conflict

Foreign Corrupt Practices Act

Foreign Emergency Support Team (FEST)

foreign fighters

foreign insurgent

foreign intelligence operative

Foreign Intelligence Surveillance Act (FISA)

foreign intelligence surveillance court

foreign military sales

foreign military sales program (programme)

foreign national charged with

foreign rescue workers

foreign spies

foreign terrorist organization

foreign threat

foreign tourists

foreign trade zones

foreign troops targeted

forensic evidence

forensic information

forensic investigator

forensic investigation

forensic psychiatry

forest elephants – ivory poaching

forest fire

forged check

forged document (s)

forged passport

forger

former agents charged

former detective

former employee charged with

former gang member

former terrorist

found bound and gagged

found alive

found at

found deceased

found dead

found dead in car

found guilty

found guilty on all charges

found hidden in

found in food

found stabbed

found stabbed in home

found stabbed to death

frantic effort (s)

fraud

fraud committed

fraud detection

fraud investigation scam

fraud misconduct

fraud monitoring

fraud prevention unit

fraud warning

fraudster

fraudulent

fraudulent identity

fraudulent tax return (s)

fraudulently obtained documents

freely elected president

freight theft

French gendarmes

French security forces

frequent gathering point

freezing rain

friendly fire

friendly fire incident

frightened tourist (s)

fringe group (s)

front companies

frost quake

fuel anger and rage

fueled rumors fugitive operating

full alert

full investigation

full scope investigation with polygraphs

full tactical gear

fundraising terrorism

fungal outbreak

funny money

further escalation

further ideological objectives

further political objectives

further religious objectives

further their cause

future unemployment

G

gain access to weapons

gain surprise

gang hideout

gang’s hideout

gang intelligence team

gang leaders

gang of looters

gang of thieves

gang related shooting (s)

gangs

gas explosion

gas leak

gas like odor

gas line explosion

gasoline shortage

gastrointestinal outbreak

gathering threat

gay arrest

gender violence

general aviation

genetic fingerprints

Geneva report

geofencing software (computers)

geographic data

geographic diffusion

geographic distance

geomapping

geopolitical

geopolitical climate

geopolitical upheaval

German Muslim community

Germanwings crash

geo hazard

ghostnet – China

glitch

global conflict (s)

global counter terrorism

global distribution of food supplies

global food system

global hotspot

Global Maritime Distress and Safety System (GMDSS)

global report on

global report on trafficking in persons

global spread

global surveillance

global terrorist network

global war on terrorism

global warming

gold heist

google dorking

govern by decree

government approved keywords

government backed militias

government code name

government officials confirmed

government opponents

government spokesman

government spokesperson

government spokeswoman

government unprepared for

government’s star witness

GPS tracking devices

grabbed clerk

graft

grave robber (s)

grave robbing

grave threat

gravity of the situation

greatest threat

green on blue attack

green zone

grenade launched

grenade like explosive

grey market

ground fire

ground incursion

ground invasion

ground water

ground zero

grounded flight (s)

group of armed men

group responsible

group was attacked

growing black market

growing civil unrest

growing concern

growing concerns over increase in

growing fears

growing in strength

growing international isolation

growing Islamist extremism

growing movement

growing problem (s)

growing tension (s)

growing tension (s) between

growing threat

growing threat increasing

growing trend

growth of crime

GSG 9

Guantanamo cocktail

guards have found

guerrilla forces

guerrillas bombed

guerrillas killed

gun and bomb attacks

gun battle

gun battles have occurred

gun crime

gun shootout

gun violence

gun violence is on the rise

gun violence is soaring

gunfire

gunfire erupted

gunfire heard

gunman

gunmen

gunmen riding on motorcycles

gunmen were shot

gunmen were shot to death

gunmen stormed

gunned down

gunshot wounds

gunshot wounds to

gunshot wounds to the head

H

H1NN flu – swine flu

H6N1 flu

haboob

hack attack

hacker

hacker (China)

hacking

hacking attack (s)

hacking attempt (s)

hacking ring

hacking services

hacking threat

hactivism

had obvious signs of trauma

hail

hail storm

halt flights

Hamas attacks

Hamas flags

hand grenade (s)

hand grenade (s) found

handed over to police

hard ivory

hard-line regime

hard sell

harmful goods

harvest down

harvest up

has been arrested

has been detained

has not been seen since

has warned

hate group

haunted tourism

have been arrested

haven for criminals

have been identified

have yet to be identified

have yet to identify

have yet to identify the body

haven for Islamic terrorists

Hawala

Hawala banking

hazard (s)

hazard identification

hazard reduction (f)

hazard warning

hazardous material (s)

HAZMAT

HAZMAT crews

HAZMAT crews respond

hazardous materials response team

he was searched

she was searched

they were searched

head of a fire

health care fraud

health crisis

health hazard

health impact assessment

health ministry

health officials report

heat wave

heavier than normal security

heavily armed

heavily armed men

heavily armed ships

heavily armed soldiers

heavily armed

heavily fortified

heavy gunfire

heavy rain

heavy snow

heavy storms

heavy storms swept through the area

heightened security concerns

heist

held captive

held captive in

held in robbery

held in shooting

helibase

helicopter downed by

helicopter crashed

helicopter drop point

her attacker

heroin mixed with fentanyl

heroin mixed with gasoline

heroin trade

Hezbollah

Hezbollah operative

Hezbollah planning attacks

Hezbollah planning attacks on western targets

Hezbollah terror cell (s)

Hizb’ allah

Hizballah

Hizbollah

hid on rooftop (s)

hide evidence of wrongdoing

hidden in

hidden in baggage

hidden luggage

hidden in metal drums

hidden services

hideout

hiding secrets in open

high bacteria counts in the water

high death rate

high level authorities

high level of alert

high level (s) of crime

high powered machine guns

high ranking al-Qaeda official

high readiness force (HRF)

high risk

high risk area

high risk traffic stop

high risk traveler

high standing water

high tech killings – drones

high threat

high threat post (s)

high value individuals

high value target

high winds

high winds, snow, and ice

higher homicide rate than

highest level of government

highest level of travel warning

hijacked a bus

hijacked truck

hiker

hiker missing

hiker rescued

historic district

historic talks

hit and run accident

hit and run operations

hit by another vehicle

hitlerite (s)

hitting public services

HIV outbreak

HIV pandemic

hold culprits accountable

hold secret meetings

holding hostage

holding inmates hostage

holding patients hostage

holiday makers (UK)

home grown Jihadist

homegrown extremist (s)

homegrown ISIL attack (s)

homegrown ISIS attack (s)

homegrown militant group (s)

home-grown terrorist

homegrown terrorist

homegrown violence

homegrown violent extremist

home invasion

Homeland Security

Homeland Security Advisory System

homeland threat

homemade bomb (s)

homemade chemical bomb incident (s)

homemade grenade

homicide rate

homicide victim (s)

honor brigade

horrendous conditions

host country

host government

hostage beheaded

hostage crisis

hostage killed

hostage negotiation

hostage negotiation team

hostage rescue situation

hostage (s) rescued

hostage situation

hostage standoff

hostage taking

hostile actions

hostile environment

hostile groups

hostilities

hot car death

hot spot – f

hotel bombed

hotel evacuated

hotel targeted (s)

hotshot crews – f

house by house search

housing shortage

huge explosion

huge fire

huge population growth

human cargo

human dumping ground

human rights

human rights abuses

human rights violation (s)

human sewage

human smuggling routes

human to animal

human to human

human trafficking

human trafficking hotline

human trafficking intervention court (s)

human trafficking ring

humanitarian aid

humanitarian threat

hundreds displaced

hundreds flee

hundreds infected

hundreds of casualties

hundreds of civilians

hundreds of civilian casualties

hundreds of families

hundreds of individuals

hundreds of people

hundreds of thousands of people

hundreds of tourists

hunger strike

hurricane

hurricane alert

hurricane deaths

hurricane surge

hurt critically

hurt seriously

hydro meteorological hazards

I

ice snow wind threaten power

icy road (s)

ideologue

identity of suspect by video

identify suspect

identify terrorists attempting to buy

identity of terrorist (s)

identity fraud

identity theft

identifying suspicious transactions

ideological clashes

ignited concerns

ignition and fuel sources of the fire

ill-fated flight (s)

illegal activities

illegal armed group (s)

illegal border crime

illegal border crossing (s)

illegal documents

illegal drones

illegal drug trade

illegal drug trade in

illegal immigration network (s)

illegal leaks

illegal logging

illegal mining

illegal reentry

illegal spying

illegal surveillance

illegal tax evasion

illegal trade

illegal trade of goods

illegal transaction (s)

illegal taps

illegal wildlife trade

illegally entered

illicit arms trade

illicit drugs

illicit compounds

illicit financial flows

illicit goods

illicit medicine (s)

illicit proceeds

illicit small arms light weapons trade

immigrant population

immigration checkpoint

immigration fugitive

immigration office (s)

immigration officer (s)

immigration scam

imminent attack

imminent danger

imminent threat

immune system

impending disaster

import export pricing fraud

imposed economic measures against

imposed martial law

imposed sanctions

imposter

improper flight operations

improve security

improvised explosive device (s)

improvised explosive material (s)

improvised weapon (s)

in a news release

in custody death

in-flight emergency

in harms way

in recent days

in recent months

in recent years

in separate locations

in serious condition

in the months prior to the attack

in the same period

incidence

incidence of stabbings

incident

incident command system(ICS) f

incident is under investigation

incident occurred on

incident raising concerns

incident site

incidents in

incidence of civil disorder

incident response

incidents of violence

inclement weather

including diplomats

including foreigners

including tourists

increase in

increase in attacks

increase in crime

increase detection (s)

increase in illness (es)

increase in security

increase in such attacks

increase risk (s)

increase of risk (s)

increased military aid

increased military presence

increased policing

increased push by law enforcement

increased Russian military activity

increased security

increased terrorism threat

increased vigilance

increasing tensions between the two countries

indicators

indirect threat – crime

industrial disaster

industrial espionage

industrial pollution

infection

infection and transmission

infection rate high

infection rate low

infectious disease (s)

infiltrate

infiltrated

infiltrator tradecraft

influenza

influenza outbreak (s)

influenza surveillance report

information extraction

initial attack

informal financial network

informal trade

information of sharing

information sharing

infrared detector – f

infrastructure attack

infrastructure of terrorism

initiative

initiation of hostilities

injured

injured after

injured after attack

injured by gunfire

injured by turbulence

injured climber dies

injured climber killed

injured in accident

injured in bomb explosion

injured boy

injured child

injured girl

injured man

injured person

injured while on vacation

injured woman

injured were taken

injured while fleeing

injuries on the body

injuries on the back of the body

injuries on the front of the body

injuries on the side of the body

injuries recorded

injury sustained

injury to the head

injury to the skull

in-Q-tel – Venture arm of CIA

inquest

inside job

insider threats

institutional vandalism

insurgency

insurrection

intelligence agency mossad

intelligence agencies

intelligence brief

intelligence briefing

intelligence bulletin

intelligence collection

intelligence community

intelligence complex

intelligence database

intelligence failures

intelligence interrogation

intelligence operative (s)

intelligence partners

intelligence received

intelligence report

intelligence sharing

intelligence target

intelligence threat facing

intense conflict

intense fighting

intense storms

intent of group

intent to hurt

intercept calls

intercept data

intercept of group

intercept phone calls

intercepted phone calls

interface zone – f

internal conflict

internal corporate sabotage

international alert system

international border

international sanctions

international shark attack file

international warrant

internet attacks

internet predators

internet services went dark

Interpol

Interpol alert

Interpol arrest

interstate closed

interstate closed for several hours

intifada (uprising)

intrusion routed through computers

intrusive

invasion

invasive

invasive species crime

invasive species

investigate (s)

investigate (s) contaminated water

investigate (s) crime

investigate (s) murder

investigate terrorist activity

investigating a suspicious device

investigation activities

investigation launched

investigation underway

investigative committee

investigative journalists

investigators reported their findings

involved in a crash

involved in the crash

ivory

IRA factions

Iran supports

Iran supports Hamas

Iran supports Hezbollah

Iran supports ISIS

Iran supports terrorism

Iran threat assessment

Iran warns

Iran’s military aid

Iranian covert activities

Iranian covert operations

Iranian leaders

Irish travelers

irregular arrivals

irregular migrants

irregular migration

irregular warfare

is described as

IS

IS forces

IS threat

ISIL

ISIS activity

ISIS affiliates

ISIS bombs

ISIS claims responsibility

ISIS gains ground

ISIS gains ground in

ISIS Islamic State

ISIS kill list

ISIS organization

ISIS threat

Islamic Courts

Islamic extremism

Islamic extremist activity

Islamic militant movement

Islamic militant (s)

Islamic State (ISIS)

Islamic state hacking division

Islamic state group

Islamic state militant group

Islamic suicide attack

Islamic terrorism

Islamist

Islamist faction

Islamist groups

Islamist insurgency

Islamist leaning separatist rebel

Islamist militancy

Islamist target

Islamophobia

isolated area (s)

isolated state

Israel bus attack

Israeli prime minister

issue an alert

issue came to a head

issued distress call

issues alert

issues warning

items seized by

ivory seized

ivory to bribe local officials

ivory smugglers

ivory trafficking

J

jail

jailed

jailed for fraud

Japanese tourist injured

Japanese tourist killed

Japanese tourist missing

jelly fish sting

Jerusalem attack

jewelry heist

Jewish targets

jets entering service

Jihadi attack

Jihadi organization

Jihadist

Jihadist cleric

Jihadist insurgents

Jihadist networks

Jihadist networks in

Jihadist rhetoric

Jihadist websites

joint investigation

journalist arrested

journalist detained

journalist freed

journalist injured

journalist kidnapped

journalist killed

journalist missing

journalist released

journalist reported

JPATS – U.S. Marshals

jumped from balcony

jury convicts

jury duty scam

K

kamikaze attack

kamikaze drone

kayaking accident

ketamine

keylogger

key transit point

kidnap

kidnap for ransom group

kidnapped

kidnapped for ransom

kidnapped victim released

kidnapping (s)

kidnapping Europeans

kidnapping has occurred

kidnapping westerners

kill (ed)

kill hundreds of thousands of people

kill list

killed

killed after

killed after militant attack

killed by police

killed in

killed in air strike

killed in collision

killed in a police shootout

killed in gunfight

killed in shooting

killed in shootout

killed passengers

killed people

killed people worldwide

killing all aboard

kills _ percent of those infected

knapsack bomb (s)

knife attack

knife crime

knife violence

knock down fire

knocked out substation

knockoffs

knockout game

knowledge of border area

known militants

L

laced with

laced with poison

lacerations

lacerations to the body

lacerations to the throat

lack of threats

laid ambush

land mine

large area

large area of the country

large area of the state

large fire

large number of

large number of cases

large number of soldiers

large-scale fire

large seizures

laser guided bomb

laser incident

laser pointer attack

latest attack (s)

latest development

latest threat

latest update

launch (ed) attack

launch explosives

launch tube

launched a military raid

launched ground offensive

launched investigation

launched murder investigation

launches air strikes in (air strikes)

launches ground offensive

launches new efforts

laundering money

laundering the proceeds

law enforcement

law enforcement agency (ies)

law enforcement target (s)

law of the sea

lay in wait for police

lead to civil unrest

leading cause of

leading to insecurity

leaked cable

leaked data

leaked document

leaked documents

leaked emails

leaked official document

leaked official documents

leaked report

Lebanon clan

Lebanese Venezuelan clan

led a chase

left at

left for dead

leftist rebel group

let burn policy

let guard down

lethality in terrorist attacks

letter bomb

levee breached

license plate tracking program

life threatening

lift barriers

light rain

light weapons

lightening

lightning

lighting sparked fire in

lighting storm

lily pad strategy (military)

line was cut

link between

linked to

linked to attack

linked to crime

linked to the murder

linked to trafficking drugs

listed as threatened (ivory)

listed passengers

loaded with explosives

loan fraud

loan scheme

local hostilities

local media

local media reported

local sentiment

local trafficker

located in an area

location

location information cell phone

locations of informants

lock down

locked down (lockdown)

logging debris – f

logging slash – f

logos used by insurgents, terrorists, paramilitary groups

London terror attack

lone terrorist

lone wolf attack (s)

lone wolf terrorists

lone wolves

long term outage

long term threat

lookouts – f

loose coalitions

looted from

looters

looting

loss/theft report

lost phone service

loud explosion (s)

love scam

low flying planes

low-intensity conflict

low level protection

low-lying dam

lured the victim

M

machete attack

machete attack computer

made an emergency landing

mail

mail bomb

mail stolen

main security threat

main security threats

main suspects

major disaster declared

major disaster declaration

major donor

major drug bust

major incident

major outage (s)

major risk (s)

major rockslide

major threat (s)

major transit route

major transit route for

major transit route for migrants

major transit route for people smuggling

major transit route for smuggling

majority of cases imported

make alternate travel arrangements

malaria outbreak

malaria deaths

malicious activity

malicious code

malicious code hacking services

malicious code sales

malicious code software

malicious cyber actors

malicious email

malicious software

malicious threat (s)

malware

malware hidden in

malware infection rates

man charged in

man charged with

man dead in suspected terrorist attack

man found dead

man gunned down

manmade disaster

man-made disasters

man not guilty

man portable air defence systems (MAN PADS)

man shot by police

man tried to rob

man was arrested for

mandatory evacuation (s)

manhunt

manhunt continues in

manifesto

manufacturing explosives

mapping veins in hand

maritime crime (s)

maritime disaster (s)

maritime domain

maritime domain awareness (MDA)

maritime expansionism

maritime headquarters

maritime security

maritime security threat

marred by violence

martial law declared

martial law preparation

masked man

mass casualties

mass casualties expected

mass civil breakdown

mass demonstrations

mass grave (s)

mass mobilization (s)

mass mobilization against

mass shooters

mass shooting (s)

mass stabbing

mass street demonstrations

mass surveillance

massive blackout

massive data breach

massive manhunt

massive military movement

massive show of force

massive street protests

mastermind

mastermind behind attack

mastermind of attack

material and economic loss

material support

material support for terrorists

maternity tourism

may have been abducted

may have been compromised

may have been kidnapped

may have been killed

may spread

measures are a result

media account

media gag order

media reported

media said

medical cyber crime

medical data breach

medical fraud

medical identity

medical identity theft

medical threat assessment

medicine shortage

meningitis outbreak

MERS

metadata

metadata program

method of attack

meth crimes on the rise

Mexican criminal organization

Mexican vigilantes

Mexico crime

microbial threat (s)

midair encounters

migrant route (s)

migrant worker (s)

migration fraud

miles of track

militant

militant group

militant hideout

militant Islamist

militant mujahi movement

militant network

militant organization

militant ties

militant website (s)

militants threaten

militants threaten attack

militarized border

military

military accord

military asset (s)

military base (s)

military camp (s)

military concentration camps

military conditions

military convoy

military cooperation

military deployment

military detention

military exercise

military experts

military forces moved into

military industrial complex

military installations

military information

military intervention

military movement (s)

military plan (s)

military preparation (s)

military presence

military presence in Africa

military regime

military strategy

military tactics

military threat

military track terrorists

military weakness

military’s actions

militia

minimize risk

mining hazard (s)

minority political group (s)

missile firings

missing American

missing abroad

missing boy

missing child

missing girl

missing hiker

missing hiker found

missing hiker found dead

missing in

missing man

missing person (s)

missing person national park

missing person state park

missing persons report

missing national park

missing state park

missing tourist (s)

missing woman

missing women

mitigate perceived threats

mitigate situation

mitigating the risk

mitigation

mob killing

mob rule

mobile phone tracking

mobile security

mobilization and deployment

moderate rain

modern warfare

money launderer

money laundering techniques

monitor communications

monitor extremist activity

monitor movement

month’s-long protests

mop-up-fire

more deaths

more dogs infected

more flu deaths

more humans infected

more people infected

mortar attack (s)

mortar attacks from

mosque assault

mosque attack

mosque attacked

mosque bombing plot

most concentrated

most prevalent crimes

most wanted

most wanted men

mother charged in

mother killed

motion sensors

motive unclear

motives

motorcycle attack

mountain climber

mountain hideout (s)

moved across the border

moved drugs

moved to safer ground

moved to safety

moved troops and weapons

movement leader

moving target (s)

mud slide

mudslide

mudslinging

multinational effort

multiple effort

multiple attacks

multiple deaths

multiple deaths in shooting

multiple fatalities

multiple injuries

multiple injuries reported

multiple looting

multiple shootings

multiple stab wounds

multiple targets

multi-vector threat

murder for hire

murder investigation launched

murder-suicide

murder suspect apprehended

murder suspect arrested

murder suspect detained

murder was carried out

murder weapon

murders up

muscle operative

Muslim Brotherhood

Muslim cleric

Muslim community

Muslim immigrants

Muslim owned businesses

Muslim riots

Muslims are targeted

N

naked body found

narco air route

narco submarine

narco terrorism

narco-trafficking

narco-trafficking route (s)

narco-trafficking route (s) Africa

narco-trafficking route (s) Asia

narco-trafficking route (s) Europe

narco-trafficking route (s) Latin America

narco-trafficking route (s) Mexico

National Clandestine Service (NCS) (CIA)

national security

national disaster (s)

national emergency

national emergencies

National Fire Protection Association (NFPA)

National Incident Management System (NIMS)

National Interagency Fire Center (NIFC)

national preparedness

national wildfire coordinating group

national security

national security implications

national security interests

national terrorism advisory system (U.S.)

national uncivilized traveler record (China)

national unity

NATO transformation seminar

natural disaster (s)

natural hazard (s)

nature of threat

nature of warfare

naval operations near

NCTC Information Sharing and Knowledge Development Directorate (ISKD)

near border

near certainty

near collision

near collision with a commercial aircraft

near collision with a drone

near collision with a private aircraft

near conflict zone

near Mexico border

near mosques

near the airport crime

near the border

near the conflict zone

necklace knife

nefarious reason

negotiations between

negotiations stalled

negotiations with

negotiations with Iran

neighboring countries

neo-paramilitary

neo-Taliban insurgency

nerve agents

network

network (s) vulnerable

new attack (s) inevitable

new cases reported

new hub

new law

new sanctions

new surveillance law

new travel alert

New York State Terrorist Registry ACT

news broke that

new sanctions

newsworthy

nexus between

nexus to terrorism

night raids

no arrest (s) have been made

no credible intelligence

no evidence

no evidence of shooting

no evidence of a shooting

no fatalities

no-fly list

no fly zone

no fly zone database

no go zones

no immediate claim of

no injuries reported

no obvious signs of trauma

no one was injured

no one was killed

no preliminary signs of foul play

no signs of trauma found on body

non-conventional weapons

non-credible threat

non-fatal shooting (s)

non-lethal aid

non-state actors

non-state groups

non-state terrorist incident

nor’easter

norovirus

norovirus outbreak (s)

not acknowledged publicly

nothing to indicate he was

NSA cell phone location tracking program

NSA collection program

NSA intercept

NSA tradecraft

nuclear agenda

nuclear arms race

nuclear arsenal

nuclear bunker

nuclear detection system

nuclear domino effect

nuclear negotiations

nuclear posture

nuclear program

nuclear strategy

nuclear talks

nuclear test

nuclear weapon ballistic missile

nuclear weapons

number of anti-aircraft systems

number of new infections has risen

number of violent crimes

O

objective of damaging – military activity or attack

obscure group

obtained images from cameras

obtained images from surveillance cameras

obvious signs of trauma

occupation forces

occurs in

odd weather

offensive cyber weapon

office of naval research

officer-involved shooting (s)

officers dispatched

officers responded

official accounts

official not authorized to comment publicly

officials discussed

officials indicted

officials reported

officials suspected

officials vowed

offshore account (s)

oil shipping facility

oil smuggling

oil spot strategy (military)

oil terminal

oil thieves

on a bus carrying

on alert

on alert for terrorist attack (s)

one of the hostages

ongoing conflict

ongoing investigation (s)

onion routing

online alias (es)

online child pornography

online data breach (s)

online enticement of children

online rental scam (s)

online scam (s)

online security

online security threat (s)

online surveillance

open (ed) border

open civil war

open source intelligence gathering

open to abuse

opened a criminal investigation

opened fire

operating as a front

operating illegally in

operating in

operation Jade Helm 15

operational operative

operational leader

operational planner

operative

opium bride (Afghanistan)

opium harvest

opium harvest (Afghanistan)

opium harvest (Mexico)

opium smuggling routes

opposition

opposition faction (s)

opposition movement

optics

order to kill

ordered assassination

ordered at gunpoint

ordered from vehicle at gunpoint

ordered military airstrike

ordered military air strike

organ harvesting

Organization of Islamic Cooperation

organized crime

organized crime groups

organized criminal groups

origin of the fire

originated from

OSINT news

other law enforcement agencies

out of an abundance of caution

outage

outage shut down

outage shut down critical services

outages

outbound travel alert (OTA) – Hong Kong system of global travel alerts

outbreak declared

outbreak of civil unrest

outbreak of violence

outlook deteriorating

outside U.S. consulate

overseas conflict (s)

over prescribe antibiotics

overseas listening posts

overt violent extremist

overtly

P

package bomb

packages disguised as

packages disguised as rocks

packed with explosives

PAK army

paper tiger

parcel bomb

palmer drought severity index (PDL) – f

pamphlets

pamphlets distributed

pamphlets dropped

pandemic

paperless airfreight

parole violation

particularly vulnerable

passed classified information

passenger aircraft

passenger ill on flight

passenger (s) inspection (s)

passenger name record (PNR)

passenger removed from aircraft

passenger removed from flight

passengers evacuated

passengers forced to evacuate

passenger (s) killed

passport altered

passport fraud

passport and visa fraud

passport revoked

patriotic Europeans against the Islamisation of the west (PEGIDA)

patrol

patrol car

patrolling at night

pay to stay vacation scam

peace talks

peak oil theory

pedo

pending indictment

pension fraud

pension scam

Pentagon alert

Pentagon set to

people are fleeing

people arrested

people detained

people have been killed

people in camps

people injured

people killed in attack (s)

people smuggling

people taken to hospitals

people were killed

people were shot

people were wounded

perpetrators

persistent

person responsible

personal belongings seized

personal privacy

personnel responded quickly

personnel responded quickly to the scene

pestilence

petrol bomb

petroleum reserves

pharmaceutical crime (INTERPOL)

pharmaceutical drug cartel

pharmaceutical drug crime ring

pharmaceutical drug ring

pharmaceutical fraud

phishing

phishing scam

phone records

phone service out

photo substitution

photo substitution passport

physical attack to grid

physical damage

physical damage assessment

physical threat

pile-up

pill mill

pilot error

pilot indicated that

pilot reported

pilot reported turbulence

Pinocchio test – whopper

pipe bomb

pipe bomb plot

pipe packed with explosives

pipe packed with firework explosives

pipeline

pipeline accident

pipeline attack

pipeline explosion

pipeline leak

pipeline shutdown

pirate tactics

pirate zone

pirates attack vessel

pirates attack vessels

placed near a police station

placed on lockdown

plague (s)

plagued by

plane crash

plane crashed

plane forced to land

plane forced to make an emergency landing

planning a series of attacks

planning a series of terrorist attacks

planned attacks on

planning attacks

planning attacks on

planning attacks on churches

planning attacks on civilians

planning attacks on mosques

planning attacks on synagogues

planning attacks on targets

planning attacks on western targets

planning car bomb attack

plant disinformation

planted a bomb

planted bomb (s)

planted explosive devices

planted explosives

planted several explosive devices

planting stories

plastic 3-D printed gun

plot (s)

plot broken up

plot to destabilize the government

plotted attacks

plundered

plunged off road

poaching

poaching sea bladders

point of origin – f

pointed gun at

pointed rifle at

polar bear hunting

police

police alert (ed)

police alerted to a bomb

police are investigating a shooting

police are treating the case as

police barricade

police break up

police clash with

police clash with protesters

police crackdown on

police conducted an operation

police conducted wiretaps

police destroy

police have not ruled out foul play

police hunt for

police injured

police investigate

police investigate assault

police investigate murder

police investigate robbery

police investigate shooting

police investigate terrorist activity

police investigation

police investigating

police officer accused of

police officer arrested

police officer fired

police officer injured

police officer killed

police officer shot

police officer shooting

police officer shot man dead

police opened fire

police patrol deployment

police perimeter

police presence

police recover

police reports indicate

police responded

police report activity

police report suspicious activity

police responded to a report of a

police said the body is that of a

police search for

police shooting

police station attacked

police stormed site

police source (s)

police sued

police tactics

policy toward North Korea

political conflict

political controversy

political crisis

political deadlock

political figures

political instability

political retribution

political retribution against

political scandal

political strike

political terrorism

political unrest

political intrigue

political upheaval

political weapon

politics of fear

pollution alert

ponzi scheme

ponzi schemer

poor safety record

poppy farmer

poppy harvest

popular protests

popular with tourists

population expansion

population growth

population trend

porno

porous border (s)

porous border between

portable launchers

port (s) of entry (POE)

port security

pose an immediate threat

pose biggest threat

poses a threat

posing as

position further weakened

possible contamination

possible salmonella contamination

possible scam

possible terror attack

possible terrorist attack

posting pictures on facebook

potassium chlorate (bombs)

potential adversaries

potential attack (s)

potential bomb chemical

potential criminality

potential crisis

potential delays

potential for terrorism

potential harm

potential of shutting down

potential risk

potential risks

potential scam

potential security problem

potential target

potential target (s) for terrorists

potential terror threat

potential terrorist

potential terrorist threats

potential threat (s)

potentially dangerous

power blackouts

power grid

power lines damaged

power outage (s)

power outage (s) in

power sector

power shortage

power surge

predator b drone

pre-clearance

pre-incident indicators (PINS) of violence

precarious situation

precursor chemicals

precursor chemicals seized

predictive analysis

predictive analytics

prescribed burn (s)  - f

predator drones

preempt and disrupt terrorist activities

preemptive of a terrorist attack

preliminary signs of foul play

presence continues to grow

presence of civilians

present situation

preservation letter

preserve anonymity

presumed dead

pretending to be a teen

prevent a terrorist attack from taking place

prevented from taking off

prevention

prevention of a military attack

prevention of an attack

prevention of terrorist attack

prevention measures

preventive action

previous attacks on

price hike (s)

prices have climbed

pricing inaccuracies

primary threat (s)

prime breeding ground for terrorism

prime minister said

principal keywords

prior to the assassination

prior to the attack

prism surveillance program

prison break

prison escape

prison walls

prisoner attacks

prisoner swap

privacy breach

privacy concerns

privately held data

privatized military companies

proactive actions

probably method of attack

probation violation

probe launched

proclaimed a state of emergency

procure weapons

product extortion

profits finance drug gangs

propagates the disease

project prophecy CIA

prominent al-Qaeda member

promote extremism

promote militancy

prompted red alert

prompts warning

pronounced

propagandist

propagates the disease

prosecutors also charged

protecting crowded places

protection measure (s)

protection measure for earthquake (s)

protection measure for landslide (s)

protection measure for damage to building (s) from cyclone (s)

protection measures from damage to buildings during earthquake (s)

protection measures from damage to buildings during floods

protection measures tsunami (s)

protection money

protective action

protest (s)

protest against

protest outside U.S. consulate

protest outside U.S. embassy

protest planned

protest (s) turned violent

protest (s) turns violent

protest (s) in

protest (s) quickly spread

protester arrested

protesters arrested

protester (s) attack (ed)

protesters burn flags

protesters raid

protests aimed at toppling

protests being planned

protests turned violent

provide air protection

provide air support

provided material support

public emergency

public disaster

public health

public health alert

public health announcement

public security

public safety spokesman

public speeches

public venues

pulmonary irritants

punctured a pipeline

purchased high explosives

purple notice

put on red alert

Q - R

quantum computer

quell civil unrest

quell protests

quell riots

questions remain

quick action

quickly denounced

quickly deteriorate (d)

race riots

radar data not available

radar equipment

radiation accident

radiation alert

radical

radical group (s)

radical Islam

radicalized

radicalization

radio-activated improvised explosive devices

radioactive material (s)

radioactive material (s)

radioactive theft

radiological dispersal device (RDD) – dirty bomb

radiological weapons

railroad crossing

railroad crossing arms

raises fear (s)

rally site protest

random attacks

random killings

ransom

ransom demand

ransom paid

rapid reaction force

rapid shift

rare earth elements

rare earth industry

rare earth mines

raw intelligence

reactive military action

real threats

rebel attack (s)

rebel attacks claim more than one hundred lives

rebel group (s)

rebel held territory (ies)

rebel position (s)

rebellion

rebuked

received widespread attention

recent attacks

recent convert to Islam

recent events

recent incident (s)

recent outbreak (s)

recent security incident

recent suicide bombings

reconnaissance exercise

record breaking cold

record breaking heat

record breaking heat to continue

record breaking heat to ease

record breaking heat wave

records compromised

records from department of safety

records stolen

recover arms

recovery

recovery effort

recovery efforts

recovery of body

red alert

red button hacking

red cell program

red cell team

red death stamp (ISIS)

red flag day – f

red team

reduce exposure

reemergence of militant cells

refugee crisis

regime change

regional hegemon

regional hegemony

regional nuclear war

regional radical

regional security

regional tension (s)

reign of terror

reinforced controls at border crossings and airports

related attack (s)

related threat

relations between Seoul and Beijing

released data today

released publicly

relief

relief aid

relieved of duty (ies)

reliable source (s)

religious extremist

religious terrorist group

remain classified

remain unaccounted for

remains jailed

remains speculative

remote area

remote controlled aircraft

remote controlled warfare

remote desert area

remote region

renewed their offensive

rental car scam

repeated emergence

report filed with

reported cases

reported cases of

reported cases of dengue

reported cases of Ebola

reported cases of flu

reported cases of influenza

reported cases of malaria

reported influenza case (s)

reported killed

reported lost

reported missing

reported suspicious activity

reports of

reports of a fight

reports of fighting

reports of continued gunfire

reports suggest

reports of violence

reports of violence increase

repressive regime

reprisal attack

rescue attempt

rescue mission

rescue scenario

rescue workers

rescuers were alerted

residents still without power

residents told to evacuate

residents told to leave

residents were without power

residents without electricity

residents without water

residents without power

resistant tuberculosis

resistance

response

response team

response to the threat

responded to a report of a fire

responded to an add on craigslist

responded to an add posted on the internet

responded with air strikes

responsible for numerous attacks

restricted airspace

restricted area

restricted area access

resulting in more deaths

retaliatory attack (s)

retaliatory measures

retrieve data

returnees

returning Jihadists

reveals that

revenge attack (s)

reverse lookup programs

reverse prostitution sting

reverse sting

rickshaw attack

rickshaw bomb

rickshaw bomb kills

riddled with bullet holes

right-wing violence

riot

riot broke out

riots break out

riots claim more than 100 lives

rip current

riptide

rise in

rise in heroin use

rise in number of people

rise of cyber crime

rising tension (s)

risk assessment

risk and threat assessment

risk identification

risk of coup

risk of escalation

risk of kidnap

risk zone

road blockages

roadblock

roadblock military

roadblock police

road closure (s)

road damage

road traffic safety

roadblocks were reported

robbed

robbed at gunpoint

robbers fatally shot

robbers were armed

robbery

robbery suspect arrested

robbery suspect detained

robbing a convenience  store

robbing a gas station

rock assault

rocket propelled grenade launcher

rockets fired on Israel

rogue drones

rogue state (s)

rolling blackout (s)

rolling roadblock (UK)

romas – Bulgarian gypsies

romance scam

rounds of ammunition

route between the countries

roving bands

roving wire tap (s)

run on deposits

runway excursion

Russian military aggression

Russian regional airlines, accidents

Russia warns

Russian aggression

Russian bombers

Russian bride scam (s)

Russian crime gang

Russian cyber threat

Russian hackers

Russian military operations

ruthlessness people

S

sabotage threat

safe

safe haven

safe haven for Islamic terrorists

safety concern (s)

safety of customers

safety of infrastructure

San Diego sector

sanctions against

sanctions against Iran

sanctions against Russia

sanctions imposed

sanctuary to terrorists

SARS

satanic cult

scale of damages

scam advisory

scam alert

scam baiting

scam buster

scammer (s)

scenario

scenario analysis

scenario planning

scheduled to leave

scheme to steal

school closure

school shooting (s)

scope of the mission (s)

scope of the threats

scores die

scores killed

scout plane

scouting plane

scrape networks

scrubbing social media site

search

search and rescue efforts

search effort (s)

search efforts called off

search team (s)

search team found

searching for flight

searching for missing flight

seasonal weather

seat reduction

secret court

secret room

secret society

secret surveillance network

secret UFO locations

secretive special forces

section 215 Patriot Act

secluded location

sectarian violence

secret acts

secret equipment

secret facility

secret Iranian facility

secret Iranian nuclear facility

secret Iranian nuke facility

secret fifth column

secret meeting (s)

secret military base (s)

secret military intelligence report (s)

secret tribunal

secretive special forces (mil)

sectarian war

securing nation’s borders

securing systems

security

security breach

security breach airport

security challenge

security clearance

security concerns

security details

security drill

security experts warn

security fears

security flaw

security forces

security forces pulled out

security forces sealed off

security grossly inadequate

security issues

security lapse

security measure (s)

security measures revised

security message for U.S. citizens

security operative (s)

security services

security situation (s)

security team is investigating

security threat

security threats

security tightened

security to be stepped up

security warning (s)

security worries grow

seek evacuation

seek refuge at embassy

seeking a consensus

seized at airport

seized by opposition forces

seized drugs

seizing the town of

seizure of

seizure of drugs

seizure of pharmaceuticals

seizure of plants

self guided bullets

selling military secrets

send lewd photos

senior intelligence officer (SIO)

senior intelligence official

sensitive information

sensitive security information

sentencing date not announced

sentencing date postponed

sent weapons to

serial killer

serial rapist

series of attacks

series of explosions

series of shark attacks

serious concern (s)

serious threat (s)

served in prison

server compromised

servers targeted

service reduction

services first went down around

serving fronts for prostitution

setting off a car bomb outside

severe damage

severe drought

severe unemployment

severe infection

severe inflation

severe storm

severe thunderstorm warning

severe threat

severe weather

severe weather alert

severe winter storm

severed fingers crime

severely degraded (terrorism)

severely diminished

sewage contaminated water

sewage overflow

sex crimes involving children

sex offender

sex offender arrested

sex offender detained

sex offender registration

sextortion

sex tourism business

sexual assault

sexual assault patterns

sexual crime (s)

sexual exploitation

sexual offense

sexual predator (s)

sexual servitude

sexually transmitted disease (s)

shadow networks

Shariah law

shark attack

sharp force injuries

sharp increase in

shelter in place order

sheltering an illegal alien

sheltering illegal aliens

sheriff’s officials say

shift in human geography

shifting their locations

Shiite Houthi rebels

ship capsized

ship carrying weapons

ship righted

shocking attack (s)

shoot down

shoot down aircraft

shooting

shooting arrest

shooting suspect

shooting suspect barricaded

shootings on the rise

shootings up percent

shortage

shortage of supplies

shortage of medical supplies

shortage medical supplies

short term threat

shortages of commodities

shot and killed himself

shot dead

shot by

shot outside

shot suspect after

shot to death

shots fired

show of force

shangri-la dialogue

siege

siege consulate

siege embassy

sign (ed) a decree

significant attack

significantly disrupted

signs of trouble

signs military accord

Sikh temple

sim card theft

Singapore Police Force Crisis Negotiation Unit

situational awareness report

situation continues to deteriorate

situation deteriorating

situation in _ serious

skids off runway

skirting the law

slain officer

slaying of police

sledding accident

sleeper teams

sleeper tradecraft

sleet

small arms

small explosives went off

small groups of people

small plane crash

small protests

Smart Traveler Enrollment Program (STEP) (U.S.)

smoldering

smuggled

smuggled alcohol

smuggled cigarettes

smuggled goods

smuggled in

smuggled into

smuggled into the U.S.

smuggler (s)

smuggler (s) arrested

smuggler (s) detained

smuggling

smuggling business

smuggling corridor

smuggling imports

smuggling inside her body

smuggling inside his body

smuggling routes

smuggling routes from

smuggling syndicate

smuggling travel networks

snapchat

sneaking over border

sniper

sniper assault

sniper attack (s)

sniper operations

sniper shot

snow storm

snowfall measurement

social media

social media hacking

social media network

social media threat

social stability

social terrorism

social unrest in

socially reinforced violent extremist

soft targets

sold credit card data

sold illegally

soldier injured

soldier killed

soldier shot

soldier shot dead

Somali pirates

Sony hack

sorties against targets

source of the virus

sources close to

sources from the U.S. government

sources of the fire

sources reported

sources said

spammer

sparks evacuation

sparsely populated areas

speaking on condition of anonymity

special access program

special envoy

special ops

special rapporteur

special response team (SRT)

special task force

specific threat (s)

specific threats Olympics

spike in

spike in crime

spike in kidnappings

spillover violence

spiral of violence

spiraling violence

splinter group (s)

spoofed telephone number

spoofing tactics

sporadic clashes

sporting events

spotted wearing

spread of infection

spread of infectious disease (s)

spread of radical Islam

spread terror

sprengel explosive

spy agencies

squatter arrested

sql injection

stab

stab wound (s)

stabbed and shot

stabbed in neck

stabbed in the face

stabbed in the head

stabbed to death

stabbed with screwdriver

stabbing attack

stabbing death (s)

stage attack (s)

stagnant water

static security

standoff

stand down order

stark warning

started shooting

State Department has advised Americans

State Department issues warning

state of emergency

state sponsored terrorism

state-sponsored terrorism

state terrorism

statement said

stealth technology

steel combs (stabbing)

still missing

sting operation

stingray phone surveillance

stock exchange halts trading

stole a

stolen billfold

stolen car (s)

stolen documents

stolen drugs

stolen email addresses

stolen emails

stolen explosives

stolen goods

stolen gun

stolen identity refund scam

stolen iridium

stolen passport

stolen password

stolen purse

stolen travel documents

stolen vehicle

stolen wallet

stolen weapon (s)

stopping flights

stopping flights from

stopping flights to

storm capsized

storm causes damage, flooding

storm causes wrecks

storm surge

storms cause damage

storms cause damage in

storms cause power cuts in

storms cause road closures

storm cause widespread

storms paralyze

storm paralyzes

storms sweep across

storms sweep across state

stranded

stranded in

stranded motorist

strapped a cord around neck

strategic alliance (s)

strategic early warning

strategic international studies

strategic mission list

strategic plan (s)

strategic presence in

strategic terrorism

street reversal prostitution

strikes and protests

strikes militant positions

string of burglaries

string of terrorist attacks

stuck

structured transactions

structuring (IRS)

student abducted

student demonstration

subdued by bystanders

subdued by police

submarine

submarine sighting (s)

subversive groups

sucker punch

sudden death

suffered severe damages

suffers massive power outage

suicide attack (s)

suicide bomber attack (s) (ed)

suicide blasts

suicide groups

suicide operations

suicide tourism

suicide vest

summary execution

super bug (s)

supply chain security

support for terrorists

support operations

surge in terrorist attacks

surge in violence

surveillance

surveillance aircraft

surveillance camera

surveillance network

surveillance of air travelers

surveillance program

surveillance technology

surveillance tools

survey the damage

survival of the passengers

suspect admits

suspect (s) arrested

suspect (s) detained

suspect in terrorist attack

suspect in terrorist attacks

suspect (s) injured

suspect (s) killed

suspect (s) opened fire

suspect (s) opens fire

suspect (s) shot

suspected accomplices

suspected active faults

suspected in the terror threat

suspected in the terrorist attack

suspected international terrorists

suspected Islamist extremists

suspected Jihadis

suspected terror threat

suspected terrorist (s)

suspected terrorist(s) attack tomorrow

suspected terrorist (s) detained without trial

suspected terrorist (s) rights

suspected terrorist (s) threats

suspected terrorists list

suspended peace talks

suspending search efforts

suspends consular services

suspicions sparked

suspicious activity (ies)

suspicious activity reports

suspicious attacks

suspicious behavior

suspicious crash

suspicious device

suspicious event (s)

suspicious internet activity

suspicious situation (s)

suspicious transaction (s)

suspicious visits

sustained campaign against

SWAT

SWAT response

SWAT team

sweep across (weather)

swept away by current

sword (s)

sword attack

Syrian electronic army

system compromised

system crashed

system to assess risk (STAR) (FBI)

systemic event – financial economy

systemic risk

swarm boats

swept up in the scam

T

tactical advantage (s)

tactical alliance

tactical nuclear weapon (s)

tactical operators

tactical team

tactics revealed

tailored access operations (TAC group – NASA)

tails operating system

tainted food

tainted with

taking a huge toll

taking on water

Taliban

Taliban commander

Taliban government

Taliban insurgency

Taliban regime

talks involving the two groups

tamper with U.S. infrastructure

tampered with

tampering with public water system

tantalum – chemical

tapped conversations

tapping phone

target for kidnappers

target for poachers – ivory

target for terrorist attack (s)

target hardening

target of investigation

target of terrorism

target people online

target soft spots

target soldiers online

target weaknesses (attack) (sabotage)

targeted assaults

targeted attack

targeted attacks

targeted attack campaign

targeted by terrorists

targeted cyber-attacks

targeted for

targeted for extortion

targeted in attack (s)

targeted killings

targeted police operations

targeted recruit

targeted surveillance

targeted the

targeting critical infrastructure

targeting dozens of victims

targeting journalists

targeting local

targeting police

targets of predators

tarmac wait time (s)

task force

task force agent

tax avoidance

tax avoidance strategy

tax evasion

tax fraud

tax haven

tax refund fraud

tax refund scam

tax scam

tax scams

tax season scams

TB epidemic

team leader

tear-gas bomb

technical hazard (s)

technical issue (s)

technological disaster

teenager (s) arrested

teenager (s) detained

telephone bomb threat

telephone scam (s)

telephone services went dark

temblor

temporary road closures

temporarily suspending services

temporary mission facility

tens of thousands have taken to the streets

tension with Beijing

tension (s) between

tensions between the U.S. and China

tensions between the U.S. and Iran

tensions between the U.S. and North Korea

tensions between the U.S.

tensions between the U.S. and Soviet Union

tensions have abated

tensions mount

tensions mount between

territorial advances

territorial ambition

territorial dispute

territorial dispute

territory claimed by

terror alert system

terror alert today

terror attack (s)

terror attack linked (s)

terror attack (s) may be linked

terror attack power grid

terror attack rumor

terror cell

terror cell in

terror funding

terror group (s)

terror plot

terror plot broken up

terror plot thwarted

terror prevention

terror suspect

terror warning

terrorism

terrorism alert

terrorism case

terrorism financing

terrorism fundraising

terrorism insurance

terrorism plot

terrorism related act

terrorism risk assessment

terrorism risk insurance

terrorism risk score

terrorism sponsor

terrorism threat

terrorist act

terrorist action

terrorist alert level

terrorist attack

terrorist attack on border

terrorist capability

terrorist cell

terrorist crime

terrorist crimes

terrorist element (s)

terrorist facilitator

terrorist fundraising

terrorist fundraising methods

terrorist groups

terrorist haven

Terrorist Identities Datamart Environment (TIDE) Central database of names in U.S.

terrorist incident (s)

terrorist methods

terrorist network

terrorist organization

terrorist plot

terrorist plots foiled

terrorist plots foiled by NSA

terrorist plots since 9/11

terrorist recruitment

terrorist registry

terrorist related activity

terrorist shooting

terrorist sleeper cells

terrorist sympathizer

terrorist tactics

terrorist target

terrorist targets

terrorist use of drones

terrorist use of social media

terrorist watch list

terrorists hiding

tested positive for

the announcement follows

the attack on

the FBI shut down

the foreigners killed were

the plane was flying in route from

the company said

the far right

the restoration project – immigration

theft of funds

thinly veiled threat

thousands affected by outages

thousands flee

thousands infected

thousands of families flee

thousands stranded

threat of terrorist act

threat matrix

threat on president

threatened with gun

threatening message

threatening messages

tier 1 target

there were indications

thorium - chemical

those most at risk

thousands protest

threat

threat actor (s)

threat against

threat alert

threat alert extended

threat assessment

threat assessment team

threat continues

threat environment

threat estimates

threat heightened

threat intelligence

threat landscape

threat level

threat level increased

threat level raised

threat of attack

threat of destabilization

threat of terrorist attack

threat perception (s)

threat potential

threat reporting

threat to airline

threat to American security

threat to critical infrastructure

threat to national security

threat to public safety

threat (s) to U.S.

threat to U.S. security

threat was linked

threats posed by

threats to national security

threaten (ed) to carry out further attacks

threaten (ed) to carry out more attacks

threatened to kill family

threatened to kill her

threatened to kill him

threatened to kill his family

threatened to kill the hostage (s)

threatening post (s)

threatening post (s) on Craigslist

threatening post (s) on Facebook

threatening post (s) on Twitter

threatens strike

threats of violence

threats of violence prompts

threats to business

three card Monte

throng of reporters

thwarted terror attack

ticket fraudsters

ticking time bomb

tied her hands

tied his hands

tied his/her hands with

time bombs

time sensitive

time sensitive information

time sensitive intelligence

timeshare fraud

tip database

tip off adversary

to deter

took control of

took control of airport

took control of oil field (s)

took refuge

took refuge in bomb shelter

top-secret documentation

TOR

TOR networks

TOR users

TOR websites

torching – f

tornadic activity

tornado

tornado deaths

tornado touched down

tornadoes destroy

torrential rain (s)

tourism industry

tourist attack (ed)

tourist drowned

tourist drowning

tourist (s) killed

tourist (s) missing

tourist (s) missing since

tourist (s) murdered

tourist police

tourist (s) robbed

tourist (s) scam

tourist (s) scammed

tourist stabbed to death

tourist (s) targeted

tourist (s) wounded

toxic disaster

tramper (hiker)

trade based laundering

traffic collision avoiding systems

trafficked children

trafficked women

trafficked workers

trafficking activities

trafficking bladders

trafficking group (s)

trafficking in children

trafficking in children and their organs

trafficking in human beings

trafficking in narcotic drugs

trafficking networks

trafficking in organ parts

trafficking in persons

trafficking proceeds

trafficking in women

tragic accident

tragic event (s)

train accident

train carrying

train collides with

train derailed

train derails

transmission line

transmission line attack

transmission substation (transformers)

transnational network of terrorist groups

transnational security issues

transnational terrorism

transplant tourism

transportation hub (s)

trapped in

trapped in boat

trapped in building (s)

trapped in house (s)

trapped inside

trapped inside building (s)

trapped inside the house

travel advisory index

travel ban

travel club scheme

travel restriction (s)

travel scam (s)

travel scam (s) – Asia

travel scam (s) Europe

travel warning (s)

traveled to

traveler stabbed to death

travelers

travelers evacuated

travelers were advised

treacherous terrain

treatment protocol

treaty deadline

tremor

tribal kidnapping (s)

tried and jailed

tried to destroy evidence

Trojan attack

troop levels

troop withdrawal (s)

troops gathered

troops along the border

troops dispatched

troops fire

troops fired at

troops on alert

troops stormed

tropical cyclone

tropical storm

trove of classified records

trove of records

trying to recruit agents

TSA checkpoint

TSA precheck

tsunami

tsunami alert

tsunami warning

Tsunami Warning Center

tube strike (U.K.)

tuberculosis case confirmed

tuberculosis outbreak

turbulence advisory

turns himself in

twitter

twitter account hacked

twitter threat

tweetstorm

types of terrorism threats

types of threats

typhoon alert

typhoon intensifies

U-V-W-X

unauthorized

unauthorized person

unbreakable code

uncertain future

unclassified document (s)

unconfirmed report (s)

unconfirmed report of attack (s)

unconfirmed report of death (s)

unconfirmed threat

under investigation for

under siege

under the agreement

undercover investigation

undercover testing airport (s)

underground military site (s)

underground site

underwear bomb

undisclosed location

undisclosed hotel

undisclosed report

unencrypted files

unexpected freezes

unexpected frosts

United States intelligence community

unilateral sanctions

universal threat

unjustified killing

unknown group

unprovoked attack (s)

unprovoked fight (s)

unregistered drones

unresponsive aircraft

unsafe incident

unstable region

unusual conditions

unusual weather conditions

unwitting co-optee

updating security measures

uprising (s)

Uppsala Conflict Data Program (UCDP)

up tick

up tick in violent crime

urban attack

urban combat

urban terrain

urban terrorism

urban warfare

urban warfare techniques

urgent action

urges attack

urges attack on U.S.

using encryption

UK threat level severe

UN concerns rises

UN imposes arms embargo

UN warns of

unknown group

U.S. airstrike

U.S. counterterrorism official

U.S. covert action

U.S. customs

U.S. customs and Border Protection officers seized

U.S. cyber command

U.S. Department of Homeland Security Advisory Program

U.S. embassy warning

U.S. interests

U.S. led coalition

U.S. Marshall (s)

U.S. radicalized

U.S. renews global terrorism alert

U.S. warns

U.S. warns citizens

U.S. warns of possible attacks

undocumented immigrants

unscreened immigrant (s)

urgent security issue (s)

urgent security issues

use of force

use of weapons

user name, password

Utah Data Center – foreign data center

utility equipment thieves

vacation scam

vague death threat

valuable data

vandalism

vandal attempted

vandals sliced

vehicle attacks

vehicle ramming

vehicle rampages

vehicles laden with explosives

vehicles set on fire

vehicular accident

vehicular assault

veiled threat – crime

venom software bug

vertical information sharing

via email

vicarious counterfeiters

victims infected with

victims infected with herpes

victims of poisoning

victims targeted

video showed video footage

video voyeurism

vigilante justice

violence against children

violence against women

violence erupts

violence erupts in

violence escalates

violence grips

violence grows

violence has increased

violence is soaring

violence not over

violence outbreak

violence spreads

violent attacks caught on tape

violent campaign

violent clash

violent clashes

violent clashes across

violent clashes between

violent clashes in

violent clashes near

violent crime

violent crime increasing

violent crime decreasing

violent demonstration

violent extremism

violent extremist activity

violent outbreak (s)

violent person

violent rain

violent series of attacks

violent storm

violent struggle

viral infection

virtual currency

virtual fence

virtual global task force

virus in bloodstream

visa waiver privileges

visa waiver program

visual evidence

visual warning system

vocal critic

voice logging

volatile area (s)

vulnerability assessment

vulnerable during transit

vulnerable sites

vulnerable target (s)

vulnerable threats

vulnerable to

vulnerable to attack

vulnerability of the system

Wahhabi extremism

wake of the attacks

war breaks out

war on terror

war propaganda

war torn

warfare

warned against possible spread of

warned against possible spread of H1N1 (influenza, flu)

warning

warning issued

warning issued by law enforcement

warning message

warnings

warns citizens of the risks of traveling

warrant

warrantless arrest

warrantless wiretap

warrantless wiretapping

was arrested

was arrested on

was arrested today

was bombed

was compromised

was detained by

was found

was found in

was found next to

was kidnapped

was killed

was killed in the attack

was killed in the shooting

was placed on probation

was punched

was shot at

was slain

was targeted

wash wash scam

watch list, watchlist

water challenges

water contamination

water crisis

water cuts

water emergency

water main broke

water main ruptured

water poisoning

water shortage (s)

water source (s)

water supply

water supply and sanitation

water war

weakened immune system

weakened immune systems from disease

weakening economy

weakening global recovery

weapons

weapons broker

weapons found

weapons grade

weapons hidden

weapons of mass destruction (wmd)

weapons paid for by

weapons smuggler

weapons test

weather alert

weather chaos

weather conditions

weather modification – alteration of weather phenomena over a limited area

weather observer

weather related issues

weather related problems

weather warfare

weather warning

weather watch

web based attacks

web crawler software

web data mining

web scraping

website vulnerability

weekend shooting (s)

well organized campaign

went into lockdown

were deployed

western hostages

wet foot, dry foot immigration policy

who was the adversary

who went missing

widespread corruption

widespread devastation

widespread flu

widespread illness

widespread looting

widespread outages

widespread panic

widespread power outages

widespread violence

WikiLeaks threat

wildfire

wildland fire

wildife smuggling

wing of al Qaeda

wipe computer

with civil unrest

with neighboring

without warning

woman charge in

woman charged with

woman found dead

woman missing

woman murdered

woman not guilty

woman raped

women targeted in attacks

worker exploitation

workplace violence

worldwide average

worldwide threat (s)

worsening conditions

worsening events

worst case scenario

would be robbers

wounded after

wounded in

wounded in action

wounded in a shooting

wounded in stabbing attack

wounds to the upper torso

wrongful death lawsuit

xenophobia

xenophobia has risen

xenophobic violence

yellow alert

Yemeni group

youth violence

LIST OF OSINT TERMS BY CATEGORY
AVIATION

according to FAA records

aerial assassinations

aerial attacks

aerial bomb

air attack

air cargo threat

air controllers strike

air marshal

air operations air piracy

air rage

air show

air strike

air strikes

air tactical group supervisor

air traffic

air traffic cancellations

air traffic cancellations across Europe

air trafficking

air travel market

aircraft

aircraft hijacking

airfield

airfreight transport

airplane

airline bomb threat

airport attack

airport evacuation

airport security

airport security checkpoints

airport security incidents

airspace

airspace surveillance

airspace violation

all flights grounded

armed drone

arrested at airport

attack on airline

aviation security

banned all air traffic

black box

board aircraft

cancelled flights

CAPPS II – Computer Assisted Passenger Prescreening System II

charter flight (s)
charter flight (s) to get out

chopper

chopper crashed

civil aviation

civil aviation authorities

civilian airspace

civilian drone incidents

civilian drones

clandestine airfields

clandestine airstrip

cockpit smoke

cockpit voice recorder

commercial flights have been temporarily suspended

do not board list

do not fly list

doomed flight

downing of drone

drone attack

drone base

drone hacking

drone shot down

drone sighting

drone sightings

drone strike (s)

drone strikes in

drones crashed

European airports closed

evacuation flight (s)

experience delays

FAA investigates plane crash

FAA issued warning

FAA issues warning

flight (s)

flights (s) between

flight cancellation (s)

flight (s) cancelled

flight conditions

flight data recorder

flight delay

flight delays

flight diverted

flight forced to

flight made emergency landing

flight makes emergency landing

flight plan filed

flight seat reduction

flights added

forced emergency landing

forced landing

general aviation

Germanwings crash

grounded flights

halt flights

helibase

helicopter downed by

helicopter crashed

helicopter drop point

high tech killings – drones

ill-fated flight (s)

improper flight operations

in-flight emergency

jets entering service

kamikaze drone

laser incident

laser pointer attack

low flying planes

made an emergency landing

midair encounters

near collision

near collision with a commercial aircraft

near collision with a drone

near collision with a private aircraft

near the airport crime

no-fly list

no fly zone

no fly zone database

number of anti-aircraft systems

passenger aircraft

passenger ill on flight

passenger (s) inspection (s)

passenger name record

passenger removed from aircraft

passenger removed from flight

pilot error

pilot indicated that

pilot reported

pilot reported turbulence

plane crash

plane crashed

plane forced to land

plane force to make an emergency landing

predator b drone

pre-clearance

predator drones

prevented from taking off

provide air protection

provide air support

radar data not available

radar equipment

remote controlled aircraft

restricted airspace

rogue drones

runway excursion

Russian regional airlines, accidents

scheduled to leave

scout plane

scouting plane

searching for flight

searching for missing flight

seat reduction

security breach airport

shoot down aircraft

skids off runway

stopping flights

stopping flights from

stopping flights to

surveillance aircraft

tailored access operations (TAC group – NASA)

tarmac wait time (s)

the plan was flying in route from

took control of airport

TSA checkpoint

TSA precheck

turbulence advisory

undercover testing airport (s)

unregistered drones

unresponsive aircraft

visual warning system

BORDER ISSUES

across border (s)

along border

along northern border

along southern border

Arizona border defenders

Arizona border recon

border

border aviation security

border clash

border closed

border closing

border conflict (s)

border control

border control agencies

border crime

border crisis

border crossing (s)

border difficulties

border force (UK)

border issues

border militarization

border policy

border reopens

border security

border skirmish

checkpoint

checkpoint miles

checkpoint refusal

civilian border patrols

clandestine migration

clogged checkpoints

close to the border

closed border

common border

coyote operation (s)

crisis at the border

cross-border attack

cross-border tensions

cross border terrorism

deportation proceedings

disputed border (s)

entered with fake passport (s)

illegal border crime

illegal border crossing (s)

illegal reentry

immigrant population

immigration checkpoint

immigration fugitive

immigration office (s)

immigration officer (s)

international border

irregular arrivals

irregular migrants

irregular migration

knowledge of border area

major transit route for migrants

major transit route for people smuggling

major transit route for smuggling

migrant route (s)

migrant worker (s)

migration fraud

militarized border

moved across border

near border

near Mexico border

near the border

open (ed) border

porous border (s)

porous border between

port (s) of entry (POE)

reinforced controls at border crossings and airports

San Diego sector

securing nation’s borders

sneaking over border

U.S. customs

U.S. customs and Border Protection officers seized

undocumented immigrants

unscreened immigrant

COMPUTER/IT/COMMUNICATIONS/CRIME/SECURITY

advanced encryption technology

advanced persistent threat (APT)

advanced threat

advanced threat analytics

aggregate data

alleged cyber-hacking

automated web tools

back off virus malware

banking Trojan

biometric (s)

biometric information

biometric passport

bitcoin

black hat conference

blocking accounts

blog

breach

breach protocol

break encryption

brute force

brute force attack

CERT coordination center (CERT/CC)

chat rooms

Chinese cyber campaign

Chinese hackers

Chinese hacking group

CIF – cyber threat intelligence management system

clandestine electronic surveillance

classified document (s)

classified information

classified leak

classified leak of information

communication channels

communication (s) interception

communication node

compromised server

computer and identity fraud

computer glitch

counter messaging

covert communications

criminal cyber-security intrusion

criminal software

cryptanalysis

cryptic-currency

current cyber threat

cyber attack

cyber attack – bank

cyber attack China

cyber attack government

cyber attack North Korea

cyber attack power grid

cyber attackers

cyber – bullied

cyber capabilities

cyber command

cyber crime

cyber espionage

cyber exploitation

cyber incident (s)

cyber intrusion

cyber mercenary

cyber mercenaries

cyber security

cyber security breach

cyber security intrusion

cyber security mission

cyber security risk

cyber security strategy

cyber security worried grow

cyber spying campaign

cyber surveillance

cyber targeting

cyber threat (s)

cyber tool (s)

dark world

darknet

data breach

data breach update

data compromised

data mining program

data mining techniques

data monitoring capability

DDOS services

declassified email (s)

denial of service attack

digital documentation

disable communications

DNA hacking

dork query

DRIDEX – malware

e-currency

email (s)

emails hacked

emails indicate

emails reveal (s)

exploit data links

extracting information

extremist (s) forum

facilitate information sharing

FBI cywatch

felony computer abuse and conspiracy

geofencing software (computers)

GhostNet – China

glitch

Google dorking

Government approved keywords

GPS tracking devices

hack attack

hacker

hacker (China)

hacking

hacking attack (s)

hacking attempt (s)

hacking ring

hacking services

hacking threat

hactivism

illegal leaks

information extraction

internet attacks

internet predators

internet services went dark

intrusion routed though computers

key logger

leaked emails

low level protection

machete attack computer

malicious code

malicious code hacking services

malicious code sales

malicious code software

malicious cyber actors

malicious email

malicious software

malicious threat (s)

malware

malware hidden in

malware infection rates

massive data breach

medical cyber crime

medical data breach

metadata

metadata program

mobile phone tracking

mobile security

multi-vector threat

network

network (s) vulnerable

offensive cyber weapon

onion routing

online alias (es)

online child pornography

online data breach

online enticement of children

personal privacy

phishing

phishing scam

posting pictures on facebook

preservation letter

principal keywords

privacy breach

privacy concerns

privately held data

quantum computer

red button hacking

retrieve data

rise of cyber crime

Russian cyber threat

Russian hackers

scrape networks

scrubbing social media site

securing systems

servers compromised

servers targeted

snapchat

social media

social media hacking

social media network

social media threat

Sony hack

spammer

spoofed telephone number

spoofing tactics

sql injection

stolen email addresses

stolen emails

stingray phone surveillance

stolen password

suspicious internet activity

system compromised

system crashed

tails operating system

tapped conversations

tapping phone

target people online

targeted cyber-attacks

TOR

TOR networks

TOR users

TOR websites

Trojan attack

Twitter

Twitter account hacked

Twitter threat

Tweetstorm

unbreakable code

U.S. cyber command

using encryption

venom software bug

vertical information sharing

via email

video showed video footage

video voyeurism

valuable data

user name, password

virtual currency

virtual fence

virtual global task force

voice logging

web based attacks

web crawler software

web data mining

web scraping

website vulnerability

Wikileaks threat

wipe computer

CRIME

abduct

abducted

abduction

abductions of

abductions of Japanese

access to building

access to school

acid attack

active investigation

active shooter

active shooter protocol

acts of violence

accused of

accused of having sex with a student

accused of smuggling

additional security measures

advanced fee fraud

affray (UK)

after the attack

agitator

alarm

alarm sounded

allegation (s)

alleged member (s)

allegedly shot

ambush

ambushed him from

ambushed him from behind

angles of security cameras

apartment placed on lockdown

apprehensions decreased

apprehensions increased

armed assailant

armed assault

armed and dangerous

armed attack

armed groups

armed men broke into

armed robbers

armed guards have been placed

armed man

armed robbery (ies)

armed standoff

armed suspect

armed woman

arrest

arrest made

arrest made after

arrested

arrested in

arrested in connection with shooting death

arrested in connection with stabbing death

arrested in prostitution ring

arrested in raid

arrested in raids

arrested in shooting

arrested in shooting death

arrested on warrant (s)

arrested last week

assassin

assassinate

assassination

assassination attempt

assault

assault by

assaulted after

at the time of the attack

attack

attack by

attack by former lover

attack by x-lover

attack came from

attack from

attack occurred

attack that occurred

attack took place in

attacked by

attacked (ed) pedestrian (s)

attacked while jogging

attacks in recent days

attacks in recent months

attacks in recent weeks

attacks increase

attacks on foreign nationals

attempts to sell stolen work

background check

bag snatching

bail in

bandits in ski masks

bank bombing

bank heist

bank holdup

bank robbery

believed to be armed

belligerent (crime)

betray

bird smuggler (s)

blackmail

bloodstain evidence

bodies found

bodies found in

bodies of missing

bodies recovered

body found

body found hidden in

both gunmen

bound with duct tape

bounty hunter

brazen attack

bring the men to justice

broke into

brutal

brutal attack

brutal attack against

brutal attack by

brutality

bury

campus shooting

captive

capture  (d)

carjacking

carried out an order of

carrying assault rifles

cartels smuggling

case reopened

changing sim cards

charge

charged in murder

charged in plot to murder

charged in shooting

charged with

charging document

charged in court

charged in plot

charges of plotting

charges were filed

child abducted

child predators

child traffickers

church shooting

clandestine brothel

clandestine grave (s)

clandestine laboratory

clandestine means

cocaine

cocaine coast India

cocaine found hidden in

cold case murder

combat crime

commit violent crime (s)

commit wire fraud

complaint states

conceal

concealed in

concealment techniques

conditional threat – crime

confrontational

contraband

contraband alcohol

contract assassination (s)

cordoned off a large area

counter narcotics efforts

courier

court records indicate

court records sealed

court shooting

cover-up

cover-up probe

crime

crime activity

crime against humanity

crime and violence are serious problems

crime blotter

crime decrease

crime increase

crime lord

crime lord arrested

crime prevention

crime record

crime related

crime ridden

crime scene

crime scene investigation

crime statistics

crime trend (s)

crimes unreported

crimeware

criminal activity (ies)

criminal charges filed

criminal charges pending

criminal corruption charges

criminal crime scene

criminal endeavor

criminal enforcement actions

criminal gangs

criminal investigation

criminal investigation continues

criminal investigations section (CIS)

criminal mischief

criminal network (s)

criminal prosecution (s)

criminal record (s)

culpability

curb illegal activity

currently being detained

currently detained

customs officer seize

customs seize

cut with a hacksaw

cybercriminal ring

death list

death threat

death threat (s)

declared insane by the courts

deferred prosecution

deliberate damage

detained by authorities

detained by police

detained in

detection of crime

detention hearing

dies in custody

dies in police custody

direct threat – crime

disguised as

disappeared

disappearance

discovered evidence

dog attack (ed)

dogs attack pedestrian (s)

doggy door burglar

domestic violence

domestic violence death (s)

domestic violence increase

door breach

dozens arrested in

dozens arrested during

dozens detained

dragnet included

drink spiking

drive-by shooting

drug bust

drug corridor

drug (s) found hidden in

drug related violence

drug trafficking hub

drug trafficking maps

drug trafficking organizations

drug trafficking route (s)

drug trafficking route (s) Asia

during the search

Dutch police arrested

elude authorities

endangered species act

entered the airport

entered the building

entered the hotel

entered locked are

entered secure area

enticed child

enticed girl

equatorial rainforests – ivory

escape (d)

escaped convict

escaped inmate

euros seized

evade authorities

evade police

evade tracking

execution carried out

exotic pet smuggling

extortion

faces charges

faces extradition

faces murder charge (s)

false imprisonment

fastest growing crimes

fatal shot

fatalities

fatality

fatally injured

fatally shot

fatally shot by

fatally stabbed

fatally stabbed by

father charged in

Ferguson effect

fetal homicide

fight between

fight broke out

file charges against the suspect

fleeing in

flow of drug money

foreign national charged with

forensic evidence

forensic information

forensic investigator

forensic investigation

forensic psychiatry

forest elephants – ivory poaching

former employee charged with

former gang member

found bound and gagged

found guilty

found guilty on all charges

found hidden in

found in food

found stabbed

found stabbed in home

found stabbed to death

freight theft

frequent gathering point

fueled rumors fugitive operating

full investigation

full scope investigation with polygraphs

full tactical gear

gang hideout

gang’s hideout

gang intelligence team

gang leaders

gang of thieves

gang related shooting (s)

gangs

gay arrest

gender violence

global report on trafficking in persons

gold heist

government’s star witness

grabbed clerk

grave robber(s)

grave robbing

group of armed men

group responsible

group was attacked

growing black market

growth of crime

guards have found

gunman

gunmen

gunmen riding on motorcycles

gunmen stormed

gunmen were shot

gunmen were shot to death

gunned down

handed over to police

hard ivory

has been arrested

has been detained

has not been seen since

have been arrested

haven for criminals

have yet to be identified

have yet to identify

he was searched

she was searched

they were searched

heist

held captive

held captive in

held in robbery

held in shooting

her attacker

heroin mixed with fentanyl

heroin mixed with gasoline

heroin trade

hid on rooftop (s)

hide evidence of wrongdoing

hidden in

hidden baggage

hidden luggage

hidden in metal drums

hidden service

hideout

hiding secrets in open

high level (s) of crime

high risk traffic stop

higher homicide rate than

hold culprits accountable

holding hostage

holding inmates hostage

holding patients hostage

home invasion

homicide rate

homicide victim (s)

hostage beheaded

hostage crisis

hostage killed

hostage negotiation

hostage negotiation team

hostage rescue situation

hostage (s) rescued

hostage situation

hostage standoff

hostage taking

house-by-house search

human cargo

human dumping ground

human rights

human rights abuses

human rights violation (s)

human smuggling routes

human trafficking

human trafficking hotline

human trafficking intervention court (s)

human trafficking ring

identify suspect

identify suspect from video

illegal activities

illegal drug trade

illegal drug trade in

illegal immigration network (s)

illegal logging

illegal mining

illegal wildlife trade

illegally entered

illicit drugs illicit compounds

illicit goods

illicit medicine (s)

incidence of stabbings

increase in crime

indirect threat – crime

inquest

inside job

institutional vandalism

international warrant

invasive species crime

investigate (s)

investigate (s) crime

investigate (s) murder

Irish travelers

items seized by

ivory

ivory seized

ivory to bribe local officials

ivory smugglers

ivory trafficking

jail

jailed

jewelry heist

jury convicts

jury duty scam

kidnap

kidnap for ransom group

kidnapped

kidnapped for ransom

kidnapped victim released

kidnapping (s)

kidnapping Europeans

kidnapping has occurred

kidnapping westerners

killed in a police shootout

knockout game

laced with

laced with poison

large seizures

launched investigation

launched murder investigation

led a chase

left at

left for dead

let guard down

license plate tracking program

line was cut

linked to crime

linked to the murder

linked to trafficking drugs

listed as threatened (ivory)

local trafficker

location information of cellphone

location of informants

loss/theft report

lured the victim

mail stolen

main suspects

major drug bust

malicious activity

man charged in

man charged with

man found dead

man gunned down

man not guilty

man shot by police

man tried to rob

man was arrested for

manhunt

manhunt continues in

mapping veins in hand

masked man

mass grave (s)

massive manhunt

may have been abducted

may have been kidnapped

may have been killed

method of attack

meth crimes on the rise

Mexican criminal organization

Mexican vigilantes

Mexico crime

missing boy

missing child

missing girl

missing in

missing man

missing person (s)

missing woman

missing women

most prevalent crimes

most wanted

most wanted men

mother charged in

motive unclear

moved drugs

multiple attacks

multiple deaths in shooting

multiple shootings

murder for hire

murder investigation launched

murder-suicide

murder suspect apprehended

murder suspect arrested

murder suspect detained

murder was carried out

murder weapon

murders up

muscle operative

naked body found

narco air route

narco submarine

narco terrorism

narco-trafficking

narco-trafficking route (s)

narco-trafficking route (s) Africa

narco-trafficking route (s) Asia

narco-trafficking route (s) Europe

narco-trafficking route (s) Latin America

narco-trafficking route (s) Mexico

no arrest (s) have been made

no evidence

no evidence of shooting

no evidence of a shooting

number of violent crimes

oil smuggling

oil thieves

on a bus carrying

one of the hostages

ongoing investigation (s)

opened a criminal investigation

opened fire

operating illegally in

operating in

opium bride (Afghanistan)

opium harvest (Mexico)

opium smuggling routes

order to kill

ordered assassination

ordered at gunpoint

ordered from a vehicle at gunpoint

organ harvesting

organized crime

organized crime groups

organized criminal groups

packages disguised as

packages disguised as rocks

parole violation

patrol

patrol car

patrolling at night

pedo – pedophile

pending indictment

people smuggling

people were shot

people were wounded

perpetrators

person responsible

personal belongings seized

pharmaceutical crime (INTERPOL)

pharmaceutical drug cartel

pharmaceutical drug crime ring

pharmaceutical drug ring

pill mill

placed on lockdown

poaching

poaching sea bladders

polar bear hunting

poppy farmer

poppy harvest

poses as

pre-incident indicators (PINS) of violence

precursor chemicals

precursor chemicals seized

potential criminality

preliminary signs of foul play

pretending to be a teen

prison break

prison escape

prison walls

prisoner attacks

prisoner swap

probation violation

probe launched

profits finance drug gangs

prosecutors also charged

protection money

purple notice (INTERPOL)

radioactive theft

ransom

ransom demand

ransom paid

remains jailed

reported suspicious activity

reports of a fight

reports of fighting

reports of continued gunfire

responded to an add on craigslist

responded to an add posted on the internet

revenge attack (s)

reverse prostitution sting

reverse sting

riddled with bullet holes

rise in heroin use

risk of kidnap

robbed

robbed at gunpoint

robbers fatally shot

robbers were armed

robbery

robbery suspect arrested

robbery suspect detained

robbing a convenience store

robbing a gas station

rock assault

Romas – Bulgarian gypsies

roving bands

Russian crime gang

scheme to steal

secret court

secret room

seized drugs

seizure of

seizure of drugs

seizure of pharmaceuticals

seizure of plants

send lewd photos

sentencing date not announced

sentencing date postponed

serial killer

serial rapist

served in prison

serving fronts for prostitution

severed fingers crime

sex crimes involving children

sex offender

sex offender arrested

sex offender detained

sex offender registration

sextortion

sexual assault

sexual assault patterns

sexual crime (s)

sexual exploitation

sexual offense

sexual predator (s)

sexual servitude

shooting

shooting arrest

shooting suspect

shooting suspect barricaded

shooting on the rise

shootings up percent

shot dead

shot by

shot outside

shot suspect after

shot to death

shots fired

SIM card theft

skirting the law

smuggled

smuggled alcohol

smuggled cigarettes

smuggled goods

smuggled in

smuggled into

smuggled into the U.S.

smuggler (s)

smuggler (s) arrested

smuggler (s) detained

smuggling

smuggling business

smuggling corridor

smuggling imports

smuggling inside her body

smuggling inside his body

smuggling routes

smuggling routes from

smuggling syndicate

smuggling travel networks

sold credit card data

sold illegally

spike in crime

spike in kidnappings

spotted wearing

squatter arrested

stole a

stolen billfold

stolen car (s)

stolen documents

stolen drugs

stolen goods

stolen gun

stolen vehicle

stolen wallet

started shooting

sting operation

standoff

strapped a cord around neck

street reversal prostitution

string of burglaries

student abducted

subdued by bystanders

subdued by police

sucker punch

summary execution

suspect admits

suspect (s) arrested

suspect (s) detained

suspect (s) injured

suspect (s) killed

suspect (s) opened fire

suspect (s) opens fire

suspect (s) shot

suspected accomplices

suspicious device

suspicious visits

system to assess risk (STAR) (FBI)

tampered with

target for kidnappers

target for poachers – ivory

target of investigation

targeted in attack (s)

targeted killings

targeting dozens of victims

targeting journalists

targeting local

targets of predators

teenager (s) arrested

teenager (s) detained

tested positive for (drug (s))

the FBI shut down

threatened with gun

threatening message

threatening messages

threatened to kill family

threatened to kill her

threatened to kill him

threatened to kill his family

threatened to kill the hostage (s)

threatening post (s)

tied her hands

tied his hands

tied his/her hands with

tip database (crime)

trafficked children

trafficked women

trafficked workers

trafficking activities

trafficking bladder

trafficking group (s)

trafficking in children

trafficking in children and their organs

trafficking in human beings

trafficking in narcotic drugs

trafficking networks

trafficking in organ parts

trafficking in persons

trafficking proceeds

trafficking in women

tribal kidnapping (s)

tried and jailed

tried to destroy evidence

turns himself in

unconfirmed report of death

unjustified killing

unprovoked attack (s)

unprovoked fight (s)

under investigation for

undercover investigation

up tick in violent crime

utility equipment thieves

vandalism

vandal attempted

vandals sliced

vehicle attacks

vehicle ramming

vehicle rampages

vehicles set on fire

vehicular assault

veiled threat – crime

victims targeted

vigilante justice

violence against children

violence against women

vague death threat

violent crime

violent crime increasing

violent crime decreasing

violent person

violent series of attacks

violent struggle

visual evidence

warrant

warrantless arrest

warrantless wiretap

warrantless wiretapping

was arrested

was arrested on

was arrested today

was detained by

was found

was found in

was found next to

was kidnapped

was killed

was killed in the shooting

was placed on probation

was punched

was shot at

was slain

was targeted

weekend shooting (s)

went into lockdown

wildlife smuggling

woman charge in

woman charged with

woman found dead

woman missing

woman murdered

woman not guilty

woman raped

women targeted in attacks

worker exploitation

workplace violence

would be robbers

wounded in a shooting

wounded in stabbing attack

wrongful death lawsuit

CRIME – FINANCIAL, FRAUD, SCAMS, WHITE-COLLAR

allegation (s) of fraud

allegedly stole proceeds

anti-money laundering

artful dodger

ATM cloning device (s)

ATM cloning device found at

authenticity of the document

bag men (bag man)

baits customer

black dollar scam

black market

black market documents

black market exploits

black money

black money scam

brand protection

bribery

bribery extortion

bribery scandal

bribery scheme

bride scam (s)

business scam

cargo

cargo extortion

charged with wire fraud

charged with money laundering

charges of embezzling

checkered past

consumer defrauded

consumption girl scam

corporate fraud

corporate tax haven

corruption

corruption is widespread

counterfeit

counterfeit alcohol

counterfeit and illegal goods

counterfeit and illegal products

counterfeit baseball caps

counterfeit cigarettes

counterfeit clothes

counterfeit consumer products

counterfeit currency

counterfeit detection

counterfeit document

counterfeit drugs

counterfeit DVD’s

counterfeit golf merchandise

counterfeit goods

counterfeit goods from China

counterfeit goods seized

counterfeit insulin

counterfeit insulin needles

counterfeit insulin pens

counterfeit medications

counterfeit merchandise

counterfeit money

counterfeit movies

counterfeit purses

counterfeit toys

counterfeit watches

craigslist scam (s)

criminal scam (s)

disability fraud

disability scam

document fraud

ethical breach

extortion scam

Facebook scam

fake birth certificate

fake degree

fake document (s)

fake driving licence (UK)

fake drugs

fake goods

fake id

fake id’s

fake IRS agents

fake logos

fake merchandise

fake passport industry

fake pharmaceuticals

fake websites

false documents

financial crime (s)

financial crisis

financial fraud

foreclosure fraud

Foreign Corrupt Practices Act

forged check

forged document (s)

forged passport

forger

fraud

fraud committed

fraud detection

fraud investigation scam

fraud misconduct

fraud monitoring

fraud prevention unit

fraud warning

fraudster

fraudulent

fraudulent identity

fraudulent tax return (s)

fraudulently obtained documents

front companies

funny money

graft

grey market

health care fraud

identity fraud

identity theft

identifying suspicious transactions

illegal documents

illegal tax evasion

illegal trade

illegal trade of goods

illegal transaction (s)

illicit proceeds

illicit financial flows

immigration scam

import export pricing fraud

imposter

informal financial network

informal trade

insider threats

jailed for fraud

knockoffs

laundering money

laundering the proceeds

loan fraud

loan scheme

love scam

medical fraud

medical identity theft

money launderer

money laundering techniques

offshore account (s)

online rental scam (s)

online scam (s)

online security

online security threat (s)

online surveillance

operating as a front

pay to stay vacation scam

pension fraud

pension scam

pharmaceutical fraud

photo substitution

photo substitution passport

ponzi scheme

ponzi schemer

possible scam

potential scam

product extortion

records stolen

rental car scam

romance scam

Russian bride scam (s)

scam advisory

scam alert

scam baiting

scam buster

scammer (s)

stolen identity refund scam

structured transactions

structuring (IRS)

suspicious transactions

swept up in the scam

targeted for extortion

tax avoidance

tax avoidance strategy

tax evasion

tax fraud

tax haven

tax refund fraud

tax refund scam

tax scam

tax scams

tax season scams

telephone scam (s)

theft of funds

threatening post (s)

threatening post (s) on Craigslist

threatening post (s) on Facebook

threatening post (s) on Twitter

ticket fraudsters

three card monte

timeshare fraud

trade based laundering

vicarious counterfeiters

wash wash scam

EXPLOSIVES

after a bomb exploded

alternative chemicals

amatol

ammonia nitrate

arsenal of homemade explosives

attack with car bombs

attempted to detonate

barrage

barrels packed with explosives

belt bomb

blast

blasting time fuse

blistering agents

blow up commuter train

bomb

bombardment

bomb detonated in

bomb attack at

bomb attack in

bomb blast

bomb blast in

bomb blast injures

bomb blast kills

bomb defuse

bomb exploded

bomb explodes

bomb explosion

bomb explosion in

bomb found

bomb found in

bomb making expert

bomb on board

bomb parts

bomb planted in

bomb plot

bomb plot thwarted bomb rocks

bomb run

bomb scare

bomb shelter

bomb site

bomb squad

bomb threat

bombing

bombing campaign

bombing victim

booby trap (s)

building explosion

C4 plastic explosive

call in bomb squad

car bomb attack

car bomb exploded

car bomb explodes

car bomb kills

car bombing attempt

chemical time bomb

claims responsibility for bomb attack

clear landmine (s)

cluster bomb (s)

cobalt – 60

complex bombed

cylinder bomb (s)

deadly blast

deadly explosion

detect IED

detonate (d)

detonate a bomb

detonate an explosive device

device discovered in

device exploded

device found

device found in

device was discovered in

did not detonate

dirty bomb

DNDO (Domestic Nuclear Detection Office

domestic nuclear detection

dynamite

encased in metal piping

errant bomb

explosion

explosion in

explosion near mosque (s)

explosion occurred

explosion outside consulate

explosion outside embassy

explosion outside U.S. consulate

explosion outside U.S. embassy

explosion proof

explosion reported in

explosion shakes

explosive (s)

explosive breach

explosive charge

explosive detective engineering

explosive device

explosive ordnance

explosive precursors

explosive storage magazines

explosive weapon (s)

explosives stolen

explosives threat

explosives went off near

false bomb threat

fertilizer

fertilizer bomb

fire bomb

grenade launched

grenade like explosive

hand grenade (s)

hand grenade (s) found

homemade bomb (s)

homemade chemical bomb incident (s)

homemade grenade

huge explosion

improvised explosive device (s)

improvised explosive material (s)

injured in bomb explosion

investigating a suspicious device

knapsack bomb (s)

land mine

laser guided bomb

launch explosives

launch tube

letter bomb

loaded explosives

loud explosion (s)

mail bomb

manufacturing explosives

nuclear agenda

nuclear arms race

nuclear arsenal

nuclear bunker

nuclear detection system

nuclear domino effect

nuclear negotiations

nuclear posture

nuclear program

nuclear strategy

nuclear talks

nuclear test

nuclear weapon ballistic missile

nuclear weapons

package bomb

packed with explosives

parcel bomb

petrol bomb

pipe bomb

pipe bomb plot

pipe packed with explosives

pipe packed with firework explosives

placed near a police station

planning car bomb attack

planted a bomb

planted bomb (s)

planted explosive devices

planted explosives

planted several explosive devices

potassium chlorate (bombs)

potential bomb chemical

purchased high explosives

radio-activated improvised explosive devices

series of explosions

setting off a car bomb outside

small explosives went off

sprengel explosive

stolen explosives

stolen iridium

tantalum – chemical

telephone bomb threat

tear-gas bomb

thorium – chemical

underwear bomb

vehicles laden with explosives

was bombed

FIRE

acres burned

aerial canopy

aerial firefighting

air tanker

anchor point

arson

arson attack

arson suspect

arson suspect arrested

arson suspected

arsonist arrested

arsonist detained

arson suspect arrested

backfire

bambi bucket

barricade

base

berm

blackline

blowup

booster pump

booster reel

brush fire

brush fire broke out

brush blade

brush hook

brush truck

building fire

bump up – fire

burn

burning

burning cars

burning flesh

burning index

burning period

burn out

bushfire

candle

cause and origin of fire

cause of fire

charred

confine a fire

contain a fire

control line

controlled burn

creeping fire

crew (s) fight fire

crown out

dead man zone

dead out

deliberate brushfire (s)

demob

dozer line

drip torch

duff

engine – fire

engine crew

escape (d) fire

extended attack – fire

fire

fire at chemical plant

fire behavior

fire broke-out

fire camp

fire cycle

fire danger

fire destroyed a

fire displaces

fire ecology

fire edge

fire fighting foam

fire in

fire in plant

fire is under control

fire lookout

fire lookout tower

fire officials suspected

fire officials suspected

fire risk

fire rockets

fire shelter

fire starters

fire threat

fire trail

firebombs

firebreak

fire line

fire storm

flanks of fire

flare up

flash fuels

flashover

forest fire

head of a fire

hot spot

hotshot crews

huge fire

ignition and fuel sources of the fire

incident command system (ICS)

infrared detector

interface zone

knock down fire

large fire

large-scale fire

let burn policy

logging debris

logging slash

lookouts (fire)

mop-up-fire

National Fire Protection Association (NFPA)

National Interagency Fire Center (NIFC)

national wildfire coordinating group

origin of the fire

palmer drought severity index (PDL)

prescribed burn (s)

responded to a report of a fire

smoldering

sources of the fire

torching

wildfire

wildland fire

GEOGRAPHY

affected areas

along the bank

along the river

along the road

area of operation

areas for hiding

areas for hiding hostages

as night fell

atoll

blanket the region

changing their locations

closed area

coastal town

cultural threat

deep in the jungle

erosion

flood zone

geographic data

geographic diffusion

geographic distance

geomapping

geo hazard

high risk area

high standing water

isolated area (s)

key transit point

large area

large area of the country

large area of the state

located in an area

location

remote location

remote desert area

remote region

route between the countries

secluded location

shift in human geography

shifting their locations

sparsely populated areas

suspected active faults

treacherous terrain

underground site

urban terrain

HEALTH/INJURIES/MEDICAL/PUBLIC HEALTH

acute symptoms

acute symptoms from radiation exposure

affects drinking water

an autopsy conducted

an autopsy will be performed

animal health emergency

animal infected with

antibiotic resistant bacteria

autopsy results

baby death

baby’s death

bacterial infections

bacterial outbreak

biohazard

biological hazard

biological research

biosecurity

bird flu

bird flu threat

bloody

bloody assault

blow to the head

blow to the ribs

botched procedure

botulism

bouts of vomiting and diarrhea

bruises to the head

cadaver

cadaver bags

cases of

cases of measles

cases of measles in

cases of police brutality

cases of polio

cases of syphilis

cases reported

cash courier

cash dealer

cash stolen

cause of death

cause serious injury or death

CDC current outbreak list

CDC watch list

Center for Disease Control (CDC)

chagas disease

chik virus

chronic disease (s)

claims life

claims more than 100 lives

claims more than 1000 lives

concussion

confirmed case

confirmed case of

confirmed cases

confusion

contagion

contagious viruses

contaminated

contaminated drinking water

contaminated food

contaminated water

corona virus

coroner says

coroner’s autopsy

corpse

CRE – infection – Carbapenem resistant Enterobacteriaceae

critical condition

critically hurt

current outbreak

cyanide poisoning

cyclospora

cyclospora outbreak

dead

dead at a local hospital

dead bodies

dead child

dead children

dead in car

dead in home

dead man

dead men

dead people

dead persons

dead woman

dead women

deadly bacterium

deadly disease

deadly infectious disease

death

deaths

declared outbreak

dengue

dengue fever

dengue outbreak

diagnosed with the disease

died as a result of injuries

died from complications

died suddenly

dies after

dies after near drowning in

dies after nearly drowning

disease control

disease outbreak

dozens hurt

dozens injured

dozens killed

drug resistant bacteria

drug shortages

easily spread

Ebola watch list

emerging deadly virus (es)

emerging disease

emerging infection

emerging infectious diseases

endemic

epidemic

epidemic diseases

epidemiological bulletin

fake medicines

fatal attack

fatal bear attacks

fatal crash

filariasis

flu deaths

flu outbreak (s)

food borne disease outbreak

food borne illness

foreign animal disease

found alive

found deceased

found dead

found dead in car

fungal outbreak

gastrointestinal outbreak

genetic fingerprints

gunshot wounds

gunshot wounds to

gunshot wounds to the head   H1NN flu – swine flu

H6N1 flu

had obvious signs of trauma

have yet to identify the body

health crisis

health hazard

health impact assessment

health ministry

health officials report

high bacteria counts in the water

high death rate

HIV outbreak

HIV pandemic

hot car death

human sewage

human to animal

human to human

hundreds infected

hurt critically

hurt seriously

immune system

in serious condition

increase in illness (es)

infection

infection and transmission

infection rate high

infection rate low

infectious disease (s)

influenza

influenza outbreak (s)

influenza surveillance report

injured

injured after

injured after attack

injured by gunfire

injured in accident

injured boy

injured child

injured girl

injured man

injured person

injured woman

injured were taken

injured while fleeing

injuries on the body

injuries on the back of the body

injuries on the side of the body

injuries recorded

injury sustained

injury to the head

injury to the skull

jelly fish sting

jumped from balcony

ketamine

kill (ed)

kill hundreds of thousands of people

killed after

killed after militant attack

killed in

killed people

kills __ percent of those infected

lacerations

lacerations to the body

lacerations to the throat

large number of cases

life threatening

majority of cases imported

malaria outbreak

malaria deaths

medical threat assessment

medicine shortage

meningitis outbreak

MERS

microbial threat (s)

more deaths

more dogs infected

more flu deaths

more humans infected

more people infected

mother killed

multiple deaths

multiple fatalities

multiple injuries

multiple injuries reported

multiple stab wounds

new cases reported

no fatalities

no injuries reported

no obvious signs of trauma

no one was injured

no one was killed

no preliminary signs of foul play

no signs of trauma found on body

norovirus

norovirus outbreak (s)

number of new infections has risen

obvious signs of trauma

outbreak declared

over prescribe antibiotics

pandemic

pestilence

plague (s)

possible salmonella contamination

presumed dead

propagates the disease

propagates the disease

public health

public health alert

public health announcement

pulmonary irritants

recent outbreak (s)

recovery of body

reported cases

reported cases of

reported cases of dengue

reported cases of Ebola

reported cases of flu

reported cases of influenza

reported cases of malaria

reported emergence of

reported influenza case (s)

resistant tuberculosis

SARS

severe drought

severe infection

sexually transmitted disease (s)

sharp force injuries

shot and killed himself

source of the virus

spread of infection

spread of infectious disease (s)

stab

stab wound (s)

stabbed in neck

stabbed in the face

stabbed in the head

stabbed to death

stabbed with screwdriver

stabbing attack

stabbing death (s)

sudden death

super bug (s)

TB epidemic

treatment protocol

tremor

tuberculosis case confirmed

tuberculosis outbreak

victims infected with

victims infected with herpes

victims of poisoning

viral infection

virus in bloodstream

warned against possible spread of

warned against possible spread of H1N1 (influenza, flu)

weakened immune system

weakened immune systems from disease

widespread flu

widespread illness

wounded after

wounded in

INTELLIGENCE, ESPIONAGE

asked for intelligence

avoid detection

background notes

big data

black bag job

black bag operation

black chamber – NSA

black op

black operation

China’s intelligence service

Chinese spies

CI center – think tank

CI team (competitive intelligence)

CIA internal review

CIA operated drone program

clandestine

clear evidence

code word (s)

collect intelligence

collective intelligence framework

conduct covert operation (s)

conduct surveillance

continued to monitor activities

corporate espionage

counter surveillance techniques

covert action

covert activities

covert activity

covert

covert tradecraft

covertly

DARPA

deception

deception practices

declassified

declassified information

defense intelligence agency (DIA)

digital surveillance

digital wiretap

domestic intelligence

domestic spies

eavesdropping

echelon system

electronic surveillance

encrypt

encryption

encryption techniques

financial intelligence units

flawed intelligence

foreign intelligence operative

Foreign Intelligence Surveillance ACT (FISA)

foreign intelligence surveillance court

foreign spies

global surveillance

government code name

illegal spying

illegal surveillance

illegal taps

industrial espionage

infiltrate

infiltrated

infiltrator tradecraft

information sharing

information of sharing

in-Q-tel – Venture arm of CIA

intelligence agency mossad

intelligence agencies

intelligence brief

intelligence briefing

intelligence bulletin

intelligence collection

intelligence community

intelligence complex

intelligence database

intelligence failures

intelligence interrogation

intelligence operative (s)

intelligence partners

intelligence received

intelligence report

intelligence sharing

intelligence target

intelligence threat facing

intercept calls

intercept data

intercept of group

intercept phone calls

intercepted phone calls

internal corporate sabotage

kill list

may have been compromised

monitor communications

monitor movement

National Clandestine Service (NCS) (CIA)

no credible intelligence

NSA cell phone location tracking program

NSA collection program

NSA intercept

NSA tradecraft

obtained images from cameras

obtained images from surveillance cameras

open source intelligence gathering

operational operative

operative

OSINT news

overseas listening posts

passed classified information

phone records

plant disinformation

planting stories

preserve anonymity

prism surveillance program

project prophecy CIA

raw intelligence

records compromised

remain (s) classified

roving wire tap (s)

scenario

scenario analysis

scenario planning

secret equipment

secret surveillance network

secret facility

security clearance

senior intelligence officer (SIO)

senior intelligence official

sensitive information

sensitive security information

shadow networks

sleeper tradecraft

spy agencies

stealth technology

surveillance

surveillance camera

surveillance network

surveillance program

surveillance technology

surveillance tools

targeted surveillance

time sensitive intelligence

top-secret documentation

trove of classified records

trove of records

trying to recruit agents

United States intelligence community

unclassified document (s)

Utah Data Center – foreign data center

was compromised

MARITIME

access to port

arrivals by sea

boat capsized

boating accident

Coast Guard reported

counter-piracy

counter piracy mission

disputed waters

drowns in boating accident

Global Maritime Distress and Safety System (GMDSS)

law of the sea

maritime crime (s)

maritime disaster (s)

maritime domain

maritime domain awareness (MDA)

maritime expansionism

maritime headquarters

maritime security

maritime security threat

pirate tactics

pirate zone

pirates attack vessel

pirates attack vessels

port security

ship capsized

ship carrying weapons

ship righted

Somali pirates

storm capsized

submarine

submarine sighting (s)

swarm boats

taking on water

trapped in boat

MILITARY/WAR/COMBAT/CONFLICT

act of war

active mission (s)

adaptive tactics

adversary organizations

adversary’s vulnerabilities

aerial bombardment

Afghan lily pad

after clash between

aggression

aggressor

air campaign

air sea battle

alliance

allied foreign troops

allies

anticipate a bombing attempt

armed combat

armed conflict

armed forces

armed forces deployed

armed insurgence

armed insurgency

armed rebel coalition

armed rebellion

armed rebels

army checkpoint

Army Operations Special Command

armored vehicle

army

army prepares

army preparing

army preparing to

army thwarts attack

assailants dressed in military uniforms

Asia – Pacific security

asymmetric challenge

asymmetric warfare

asymmetrical warfare

attack imminent

attack on checkpoint in

attack (s) on American soil

attack plot

attack on U.S. soil

attacks (ed) NATO convoy

attacks on

battalion

battle

battlefield

begins ground offensive

behind attack

behind enemy lines

botched mission

botched rescue

brink of civil war

brink of war

bunker

call to arms

called the attack a vindication

called the attacks a product of

camouflage

capability to attack

carnage

carry out an attack

carry out an attack against the United States

carry out another attack

casualty figures

cease fire breach (es)

cease fire extended

chemical warfare plant

China’s military strategy

Chinese aggression

Chinese military movement

civilian abducted

civilian population

civilian target (s)

clandestine attack

clandestine military base

clandestine military operations

clandestine U.S. Army camp

clandestine warfare

clans

clash

clashes

clashes in

clashes occurring in

closer to military action

combat

combat zone deployment

combined forces

command

command center

commando forces India

commandos

concentration

concentration of camps

concentration of troops

conflict

conflict between

conflict could spark

conflict zone

conflict zone widening

conflicts across the Middle East

conquer

contested areas

continued attack (s)

continued gunfire

conventional military forces

coordinate (s)

coordinated attacks

corps

counterattack

counterattack launched

countermand

coveOps – military

covert battlefield

covert military action

covert ops

cyber war

cyber warfare

day after the attack

daytime assault

dead after clash between

deadly campaign

deadly casualties

deadly training missions

death toll

declare war

defense capabilities

degrade and destroy

demobilization

department of defense

department of defense report

deploy

deploy (ed) troops

deployed in conjunction

deployed soldiers

deployment

deployment of military

deployment of troops

deployment of soldiers

devastating attacks

died in the attack

direct attack

direct hit

direct military action

dirty war

dispatched marines

dispatched troops

diversion tactics

diversionary tactics

DOD statement

drawdown

elite fighting squads

elite paramilitary unit

elite units

ends military operation in

enemy combatants

enemy’s vulnerabilities

ethnic cleansing

existing military pacts

face-to-face combat

failed mission

false flag attack (s)

false flag (covert action)

fewer causalities

fight with militant group

fighters ambushed

fighters from

fighting force

fleeing internal violence

foiled attack

fog of war

foot soldier

force concentration

force deployment

force protection level

forces shelled

foreign conflict

Foreign Emergency Support Team (FEST)

foreign fighters foreign military sales

foreign military sales program (programme)

foreign troops targeted

French security forces

global conflict (s)

green zone

ground fire

ground invasion

green on blue attack

ground incursion

guards have found

guerrilla forces

guerrillas bombed

guerrillas killed

heavier than normal security

heavily armed

heavily armed men

heavily armed ships

heavily armed soldiers

heavily armed police

heavily fortified

heavy gunfire

high readiness force (HRF)

high value target

hundreds of casualties

hundreds of civilians

hundreds of civilian casualties

illegal armed group (s)

imminent attack

in the months prior to the attack

increase in attacks

increased military aid

increased military presence

increased Russian military activity

initial attack

insurgency

insurrection

intense conflict

intense fighting

invasion

irregular warfare

kamikaze attack

killed in air strike

laid ambush

large number of soldiers

latest attack

launch (ed) attack

launched military raid

launched ground offensive

launches air strikes in

launches ground offensive

lily pad strategy

low-intensity conflict

massive military movement

massive show of force

military

military accord

military asset (s)

military base (s)

military camp (s)

military concentration camps

military conditions

military convoy

military cooperation

military deployment

military detention

military exercise

military experts

military forces moved into

military industrial complex

military installations

military information

military intervention

military movement (s)

military plan (s)

military preparation (s)

military presence

military presence in Africa

military regime

military strategy

military tactics

military threat

military track terrorists

military weakness

military’s actions

militia

mobilization and deployment

modern warfare

mortar attack (s)

mortar attacks from

mountain hideout (s)

moved troops and weapons

NATO transformation seminar

nature of warfare

naval operations near

near the conflict zone

neo-paramilitary

new attack (s) inevitable

night raid (s)

objective of damaging – military activity or attack

occupation forces

office of naval research

oil spot strategy (military)

ongoing conflict

operation Jade Helm 15

ordered military airstrike

ordered military air strike

overseas conflict (s)

PAK army – Pakistan army

paper tiger

Pentagon alert

Pentagon set to

planning a series of attacks

planned attacks on

planning attacks

planning attacks on

presence of civilians

prevention of a military attack

prevention of an attack

previous attacks on

privatized military companies

probable method of attack

rapid reaction force

reactive military action

rebel attack (s)

rebel attacks claim more than one hundred lives

rebel group (s)

rebel held territory (ies)

rebel position (s)

recent attacks

reconnaissance exercise

regional nuclear war

reign of terror

related attack (s)

remote controlled warfare

renewed their offensive

reprisal attack (s)

responded with air strikes

responsible for numerous attacks

retaliatory attack (s)

retaliatory measures

roadblock military

Russian bombers

Russian military aggression

Russian military operations

secretive special forces

sectarian violence

secret Iranian facility

secret Iranian nuclear facility

secret Iranian nuke facility

secret fifth column

secret military base (s)

secret military intelligence report (s)

secret tribunal

secretive special forces

sectarian war

security forces

security forces pulled out

security forces sealed off

security operative (s)

seized by opposition forces

seizing the town of

selling military secrets

series of attacks

show of force

siege

significant attack

sniper

sniper assault

sniper attack (s)

sniper operations

sniper shot

soldier injured

soldier killed

soldier shot

soldier shot dead

sorties against targets

special ops

sporadic clashes

stage attack

strategic early warning

strategic mission list

strikes militant positions

support operations

tactical advantage (s)

tactical alliance

tactical operators

tactical team

tactics revealed

target hardening

target soft spots

target soldiers online

targeted assaults

targeted attack

targeted attacks

targeted attack campaign

temporary mission facility

the attack on

troop levels

troop withdrawal (s)

troops gathered

troops along the border

troops dispatched

troops fire

troops fired at

troops on alert

troops stormed

unconfirmed report of attack (s)

under siege

underground military site (s)

Uppsala Conflict Data Program (UCDP)

urban combat

urban warfare

urban warfare techniques

U.S. air strike

U.S. covert action

violent clashes

violent clashes across

violent clashes between

violent clashes in

violent clashes near

war breaks out

war propaganda

war torn

warfare

was killed in the attack

were deployed

wounded in action

POLICE/LAW ENFORCEMENT

agency

agency outpost

AMERIPOL

anti-human trafficking police

ATF

ATF International Response Team

avoid law enforcement

campus police

conducted an operation

conducted raids

cops

cops injured

crisis negotiation unit (FBI)

crisis negotiator

deployment of police

detectives are continuing to investigate

dressed in black tactical gear

Europol

fatal police shooting (s)

FBI

FBI agents arrested

former agents charged

former detective

French gendarmes

in custody death

increased policing

increased push by law enforcement

Interpol

Interpol alert

Interpol arrest

joint investigation

JPATS – U.S. Marshals

killed by police

law enforcement

law enforcement agency (ies)

law enforcement target

lay in wait for the police

officer involved shooting (s)

officer dispatched

officers responded

other law enforcement agencies

police

police alert (ed)

police alerted to a bomb

police are investigating a shooting

police are treating the case as

police barricade

police break up

police clash with

police clash with protesters

police crackdown on

police conducted an operation

police conducted wiretaps

police destroy

police have not ruled out foul play

police hunt for

police injured

police investigate

police investigate assault

police investigate murder

police investigate robbery

police investigate shooting

police investigate terrorist activity

police investigation

police investigating

police officer accused of

police officer arrested

police officer fired

police officer injured

police officer killed

police officer shot

police officer shooting

police officer shot man dead

police opened fire

police patrol deployment

police perimeter

police presence

police recover

police reports indicate

police responded

police report activity

police report suspicious activity

police responded to a report of a

police said the body is that of a

police search for

police shooting

police station attacked

police stormed site

police source (s)

police sued

police tactics

records from department of safety

red cell team

roadblock police

sheriff’s officials say

slain officer

slaying of police

special response team (SRT)

special task force

SWAT

SWAT response

SWAT team

targeted police operations

targeting police

task force

task force agent

U.S. Marshall (s)

use of force

POLITICS/DIPLOMACY/SAFETY SECURITY

Assistant Secretary of State for Diplomatic Security

asylum

asylum seekers

attempted assassination

attempted coup

attempts to assassinate

authoritarian regime

barricades outside U.S. embassy

bilateral accord

bilateral negotiations

bombings of US embassies

breach of trust

bribery of government officials

bureaucratic resistance

campaign

changing world

Chinese foreign minister

closure extended embassy

closure extended consulate

coalition

consulate

consulate attacked

consulate attacked in

consulate bombed

consulate bombings

consulate closed

consulate closure (s)

consulate evacuated

consulate evacuated

consulate threat

consular database

corrupt officials

counterpart

counterparts

coup attempt

coup attempt failed

coup d’ état

coup plotters

coup plotters arrested

coup risk

covert relations

dangerous foreign policy

delicate talks

demonstration (s) against the coup

destabilize a region

destabilize government

diplomatic asset

diplomatic efforts

diplomatic immunity

diplomatic incident

diplomatic mission

diplomatic post

diplomatic security

dissident (s)

dual mandates

embassy

embassy attacked

embassy attacked in

embassy bombing (s)

embassy breach (ed)

embassy closed

embassy closure (s)

embassy evacuated

embassy halts service

embassy has received unconfirmed threat (s)

embassy implemented enhanced security

embassy shuts down

embassy threat

embassy warning

emerging new state

exile (s)

exiled groups

external subversion

extreme far-right movement (s)

failed coup attempt

failed state

fascist idealism

following the coup

freely elected president

further ideological objectives

further political objectives

further religious objectives

geopolitical

geopolitical climate

geopolitical upheaval

govern by decree

government backed militias

government opponents

growing international isolation

hard-line regime

hard sell

high threat post (s)

high value individual (s)

highest level of government

historic talks

hitlerite (s)

hold secret meetings

host country

host government

ideologue

ideological clashes

imposed economic measures against

imposed martial law

imposed sanctions

including diplomats

increasing tensions between the two countries

internal conflict

international sanctions

isolated state

Israeli prime minister

leaked cable

leaked document leaked documents

leaked official document

leaked official documents

loose coalitions

major donor

minority political group (s)

movement leader

mudslinging

multinational effort

national security

national unity

neighboring countries

new sanctions

new surveillance law

non-lethal aid

non-state actors

non-state groups

opposition

opposition faction (s)

opposition movement

outside U.S. consulate

peace talks

Pinocchio test – whopper

plot to destabilize the government

policy toward North Korea

political conflict

political controversy

political crisis

political deadlock

political figures

political instability

political retribution

political retribution against

political scandal

political strike

political terrorism

political unrest

political intrigue

political upheaval

political weapon

politics of fear

prime minister said

prior to the assassination

propagandist

protest outside U.S. consulate

protest outside U.S. embassy

relations between Seoul and Beijing

repressive regime

right-wing violence

rogue state (s)

Russia warns

Russian aggression

sanctions against

sanctions against Iran

sanctions against Russia

sanctions imposed

seek refuge at embassy

seeking a consensus

shangri-la dialogue

siege consulate

siege embassy

sign (ed) a decree

State Department has advised Americans

State Department issues warning

strategic alliance (s)

strategic international studies

suspended peace talks

suspends consular services

tension with Beijing

tensions between the U.S. and China

tensions between the U.S. and Iran

tensions between the U.S. and North Korea

tensions between the U.S.

tensions between the U.S. and Soviet Union

territorial advances

territorial ambition

territorial dispute

territory claimed by

the far right

the restoration project – immigration

threat on president

treaty deadline

under the agreement

unilateral sanctions

U.S. embassy warning

wet foot, dry foot immigration

who was the adversary

PUBLIC SAFETY/ENVIRONMENT/INFRASTRUCTURE/SOCIAL UNREST

agriculture safety

agriculture threat

aid agencies

anarchy

anarchists

angry citizens

angry employees

angry locals

angry workers

anticoup demonstrators

anti-foreigner violence

anti-government protesters

anti-immigrant

anti-immigrant marches

anti-Islam protests

anti-Islam protests turn violent

anti-Muslim attacks

anti-NATO activists

anti-NATO activists protest

anti-Semite incident(s)

anti-Semitism problem

Arab riot

attack against oil pipeline

attack on facilities

backup generator

backup systems failed

bank holiday

beach closed

beach closure

beach contaminated

bear attack

bears killed

biodiversity crime

black bloc

boil water advisory

boost security

bridge collapse

brown out

building closure

building damaged

building destroyed

building (s) evacuated

busiest intersections

California Earthquake Authority

capital controls

cargo disruption threats

cargo security

cargo security threats

cargo threat

cataclysm

catastrophic

catastrophic accident (s)

catastrophic circumstances

catastrophic incident (s)

catastrophic insurance

catastrophic natural disaster

caused multiple traffic accidents

causes heavy damage to

causing complete service interruptions

cautionary

cautious

cell phone services went dark

center for security policy

chain reaction crash

chaos

chemical facilities

chemical leak (s)

chemical plant fire

citizen

citizens not directly involved in a civil disorder

citywide lockdown

civilian deaths

civil disorder

civil disturbance

civil liberties

civil resistance

civil rights

civil rights violation

civil strife

civil unrest, riots

clamor

clear threat

clear warning

closure extended road (s)

collapse

communal riots

conservation cuts

conspiracy

conspiracy theory (ies)

conspire

contingency plan

constitute a danger

control measures

continuity of government (COG)

crash

crash involving

crash remains under investigation

crash scene

crash shuts down highway

crashed into

crashed into a vehicle

crisis

crisis averted

crisis continues

crisis mapping

critical

critical electric-grid sites

critical emergency

critical facilities

critical infrastructure

critical services

critical situation

critical stage

crop failure

crowd

crowd control

crowd of people

crowd seeding

crowd sourcing

current international crisis

current international law

current security situation

current security threat (s)

current security threat (s) and pattern (s)

current threat level (s)

cut off water supply

damage

danger

dangerous

dangerous behavior

dangerous encounters

dangerous situation

dangerously close

deadly flood (s)

deadly flooding

deadly unrest

death (s) to livestock

death (s) to wildlife

declared emergency

declared state of emergency

declares state of emergency

demand for ivory

demand for water

demonstration (s)

demonstration (s) against

dense rainforest – ivory

devaluation of currency

deteriorating security situation

detect threat (s)

devastating disaster

DHS – Department of Homeland Security

DHS assets

DHS blue program (human trafficking)

DHS annual threat assessment

dire threat

disaster (s)

disaster assistance

disaster management

disaster medical assistance team (DMAT)

disaster mitigation

disaster plan

disaster relief

disaster response

disaster response coordination

disaster response team

disaster unemployment assistance

disasters man made or natural

dispatch from

dispatched from

disrupted

domestic security

domestic threat

driven the unemployment rate

drivers fatigue

during a riot

early warning

early warning signs

early warning systems

ecocide

ecological risk

economic chaos

economic crisis

economic disruption

economic event

economic stagnation

economic threat

effect on agriculture

effect on population

electric grid

electric grid vulnerable

electric grid vulnerability

electric outage

electric substation

electrical substation

electromagnetic energy

elevated alert

elevated alert level

emergency action

emergency alert (s)

emergency and disaster information

emergency broadcast system

emergency command

emergency command post

emergency declared

emergency landing

emergency management

emergency phone number for the U.S. consulate

emergency phone number for the U.S. embassy

emergency preparedness

emergency protest

emergency protocol (s)

emergency relief

emergency report

emergency rescue

emergency response

emergency response team

emergency services

emergency services say

emergency session (s)

emergency warning (s)

emerging economy

emerging risks

emerging security threat

emerging threat

emerging threats

environmental emergency

epidemic of violence

equipment failure alarm

eradication efforts

escalate threat (s)

evacuated

evacuated after threat

evacuation (s)

evacuation center

evacuation (s) ordered

evacuation route (s)

event cancelled

event occurred in

everyman for himself

exercise caution

expanded recalls

experts warn of potential disaster

exploding global population

extends ban on

extends ban on sale of

extra security measures

extreme violence

extremist (s)

extremist exploitation

factories shut down

famine

FBI alert

financial threat

first responder

first responder network

first warning

flare mules – UK

flood barrier

flood barrier at risk

flood radar

flood stressed

food defense

food insecurity

food poisoning

food security

food shortage

food source

food supply

food supply risk

food tampering

forced work stoppage

forces evacuation

foreign rescue workers

foreign threat

frantic effort (s)

fueled anger and rage

full alert

further escalation

future unemployment

gang of looters

gas explosion

gas leak

gas like odor

gas line explosion

gasoline shortage

gathering threat

Geneva report

global distribution of food supplies

global food system

global hotspot

global warming

grave threat

growing civil unrest

greatest threat

growing concern

growing concerns over increase in

growing fears

ground water

ground zero

growing movement

growing problem (s)

growing tension (s)

growing tension (s) between

growing threat

growing threat increasing

growing trend

harmful goods

harvest down

harvest up

hate group

hazard (s)

hazard identification

hazard reduction

hazard warning

hazardous material (s)

HAZMAT

HAZMAT crews

HAZMAT crews respond

hazardous materials response team

heightened security concerns

high level of alert

high risk

high threat

hitting public services

Homeland Security

Homeland Security Advisory System

homeland threat

housing shortage

huge population growth

humanitarian aid

humanitarian threat

hundreds displaced

hundreds flee

hundreds of families

hundreds of individuals

hundreds of people

hundreds of thousands of people

imminent danger

imminent threat

impending disaster

improve security

incidence of civil disorder

incident response

incidents of violence

increase in security

increase in such attacks

industrial pollution

increase risk (s)

increase of risk (s)

increased security

industrial disaster

infrastructure attack

international alert system

interstate closed

interstate closed for several hours

intifada (uprising, social unrest, Arabic)

invasive species

investigate (s) contaminated water

issue an alert

issued distress call

issues alert

killed people worldwide

knocked out substation

lack of threats

latest threat

lead to civil unrest

leading to insecurity

local hostilities

long term outage

long term threat

looted from

looters

looting

loss phone service

low-lying dam

main security threat

main security threats

major disaster declared

major disaster declaration

major incident

major outage (s)

major risk (s)

major rockslide

major threat (s)

man-made disaster

man-made disasters

mandatory evacuation (s)

marred by violence

martial law declared

martial law preparation

mass casualties

mass casualties expected

mass civil breakdown

mass demonstrations

mass mobilization (s)

mass mobilization against

mass shooters

mass shooting (s)

mass stabbing

mass street demonstrations

mass surveillance

massive blackout

massive street protests

material and economic loss

minimize risk

mining hazard (s)

mitigate perceived threats

mitigate situation

mitigating the risk

mob killing

mob rule

month’s long protests

motion sensors

moved to safer ground

moved to safety

multiple looting

national disaster (s)

national emergency

national emergencies

National Incident Management System (NIMS)

national preparedness

national security

national security implications

national security interests

natural disaster (s)

natural hazard

no go zones

non-credible threat

obscure group

oil shipping facility

oil terminal

on alert

open civil war

outage

outage shut down

outage shut down critical services

outages

outbreak of civil unrest

outbreak of violence

pamphlets

pamphlets distributed

pamphlets dropped

people are fleeing

people arrested

people detained

people have been killed

people in camps

people injured

people killed in attack (s)

people taken to hospitals

people were killed

people were shot

people were wounded

personnel responded quickly

personnel responded quickly to the scene

petroleum reserves

phone service out

physical attack to grid

physical damage

physical damage assessment

pipeline

pipeline accident

pipeline attack

pipeline explosion

pipeline leak

pipeline shutdown

planning attacks on churches

planning attacks on civilians

popular protests

population expansion

population growth

population trend

pose an immediate threat

pose biggest threat

poses a threat

possible contamination

potential attack (s)

potential crisis

potential delays

potential security problem

potential target

potential threat

potentially dangerous

power blackouts

power grid

power lines damaged

power outage (s)

power outage (s) in

power sector

power shortage

power surge

prevention

prevention measures

preventive action

price hike (s)

prices have climbed

primary threat (s)

proclaimed a state of emergency

prompted red alert

prompts warning

protecting crowded places

protection measure (s)

protection measure for earthquake (s)

protection measure for landslide (s)

protection measure for damage to building (s) from cyclone (s)

protection measure from damage to buildings during floods

protection measures tsunami (s)

protective action

protest (s)

protest against

protest planned

protest (s) turned violent

protest (s) turns violent

protest (s) in

protest (s) quickly spread

protester arrest

protesters arrested

protester (s) attack (ed)

protesters burn flags

protesters raid

protests aimed at toppling

protests being planned

protests turned violent

provided material support

public emergency

public disaster

public security

public venues

punctured a pipeline

put on red alert

quell civil unrest

quell protests

quell riots

quickly deteriorate (d)

race riots

radiation accident

radiation alert

radical

radical group (s)

radioactive material (s)

radioactive material (s)

rally site protest

random attacks

random killings

real threats

rebellion

red alert

regional security

regional tension (s)

related threat

relief

relief aid

reported killed

reported lost

reported missing

reports of violence

reports of violence increase

rescue attempt

rescue mission

rescue scenario

rescue workers

rescuers were alerted

residents still without power

residents told to evacuate

residents told to leave

residents were without power

residents without electricity

residents without power

residents without water

response team

response to the threat

restricted area

restricted area access

resulting in more deaths

returnees

riot

riot broke out

riots break out

riots claim more than 100 lives

risk assessment

risk and threat assessment

risk identification

risk zone

road traffic safety

rolling blackout (s)

run on deposits

safety concern (s)

safety of customers

safety of infrastructure

satanic cult

scale of damages

school closure

school shooting (s)

scope of the threats

scores die

scores killed

search and rescue efforts

search effort (s)

search efforts called off

search team (s)

search team found

security

security breach

security challenge

security concerns

security details

security drill

security experts warn

security fears

security flaw

security grossly inadequate

security lapse

security services

security situation (s)

security team is investigating

security threat

security threats

security tightened

security to be stepped up

security warning (s)

seek evacuation

serious threat (s)

service reduction

services first went down around

severe damage

severe unemployment

severe inflation

severe threat

sewage contaminated water

sewage overflow

shelter in place order

shortage

shortage of supplies

shortage of medical supplies

shortage medical supplies

short term threat

shortages of commodities

significantly disrupted

small protests

social stability

social unrest in

sparks evacuation

specific threat (s)

specific threats Olympics

spillover violence

spiral of violence

spiraling violence

sporting events

static security

stagnant water

stark warning

state of emergency

stock exchange halts trading

suffered severe damages

suffers massive power outage

strikes and protests

subversive groups

supply chain security

surge in violence

survey the damage

suspicious situation (s)

tainted food

tainted with

taking a huge toll

tamper with U.S. infrastructure

tampering with public water system

targeted weaknesses (attack) (sabotage)

targeting critical infrastructure

technical hazard (s)

technical issue (s)

technological disaster

telephone services went dark

temporarily suspending services

tens of thousands have taken to the streets

thousands affected by outages

thousands flee

thousands infected

thousands of families flee

thousands stranded

threat matrix

the foreigners killed were

those most at risk

thousands protests

threat

threat actor (s)

threat against

threat alert

threat alert extended

threat assessment

threat assessment team

threat continues

threat environment

threat estimates

threat heightened

threat intelligence

threat landscape

threat level

threat level increased

threat level raised

threat of attack

threat of destabilization

threat perception (s)

threat potential

threat reporting

threat to airline

threat to American security

threat to national security

threat to critical infrastructure

threat to public safety

threat (s) to U.S.

threat to U.S. security

threat was linked

threats posed by

threats to national security

threaten (ed) to carry out further attacks

threaten (ed) to carry out more attacks

threatens strike

threats of violence

threats of violence prompts

threats to business

took control of oil field (s)

took refuge

took refuge in bomb shelter

toxic disaster

tragic accident

tragic event (s)

transmission line

transmission line attack

transmission substation (transformers)

transnational security issues

trapped in

trapped in building (s)

trapped in house (s)

trapped inside

trapped inside building (s)

trapped inside the house

types of threats

universal threat

unknown group

undisclosed location

undisclosed hotel

unconfirmed threat

updating security measures

uprising (s)

U.S. Department of Homeland Security Advisory Program

U.S. warns

U.S. warns citizens

U.S. warns of possible attacks

urban attack

urgent action

urgent security issue (s)

violence erupts

violence erupts in

violence escalates

violence grips

violence grows

violence has increased

violence is soaring

violence not over

violence outbreak

violence spreads

violent attacks caught on tape

violent campaign

violent demonstration

violent outbreak (s)

vulnerability assessment

vulnerable threats

vulnerable to

vulnerable to attack

vulnerable target (s)

vulnerability of the system

warning

warning issued

warning issued by law enforcement

warning message

warnings

water challenges

water contamination

water crisis

water cuts

water emergency

water main broke

water main ruptured

water poisoning

water shortage (s)

water supply

water supply and sanitation

water war

weakening economy

weakening global economy

widespread devastation

widespread looting

widespread outages

widespread panic

widespread power outages

widespread violence

worldwide threat (s)

worsening conditions

worsening events

worst case scenario

SOURCES, AUTHORITIES, OFFICIALS, SENTIMENT

according to an official from

according to court record (s)

according to information

according to  investigative reports

according to officials

according to opinion polls

according to sources

according to those familiar with the situation

acquiring information

agency recently disclosed

aid agencies report

alarming statement (s)

amid claims

amid confusion

amid heightened tensions

anguish

annihilate

announcement by the Justice Department

anti-American sentiment

anti-foreigner sentiment

anti-Japanese sentiment

as a matter of policy

authorities

authorities cracked down

authorities have determined

authorities issued an alert

authorities not release the names

authorities raided homes

authorities reported

authorities said

authorities say

authorities seize

authority

chief of mission authority

citing law enforcement sources

clandestine sources

confirmed report (s)

contacted authorities

credible

credible witness

credible eyewitness

credible report (s)

credible sighting (s)

credible source

credible threat

credible threats

credibility of alert (s)

current information suggests

current international news

data from studies show

decision making authority

declined comment

declined to speak publicly

deepens ties

detailed chronology

detailed information

dramatic increase in

drew record numbers

during a news conference

expressed misgivings

ethnic and religious tension

extensive investigation

fact finding mission

false narrative

fire department officials say

foia request

foreign affairs spokesman

further their cause

global report on

global spread

government officials confirmed

government spokesman

government spokesperson

government spokeswoman gravity of the situation

growing in strength

has warned

high level authorities

horrendous conditions

hostile environment

hostilities

ignited concerns

in a news release

in harms way

in recent days

in recent months

in recent years

in separate locations

in the same period

incidence

incident

incident is under investigation

incident occurred on

incident raising concerns

incident site

incidents in

indicators

initiation of hostilities

intent of group

intent to hurt

intrusive

invasive

investigation activities

investigation launched

investigation underway

investigative committee

investigative journalist

investigators reported their findings

is described as

issue came to a head

issues warning

journalist arrested

journalist detained

journalist freed

journalist injured

journalist kidnapped

journalist killed

journalist missing

journalist released

journalist reported

large number of

latest development

latest update

launches new efforts

leading cause of

leaked report

linked to

local media

local media reported

local sentiment

may spread

measures are a result

media account

media gag order

media reported

media said

most concentrated

motives

nature of threat

near certainty

nefarious reason

negotiations between

negotiations stalled

negotiations with

negotiations with Iran

news broke that

newsworthy

nexus between

no immediate claim of

not acknowledged publicly

nothing to indicate he was

official accounts

official not authorized to comment publicly

officials discussed

officials indicted

officials reported

officials suspected

officials vowed

optics

originated from

out of an abundance of caution

outlook deteriorating

overtly

particularly vulnerable

persistent

plagued by

position further weakened

potential adversaries

precarious situation

predictive analysis

predictive analytics

potential harm

potential risk

potential risks

presence continues to grow

present situation

prior to the attack

pronounced

public safety spokesman

public speeches

questions remain

quick action

quickly denounced

raises fear (s)

rebuked

received widespread attention

recent incident (s)

recent security incident (s)

released data today

released publicly

reliable source (s)

report filed with

relieved of duty (ies)

remains speculative

reports of

reports suggest

reveals that

rise in

rise in number of people

risk of escalation

rising tension (s)

security issues

security measure (s)

security measures revised

security worries grow

serious concern (s)

severely diminished

sharp increase in

sheriff’s officials say

shocking attack (s)

signs of trouble

situational awareness report

situation continues to deteriorate

situation deteriorating

situation in _ serious

small groups of people

sources close to

sources from the U.S. government

sources reported

sources said

speaking on condition of anonymity

special envoy

special rapporteur

spike in

spread terror

statement said

strategic plan

strategic presence in

suspicions sparked

suspicious activity (ies)

suspicious activity reports

suspicious attacks

suspicious behavior

suspicious event

sustained campaign against

systemic event – financial economy

systemic risk

talks involving two groups

targeted for

targeted the

team leader

tension (s) between

tensions have abated

tensions mount

tensions mount between

the announcement follows

the company said

there were indications

thinly veiled threat

throng of reporters

time bombs

time sensitive

time sensitive information

to deter

took control of

undisclosed report

uncertain future

unconfirmed report

unknown group

unsafe incident

unstable region

unusual conditions

UK threat level severe

UN concerns rises

UN warns of

U.S. interests

U.S. led coalition

up tick

urges attack

urges attack on U.S.

vocal critic

volatile area (s)

worldwide average

xenophobia

xenophobia has risen

xenophobia violence

yellow alert

youth violence

TERRORISM

abolish terrorist sanctuaries

act of terrorism

acts of terrorism perpetrated by

adaptive enemy (ies)

after the massacre

against U.S. interests

agro terrorism

al Queda

Al-Qaeda

al-Qa’ida

al-Qaeda affiliated grou

al-Qaeda attack

Al-Qaeda hit list

al-Qaeda leader

al-Qaeda linked group

Al-Qaeda seizes

al-Qaeda training camp

alleged members of terrorist group

alleged plot

anti-Muslim terrorism

anti-terrorism

anti-terrorism police

anti-US terrorism

Arab (s) attack

Arab attack on

armed terrorist (s)

assess terrorist threats

assessment of terrorist threats

atrocity

attacks on ISIS

attempted terrorist attack

beheaded

biological threats and terrorism

bioterror threat

bioterrorism

blasphemy police

bought by terrorists

breakaway faction

caliphate

captor

category A bioterrorism threat

cell

change of tactics

chatter

chatter indicating

Chechen female suicide bombers

child bomber

claims responsibility for attack

combat Islamic extremism

combat terrorism

compliance – terrorism

Computer Assisted Passenger Processing System (CAPPS) (counter-terrorism)

condemn all acts of terrorism

condemned attack

condemned terrorist attack

conduct Jihad

counter extremism

counter terrorism

counter terrorism database

counter terrorism effort

counter terrorism programs

counter terrorist operation

counter terrorist police

counter sponsored terrorism

continued violence in

culture of terrorism

cyber security and electronic terrorism

cyber terrorism

dabiq magazine (ISIS magazine)

Daesh – terrorist group ISIS

deadly

deadly attack (s)

decentralized terrorist movement

declaration of holy war

defeat counterterrorism measures

demolish a terrorist organization

detect terrorist activity

detonated suicide bombs

denied persons list (U.S.)

disrupt terrorist plans

domestic terrorism

domestic terrorist attacks

dormant operative

eco terrorism

eliminate al-Qaeda

emergence of small groups

environmental terrorism

expel militants

extremism

extremist group

extremist regime of the Taliban

face terror charges

failed suicide attack

family influenced violent extremist

fight against terrorism

financial backer of al-Qaeda operations

foreign insurgent

food terrorism

foreign terrorist organization

former terrorist

fringe group (s)

fundraising terrorism

German Muslim community

global counter terrorism

global terrorist network

global war on terrorism

growing Islamist extremism

GSG 9 – German counter-terrorism unit

Hamas attacks

Hamas flags

haven for Islamic terrorists

Hawala

Hawala banking

Hezbollah

Hezbollah operative

Hezbollah planning attacks

Hezbollah planning attacks on western targets

Hezbollah terror cell (s)

Hizb’ allah

Hizballah

Hizbollah

high ranking al-Qaeda official

home grown Jihadist

homegrown extremist (s)

homegrown ISIL attack (s)

homegrown ISIS attack (s)

home-grown terrorist

homegrown terrorist

homegrown violence

homegrown violent extremist

honor brigade

identify terrorists attempting to buy

identity of terrorist (s)

increased terrorism threat

infrastructure of terrorism

investigate terrorist activity

IRA factions

Iran supports

Iran supports Hamas

Iran supports Hezbollah

Iran supports ISIS

Iran supports terrorism

Iran threat assessment

Iran warns

Iran’s military aid

Iranian covert activities

Iranian covert operations

Iranian leaders

IS

IS forces

IS threat

ISIL

ISIS activity

ISIS affiliates

ISIS bombs

ISIS claims responsibility

ISIS gains ground

ISIS grains ground in

ISIS Islamic State

ISIS kill list

ISIS organization

ISIS threat

Islamic extremism

Islamic extremist activity

Islamic militant movement

Islamic militant (s)

Islamic State (ISIS)

Islamic state hacking division

Islamic state group

Islamic state militant group

Islamic suicide attack

Islamic terrorism

Islamist

Islamist faction

Islamist groups

Islamist insurgency

Islamist leaning separatist

Islamist militancy

Islamist target

Islamophobia

Israel bus attack

Jerusalem attack

Jewish targets

Jihadi attack

Jihadi organization

Jihadist

Jihadist cleric

Jihadist insurgents

Jihadist networks

Jihadist networks in

Jihadist rhetoric

Jihadist websites

known militants

Lebanon clan

Lebanese Venezuelan clan

leftist rebel group

lethality in terrorist attacks

linked to attack

logos used by insurgents, terrorists, paramilitary groups

London terror attack

lone terrorist

lone wolf attack (s)

lone wolf terrorists

lone wolves

man dead in suspected terrorist attack

mastermind

mastermind behind attack

mastermind of attack

material support for terrorists

militant

militant group

militant hideout

militant Islamist

militant mujahi movement

militant network

militant organization

militant ties

militant website (s)

militants threaten

militants threaten attack

monitor extremist activity

mosque assault

mosque attack

mosque attacked

mosque bombing plot

motorcycle attack

Muslim Brotherhood

Muslim cleric

Muslim community

Muslim immigrants

Muslim owned businesses

Muslim riots

Muslims are targeted

NCTC Information Sharing and Knowledge Development Directorate (ISKD)

near mosques

neo-Taliban insurgency

New York State Terrorist Registry ACT

nexus to terrorism

non-state terrorist incident

on alert for terrorist attack (s)

Organization of Islamic Cooperation

overt violent extremist

patriotic Europeans against the Islamisation of the west (PEGIDA)

planning a series of terrorist attacks

planning attacks on mosques

planning attacks on synagogues

planning attacks on targets

planning attacks on western targets

plot (s)

plot broken up

plotted attacks

possible terror attack

possible terrorist attack

potential for terrorism

potential of shutting down

potential target (s) for terrorists

potential terror threat

potential terrorist

potential terrorist threats

preempt and disrupt terrorist activities

preemptive of a terrorist attack

prevent a terrorist attack from taking place

prevention of terrorist attack

prime breeding ground for terrorism

prominent al-Qaeda member

promote extremism

promote militancy

radical Islam

radicalized

radicalization

recent convert to Islam

recent suicide bombings

religious extremist

religious terrorist group

reemergence of militant cells

returning Jihadists

rockets fired on Israel

section 215 Patriot Act

sanctuary to terrorists

severely degraded (terrorism)

Shariah law

Shiite Houthi rebels

Sikh temple

sleeper teams

social terrorism

socially reinforced violent extremist

soft targets

splinter group

spread of radical Islam

state sponsored terrorism

state-sponsored terrorism

state terrorism

suicide attack (s)

suicide bomber attack (s) (ed)

strategic terrorism

string of terrorist attacks

suicide blasts

suicide groups

suicide operations

suicide vest

support for terrorists

surge in terrorist attacks

suspect in terrorist attack

suspect in terrorist attacks

suspected in the terrorist attack

suspected international terrorists

suspected Islamist extremists

suspected Jihadis

suspected terror threat

suspected terrorist (s)

suspected terrorist (s) attack tomorrow

suspected terrorist (s) detained without trial

suspected terrorist (s) rights

suspected terrorist (s) threats

suspected terrorists list

Syrian electronic army

Taliban

Taliban commander

Taliban government

Taliban insurgency

Taliban regime

target for terrorist attack (s)

target of terrorism

targeted by terrorists

targeted recruit

terror alert system

terror alert today

terror attack (s)

terror attack (s) may be linked

terror attack power grid

terror attack rumor

terror cell

terror cell in

terror funding

terror group (s)

terror plot

terror plot broken up

terror plot thwarted

terror prevention

terror suspect

terror warning

terrorism

terrorism alert

terrorism case

terrorism financing

terrorism fundraising

terrorism insurance

terrorism plot

terrorism related act

terrorism risk assessment

terrorism risk insurance

terrorism risk score

terrorism sponsor

terrorism threat

terrorist act

terrorist action

terrorist alert level

terrorist attack

terrorist attack on border

terrorist capability

terrorist cell

terrorist crime

terrorist crimes

terrorist element (s)

terrorist facilitator

terrorist fundraising

terrorist fundraising methods

terrorist groups

terrorist haven

Terrorist Identities Datamart Environment (TIDE) Central database of names in U.S.

terrorist incident (s)

terrorist methods

terrorist network

terrorist organization

terrorist plot

terrorist plots foiled

terrorist plots foiled by NSA

terrorist plots since 9/11

terrorist recruitment

terrorist registry

terrorist related activity

terrorist shooting

terrorist sleeper cells

terrorist sympathizer

terrorist tactics

terrorist target

terrorist targets

terrorist use of drones

terrorist use of social media

terrorist watch list

terrorist hiding

threat of terrorism

threat of terrorist act

tier one target

thwarted terror attack

transnational network of terrorist groups

transnational terrorism

types of terrorism threats

U.S. counterterrorism official

U.S. radicalized

U.S. renews global terrorism alert

urban terrorism

violent extremism

violent extremist activity

Wahhabi extremism

wake of the attacks

war on terror

watchlist, watch list

western hostages

wing of al Qaeda

Yemeni group

TRAVEL/TRANSPORTATION/CRIME/SAFETY

advising against all travel

alert for American citizens

alligator attack

American killed

American missing

American wounded

Amtrak suspends

arrests made abroad

attack on tourist (s)

backpackers

backpacker district

banned from traveling

black travel alert – Hong Kong Security Bureau

British holiday maker

British tourist

British tourist killed

bus accident

bus assaulted

bus hijacked

bus plunge

bus plunged

bus plunges

bus plunges off cliff

cancelled passport (s)

careen

cargo train derailed

carrier

citizens traveling to or residing in

climber dies

climber killed

climbing accident (s)

climbing death (s)

deadly tractor-trailer crash

death on cruise ship

defer non-essential travel

died at the scene

died at the scene of the accident

disrupted travel

disrupted travel for commuters

diving accident

expatriate (s) (expat)

expatriate (s) (expat) arrested

expatriate (s) (expat) detained

expatriate (s) (expat) killed

expatriate (s) (expat) missing

expatriate (s) (expat) murdered

expatriate (s) (expat) raped

expatriate (s) (expat) robbed

fake passport (s)

foreign tourists

frightened tourist (s)

haunted tourism

high risk traveler

highest level of travel warning

hijacked a bus

hijacked a truck

hiker

hiker missing

hiker rescued

historic district

hit and run accident

hit and run operations

hit by another vehicle

holiday makers (U.K.)

hotel bombed

hotel evacuated

hotel targeted

hundreds of tourists

including tourists

injured climber dies

injured climber killed

injured while on vacation

international shark attack file

involved in a crash

involved in the crash

Japanese tourist injured

Japanese tourist killed

Japanese tourist missing

kayaking accident

killed in collision

killing all aboard

listed passengers

major transit route

major transit route for

make alternate travel arrangements

maternity tourism

miles of track

missing American

missing abroad

missing hiker

missing hiker found

missing hiker found dead

missing in

missing person national park

missing person state park

missing national park

missing state park

missing tourist (s)

mountain climber

national uncivilized traveler record (China)

new travel alert

outbound travel alert (OTA) – Hong Kong system of global travel alerts

passenger (s) inspection (s)

passengers evacuated

passengers forced to evacuate

passenger (s) killed

passport altered

passport fraud

passport and visa fraud

passport revoked

pile-up

plunged off road

popular with tourists

railroad crossing

railroad crossing arms

road blockages

roadblock

road closure (s)

road damage

roadblocks were reported

rolling roadblock (UK)

security message for U.S. citizens

series of shark attacks

sex tourism business

shark attack

sledding accident

Smart Traveler Enrollment Program (STEP) (U.S.)

still missing

stolen passport

stolen travel documents

stranded

stranded in

stranded motorist

suicide tourism

surveillance of air travelers

survival of passengers

suspending search efforts

suspicious crash

temporary road closures

tourism industry

tourist attack (ed)

tourist drowned

tourist drowning

tourist (s) killed

tourist (s) missing

tourist (s) missing since

tourist (s) murdered

tourist police

tourist (s) robbed

tourist (s) scam

tourist (s) scammed

tourist stabbed to death

tourist (s) targeted

tourist (s) wounded

tramper (hiker)

traffic collision avoiding systems

train accident

train carrying

train collides with

train derailed

train derails

transportation hub (s)

travel advisory index

transplant tourism

travel ban

travel club scheme

travel restriction (s)

travel scam (s)

travel scam (s) – Asia

travel scam (s) – Europe

travel warning (s)

traveled to

traveler stabbed to death

travelers

travelers evacuated

travelers were advised

tube strike (U.K.)

vacation scam

vehicular accident

visa waiver privileges

visa waiver program

vulnerable during transit

warns citizens of the risks of traveling

UNCATEGORIZED

abnormal incident

acted immediately

active in the region

actively monitored

activity is likely

affiliated organizations

alert

attrition

automatic

banned from entering

blindside

blood letting

China dumping goods

China plays a role

components of risk

comprehensive plan

compromised operations

confidential report

consequences

consolidate

control

courageous

cross-hairs

declared intent

destabilization

detection of

domino effect

engage in

erroneously reported

exists in

facilitator

flawed decision

foreign trade zones

found at

gain surprise

including foreigners

hunger strike

increase in

increase detection (s)

increased vigilance

initiative

lift barriers

mail

material support

mitigation

multiple effort

new hub

new law

occurs in

open to abuse

operational operative

operational leader

operational planner

paperless airfreight

peak oil theory

proactive actions

rapid shift

rare earth elements

rare earth industry

rare earth mines

recovery

recovery effort

recovery efforts

remain unaccounted for

resistance

response

ruthlessness people

secret society

secret UFO locations

special access program

stuck

tip off adversary

unauthorized

unauthorized person

well organized campaign

who was the adversary

WEAPONS

access to weapons

after knife attack

after the shooting started

ammunition

AK-47 assault rifle

AR-15 resistance

armament

armament programs

armory

arms

arms control

arms embargo

arsenal

artillery

assassin weapons

asymmetrical weapons

attempting to use a weapon of mass destruction

automatic assault weapon

barrage

bio weapon (s)

biological weapon (s)

bullet

bulletproof

bullets sprayed

bulletproof Kevlar gear

cache

cache of illegal arms

cache of weapons

capstun

cap-stun

CBRN devices

CBRN weapons

chemical weapons

compact missile launcher

contagious bioweapons

contagious bioweapons research

continued gunfire

conventional weapons

cyber weapon

delivering nuclear weapons

deployment of weapons of mass destruction

electronic warfare systems

extends ban on guns

fired three shots into the air

fired shots into the air

friendly fire

friendly fire incident

gain access to weapons

Guantanamo cocktail

gun and bomb attacks

gun battle

gun battles have occurred

gun shootout

gun violence

gun violence is on the rise

gun violence is soaring

gunfire

gunfire erupted

gunfire heard

high powered machine guns

illicit arms trade

illicit small arms light weapons trade

improvised weapon (s)

killed in a gunfight

killed in shooting

killed in shootout

knife attack

knife crime

knife violence

light weapons

machete attack

man portable air defence systems (MAN PADS)

missile firings

moving target (s)

multiple targets

necklace knife

nerve agents

non-conventional weapons

plastic 3-D printed gun

pointed gun at

pointed rifle at

portable launchers

procure weapons

radiological dispersal device (RDD) – dirty bomb

radiological weapons

recover arms

rocket propelled grenade launcher

rounds of ammunition

self guided bullets

shoot down

small arms

steel combs (stabbing)

stolen weapons

sword (s)

sword attack

UN imposes arms embargo

use of weapons

weapons

weapons broker

weapons found

weapons grade

weapons hidden

weapons of mass destruction (wmd)

weapons paid for by

weapons smuggler

weapons test

WEATHER/CLIMATE ISSUES

across half the state

across large parts

across state

across the region

artic blast

black ice

blizzard

cancelled due to weather conditions

chance of rain

changes in rainfall

citizen weather observer program

climate change

climate engineering

Climatic Prediction Center (CPC)

coastal flooding

cold wave

cold weather operations

cold weather temperatures

deadly ice storms

deadly storm (s)

deadly tropical storm (s)

deadly weather

devastating mudslide

devastating storm (s)

drought

earthquake damage

earthquake magnitude

earthquake shakes

earthquake swarm (s)

extreme drought (s)

extreme weather

extreme weather events

flash flood warning

flash flood watch

flood (s)

flood damage

flood threat continues

flood toll rises

flooding

flooding in

floods in

floods kill

foggy weather

following heavy flooding

following heavy flooding and rains

following heavy rains

freezing rain

frost quake

haboob

hail

hail storms

heat wave

heavy rain

heavy snow

heavy storms

heavy storms swept through the area

high winds

high winds, snow, and ice

hurricane

hurricane alert

hurricane deaths

hurricane surge

hydro meteorological hazards

ice snow wind threaten power

icy road (s)

inclement weather

intense storms

levee breached

light rain

lightening

lighting

lighting sparked fire in

lighting storm

moderate rain

mud slide

mudslide

nor’easter

odd weather

pollution alert

record breaking cold

record breaking heat

record breaking heat to continue

record breaking heat to ease

record breaking heat wave

rip current

riptide

seasonal weather

severe storm

severe thunderstorm warning

severe weather

severe weather alert

severe winter storm

sleet

snow storm

snowfall measurement

storm causes damage, flooding

storm causes wrecks

storm surge

storms cause damage

storms cause damage in

storms cause power cuts in

storms cause road closures

storm cause widespread

storms paralyze

storm paralyzes

storms sweep across

storms sweep across state

sweep across

swept away by current

temblor

tornadic activity

tornado

tornado deaths

tornado touched down

tornadoes destroy

torrential rain (s)

tropical cyclone

tropical storm

tsunami

tsunami alert

tsunami warning

Tsunami Warning Center

typhoon alert

typhoon intensifies

unexpected freezes

unexpected frosts

unusual weather conditions

violent rain

violent storm

weather alert

weather chaos

weather conditions

weather modification

weather observer

weather related issues

weather related problems

weather warfare

weather warning

weather watch

