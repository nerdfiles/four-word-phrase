/**
 * @name diagonalized-fwp
 * @description
 * Four word phrase generates an output from a corpus, we diagonalize the 
 * result for the lulz and maybe chain it to a blanket.
 */
const promisify = require('promisify');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const natural = require('natural');
const NGrams = natural.NGrams
const tokenizer = new natural.WordTokenizer();
const wordnet =  require('wordnet-db');
const WordPOS = require('wordpos');
const wordpos = new WordPOS();

// const cmd = 'node generatePhrases.js guy-debord-the-society-of-the-spectacle.p.html | tail -n 10';
const cmd = 'node generatePhrases.js badiou--bataille.md | tail -n 10';

/**
 * nounGeneratorCorpus
 *
 * @param items
 * @returns {undefined}
 */
async function nounGeneratorCorpus(items) {
  return new Promise((resolve, reject) => {
    wordpos.getNouns(items.join(' '), async (nounListResponse) => {
      if (!nounListResponse) {
        reject({ status: 'error' });
      }
      resolve(nounListResponse);
    });
  });
}

/**
 * nounGenerator
 *
 * @returns {undefined}
 */
async function nounGenerator() {
  const list = [
    'shoe', 'dinosaur', 'planet',
    'square', 'logic', 'model'
  ];

  const random = Math.floor(Math.random() * list.length);
  return list[random];
}


async function diagonalizer() {
  const {
    stdout,
    stderr
  } = await exec(cmd);
	const corpus = stdout;
	const simpleTokenization = tokenizer.tokenize(corpus);
	//console.log(simpleTokenization);
  const holophrases = NGrams.ngrams(simpleTokenization, 4);
  const stanza = [];
  holophrases.forEach(async (ref) => {
    const phraseItems = [];
    ref.forEach((phrase) => {
      if (!phrase.includes('Phrase') && phrase.length > 3) {
        phraseItems.push(phrase.split(' '));
      }
    });
    // console.log('---- PHRASE:');
    // console.log(phraseItems);

    wordpos.getAdjectives(phraseItems.join(' '), async (res) => {
      const nounInstanceCorpus = await nounGeneratorCorpus(phraseItems);
      // console.log(nounInstanceCorpus);
      // const nounInstance = await nounGenerator();
      const article = 'the';
      const outputSentence = [
        article, 
        res.join(' '), 
        nounInstanceCorpus.join(' ')
      ].join(' ');
      const p = new Promise(function (resolve) {
        if (phraseItems.length > 1) {
          resolve(outputSentence);
        }
      });
      stanza.push(p);
      const result = await Promise.all(stanza);
      result.length = 30;
      console.log(result);
    });
  });
}

diagonalizer();

// EOF
