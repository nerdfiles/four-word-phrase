#!/bin/sh

# free
cat fb--chuck/1--500\,000\ Emails\ List.txt | freemail-cli > fb--chuck/free-emails/1--500\,000\ Emails\ List.txt
cat fb--chuck/2--800K\ NEW\ Emails.txt | freemail-cli > fb--chuck/free-emails/2--800K\ NEW\ Emails.txt
cat fb--chuck/3--500k\ emails.txt | freemail-cli > fb--chuck/free-emails/3--500k\ emails.txt
cat fb--chuck/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt | freemail-cli > fb--chuck/free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt

node generateDeduped.js fb--chuck/free-emails/1--500\,000\ Emails\ List.txt fb--chuck/free-emails/1--500\,000\ Emails\ List.deduped.txt
node generateDeduped.js fb--chuck/free-emails/2--800K\ NEW\ Emails.txt fb--chuck/free-emails/2--800K\ NEW\ Emails.deduped.txt
node generateDeduped.js fb--chuck/free-emails/3--500k\ emails.txt fb--chuck/free-emails/3--500k\ emails.deduped.txt
node generateDeduped.js fb--chuck/free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt fb--chuck/free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).deduped.txt

# non-free (work, company)
cat fb--chuck/1--500\,000\ Emails\ List.txt | freemail-cli -i > fb--chuck/non-free-emails/1--500\,000\ Emails\ List.txt
cat fb--chuck/2--800K\ NEW\ Emails.txt | freemail-cli -i > fb--chuck/non-free-emails/2--800K\ NEW\ Emails.txt
cat fb--chuck/3--500k\ emails.txt | freemail-cli -i > fb--chuck/non-free-emails/3--500k\ emails.txt
cat fb--chuck/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt | freemail-cli -i > fb--chuck/non-free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt

node generateDeduped.js fb--chuck/non-free-emails/1--500\,000\ Emails\ List.txt fb--chuck/non-free-emails/1--500\,000\ Emails\ List.deduped.txt
node generateDeduped.js fb--chuck/non-free-emails/2--800K\ NEW\ Emails.txt fb--chuck/non-free-emails/2--800K\ NEW\ Emails.deduped.txt
node generateDeduped.js fb--chuck/non-free-emails/3--500k\ emails.txt fb--chuck/non-free-emails/3--500k\ emails.deduped.deduped.txt
node generateDeduped.js fb--chuck/non-free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt fb--chuck/non-free-emails/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).deduped.txt

# disposable
cat fb--chuck/1--500\,000\ Emails\ List.txt | freemail-cli -d > fb--chuck/disposable/1--500\,000\ Emails\ List.txt
cat fb--chuck/2--800K\ NEW\ Emails.txt | freemail-cli -d > fb--chuck/disposable/2--800K\ NEW\ Emails.txt
cat fb--chuck/3--500k\ emails.txt | freemail-cli -d > fb--chuck/disposable/3--500k\ emails.txt
cat fb--chuck/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt | freemail-cli -d > fb--chuck/disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt

node generateDeduped.js fb--chuck/disposable/1--500\,000\ Emails\ List.txt fb--chuck/disposable/1--500\,000\ Emails\ List.deduped.txt
node generateDeduped.js fb--chuck/disposable/2--800K\ NEW\ Emails.txt fb--chuck/disposable/2--800K\ NEW\ Emails.deduped.txt
node generateDeduped.js fb--chuck/disposable/3--500k\ emails.txt fb--chuck/disposable/3--500k\ emails.deduped.deduped.txt
node generateDeduped.js fb--chuck/disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt fb--chuck/disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).deduped.txt

# non-disposable
cat fb--chuck/1--500\,000\ Emails\ List.txt | freemail-cli -di > fb--chuck/non-disposable/1--500\,000\ Emails\ List.txt
cat fb--chuck/2--800K\ NEW\ Emails.txt | freemail-cli -di > fb--chuck/non-disposable/2--800K\ NEW\ Emails.txt
cat fb--chuck/3--500k\ emails.txt | freemail-cli -di > fb--chuck/non-disposable/3--500k\ emails.txt
cat fb--chuck/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt | freemail-cli -di > fb--chuck/non-disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt

node generateDeduped.js fb--chuck/non-disposable/1--500\,000\ Emails\ List.txt fb--chuck/non-disposable/1--500\,000\ Emails\ List.deduped.txt
node generateDeduped.js fb--chuck/non-disposable/2--800K\ NEW\ Emails.txt fb--chuck/non-disposable/2--800K\ NEW\ Emails.deduped.txt
node generateDeduped.js fb--chuck/non-disposable/3--500k\ emails.txt fb--chuck/non-disposable/3--500k\ emails.deduped.txt
node generateDeduped.js fb--chuck/non-disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).txt fb--chuck/non-disposable/4--200K_Email_List_USA_Only_AOL_Fresh\ \(2015\).deduped.txt
