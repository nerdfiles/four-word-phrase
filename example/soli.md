Solitary confinement is torture: Your Say
The punishment violates fundamental human needs.

I'm writing to protest the use of solitary confinement in U.S. prisons and jails. Research has shown that humans are social creatures, and that depriving them of meaningful social contact by isolating them in cells for long periods of time is torture. America's prisoners were not sentenced to torture. Yet, according to the ACLU, more than 80,000 prisoners are being held in solitary confinement in the U.S. Many of these people are non-violent and have been held in isolation for prolonged periods of time, and some have never even been convicted of a crime.

There are many ways to destroy a person, but one of the simplest and most devastating is through prolonged solitary confinement. Deprived of meaningful human interaction, otherwise healthy prisoners become unhinged. A large number of organizations recognize that prolonged solitary confinement is counterproductive, including the United Nations, the ACLU, Physicians for Human Rights and the National Religious Campaign Against Torture. It has also recently been denounced by U.S. Supreme Court Justice Anthony Kennedy and the president of the United States.

Even more harmful to the ideas of justice is holding someone in isolation prior to trial. This has the very powerful effect of forcing innocent people into taking a plea of guilty, as some will do anything to stop the torture.


USA TODAY
Policing the USA

Research into the psychology of inmates held in isolation has found that they suffer significant trauma, including mental illness and brain damage. Even short-term isolation can cause panic attacks, anxiety, loss of control, paranoia, hallucinations, depression, insomnia, cognitive dysfunction and self-mutilation. The longer the confinement the more damaged the person and the longer they may take to recover — if at all.

Social interaction is neither a right nor a privilege, it is a fundamental human need. Some people's view that solitary confinement is an acceptable punishment runs against almost all international agreements on human rights. If another country were torturing Americans, the outcry would be deafening. So how can we accept our own government torturing our own citizens? It's time to stop the torture, it’s time to put an end to solitary confinement.

John Taylor; McGray, Ga.

Earlier this year, President Obama restricted the use of solitary confinement. Facebook comments are edited for clarity and grammar:

Just for the record, we are not talking about nice people here. We are talking about sociopaths who failed to follow society’s rules, wound up in prison and still refused to follow the rules. Hence, solitary confinement.

President Obama, now you’re feeling a mite tender toward these poor unfortunates? Well, please forgive me if I fail to share your sentiments. Not everyone can be saved.

— Paul Hafner

Other than inflicting mental anguish, increasing medical costs, increasing inmate housing costs and increasing recidivism, what does solitary confinement accomplish?

— Rodney Smith
