Understanding Trump

By George Lakoff

There is a lot being written and spoken about Trump by intelligent and articulate commentators whose insights I respect. But as a longtime researcher in cognitive science and linguistics, I bring a perspective from these sciences to an understanding of the Trump phenomenon. This perspective is hardly unknown. More than half a million people have read my books, and Google Scholar reports that scholars writing in scholarly journals have cited my works well over 100,000 times.

Yet you will probably not read what I have to say in the NY Times, nor hear it from your favorite political commentators. You will also not hear it from Democratic candidates or party strategists. There are reasons, and we will discuss them later I this piece. I am writing it because I think it is right and it is needed, even though it comes from the cognitive and brain sciences, not from the normal political sources. I think it is imperative to bring these considerations into public political discourse. But it cannot be done in a 650-word op-ed. My apologies. It is untweetable.

I will begin with an updated version of an earlier piece on who is supporting Trump and why — and why policy details are irrelevant to them. I then move to a section on how Trump uses your brain against you. I finish up discussing how Democratic campaigns could do better, and why they need to do better if we are to avert a Trump presidency.

Who Supports Trump and Why


Donald J. Trump has managed to become the Republican nominee for president, Why? How? There are various theories: People are angry and he speaks to their anger. People don’t think much of Congress and want a non-politician. Both may be true. But why? What are the details? And Why Trump?

He seems to have come out of nowhere. His positions on issues don’t fit a common mold.

He has said nice things about LGBTQ folks, which is not standard Republican talk. Republicans hate eminent domain (the taking of private property by the government) and support corporate outsourcing for the sake of profit, but he has the opposite views on both. He is not religious and scorns religious practices, yet the Evangelicals (that is, the white Evangelicals) love him. He thinks health insurance and pharmaceutical companies, as well as military contractors, are making too much profit and wants to change that. He insults major voting groups, e.g., Latinos, when most Republicans are trying to court them. He wants to deport 11 million immigrants without papers and thinks he can. He wants to stop Muslims from entering the country. What is going on?

The answer requires a bit of background.

In the 1900’s, as part of my research in the cognitive and brain sciences, I undertook to answer a question in my field: How do the various policy positions of conservatives and progressives hang together? Take conservatism: What does being against abortion have to do with being for owning guns? What does owning guns have to do with denying the reality of global warming? How does being anti-government fit with wanting a stronger military? How can you be pro-life and for the death penalty? Progressives have the opposite views. How do their views hang together?

The answer came from a realization that we tend to understand the nation metaphorically in family terms: We have founding fathers. We send our sons and daughters to war. We have homeland security. The conservative and progressive worldviews dividing our country can most readily be understood in terms of moral worldviews that are encapsulated in two very different common forms of family life: The Nurturant Parent family (progressive) and the Strict Father family (conservative).

What do social issues and the politics have to do with the family? We are first governed in our families, and so we grow up understanding governing institutions in terms of the governing systems of families.

In the strict father family, father knows best. He knows right from wrong and has the ultimate authority to make sure his children and his spouse do what he says, which is taken to be what is right. Many conservative spouses accept this worldview, uphold the father’s authority, and are strict in those realms of family life that they are in charge of. When his children disobey, it is his moral duty to punish them painfully enough so that, to avoid punishment, they will obey him (do what is right) and not just do what feels good. Through physical discipline they are supposed to become disciplined, internally strong, and able to prosper in the external world. What if they don’t prosper? That means they are not disciplined, and therefore cannot be moral, and so deserve their poverty. This reasoning shows up in conservative politics in which the poor are seen as lazy and undeserving, and the rich as deserving their wealth. Responsibility is thus taken to be personal responsibility not social responsibility. What you become is only up to you; society has nothing to do with it. You are responsible for yourself, not for others — who are responsible for themselves.

Winning and Insulting

As the legendary Green Bay Packers coach, Vince Lombardi, said,

“Winning isn’t everything. It’s the only thing.” In a world governed by personal responsibility and discipline, those who win deserve to win. Why does Donald Trump publicly insult other candidates and political leaders mercilessly? Quite simply, because he knows he can win an onstage TV insult game. In strict conservative eyes, that makes him a formidable winning candidate who deserves to be a winning candidate. Electoral competition is seen as a battle. Insults that stick are seen as victories — deserved victories.

Consider Trump’s statement that John McCain is not a war hero. The reasoning: McCain got shot down. Heroes are winners. They defeat big bad guys. They don’t get shot down. People who get shot down, beaten up, and stuck in a cage are losers, not winners.

The Moral Hierarchy

The strict father logic extends further. The basic idea is that authority is justified by morality (the strict father version), and that, in a well-ordered world, there should be (and traditionally has been) a moral hierarchy in which those who have traditionally dominated should dominate. The hierarchy is: God above Man, Man above Nature, The Disciplined (Strong) above the Undisciplined (Weak), The Rich above the Poor, Employers above Employees, Adults above Children, Western culture above other cultures, America above other countries. The hierarchy extends to: Men above women, Whites above Nonwhites, Christians above nonChristians, Straights above Gays.

We see these tendencies in most of the Republican presidential candidates, as well as in Trump, and on the whole, conservative policies flow from the strict father worldview and this hierarchy

Family-based moral worldviews run deep. Since people want to see themselves as doing right not wrong, moral worldviews tend to be part of self-definition — who you most deeply are. And thus your moral worldview defines for you what the world should be like. When it isn’t that way, one can become frustrated and angry.

There is a certain amount of wiggle room in the strict father worldview and there are important variations. A major split is among (1) white Evangelical Christians, (2) laissez-fair free market conservatives, and (3) pragmatic conservatives who are not bound by evangelical beliefs.

White Evangelicals

Those whites who have a strict father personal worldview and who are religious tend toward Evangelical Christianity, since God, in Evangelical Christianity, is the Ultimate Strict Father: You follow His commandments and you go to heaven; you defy His commandments and you burn in hell for all eternity. If you are a sinner and want to go to heaven, you can be ‘born again” by declaring your fealty by choosing His son, Jesus Christ, as your personal Savior.

Such a version of religion is natural for those with strict father morality. Evangelical Christians join the church because they are conservative; they are not conservative because they happen to be in an evangelical church, though they may grow up with both together.

Evangelical Christianity is centered around family life. Hence, there are organizations like Focus on the Family and constant reference to “family values,” which are to take to be evangelical strict father values. In strict father morality, it is the father who controls sexuality and reproduction. Where the church has political control, there are laws that require parental and spousal notification in the case of proposed abortions.

Evangelicals are highly organized politically and exert control over a great many local political races. Thus Republican candidates mostly have to go along with the evangelicals if they want to be nominated and win local elections.

Pragmatic Conservatives

Pragmatic conservatives, on the other hand, may not have a religious orientation at all. Instead, they may care primarily about their own personal authority, not the authority of the church or Christ, or God. They want to be strict fathers in their own domains, with authority primarily over their own lives. Thus, a young, unmarried conservative — male or female —may want to have sex without worrying about marriage. They may need access to contraception, advice about sexually transmitted diseases, information about cervical cancer, and so on. And if a girl or woman becomes pregnant and there is no possibility or desire for marriage, abortion may be necessary.

Trump is a pragmatic conservative, par excellence. And he knows that there are a lot of Republican voters who are like him in their pragmatism. There is a reason that he likes Planned Parenthood. There are plenty of young, unmarried (or even married) pragmatic conservatives, who may need what Planned Parenthood has to offer — cheaply and confidentially by way of contraception, cervical cancer prevention, and sex ed.

Similarly, young or middle-aged pragmatic conservatives want to maximize their own wealth. They don’t want to be saddled with the financial burden of caring for their parents. Social Security and Medicare relieve them of most of those responsibilities. That is why Trump wants to keep Social Security and Medicare.

Laissez-faire Free Marketeers

Establishment conservative policies have not only been shaped by the political power of white evangelical churches, but also by the political power of those who seek maximally laissez-faire free markets, where wealthy people and corporations set market rules in their favor with minimal government regulation and enforcement. They see taxation not as investment in publicly provided resources for all citizens, but as government taking their earnings (their private property) and giving the money through government programs to those who don’t deserve it. This is the source of establishment Republicans’ anti-tax and shrinking government views. This version of conservatism is quite happy with outsourcing to increase profits by sending manufacturing and many services abroad where labor is cheap, with the consequence that well-paying jobs leave America and wages are driven down here. Since they depend on cheap imports, they would not be in favor of imposing high tariffs.

But Donald Trump is not in a business that makes products abroad to import here and mark up at a profit. As a developer, he builds hotels, casinos, office buildings, golf courses. He may build them abroad with cheap labor but he doesn’t import them. Moreover, he recognizes that most small business owners in America are more like him — American businesses like dry cleaners, pizzerias, diners, plumbers, hardware stores, gardeners, contractors, car washers, and professionals like architects, lawyers, doctors, and nurses. High tariffs don’t look like a problem.

Many business people are pragmatic conservatives. They like government power when it works for them. Take eminent domain. Establishment Republicans see it as an abuse by government — government taking of private property. But conservative real estate developers like Trump depend on eminent domain so that homes and small businesses in areas they want to develop can be taken by eminent domain for the sake of their development plans. All they have to do is get local government officials to go along, with campaign contributions and the promise of an increase in local tax dollars helping to acquire eminent domain rights. Trump points to Atlantic City, where he built his casino using eminent domain to get the property.

If businesses have to pay for their employees’ health care benefits, Trump would want them to have to pay as little as possible to maximize profits for businesses in general. He would therefore want health insurance and pharmaceutical companies to charge as little as possible. To increase competition, he would want insurance companies to offer plans nationally, avoiding the state-run exchanges under the Affordable Care Act. The exchanges are there to maximize citizen health coverage, and help low-income people get coverage, rather than to increase business profits. Trump does however want to keep the mandatory feature of ACA, which establishment conservatives hate since they see it as government overreach, forcing people to buy a product. For Trump, however, the mandatory feature for individuals increases the insurance pool and brings down costs for businesses.

Direct vs. Systemic Causation

Direct causation is dealing with a problem via direct action. Systemic causation recognizes that many problems arise from the system they are in and must be dealt with via systemic causation. Systemic causation has four versions: A chain of direct causes. Interacting direct causes (or chains of direct causes). Feedback loops. And probabilistic causes. Systemic causation in global warming explains why global warming over the Pacific can produce huge snowstorms in Washington DC: masses of highly energized water molecules evaporate over the Pacific, blow to the Northeast and over the North Pole and come down in winter over the East coast and parts of the Midwest as masses of snow. Systemic causation has chains of direct causes, interacting causes, feedback loops, and probabilistic causes — often combined.

Direct causation is easy to understand, and appears to be represented in the grammars of all languages around the world. Systemic causation is more complex and is not represented in the grammar of any language. It just has to be learned.

Empirical research has shown that conservatives tend to reason with direct causation and that progressives have a much easier time reasoning with systemic causation. The reason is thought to be that, in the strict father model, the father expects the child or spouse to respond directly to an order and that refusal should be punished as swiftly and directly as possible.

Many of Trump’s policy proposals are framed in terms of direct causation.

Immigrants are flooding in from Mexico — build a wall to stop them. For all the immigrants who have entered illegally, just deport them — even if there are 11 million of them working throughout the economy and living throughout the country. The cure for gun violence is to have a gun ready to directly shoot the shooter. To stop jobs from going to Asia where labor costs are lower and cheaper goods flood the market here, the solution is direct: put a huge tariff on those goods so they are more expensive than goods made here. To save money on pharmaceuticals, have the largest consumer — the government — take bids for the lowest prices. If Isis is making money on Iraqi oil, send US troops to Iraq to take control of the oil. Threaten Isis leaders by assassinating their family members (even if this is a war crime). To get information from terrorist suspects, use water-boarding, or even worse torture methods. If a few terrorists might be coming with Muslim refugees, just stop allowing all Muslims into the country. All this makes sense to direct causation thinkers, but not those who see the immense difficulties and dire consequences of such actions due to the complexities of systemic causation.

Political Correctness

There are at least tens of millions of conservatives in America who share strict father morality and its moral hierarchy. Many of them are poor or middle class and many are white men who see themselves as superior to immigrants, nonwhites, women, nonChristians, gays — and people who rely on public assistance. In other words, they are what liberals would call “bigots.” For many years, such bigotry has not been publicly acceptable, especially as more immigrants have arrived, as the country has become less white, as more women have become educated and moved into the workplace, and as gays have become more visible and gay marriage acceptable. As liberal anti-bigotry organizations have loudly pointed out and made a public issue of the unAmerican nature of such bigotry, those conservatives have felt more and more oppressed by what they call “political correctness” — public pressure against their views and against what they see as “free speech.” This has become exaggerated since 911, when anti-Muslim feelings became strong. The election of President Barack Hussein Obama created outrage among those conservatives, and they refused to see him as a legitimate American (as in the birther movement), much less as a legitimate authority, especially as his liberal views contradicted almost everything else they believe as conservatives.

Donald Trump expresses out loud everything they feel — with force, aggression, anger, and no shame. All they have to do is support and vote for Trump and they don’t even have to express their ‘politically incorrect’ views, since he does it for them and his victories make those views respectable. He is their champion. He gives them a sense of self-respect, authority, and the possibility of power.

Whenever you hear the words “political correctness” remember this.

Biconceptuals

There is no middle in American politics. There are moderates, but there is no ideology of the moderate, no single ideology that all moderates agree on. A moderate conservative has some progressive positions on issues, though they vary from person to person. Similarly, a moderate progressive has some conservative positions on issues, again varying from person to person. In short, moderates have both political moral worldviews, but mostly use one of them. Those two moral worldviews in general contradict each other. How can they reside in the same brain at the same time?

Both are characterized in the brain by neural circuitry. They are linked by a commonplace circuit: mutual inhibition. When one is turned on the other is turned off; when one is strengthened, the other is weakened. What turns them on or off? Language that fits that worldview activates that worldview, strengthening it, while turning off the other worldview and weakening it. The more Trump’s views are discussed in the media, the more they are activated and the stronger they get, both in the minds of hardcore conservatives and in the minds of moderate progressives.

This is true even if you are attacking Trump’s views. The reason is that negating a frame activates that frame, as I pointed out in the book Don’t Think of an Elephant! It doesn’t matter if you are promoting Trump or attacking Trump, you are helping Trump.

A good example of Trump winning with progressive biconceptuals includes certain unionized workers. Many union members are strict fathers at home or in their private life. They believe in “traditional family values” — a conservative code word — and they may identify with winners.

Why Has Trump won the Republican nomination? Look at all the conservative groups he appeals to!

Why His Lack of Policy Detail Doesn’t Matter

I recently heard a brilliant and articulate Clinton surrogate argue against a group of Trump supporters that Trump has presented no policy plans for increasing jobs, increasing economics growth, improving education, gaining international respect, etc. This is the basic Clinton campaign argument. Hillary has the experience, the policy know-how, she can get things done, it’s all on her website. Trump has none of this. What Hillary’s campaign says is true. And it is irrelevant.

Trump supporters and other radical Republican extremists could not care less, and for a good reason. Their job is to impose their view of strict father morality in all areas of life. If they have the Congress, and the Presidency and the Supreme Court, they could achieve this. They don’t need to name policies, because the Republicans already have hundreds of policies ready to go. They just need to be in complete power.

How Trump Uses Your Brain to His Advantage

Any unscrupulous, effective salesman knows how to use you brain against you, to get you to buy what he is selling. How can someone “use your brain against you?” What does it mean?

All thought uses neural circuitry. Every idea is constituted by neural circuitry. But we have no conscious access to that circuitry. As a result, most of thought — an estimated 98 percent of thought is unconscious. Conscious thought is the tip of the iceberg.

Unconscious thought works by certain basic mechanisms. Trump uses them instinctively to turn people’s brains toward what he wants: Absolute authority, money, power, celebrity.

The mechanisms are:

1. Repetition. Words are neurally linked to the circuits that determine their meaning. The more a word is heard, the more the circuit is activated and the stronger it gets, and so the easier it is to fire again. Trump repeats. Win. Win, Win. We’re gonna win so much you’ll get tired of winning.

2. Framing: Crooked Hillary. Framing Hillary as purposely and knowingly committing crimes for her own benefit, which is what a crook does. Repeating makes many people unconsciously think of her that way, even though she has been found to have been honest and legal by thorough studies by the right-wing Bengazi committee (which found nothing) and the FBI (which found nothing to charge her with, except missing the mark ‘(C)’ in the body of 3 out of 110,000 emails). Yet the framing is working.

There is a common metaphor that Immorality Is Illegality, and that acting against Strict Father Morality (the only kind off morality recognized) is being immoral. Since virtually everything Hillary Clinton has ever done has violated Strict Father Morality, that makes her immoral. The metaphor thus makes her actions immoral, and hence she is a crook. The chant “Lock her up!” activates this whole line of reasoning.

3. Well-known examples: When a well-publicized disaster happens, the coverage activates the framing of it over and over, strengthening it, and increasing the probability that the framing will occur easily with high probability. Repeating examples of shootings by Muslims, African-Americans, and Latinos raises fears that it could happen to you and your community — despite the miniscule actual probability. Trump uses this to create fear. Fear tends to activate desire for a strong strict father — namely, Trump.

4. Grammar: Radical Islamic terrorists: “Radical” puts Muslims on a linear scale and “terrorists” imposes a frame on the scale, suggesting that terrorism is built into the religion itself. The grammar suggests that there is something about Islam that has terrorism inherent in it. Imagine calling the Charleston gunman a “radical Republican terrorist.”

Trump is aware of this to at least some extent. As he said to Tony Schwartz, the ghost-writer who wrote The Art of the Deal for him, “I call it truthful hyperbole. It’s an innocent form of exaggeration — and it’s a very effective form of promotion.”

5. Conventional metaphorical thought is inherent in our largely unconscious thought. Such normal modes of metaphorical thinking are not noticed as such.

Consider Brexit, which used the metaphor of “entering” and “leaving” the EU. There is a universal metaphor that states are locations in space: you can enter a state, be deep in some state, and come out that state. If you enter a café and then leave the café , you will be in the same location as before you entered. But that need not be true of states of being. But that was the metaphor used with Brexit; Britons believed that after leaving the EU, things would be as before when the entered the EU. They were wrong. Things changed radically while they were in the EU. That same metaphor is being used by Trump: Make America Great Again. Make America Safe Again. And so on. As if there was some past ideal state that we can go back to just by electing Trump.

6. There is also a metaphor that A Country Is a Person and a metonymy of the President Standing For the Country. Thus, Obama, via both metaphor and metonymy, can stand conceptually for America. Therefore, by saying that Obama is weak and not respected, it is communicated that America, with Obama as president, is weak and disrespected. The inference is that it is because of Obama.

7. The country as person metaphor and the metaphor that war or conflict between countries is a fistfight between people, leads to the inference that just having a strong president will guarantee that America will win conflicts and wars. Trump will just throw knockout punches. In his acceptance speech at the convention, Trump repeatedly said that he would accomplish things that can only be done by the people acting with their government. After one such statement, there was a chant from the floor, “He will do it.”

8. The metaphor that The nation Is a Family was used throughout the GOP convention. We heard that strong military sons are produced by strong military fathers and that “defense of country is a family affair.” From Trump’s love of family and commitment to their success, we are to conclude that, as president he will love America’s citizens and be committed to the success of all.

9. There is a common metaphor that Identifying with Your Family’s National Heritage Makes You a Member of That Nationality. Suppose your grandparents came from Italy and you identify with your Italian ancestors, you may proudly state that you are Italian. The metaphor is natural. Literally, you have been American for two generations. Trump made use of this commonplace metaphor in attacking US District Court Judge Gonzalo Curiel, who is American, born and raised in the United States. Trump said he was a Mexican, and therefore would hate him and tend to rule against him in a case brought against Trump University for fraud.

10. Then there is the metaphor system used in the phrase “to call someone out.” First the word “out.” There is a general metaphor that Knowing Is Seeing as in “I see what you mean.” Things that are hidden inside something cannot be seen and hence not known, while things are not hidden but out in public can be seen and hence known. To “out” someone is to made their private knowledge public. To “call someone out” is to publicly name someone’s hidden misdeeds, thus allowing for public knowledge and appropriate consequences.

This is the basis for the Trumpian metaphor that Naming is Identifying. Thus naming your enemies will allow you to identify correctly who they are, get to them, and so allow you to defeat them. Hence, just saying “radical Islamic terrorists” allows you to pick them out, get at them, and annihilate them. And conversely, if you don’t say it, you won’t be able to pick them out and annihilate them. Thus a failure to use those words means that you are protecting those enemies — in this case Muslims, that is, potential terrorists because of their religion.

I’ll stop here, though I could go on. Here are ten uses of people’s unconscious normal brain mechanisms that are manipulated by Trump and his followers for his overriding purpose: to be elected president, to be given absolute authority with a Congress and Supreme Court, and so to have his version of Strict Father Morality govern America into the indefinite future.

These ten forms of using people’s everyday brain mechanisms for his own purposes have gotten Trump the Republican nomination. But millions more people have seen and heard Trump and company on tv and heard them on the radio. The media pundits have not described those ten mechanisms, or other brain mechanisms, that surreptitiously work on the unconscious minds of the public, even though the result is that Big Lies repeated over and over are being believed by a growing number of people.

Even if he loses the election, Trump will have changed the brains of millions of Americans, with future consequences. It is vitally important people know the mechanisms used to transmit Big Lies and to stick them into people’s brains without their awareness. It is a form of mind control.

People in the media have a duty to report it when the see it. But there are constraints on the media.

Certain things have not been allowed in public political discourse in the media. Reporters and commentators are supposed to stick to what is conscious and with literal meaning. But most real political discourse makes use of unconscious thought, which shapes conscious thought via unconscious framing and commonplace conceptual metaphors. It is crucial, for the history of the country and the world, as well as the planet, that all of this be made public.

And it is not just the media. Such responsibility rests with ordinary citizens who become aware of unconscious brain mechanisms like the ten we have just discussed. This responsibility also rests with the Democratic Party and their campaigns at all levels.

Is the use of the public’s brain mechanisms for communication necessarily immoral? Understanding how people really think can be used to communicate truths, not Big Lies or ads for products.

This knowledge is not just known to cognitive linguists. It is taught in Marketing courses in business schools, and the mechanisms are used in advertising, to get you to buy what advertisers are selling. We have learned to recognize ads; they are set off by themselves. Even manipulative corporate advertising with political intent (like ads for fracking) is not as dangerous as Big Lies leading to authoritarian government determining the future of our country.

How Can Democrats Do Better?

First, don’t think of an elephant. Remember not to repeat false conservative claims and then rebut them with the facts. Instead, go positive. Give a positive truthful framing to undermine claims to the contrary. Use the facts to support positively-framed truth. Use repetition.

Second, start with values, not policies and facts and numbers. Say what you believe, but haven’t been saying. For example, progressive thought is built on empathy, on citizens caring about other citizens and working through our government to provide public resources for all, both businesses and individuals. Use history. That’s how America started. The public resources used by businesses were not only roads and bridges, but public education, a national bank, a patent office, courts for business cases, interstate commerce support, and of course the criminal justice system. From the beginning, the Private Depended on Public Resources, both private lives and private enterprise.

Over time those resources have included sewers, water and electricity, research universities and research support: computer science (via the NSF), the internet (ARPA), pharmaceuticals and modern medicine (the NIH), satellite communication (NASA and NOA), and GPS systems and cell phones (the Defense Department). Private enterprise and private life utterly depend on public resources. Have you ever said this? Elizabeth Warren has. Almost no other public figures. And stop defending “the government.” Talk about the public, the people, Americans, the American people, public servants, and good government. And take back freedom. Public resources provide for freedom in private enterprise and private life.

The conservatives are committed to privatizing just about everything and to eliminating funding for most public resources. The contribution of public resources to our freedoms cannot be overstated. Start saying it.

And don’t forget the police. Effective respectful policing is a public resource. Chief David O. Brown of the Dallas Police got it right. Training, community policing, knowing the people you protect. And don’t ask too much of the police: citizens have a responsibility to provide funding so that police don’t have to do jobs that should be done by others.

Unions need to go on the offensive. Unions are instruments of freedom — freedom from corporate servitude. Employers call themselves job creators. Working people are profit creators for the employers, and as such they deserve a fair share of the profits and respect and acknowledgement. Say it. Can the public create jobs. Of course. Fixing infrastructure will create jobs by providing more public resources that private lives and businesses depend on. Public resources to create more public resources. Freedom creates opportunity that creates more freedom.

Third, keep out of nasty exchanges and attacks. Keep out of shouting matches. One can speak powerfully without shouting. Obama sets the pace: Civility, values, positivity, good humor, and real empathy are powerful. Calmness and empathy in the face of fury are powerful. Bill Clinton won because he oozed empathy, with his voice, his eye contact, and his body. It wasn’t his superb ability as a policy wonk, but the empathy he projected and inspired.

Values come first, facts and policies follow in the service of values. They matter, but they always support values.

Give up identity politics. No more women’s issues, black issues, Latino issues. Their issues are all real, and need public discussion. But they all fall under freedom issues, human issues. And address poor whites! Appalachian and rust belt whites deserve your attention as much as anyone else. Don’t surrender their fate to Trump, who will just increase their suffering.

And remember JFK’s immortal, “Ask not what your country can do for you, but what you can do for your country.” Empathy, devotion, love, pride in our country’s values, public resources to create freedoms. And adulthood.

Be prepared. You have to understand Trump to stand calmly up to him and those running with him all over the country.
