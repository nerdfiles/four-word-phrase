###
@fileOverview
Generate a dictionary from Moby Dick and write it to file.
###

fs          = require 'fs'
path        = require 'path'
fwp         = require '..'

__interface = (new fwp.util.Interface).dictionary()

filename = __interface.filename
dictionary = __interface.dictionary


filePath = path.join __dirname, filename
readStream = fs.createReadStream filePath

dictionaryFilePath = path.join __dirname, dictionary
writeStream = fs.createWriteStream dictionaryFilePath


dictionaryTransformStream = new fwp.util.DictionaryTransformStream {
  # Since we're going to stream to file, we don't want it to write discrete
  # words as objects.
  objectMode: false
  # These are the default settings: tokenize on whitespace and a few
  # punctuation marks and look for fairly ordinary words between 6 and 14
  # characters long.
  wordDelimiter: /[\s\.,!\?]+/
  acceptanceRegExp: /^[a-z\-]{6,14}$/
  rejectionRegExp: /-{2,}|-.*-/
  # For small texts, little reason to manage memory by allowing duplicates
  # past.
  duplicateCacheSize: Infinity
}

readStream.pipe(dictionaryTransformStream).pipe(writeStream)
