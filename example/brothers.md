The Five Chinese Brothers
 
Once upon a time there were five Chinese Brothers and they all looked exactly alike.  They lived with their mother in a little house not far from the sea.
 
The first Chinese brother could swallow the sea.  The second Chinese brother had an iron neck.  The third Chinese brother could stretch and stretch and stretch his legs.  The fourth Chinese brother could not be burned.  The fifth Chinese brother could hold his breath indefinitely.
 
Every morning the first Chinese brother could go fishing, and whatever the weather, he would come back to the village with beautiful and rare fish which he had caught and could sell at the market for a very good price.
 
One day, as he was leaving the market place, a little boy stopped him and asked him if he could go fishing with him.
 
“No, it could not be done,” said the first Chinese brother.
 
But the little boy begged and begged and finally the first Chinese brother consented.  “Under one condition,” said he, “and that is that you shall obey me promptly.”
 
“Yes, yes,” the little boy promised.
 
Early the next morning, the first Chinese brother and the little boy went down to the beach.  “Remember, “said the first Chinese brother, “you must obey me promptly.  When I make a sign for you to come back, you must come at once.”  “Yes, yes,” the little boy promised.
 
Then the first Chinese brother swallowed the sea.  And all the fish were left high and dry at the bottom of the sea.  And all the treasures of the sea lay uncovered.
 
The little boy was delighted.  He ran here and there stuffing his pockets with strange pebbles, extraordinary shells and fantastic algae.
 
Near the shore the first Chinese brother gathered some fish while he kept holding the sea in his mouth.  Presently he grew tired.  It is very hard to hold the sea.  So he made a sign with his hand for the little boy to come back.  The little boy saw him but paid no attention.
 
The first Chinese brother made great movements with his arms that meant “Come back!”  But did the little boy care?  Not a bit and he ran further away.
 
Then the first Chinese brother felt the sea swelling inside him and he made desperate gestures to call the little boy back.  But the little boy made faces at him and fled as fast as he could.
 
The first Chinese brother held the sea until he thought he was going to burst.  All of a sudden the sea forced its way out of his mouth, went back to its bed . . . and the little boy disappeared.
 
When the first Chinese brother returned to the village alone, he was arrested, put in prison, tried and condemned to have his head cut off.  On the morning of the execution he said to the judge: “Your Honor, will you allow me to go and bid my mother good-bye?”  “It is only fair,” said the judge.
 
So the first Chinese brother went home . . . and the second Chinese brother came back in his place.  All the people were assembled on the village square to witness the execution.  The executioner took his sword and struck a mighty blow.  But the second Chinese brother got up and smiled.  He was the one with the iron neck and they simply could not cut his head off. 
 
Everybody was angry and they decided that he should be drowned.  On the morning of the execution, the second Chinese brother said to the judge: “Your Honor, will you allow me to go and bid my mother good-bye?”  “It is only fair,” said the judge.
 
So the second Chinese brother went home . . . and the third Chinese brother came back in his place.  He was pushed on a boat which made for the open sea.  When they were far out on the ocean, the third Chinese brother was thrown overboard.  But he began to stretch and stretch and stretch his legs, way down to the bottom of the sea, and all the time his smiling face was bobbing up and down on the crest of the waves.  He simply could not be drowned.
 
Everybody was very angry, and they all decided that he should be burned.  On the morning of the execution, the third Chinese brother said to the judge: “Your Honor, will you allow me to go and bid my mother good-bye?”  “It is only fair,” said the judge.
 
So the third Chinese brother went home . . . and the fourth Chinese brother came back in his place.  He was tied up to a stake.  Fire was set to it and all the people stood around watching it.  In the midst of the flames they heard him say: “This is quite pleasant.”  “Bring some more wood,” the people cried.  The fire roared higher.
 
“Now it is quite comfortable,” said the fourth Chinese brother, for he was the one who could not be burned.  Everybody was getting more and more angry every minute and they all decided to smother him.  On the morning of the execution, the fourth Chinese brother said to the judge: “Your Honor, will you allow me to go and bid my mother good-bye?”  “It is only fair,” said the judge.
 
So the fourth Chinese brother went home . . . and the fifth Chinese brother came back in his place.  A large brick oven had been built on the village square and it had been all stuffed with whipped cream.  The fifth Chinese brother was shoveled into the oven, right in the middle of the cream, the door was shut tight, and everybody sat around and waited.
 
They were not going to be tricked again!  So they stayed there all night and even a little after dawn, just to make sure.  Then they opened the door and pulled him out.  And he shook himself and said, “My! That was a good sleep!”
 
Everybody stared open-mouthed and round-eyed.  But the judge stepped forward and said, “We have tried to get rid of you in every possible way and somehow it cannot be done.  It must be that you are innocent.”
 
“Yes, yes,” shouted all the people.  So they let him go and he went home.
 
And the five Chinese brothers and their mother all lived together happily for many years.

