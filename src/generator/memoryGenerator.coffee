###
@fileOverview
In-memory implementation class for generating phrases.
###
util      = require 'util'
_         = require 'underscore'
Generator = require '../generator'


###
@class
Class for generating deterministic unique phrases from a dictionary, with
in-memory storage.

This is suitable for testing and experimentation.

@param  {object} options
        Configuration for this instance.
###
class MemoryGenerator extends Generator
  constructor: (options) ->
    options = options or {}
    super options

    @dictionary = []

    # Count of number of phrases obtained. Defaults to 0.
    @count = 0
    return


  ###
  @see Generator.appendWordsToDictionary
  ###
  appendWordsToDictionary: (words, callback) ->
    words = [words]  unless _.isArray(words)
    @dictionary = _.union @dictionary, words
    callback()
    return


  ###
  @see Generator#getWord
  ###
  getWord: (wordIndex, callback) ->
    callback null, @dictionary[wordIndex]
    return


  ###
  @see Generator.getDictionaryLength
  ###
  getDictionaryLength: (callback) ->
    callback null, @dictionary.length
    return


  ###
  @see Generator.getCount
  ###
  getCount: (callback) ->
    callback null, @count
    return


  ###
  @see Generator.setCount
  ###
  setCount: (count, callback) ->
    @count = count
    callback()
    return


  ###
  @see Generator.incrementCount
  ###
  incrementCount: (callback) ->
    @count = @count + 1
    callback null, @count
    return


module.exports = MemoryGenerator
