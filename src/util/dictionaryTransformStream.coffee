###
@fileOverview
A transforming stream that accepts reads and writes text, transforming a
raw text into something less voluminous and more useful as the basis for
a dictionary.
###
Transform = require('stream').Transform
util = require('util')
lruCache = require('lru-cache')


###
@class
A transforming stream that converts raw text into something less voluminous
and more useful as the basis for a dictionary.

This defaults to acting as an object stream: it reads from text streams but
writes discrete string words. This is useful when reading in to add words to
a generator implementation.

To make this more helpful for writing dictionaries to file, set objectMode to
false in the options. In that mode it will write words with line breaks.

In addition to the normal stream options, the following are used:

{

  # If objectMode is set to false, then it is false. Otherwise it defaults
  # to true. This determines whether the output is discrete words as strings
  # or streamed text with line breaks between words.

  objectMode: false,

  # For tokenizing the stream of words.

  wordDelimiter: /[\s\.,!\?]+/,

  # All words failing this regular expression are rejected. Use it to
  # control acceptable length and characters. The word is lowercased
  # before matching.

  acceptanceRegExp: /^[a-z\-]{6,14}$/,

  # All words matching this regular expression are rejected.

  rejectionRegExp: /-{2,}|-.*-/,

  # Limit duplicate cache size if memory is a concern. This will let
  # duplicates through.

  duplicateCacheSize: Infinity

}

@param {Object} options
Options for the stream.
###
class DictionaryTransformStream extends Transform
  constructor: (options) ->
    # Set up the stream.
    options = options || {}
    # Default object mode to true.
    options.objectMode = true  if options.objectMode isnt false
    super options

    @objectMode = options.objectMode
    # Set up the specifics for splitting up the input and removing duplicates.
    @wordDelimiter = options.wordDelimiter or /[\s\.,!\?]+/
    @acceptanceRegExp = options.acceptanceRegExp or /^[a-z\-]{6,14}$/
    @rejectionRegExp = options.rejectionRegExp or /-{2,}|-.*-/
    @duplicateCacheSize = options.duplicateCacheSize or Infinity

    @cache = lruCache(max: @duplicateCacheSize)

    # When we're stuck halfway through a word this holds the piece we haven't
    # processed yet.
    @fragment = ''
    return

  # @see Transform#_transform
  _transform: (chunk, @encoding, callback) ->
    self = this
    chunk = chunk
    str = @fragment + chunk.toString()
    words = str.split @wordDelimiter

    # The last of it might be a word fragment.
    self.fragment = words.pop()

    words.forEach((word) ->
      self.pushWord(word)
      return
    )

    callback()
    return


  # @see Transform#_flush
  _flush: () ->
    # If we still have a fragment left at the end, then it was actually a full
    # word.
    @pushWord @fragment
    @fragment = ''
    return


  ###
  Utility methods.
  ###

  # If the word is acceptable, push it to be written.
  #
  # @param {string} word
  pushWord: (word) ->
    # @TODO console.log word
    if word and not @cache.get(word) and @acceptanceRegExp.test(word) and not @rejectionRegExp.test(word)
      @cache.set word, true
      # If not in object mode we're streaming out a line-break delimited list
      # of words.
      word = word + "\n"  unless @objectMode
      @push word
    return


module.exports = DictionaryTransformStream
