// Generated by CoffeeScript 1.7.1
(function() {
  var DictionaryTransformStream, Importer, ReadStreamImporter, util, _,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  util = require('util');

  _ = require('underscore');

  DictionaryTransformStream = require('../util/dictionaryTransformStream');

  Importer = require('../importer');

  "@class\nA dictionary importer class working from streamed text.\n\n@see Importer";

  ReadStreamImporter = (function(_super) {
    __extends(ReadStreamImporter, _super);

    function ReadStreamImporter(generator) {
      generator = generator || null;
      ReadStreamImporter.__super__.constructor.call(this, generator);
      return;
    }

    "Import a dictionary from a read stream.\n\nThe options have the form:\n\n{\n  readStream: readStream,\n              For tokenizing the stream of words.\n  wordDelimiter: /[\s!\?]+/,\n              All words failing this regular expression are rejected. Use it to\n              control acceptable length and characters. The word is lowercased\n              before matching.\n  acceptanceRegExp: /^[a-z\-]{6,14}$/,\n              Limit duplicate cache size if memory is a concern. This will let\n              duplicates through.\n  duplicateCacheSize: Infinity\n}\n\n@see Importer#import";

    ReadStreamImporter.prototype["import"] = function(options, callback) {
      var dts, dtsOptions, finish, newWord, self, words;
      self = this;
      options = options || {};
      words = {};
      dtsOptions = _.pick({
        options: options,
        'wordDelimiter': 'wordDelimiter',
        'acceptanceRegExp': 'acceptanceRegExp',
        'duplicateCacheSize': 'duplicateCacheSize'
      });
      dts = new DictionaryTransformStream(dtsOptions);
      if (!options.readStream) {
        return callback("options.readStream is required.");
      }
      newWord = function(word) {
        return words[word] = true;
      };
      finish = function(error) {
        if (error) {
          options.readStream.unpipe(dts);
          callback(error);
        }
        return self.generator.appendWordsToDictionary(_.keys(words), callback);
      };
      dts.on('data', newWord);
      dts.on('error', finish);
      dts.on('finish', finish);
      options.readStream.pipe(dts);
    };

    return ReadStreamImporter;

  })(Importer);

  module.exports = ReadStreamImporter;

}).call(this);
