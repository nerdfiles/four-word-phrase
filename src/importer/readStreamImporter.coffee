###
@fileOverview
A dictionary importer class working from streamed text.
###

util = require('util')
_ = require('underscore')
DictionaryTransformStream = require('../util/dictionaryTransformStream')
Importer = require('../importer')

###
@class
A dictionary importer class working from streamed text.

@see Importer
###

class ReadStreamImporter extends Importer
  constructor: (generator) ->
    generator = generator or null
    super generator
    return

  ###
  Import a dictionary from a read stream.

  The options have the form:

  {
    readStream: readStream,
                For tokenizing the stream of words.
    wordDelimiter: /[\s!\?]+/,
                All words failing this regular expression are rejected. Use it to
                control acceptable length and characters. The word is lowercased
                before matching.
    acceptanceRegExp: /^[a-z\-]{6,14}$/,
                Limit duplicate cache size if memory is a concern. This will let
                duplicates through.
    duplicateCacheSize: Infinity
  }

  @see Importer#import
  ###
  import: (options, callback) ->
    self = this
    options = options or {}
    words = {}

    dtsOptions = _.pick {
      options,
      'wordDelimiter',
      'acceptanceRegExp',
      'duplicateCacheSize'
    }
    dts = new DictionaryTransformStream dtsOptions
    return callback("options.readStream is required.")  unless options.readStream

    newWord = (word) ->
      # TODO: batching, add words along the way rather than all at the end.
      words[word] = true

    finish = (error) ->
      if (error)
        options.readStream.unpipe(dts)
        callback(error)
      #console.log words
      self.generator.appendWordsToDictionary _.keys(words), callback

    dts.on 'data', newWord
    dts.on 'error', finish
    dts.on 'finish', finish

    options.readStream.pipe dts
    return

module.exports = ReadStreamImporter
