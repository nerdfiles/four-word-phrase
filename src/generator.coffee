###
@fileOverview
Main interface class for generating deterministic sequences of pseudo-random
phrases.
###
crypto = require('crypto')
Random = require('random-js').Random
MersenneTwister19937 = require('random-js').MersenneTwister19937
_ = require('underscore')
async = require('async')
seedrandom = require('seedrandom')
pryjs = require('pryjs')


# @class
# Class for generating deterministic pseudo-random phrases from a dictionary.
#
# Options will vary by subclass, but must include:
#
# {
#   # The basis for the pseudo-random number generator seeds.
#   baseSeed: ''
# }
#
# @param {object} options
#   Configuration for this instance.
class Generator
  constructor: (options) ->
    @options = _.extend {
      baseSeed: ''
    }, options


  ###
  Generate a phrase of a set number of words.

  This will be deterministic for the provided settings. If starting over with
  the same base seed and dictionary then the sequence of phrases will be the
  same.

  @param {number} phraseLength
    The number of words in the phrase.
  @param {Function} callback
    Of the form function(error, string[]).
  ###
  nextPhrase: (phraseLength, callback) ->
    self = this
    dictionaryLength = undefined
    count = undefined
    prngFn = undefined
    words = undefined

    async.series
      getDictionaryLength: (asyncCallback) ->
        self.getDictionaryLength (error, length) ->
          dictionaryLength = length
          asyncCallback(error)
          return
        return

      incrementCount: (asyncCallback) ->
        self.incrementCount (error, incrementedCount) ->
          count = incrementedCount
          asyncCallback(error)
          return
        return

      obtainPrng: (asyncCallback) ->
        self.getPhrasePrngFunction count, (error, fn) ->
          prngFn = fn
          asyncCallback(error)
          return
        return

      assemblePhrase: (asyncCallback) ->
        wordIndexes = []
        index = 0
        while index < phraseLength
          wordIndexes[index] = Math.floor(prngFn() * dictionaryLength)
          index++
        async.map wordIndexes, ((dictionaryIndex, innerAsyncCallback) ->
          self.getWord dictionaryIndex, innerAsyncCallback
          return
        ), (error, results) ->
          words = results
          asyncCallback error
          return
        return

    , (error) ->
      words = []  unless _.isArray(words)
      callback error, words
      return
    return


  ###
  Obtain a seeded pseudo-random number generator function for the given count.

  The function can be called prngFn() to return a uniform random number between
  0 and 1, as is the usual practice.

  @param {number} count
    The count of the phrase to be generated using this prng function.
  @param {Function} callback
    Of the form function (error, prngFn).
  ###
  getPhrasePrngFunction: (count, callback) ->
    if (!/^\d+$/.test(count))
      return callback(new Error('Count must be an integer. Value provided: ' + count))

    seed = '' + @options.baseSeed + count
    console.log seed
    seed = crypto.createHash('sha256').update(seed).digest('hex')
    prng = seedrandom(seed)
    # eval(pryjs.it)
    # random = new Random(MersenneTwister19937.autoSeed())
    # prng = random.integer(1, count)
    callback null, prng
    return


  # Subclass methods.

  ###
  Add to the dictionary used by this generator instance.

  @param {string[]} words
    The words to append to the dictionary.
  @param {Function} callback
    Of the form function (error).
  ###
  appendWordsToDictionary: (words, callback) ->
    callback(new Error('Not implemented'))
    return


  ###
  Get a word from the dictionary by referencing a specific index in a specific
  ordering.

  @param {number} wordIndex
    Index of the word in the dictionary ordering.
  @param {Function} callback
    Of the form function (error, word).
  ###
  getWord: (wordIndex, callback) ->
    callback(new Error('Not implemented'))
    return


  ###
  Return the length of the dictionary.

  @param {Function} callback
    Of the form function (error, length).
  ###
  getDictionaryLength: (callback) ->
    callback(new Error('Not implemented'))
    return


  ###
  Obtain the count of the number of phrases requested.

  @param {Function} callback
    Of the form function (error, count).
  ###
  getCount: (callback) ->
    callback(new Error('Not implemented'))
    return


  ###
  Set the count of the number of phrases requested.

  This is only necessary if you need to adjust the stored count, such as in
  testing. Generally it is left unused.

  @param {number} count
    Integer count.
  @param {Function} callback
    Of the form function (error).
  ###
  setCount: (count, callback) ->
    callback(new Error('Not implemented'))
    return


  ###
  Increment the count of the number of phrases requested.

  @param {Function} callback
    Of the form function (error, count).
  ###
  incrementCount: (callback) ->
    callback(new Error('Not implemented'))
    return


module.exports = Generator
