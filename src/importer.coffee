###
@fileOverview
An interface class for dictionary importers.
###

###
@class
An interface class for dictionary importers.

@param {Generator} generator
  A generator instance that will have the imported words added to
  its dictionary.
###
class Importer
  constructor: (generator) ->
    @generator = generator

  # Subclass methods.

  ###
  Import words from a source to the generator for this Importer instance.

  @param {object} options
    Options for the import.
  @param {Function} callback
    Of the form function (error).
  ###
  import: (options, callback) ->
    callback new Error('Not implemented')


module.exports = Importer
