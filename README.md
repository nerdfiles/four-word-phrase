# Four Word Phrase

Four Word Phrase is a toolkit for the creation of short phrases by
pseudo-randomly picking words from a provided dictionary. When using a given
seed and dictionary the sequence of phrases generated is identical.

## Getting Started

Install via NPM:

```
npm install four-word-phrase
```

Generate a dictionary from the text of Moby Dick and use it to create
pseudo-random phrases:

```coffeescript

async = require("async")
fs = require("fs")
path = require("path")
fwp = require("four-word-phrase")

generator = new fwp.generator.MemoryGenerator(baseSeed: "a random seed")
filePath = "path/to/four-word-phrase/example/mobyDick.txt"
readStream = fs.createReadStream(filePath)
importer = new fwp.importer.ReadStreamImporter(generator)

importer.import
  readStream: readStream
, (error) ->
  return console.error(error)  if error

  phraseLength = 4
  async.times 10, ((index, asyncCallback) ->

    generator.nextPhrase phraseLength, asyncCallback
    return
  ), (error, phrases) ->
    return console.error(error)  if error

    phrases.forEach (phrase, index) ->
      console.log "Phrase " + (index + 1) + ": " + phrase.join(" ")
      return
    return
  return
```

## Notes on Sufficient Uniqueness

UUIDs have a space of ~10e38 possible options, so there is a vanishing chance of
collision even if you generate a lot of them. Word sequences that are usefully
short (meaning short enough to memorize reliably) can't approach that. Four
words is probably the limit there, and this has a far smaller space:

|                       | 3 words | 4 words | 10 words |
| --------------------- | ------- | ------- | -------- |
| Dictionary: 1,000     | ~10e9   | ~10e12  | ~10e30   |
| Dictionary: 10,000    | ~10e12  | ~10e16  | ~10e40   |
| Dictionary: 100,000   | ~10e15  | ~10e20  | ~10e50   |

For reference, a dictionary generated from Moby Dick and restricted to words of
six to fourteen letters has ~11,000 words. The English language as a whole may
have ~1,000,000 words, but far from all of those are useful for this purpose.
Sequences of short words are no easier to remember, but are less secure as
they can be more readily brute-forced.

## Creating a New Generator Implementation

To create a Generator with a different storage backend, subclass the base
`Generator` class and implement its undefined methods.

```coffeescript

# util = require('util')
Generator = require('four-word-phrase').generator.Generator

class SchmancyGenerator
  constructor = (options) ->
    super options

  # Setup and Methods here.

# Unnecessary and slower if using CoffeeScript's class sugar.
# util.inherits SchmancyGenerator, Generator

# Implement the necessary methods, e.g.:
SchmancyGenerator::appendWordsToDictionary = (words, callback) ->
  # ...
module.exports = SchmancyGenerator
```

##Linguistic Features

- https://github.com/NaturalNode/natural
- http://syntactic.omershapira.com/

##'Pataphysical Constraint Exploration

The goal will be to develop a Generic Schema (GS) for Natural Language
Generation Systems to consume Organic Narratological Architectures (ONAs)
derived from size (*histoire*) and shape (*récit*) of texts (with narrative
and descriptive parameters) using 'pataphysical concepts for defining and
testing ontic measurement of textually rooted macro-ontologies (e.g., Can
'cerberus' substitute 'unicorn' salva veritate? Are certain 'useful
fictions' natural kinds? Do fictional characters ever dream of actual
particulars?)
