
# This coding will essentially harvest Google Search for SERP Schemas (Schema.org);
# machinate with fwp, use Protégé OWL Schemas as Storybooks, generate NLG 
# Metaschemas (for http://protegewiki.stanford.edu/wiki/WebProtegeAdminGuide) — 
# Valid Storybooks will necessarily yield Valid Winograph Schema Dialogues (use 
# xSort templates as prototypes for User Paraconsistency Emotion Variance 
# Typologies within the context of Generational Virtue Paradigms, or just A/B 
# Tests).
#
# Antinomies will naturally occur across Generational Norms which are consistent 
# with the Categorical Imperative. Lawyer and Judge might wake on the same day, 
# having the same kind of breakfast. A series of meaningless facts has the 
# intensional import of a compossibility of worlds which exclude the 
# performational content of a language which can describe the consistency of 
# actual truth schemas.
#
# Practically speaking, we can use Winograph Schemas to predict ad frameworks 
# which might be deflationary or hopelessly pending on detrimental or 
# deleterious truth mechanisms which enable phenomena such as Bad Faith or 
# Frequent Categorical Store Distortions.

fs = require('fs')
utils = require('utils')

casper = require('casper').create {
  verbose        : true
  logLevel       : 'debug'
  waitTimeout    : 10000
  stepTimeout    : 10000
  retryTimeout   : 150
  #clientScripts : ["jquery.min.js"]
  viewportSize:
    width  : 1176
    height : 806
  pageSettings:
    loadImages         : false
    loadPlugins        : false
    userAgent          : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv: 22.0) Gecko/20100101 Firefox/22.0'
    webSecurityEnabled : false
    ignoreSslErrors    : true
  onWaitTimeout: () ->
    @echo('wait timeout')
    @clear()
    @page.stop()
  onStepTimeout: (timeout, step) ->
    @echo('step timeout')
    @clear()
    @page.stop()
}

url = 'https://www.lewellingvineyards.com/trade'

casper.start url, ->
  @echo "Page title: #{ @getTitle() }"

casper.then ->
  @echo 'Vous allez !'

getLinks = ->
  links = document.querySelectorAll('a')
  Array::map.call links, (e) ->
    e.getAttribute 'href'

getCells = ->
  cells = document.querySelectorAll('td')
  Array::map.call cells, (e) ->
    e.innerText

links = cells = []

check = () ->
  links = links.concat @evaluate(getLinks)
  cells = cells.concat @evaluate(getCells)
  fs.write("/Users/nerdfiles/Projects/four-word-phrase/test/e2e/links.txt", links.toString(), 'w')
  fs.write("/Users/nerdfiles/Projects/four-word-phrase/test/e2e/cells.txt", cells.toString(), 'w')
  @capture 'page.png'
  @exit()

casper.run check
