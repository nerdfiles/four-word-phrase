process = require 'process'
filename = dictionary = seed = undefined

class Interface

  constructor: () ->
    return

  phrases: () ->

    process.argv.forEach((arg, index) ->
      if index == 2
        filename = arg
      if index == 3
        seed = arg
    )

    return
      filename: filename
      seed: seed

  dictionary: () ->

    process.argv.forEach((arg, index) ->
      if index == 2
        filename = arg
      if index == 3
        dictionary = arg
    )

    return
      filename: filename
      dictionary: dictionary

module.exports = Interface
