/**
 * @jsdoc overview
 * @description
 */
var util = require('util')

class Markov {

  /**
   * @name constructor
   * @returns {undefined}
   */
  constructor() {
    this.chain = {}
  }

  /**
   * @name digest
   * @returns {undefined}
   */
  digest(corpus) {
    var currentWord, words = corpus.split(/\s+/)
    var nextWord

    for (let i = 0; i < words.length; i++) {
      currentWord = words[i]
      nextWord = words[i + 1]

      this.chain[currentWord] = this.chain[currentWord] || []
      this.chain[currentWord].push(nextWord)
    }
    console.log(this.chain)
  }

  /**
   * @name {String} pick Seed word determines an ordered set within the
   * potential words list.
   * @returns {Object}
   */
  pick(seedWord) {
    const potentialWords = this.chain[seedWord] ?
      this.chain[seedWord] :
      this.chain[this.getRandomWord()]

    return potentialWords[Math.floor(Math.random() * potentialWords.length)]
  }

  /**
   * @name getRandomWord
   * @returns {Object}
   */
  getRandomWord() {
    const keys = Object.keys(this.chain)
    return keys[keys.length * Math.random() << 0]
  }

  /**
   * @name generate
   * @param {Number} length Some finite cap.
   * @param {String} seedWord Seed word.
   * @returns {String}
   */
  generate(length, seedWord) {
    var currentWord = seedWord || this.getRandomWord()
    const output = []

    for (let i = 0; i < length; i++) {
      output.push(currentWord)
      currentWord = this.pick(currentWord)
    }

    return output.join(' ')
  }

}

var m = new Markov()

m.digest("I find it upsetting oh? I'ma take dat If someone who borrowed my keyboard could give it back, um, then I would have one. you'll still want to use those tools to manage ruby, rather than apt feel free to (and you'll probably have to) install rvm rbenv globally on your machine/server/whatever as-such, ruby packaged by ubuntu is probably not packaged by rubyists basically ,the ruby community has refused to support efforts by os vendors to ship rubies because the problem is already solved since it's a 'property' of an object and ruby doesn't really allow you to do that without a method but there are a lot of missing optional parenthesis, there I don't actually know if alpha is going to ship on the 30th as deadline isn't a word that has strong meaning, as far as I understand Ok, I don't think it will be 11th hour -- I'd reach out to Ann-Marie and confirm that they don't need me to help close out alpha I know we've had this conversation a few times is a smell that you are not communicating effectively or using a persistent medium for those conversations. Id size that at a 3 -- I can probably help you with that.  Also, given recent revelations, I would just mark it as done, or toss it back to Jenn to see if she actually wants it at all. Sounds like a reason not to drive a subaru Im realizing that after 6 months of answering the questions what does Did you have to escalate any issues last week? actually mean for me at Dātu")
// console.log(util.inspect(m, {showHidden: false, depth: null}))

console.log(m.generate(1))
